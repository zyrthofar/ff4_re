def _8000
	# 03:8000 4C 09 80    JMP $8009
	_8009
end

def _8003
	# 03:8003 4C 09 80    JMP $8009
	_8009
end

def _8006
	# 03:8006 4C 36 80    JMP $8036
	_8036
end

def _8009
	# 03:8009 08          PHP
	# 03:800A C2 30       REP #$30
	# 03:800C 8B          PHB
	# 03:800D 0B          PHD
	# 03:800E 48          PHA
	# 03:800F DA          PHX
	# 03:8010 5A          PHY
	# 03:8011 A9 00 00    LDA #$0000
	# 03:8014 E2 20       SEP #$20
	# 03:8016 C2 10       REP #$10
	# 03:8018 20 8A 80    JSR $808A
	_808A

	# 03:801B A9 00       LDA #$00
	# 03:801D 8F 00 21 00 STA $002100
	# 03:8021 8F 0C 42 00 STA $00420C
	# 03:8025 8F 0B 42 00 STA $00420B
	# 03:8029 8F 00 42 00 STA $004200
	mem8[0x002100] = 0
	mem8[0x00420C] = 0
	mem8[0x00420B] = 0
	mem8[0x004200] = 0

	# 03:802D C2 30       REP #$30
	# 03:802F 7A          PLY
	# 03:8030 FA          PLX
	# 03:8031 68          PLA
	# 03:8032 2B          PLD
	# 03:8033 AB          PLB
	# 03:8034 28          PLP

	# 03:8035 6B          RTL
	return
end

def _8036(a)
	# 03:8036 8F 75 39 7E STA $7E3975
	mem8[0x7E3975] = a # ?

	# 03:803A 08          PHP
	# 03:803B C2 30       REP #$30
	# 03:803D 8B          PHB
	# 03:803E 0B          PHD
	# 03:803F 48          PHA
	# 03:8040 DA          PHX
	# 03:8041 5A          PHY
	# 03:8042 78          SEI
	# 03:8043 A9 00 00    LDA #$0000
	# 03:8046 E2 20       SEP #$20
	# 03:8048 C2 10       REP #$10
	# 03:804A A2 00 00    LDX #$0000
	# 03:804D DA          PHX
	# 03:804E 2B          PLD
	# 03:804F A9 7E       LDA #$7E
	# 03:8051 48          PHA
	# 03:8052 AB          PLB
	d = 0x0000
	b = 0x7E

	# 03:8053 20 FD 97    JSR $97FD
	_97fd

	# 03:8056 C2 30       REP #$30
	# 03:8058 7A          PLY
	# 03:8059 FA          PLX
	# 03:805A 68          PLA
	# 03:805B 2B          PLD
	# 03:805C AB          PLB
	# 03:805D 28          PLP

	# 03:805E 6B          RTL
	return
end

def _805f
	# 03:805F AD 3E 35    LDA $353E
	# 03:8062 49 01       EOR #$01
	# 03:8064 8D 3E 35    STA $353E
	# 03:8067 D0 1B       BNE $8084 (+27 B)
	mem8[0x353E] ^= 0x01
	if mem8[0x353E] == 0
		# 03:8069 AD 3F 35    LDA $353F
		# 03:806C C9 02       CMP #$02
		# 03:806E D0 05       BNE $8075 (+5 B)
		if mem8[0x353F] == 0x02
			# 03:8070 A9 00       LDA #$00
			# 03:8072 8D 3F 35    STA $353F
			mem8[0x353F] = 0
		end

		# 03:8075 AD 3F 35    LDA $353F
		# 03:8078 D0 02       BNE $807C (+2 B)
		# 03:807A 80 05       BRA $8081 (+5 B)
		if mem8[0x353F] != 0
			# 03:807C A9 0D       LDA #$0D
			# 03:807E 20 85 80    JSR $8085
			_8085(0x0D)
		end

		# 03:8081 EE 3F 35    INC $353F
		mem8[0x353F] += 1
	end

	# 03:8084 60          RTS
	return
end

def _8085(a)
	# 03:8085 22 03 80 02  JSL $028003
	_028003(a)

	# 03:8089 60          RTS
	return
end

def _808A
	# 03:808A 20 CB 82    JSR $82CB  Sets some bytes in the $2000AND $4000 (????)
	_82cb

	0xFF.times do |x|
		# 03:808D A2 FF 00    LDX #$00FF  Copies the 256 rANDom bytes to $1900-19FF
		# 03:8090 BF 00 EE 14 LDA $14EE00,x
		# 03:8094 9D 00 19    STA $1900,x
		# 03:8097 CA          DEX
		# 03:8098 10 F6       BPL $8090 (-10 B)
		mem8[0x1900 + x] = mem8[0x14EE00 + x]
	end

	# 03:809A 20 35 82    JSR $8235  Resets a lot of bytes
	_8235

	# 03:809D A2 7F 00    LDX #$007F  Adds the bytes $0600-$067F together in $97
	# 03:80A0 7B          TDC
	# 03:80A1 18          CLC
	# 03:80A2 7D 00 06    ADC $0600,x
	# 03:80A5 CA          DEX
	# 03:80A6 10 FA       BPL $80A2 (-6 B)
	# 03:80A8 85 97       STA $97
	a = 0
	(0x00..0x7F).each do |x|
		a += mem8[0x0600 + x]
	end
	mem8[0x97] = a

	# 03:80AA 20 FD B2    JSR $B2FD  Sets bytes in the $3300-3400s
	_b2fd

	# Puts $1802’sBIT7 in $352C (magnetized location)
	# If $1802’sBIT6 is set, $1802 = #$07, else keepBITs0-5
	# 03:80AD AD 02 18    LDA $1802
	# 03:80B0 48          PHA
	# 03:80B1 48          PHA
	# 03:80B2 29 80       AND #$80
	# 03:80B4 8D 2C 35    STA $352C
	# 03:80B7 68          PLA
	# 03:80B8 29 40       AND #$40
	# 03:80BA 85 A9       STA $A9
	# 03:80BC 68          PLA
	# 03:80BD 29 3F       AND #$3F
	# 03:80BF 8D 02 18    STA $1802
	# 03:80C2 A5 A9       LDA $A9
	# 03:80C4 F0 05       BEQ $80CB (+5 B)
	# 03:80C6 A9 07       LDA #$07
	# 03:80C8 8D 02 18    STA $1802
	a = mem8[0x1802]
	mem8[0x352C] = a & 0x80

	if a & 0x40 == 0
		mem8[0x1802] = a & 0x3F
	else
		mem8[0x1802] = 0x07
	end

	# Moves #1801’sBIT7 in $38EF (Attack Seq. Bank #)
	# 03:80CB AD 01 18    LDA $1801
	# 03:80CE 29 80       AND #$80
	# 03:80D0 8D EF 38    STA $38EF
	# 03:80D3 AD 01 18    LDA $1801
	# 03:80D6 29 7F       AND #$7F
	# 03:80D8 8D 01 18    STA $1801
	mem8[0x38EF] = mem8[0x1801] & 0x80
	mem8[0x1801] &= 0x7F

	# Transfers the 8 bytes from the current Encounter to $299C-$29A3
	# 03:80DB C2 20       REP #$20
	# 03:80DD AD 00 18    LDA $1800
	# 03:80E0 C9 00 01    CMP #$0100
	# 03:80E3 90 07       BCC $80EC (+7 B)
	if mem16[0x1800] - 0x0100 < 0
		# 03:80E5 38          SEC
		# 03:80E6 E9 00 00    SBC #$0000  <- ? Maybe makes sure $1801'sBIT15 is set?
		# 03:80E9 8D 00 18    STA $1800
		mem16[0x1800] |= 0x8000 # ??
	end

	# 03:80EC AD 00 18    LDA $1800
	# 03:80EF 8D 3D 39    STA $393D
	# 03:80F2 A9 08 00    LDA #$0008
	# 03:80F5 8D 3F 39    STA $393F
	# 03:80F8 20 B9 83    JSR $83B9
	# 03:80FB 7B          TDC
	# 03:80FC E2 20       SEP #$20
	# 03:80FE AE 41 39    LDX $3941
	# 03:8101 7B          TDC
	# 03:8102 A8          TAY
	x = mem16[0x1800] * 0x0008
	y = 0
	0x08.times do
		# 03:8103 BF 00 80 0E LDA $0E8000,x [Enemy Encounter Data]
		# 03:8107 99 9C 29    STA $299C,y
		# 03:810A C8          INY
		# 03:810B E8          INX
		# 03:810C C0 08 00    CPY #$0008
		# 03:810F D0 F2       BNE $8103 (-14 B)
		mem8[0x299C + y] = mem8[0x0E8000 + x]
		x += 1
		y += 1
	end

	# Encounter Data Byte 0
	# 03:8111 AD 9C 29    LDA $299C
	# 03:8114 8D A4 29    STA $29A4
	# 03:8117 29 08       AND #$08
	# 03:8119 8D 81 35    STA $3581
	a = mem8[0x299c]
	mem8[0x29A4] = a
	mem8[0x3581] = a & 0x08 # Boss Death

	# Copies the Enemy InDEX in Groups 0-2 to $29AD-29AFAND $29B1-29B3
	# 03:811C A2 03 00    LDX #$0003
	# 03:811F 9B          TXY
	# 03:8120 88          DEY
	x = 0x0003
	y = 0x0002
	begin
		# 03:8121 BD 9C 29    LDA $299C,x
		# 03:8124 99 AD 29    STA $29AD,y
		# 03:8127 99 B1 29    STA $29B1,y
		# 03:812A CA          DEX
		# 03:812B 88          DEY
		# 03:812C 10 F3       BPL $8121 (-13 B)
		a = mem8[0x299C + x]
		mem8[0x29AD + y] = a
		mem8[0x29B1 + y] = a
		x -= 1
		y -= 1
	end until y < 0

	# #$FF as group 3 (?)
	# 03:812E A9 FF       LDA #$FF
	# 03:8130 8D B0 29    STA $29B0
	# 03:8133 8D B4 29    STA $29B4
	mem8[0x29B0] = 0xFF
	mem8[0x29B4] = 0xFF

	# Copies the enemy count in each group in $29CA-29CC
	# 03:8136 7B          TDC
	# 03:8137 AA          TAX
	# 03:8138 AD A0 29    LDA $29A0
	# 03:813B 85 AB       STA $AB
	mem8[0xAB] = mem8[0x29A0]
	3.times do |x|
		# 03:813D 9E CA 29    STZ $29CA,x
		# 03:8140 06 AB       ASL $AB
		# 03:8142 3E CA 29    ROL $29CA,x
		# 03:8145 06 AB       ASL $AB
		# 03:8147 3E CA 29    ROL $29CA,x
		# 03:814A E8          INX
		# 03:814B E0 03 00    CPX #$0003
		# 03:814E D0 ED       BNE $813D (-19 B)
		mem8[0x29CA + x] = mem8[0xAB] & 0x04
		mem8[0xAB] /= 4
	end

	# Gather similar enemies together
	# 03:8150 7B          TDC
	# 03:8151 AA          TAX
	0x02.times do |x|
		# 03:8152 9B          TXY
		# 03:8153 C8          INY
		y = x + 1

		begin
			# 03:8154 BD AD 29    LDA $29AD,x
			# 03:8157 C9 FF       CMP #$FF
			# 03:8159 F0 1D       BEQ $8178 (+29 B)
			a = mem8[0x29AD + x]
			if a == 0xFF
				break
			end

			# 03:815B D9 AD 29    CMP $29AD,y
			# 03:815E D0 12       BNE $8172 (+18 B)
			if a == mem8[0x29AD + y]
				# 03:8160 18          CLC
				# 03:8161 BD CA 29    LDA $29CA,x
				# 03:8164 79 CA 29    ADC $29CA,y
				# 03:8167 9D CA 29    STA $29CA,x
				# 03:816A 7B          TDC
				# 03:816B 99 CA 29    STA $29CA,y
				# 03:816E 3A          DEC a
				# 03:816F 99 AD 29    STA $29AD,y
				mem8[0x29CA + x] += mem8[0x29CA + y]
				mem8[0x29CA + y] = 0
				mem8[0x29AD + y] = 0xFF
			end

			# 03:8172 C8          INY
			# 03:8173 C0 03 00    CPY #$0003
			# 03:8176 D0 DC       BNE $8154 (-36 B)
		end until y == 0x0003

		# 03:8178 E8          INX
		# 03:8179 E0 02 00    CPX #$0002
		# 03:817C D0 D4       BNE $8152 (-44 B)
		x += 1
	end

	# Puts #$FF in $29B5-29BCAND $29BD-29C4
	# 03:817E A9 FF       LDA #$FF
	# 03:8180 A0 07 00    LDY #$0007
	0x0007.times do |y|
		# 03:8183 99 B5 29    STA $29B5,y
		# 03:8186 99 BD 29    STA $29BD,y
		# 03:8189 88          DEY
		# 03:818A 10 F7       BPL $8183 (-9 B)
		mem8[0x29B5 + y] = 0xFF
		mem8[0x29BD + y] = 0xFF
	end

	# For every enemy position, determines the associated group inDEX
	# 03:818C C8          INY
	# 03:818D BB          TYX
	0x0003.times do |x|
		# 03:818E BD CA 29    LDA $29CA,x
		# 03:8191 85 AB       STA $AB
		# 03:8193 F0 0E       BEQ $81A3 (+14 B)
		mem8[0x29CA + x].times do
			# 03:8195 8A          TXA    If at least one enemy
			# 03:8196 99 B5 29    STA $29B5,y
			# 03:8199 99 BD 29    STA $29BD,y
			# 03:819C C8          INY
			# 03:819D C6 AB       DEC $AB
			# 03:819F A5 AB       LDA $AB
			# 03:81A1 D0 F2       BNE $8195 (-14 B)
			mem8[0x29B5 + y] = x
			mem8[0x29BD + y] = x
			y += 1
		end

		# 03:81A3 E8          INX
		# 03:81A4 E0 03 00    CPX #$0003
		# 03:81A7 D0 E5       BNE $818E (-27 B)
	end

	# Copies the Current Layout Table to $29A5-29AC
	# 03:81A9 AD A1 29    LDA $29A1
	# 03:81AC 85 DF       STA $DF
	# 03:81AE A9 08       LDA #$08
	# 03:81B0 85 E1       STA $E1
	# 03:81B2 20 E0 83    JSR $83E0
	# 03:81B5 A6 E3       LDX $E3
	# 03:81B7 7B          TDC
	# 03:81B8 A8          TAY
	x = mem8[0x29A1] * 0x08
	0x0008.times do |y|
		# 03:81B9 BF 00 90 0E LDA $0E9000,x  Enemy Layout Table
		# 03:81BD 99 A5 29    STA $29A5,y
		# 03:81C0 E8          INX
		# 03:81C1 C8          INY
		# 03:81C2 C0 08 00    CPY #$0008
		# 03:81C5 D0 F2       BNE $81B9 (-14 B)
		mem8[0x29A5 + y] = mem8[0x0E9000 + x]
		x += 1
	end

	# Total count of enemies in $29CD
	# 03:81C7 18          CLC
	# 03:81C8 AD CA 29    LDA $29CA
	# 03:81CB 6D CB 29    ADC $29CB
	# 03:81CE 6D CC 29    ADC $29CC
	# 03:81D1 8D CD 29    STA $29CD
	mem8[0x29CD] = mem8[0x29CA] + mem8[0x29CB] + mem8[0x29CC]

	# Makes a copy of the enemy counts to $38F0-38F2
	# 03:81D4 AD CA 29    LDA $29CA
	# 03:81D7 8D F0 38    STA $38F0
	# 03:81DA AD CB 29    LDA $29CB
	# 03:81DD 8D F1 38    STA $38F1
	# 03:81E0 AD CC 29    LDA $29CC
	# 03:81E3 8D F2 38    STA $38F2
	mem8[0x38F0] = mem8[0x29CA]
	mem8[0x38F1] = mem8[0x29CB]
	mem8[0x38F2] = mem8[0x29CC]

	# Copies the Current Target System to $29CF-29DE
	# 03:81E6 AD A3 29    LDA $29A3
	# 03:81E9 85 DF       STA $DF
	# 03:81EB A9 10       LDA #$10
	# 03:81ED 85 E1       STA $E1
	# 03:81EF 20 E0 83    JSR $83E0
	# 03:81F2 A6 E3       LDX $E3
	# 03:81F4 7B          TDC
	# 03:81F5 A8          TAY
	x = mem8[0x29A3] * 0x10
	0x0010.times do |y|
		# 03:81F6 BF C0 BA 0E LDA $0EBAC0,x
		# 03:81FA 99 CF 29    STA $29CF,y
		# 03:81FD E8          INX
		# 03:81FE C8          INY
		# 03:81FF C0 10 00    CPY #$0010
		# 03:8202 D0 F2       BNE $81F6 (-14 B)
		mem8[0x29CF + y] = mem8[0x0EBAC0 + x]
		x += 1
	end

	# 03:8204 20 29 89    JSR $8929  PREPares the battle,INCluding the relative speeds
	_8929

	# If first enemy's level is 97, 98, or 99, puts #$03, #$07, or #$0A in $38D6
	# 03:8207 AD 82 22    LDA $2282
	# 03:820A C9 61       CMP #$61
	# 03:820C 90 0B       BCC $8219 (+11 B)
	a = mem8[0x2282]
	if a >= 0x61
		# 03:820E 38          SEC
		# 03:820F E9 61       SBC #$61
		# 03:8211 AA          TAX
		# 03:8212 BF 0F FF 13 LDA $13FF0F,x
		# 03:8216 8D D6 38    STA $38D6
		mem8[0x38D6] = mem8[0x13FF0F + (a - 0x61)]
	end

	# Loads the battle music (#$03 = no music)
	# 03:8219 AD E5 38    LDA $38E5
	# 03:821C 29 0C       AND #$0C
	# 03:821E 4A          LSR a
	# 03:821F 4A          LSR a
	# 03:8220 C9 03       CMP #$03
	# 03:8222 F0 09       BEQ $822D (+9 B)
	a (mem8[0x38E5] & 0x0C) * 4
	if a != 0x03
		# 03:8224 AA          TAX
		# 03:8225 BF 0C FF 13 LDA $13FF0C,x
		# 03:8229 22 12 FF 13 JSL $13FF12
		a = mem8[0x13FF0C + a]
		_13FF12
	end

	# 03:822D A9 03       LDA #$03
	# 03:822F 20 85 80    JSR $8085
	_8085(0x03)

	# 03:8232 4C D9 85    JMP $85D9
	_85D9 #JMP, notRTS
end

# Resets most of the data
def _8235
	# Puts #$00 to $80-FF
	# 03:8235 A2 7F 00    LDX #$007F
	# 03:8238 74 80       STZ $80,x
	# 03:823A CA          DEX
	# 03:823B 10 FB       BPL $8238 (-5 B)
	0x0080.times do |x|
		mem8[0x80 + x] = 0
	end

	# Puts #$00 to $2000-397D
	# Clears the whole of the battle bytes
	# 03:823D A2 7D 19    LDX #$197D
	# 03:8240 9E 00 20    STZ $2000,x
	# 03:8243 CA          DEX
	# 03:8244 10 FA       BPL $8240 (-6 B)
	0x197E.times do |x|
		mem8[2000 + x] = 0
	end

	# Puts #$00 to $1804-180A
	# 03:8246 A2 07 00    LDX #$0007
	# 03:8249 9E 04 18    STZ $1804,x
	# 03:824C CA          DEX
	# 03:824D 10 FA       BPL $8249 (-6 B)
	0x0008.times do |x|
		mem8[0x1804 + x] = 0
	end

	# Puts #$80 every 4 bytes in $2C7A-3389
	# Clears the CommAND InDEX attributed to character magics,
	# inventory items, equipped weapons,AND battle commANDs
	# 03:824F A2 10 07    LDX #$0710
	# 03:8252 A9 80       LDA #$80
	# 03:8254 9D 7A 2C    STA $2C7A,x
	# 03:8257 CA          DEX
	# 03:8258 CA          DEX
	# 03:8259 CA          DEX
	# 03:825A CA          DEX
	# 03:825B 10 F7       BPL $8254 (-9 B)
	x = 0x0710
	while x >= 0
		mem8[0x2C7A + x] = 0x80
		x -= 4
	end

	# Puts #$FF to $397F-6CBD
	# Clears the attackAND counter sequences
	# 03:825D A2 3F 33    LDX #$333F
	# 03:8260 A9 FF       LDA #$FF
	# 03:8262 9D 7F 39    STA $397F,x
	# 03:8265 CA          DEX
	# 03:8266 10 FA       BPL $8262 (-6 B)
	0x3340.times do |x|
		mem8[0x397F + x] = 0xFF
	end

	# Puts #$FF to $3929-392D
	# 03:8268 A2 05 00    LDX #$0005
	# 03:826B 9D 29 39    STA $3929,x
	# 03:826E CA          DEX
	# 03:826F 10 FA       BPL $826B (-6 B)
	0x0006.times do |x|
		mem8[0x3929 + x] = 0xFF
	end

	# Puts #$FF to $35F7-35FD
	# 03:8271 A2 07 00    LDX #$0007
	# 03:8274 9D F7 35    STA $35F7,x
	# 03:8277 CA          DEX
	# 03:8278 10 FA       BPL $8274 (-6 B)
	0x0008.times do |x|
		mem8[0x35F7 + x] = 0xFF
	end

	# 03:827A 85 D0       STA $D0
	# 03:827C 8D 7B 35    STA $357B
	# 03:827F 8D 7C 35    STA $357C
	# 03:8282 8D 83 35    STA $3583
	# 03:8285 8D 5E 35    STA $355E
	# 03:8288 8D 01 36    STA $3601
	# 03:828B 8D 02 36    STA $3602
	# 03:828E 8D D6 38    STA $38D6
	mem8[0xD0] = 0xFF
	mem8[0x357B] = 0xFF
	mem8[0x357C] = 0xFF
	mem8[0x3583] = 0xFF
	mem8[0x355E] = 0xFF
	mem8[0x3601] = 0xFF
	mem8[0x3602] = 0xFF
	mem8[0x38D6] = 0xFF

	# Sets the ChangeAND Defend commANDs
	# 03:8291 A9 1A       LDA #$1A
	# 03:8293 8D 17 33    STA $3317
	# 03:8296 8D 33 33    STA $3333
	# 03:8299 8D 4F 33    STA $334F
	# 03:829C 8D 6B 33    STA $336B
	# 03:829F 8D 87 33    STA $3387
	# 03:82A2 1A          INC a
	# 03:82A3 8D 1B 33    STA $331B
	# 03:82A6 8D 37 33    STA $3337
	# 03:82A9 8D 53 33    STA $3353
	# 03:82AC 8D 6F 33    STA $336F
	# 03:82AF 8D 8B 33    STA $338B
	mem8[0x3317] = 0x1A
	mem8[0x3333] = 0x1A
	mem8[0x334F] = 0x1A
	mem8[0x336B] = 0x1A
	mem8[0x3387] = 0x1A
	mem8[0x331B] = 0x1B
	mem8[0x3337] = 0x1B
	mem8[0x3353] = 0x1B
	mem8[0x336F] = 0x1B
	mem8[0x338B] = 0x1B

	# 03:82B2 AD AC 16    LDA $16AC
	# 03:82B5 8D 38 35    STA $3538
	# 03:82B8 8D EE 38    STA $38EE
	a = mem8[0x16AC]
	mem8[0x3538] = a
	mem8[0x38EE] = a

	# Puts #$0200 to $35A4-35BB
	# 03:82BB A2 18 00    LDX #$0018
	# 03:82BE A9 02       LDA #$02
	# 03:82C0 9D A4 35    STA $35A4,x
	# 03:82C3 9E A5 35    STZ $35A5,x
	# 03:82C6 CA          DEX
	# 03:82C7 CA          DEX
	# 03:82C8 10 F6       BPL $82C0 (-10 B)
	x = 0x0018
	while x >= 0
		mem8[0x35A4 + x] = 0x02
		mem8[0x35A5 + x] = 0
		x -= 2
	end

	# 03:82CA 60          RTS
	return
end

# Changes the following bytes in Bank 0:
# $2100 80 00 00 00    09 00 63  59 73 73 22 55 00 00 00
# $2110 00 00 00 00 00 80
# $2120          00 00 00 00 00  00 00 00    00 00 00 00
# $2130 00 00    00
# $4200 00                                00 00
def _82cb
	# 03:82CB A9 00       LDA #$00
	# 03:82CD 48          PHA
	# 03:82CE AB          PLB
	# 03:82CF 8D 00 42    STA $4200
	# 03:82D2 A2 00 00    LDX #$0000  Reg D = 0
	# 03:82D5 DA          PHX
	# 03:82D6 2B          PLD
	b = 0x00
	mem8[0x4200] = 0
	d = 0x0000

	# 03:82D7 A9 80       LDA #$80
	# 03:82D9 8D 00 21    STA $2100
	# 03:82DC A9 09       LDA #$09
	# 03:82DE 8D 05 21    STA $2105
	# 03:82E1 A2 00 00    LDX #$0000
	# 03:82E4 8E 02 21    STX $2102
	# 03:82E7 8A          TXA
	# 03:82E8 8D 01 21    STA $2101
	# 03:82EB A9 22       LDA #$22
	# 03:82ED 8D 0B 21    STA $210B
	# 03:82F0 A9 55       LDA #$55
	# 03:82F2 8D 0C 21    STA $210C
	# 03:82F5 A9 63       LDA #$63
	# 03:82F7 8D 07 21    STA $2107
	# 03:82FA A9 59       LDA #$59
	# 03:82FC 8D 08 21    STA $2108
	# 03:82FF A9 73       LDA #$73
	# 03:8301 8D 09 21    STA $2109
	# 03:8304 8D 0A 21    STA $210A
	# 03:8307 A9 80       LDA #$80
	# 03:8309 8D 15 21    STA $2115
	mem8[0x2100] = 0x80
	mem8[0x2105] = 0x09
	mem16[0x2102] = 0x0000
	mem8[0x2101] = 0
	mem8[0x210B] = 0x22
	mem8[0x210C] = 0x55
	mem8[0x2107] = 0x63
	mem8[0x2108] = 0x59
	mem8[0x2109] = 0x73
	mem8[0x210A] = 0x73
	mem8[0x2115] = 0x80

	# 03:830C 7B          TDC
	# 03:830D AA          TAX
	# 03:830E 8D 06 21    STA $2106
	# 03:8311 8D 0D 21    STA $210D
	# 03:8314 8D 0D 21    STA $210D
	# 03:8317 8D 0E 21    STA $210E
	# 03:831A 8D 0E 21    STA $210E
	# 03:831D 8D 0F 21    STA $210F
	# 03:8320 8D 0F 21    STA $210F
	# 03:8323 8D 10 21    STA $2110
	# 03:8326 8D 10 21    STA $2110
	# 03:8329 8D 11 21    STA $2111
	# 03:832C 8D 11 21    STA $2111
	# 03:832F 8D 12 21    STA $2112
	# 03:8332 8D 12 21    STA $2112
	# 03:8335 8D 13 21    STA $2113
	# 03:8338 8D 13 21    STA $2113
	# 03:833B 8D 14 21    STA $2114
	# 03:833E 8D 14 21    STA $2114
	# 03:8341 8D 23 21    STA $2123
	# 03:8344 8D 24 21    STA $2124
	# 03:8347 8D 25 21    STA $2125
	# 03:834A 8D 26 21    STA $2126
	# 03:834D 8D 27 21    STA $2127
	# 03:8350 8D 28 21    STA $2128
	# 03:8353 8D 29 21    STA $2129
	# 03:8356 8E 2A 21    STX $212A
	# 03:8359 8D 2C 21    STA $212C
	# 03:835C 8D 2D 21    STA $212D
	# 03:835F 8D 2E 21    STA $212E
	# 03:8362 8D 2F 21    STA $212F
	# 03:8365 8D 0B 42    STA $420B
	# 03:8368 8D 0C 42    STA $420C
	# 03:836B 8D 31 21    STA $2131
	# 03:836E 8D 33 21    STA $2133
	# 03:8371 8D 30 21    STA $2130
	mem8[0x2106] = 0
	mem8[0x210D] = 0
	mem8[0x210D] = 0 # ?
	mem8[0x210E] = 0
	mem8[0x210E] = 0 # ?
	mem8[0x210F] = 0
	mem8[0x210F] = 0 # ?
	mem8[0x2110] = 0
	mem8[0x2110] = 0 # ?
	mem8[0x2111] = 0
	mem8[0x2111] = 0 # ?
	mem8[0x2112] = 0
	mem8[0x2112] = 0 # ?
	mem8[0x2113] = 0
	mem8[0x2113] = 0 # ?
	mem8[0x2114] = 0
	mem8[0x2114] = 0 # ?
	mem8[0x2123] = 0
	mem8[0x2124] = 0
	mem8[0x2125] = 0
	mem8[0x2126] = 0
	mem8[0x2127] = 0
	mem8[0x2128] = 0
	mem8[0x2129] = 0
	mem8[0x212A] = 0
	mem8[0x212C] = 0
	mem8[0x212D] = 0
	mem8[0x212E] = 0
	mem8[0x212F] = 0
	mem8[0x420B] = 0
	mem8[0x420C] = 0
	mem8[0x2131] = 0
	mem8[0x2133] = 0
	mem8[0x2130] = 0

	# 03:8374 A9 7E       LDA #$7E  Sets B back to #$7E
	# 03:8376 48          PHA
	# 03:8377 AB          PLB
	b = 0x7E

	# 03:8378 60          RTS
	return
end

# RANDomizesAND returns a value between xAND a
# Input: x, a
# Output: a
def _8379(x, a)
	# 03:8379 E2 10       SEP #$10
	# 03:837B 86 96       STX $96
	# 03:837D E0 FF       CPX #$FF
	# 03:837F D0 02       BNE $8383 (+2 B)
	# 03:8381 80 33       BRA $83B6 (+51 B)
	if x != 0xFF
		# 03:8383 C9 00       CMP #$00
		# 03:8385 F0 2F       BEQ $83B6 (+47 B)
		# 03:8387 C5 96       CMP $96
		# 03:8389 F0 2B       BEQ $83B6 (+43 B)
		if a != 0 && a != x
			# 03:838B A6 97       LDX $97
			# 03:838D 38          SEC
			# 03:838E E5 96       SBC $96
			# 03:8390 C9 FF       CMP #$FF
			# 03:8392 D0 05       BNE $8399 (+5 B)
			x2 = mem8[0x97]
			a -= x
			if a == 0xFF
				# 03:8394 BD 00 19    LDA $1900,x
				# 03:8397 80 1D       BRA $83B6 (+29 B)
				a = mem8[0x1900 + x2]
			else
				# 03:8399 1A          INC a
				# 03:839A 8D 47 39    STA $3947
				# 03:839D 9C 48 39    STZ $3948
				# 03:83A0 BD 00 19    LDA $1900,x
				# 03:83A3 AA          TAX
				# 03:83A4 8E 45 39    STX $3945
				# 03:83A7 C2 10       REP #$10
				# 03:83A9 20 07 84    JSR $8407
				# 03:83AC E2 10       SEP #$10
				# 03:83AE 18          CLC
				# 03:83AF AD 4B 39    LDA $394B
				# 03:83B2 65 96       ADC $96
				a = mem8[0x1900 + x2] % (a + 1) # (a + 1) so that the bounds areINClusive
				a += x

				# 03:83B4 E6 97       INC $97
				mem8[0x97] += 1
			end
		end
	end

	# 03:83B6 C2 10       REP #$10
	# 03:83B8 60          RTS
	return a
end

# Multiplies $393D by $393F.
# Input: $393D, $393F (16b)
# Output: $3941-$3944 (32b?)
def _83b9
	# 03:83B9 C2 20       REP #$20

	# 03:83BB A2 10 00    LDX #$0010
	# 03:83BE 9C 41 39    STZ $3941
	# 03:83C1 9C 43 39    STZ $3943
	# 03:83C4 6E 3F 39    ROR $393F
	# 03:83C7 90 0A       BCC $83D3 (+10 B)

	# 03:83C9 18          CLC
	# 03:83CA AD 3D 39    LDA $393D
	# 03:83CD 6D 43 39    ADC $3943
	# 03:83D0 8D 43 39    STA $3943

	# 03:83D3 6E 43 39    ROR $3943
	# 03:83D6 6E 41 39    ROR $3941
	# 03:83D9 CA          DEX
	# 03:83DA D0 E8       BNE $83C4 (-24 B)

	# 03:83DC 7B          TDC
	# 03:83DD E2 20       SEP #$20
	# 03:83DF 60          RTS
	return
end

# Multiplies $DF by $E1.
# Input: $DF, $E1 (8b)
# Output: $E3 (16b)
def _83e0
	# 03:83E0 64 E0       STZ $E0  Restrict the operANDs to 8b
	# 03:83E2 64 E2       STZ $E2
	# 03:83E4 C2 20       REP #$20

	# 03:83E6 A2 10 00    LDX #$0010
	# 03:83E9 64 E3       STZ $E3
	# 03:83EB 9C 4D 39    STZ $394D
	# 03:83EE 66 E1       ROR $E1
	# 03:83F0 90 09       BCC $83FB (+9 B)

	# 03:83F2 18          CLC
	# 03:83F3 A5 DF       LDA $DF
	# 03:83F5 6D 4D 39    ADC $394D
	# 03:83F8 8D 4D 39    STA $394D

	# 03:83FB 6E 4D 39    ROR $394D
	# 03:83FE 66 E3       ROR $E3
	# 03:8400 CA          DEX
	# 03:8401 D0 EB       BNE $83EE (-21 B)

	# 03:8403 7B          TDC
	# 03:8404 E2 20       SEP #$20
	# 03:8406 60          RTS
	return
end

# Calculates the division of $3945 / $3947. $3949 is the integer division, $394B is the remainder.
# Input: $3945, $3947
# Output: $3949, $394B
def _8407
	# 03:8407 C2 20       REP #$20
	# 03:8409 9C 49 39    STZ $3949
	# 03:840C 9C 4B 39    STZ $394B
	# 03:840F AD 45 39    LDA $3945
	# 03:8412 F0 2B       BEQ $843F (+43 B)
	# 03:8414 AD 47 39    LDA $3947
	# 03:8417 F0 26       BEQ $843F (+38 B)
	# 03:8419 18          CLC
	# 03:841A A2 10 00    LDX #$0010

	# 03:841D 2E 45 39    ROL $3945
	# 03:8420 2E 4B 39    ROL $394B
	# 03:8423 38          SEC
	# 03:8424 AD 4B 39    LDA $394B
	# 03:8427 ED 47 39    SBC $3947
	# 03:842A 8D 4B 39    STA $394B
	# 03:842D B0 0A       BCS $8439 (+10 B)
	# 03:842F AD 4B 39    LDA $394B
	# 03:8432 6D 47 39    ADC $3947
	# 03:8435 8D 4B 39    STA $394B
	# 03:8438 18          CLC
	# 03:8439 2E 49 39    ROL $3949
	# 03:843C CA          DEX
	# 03:843D D0 DE       BNE $841D (-34 B)

	# 03:843F 7B          TDC
	# 03:8440 E2 20       SEP #$20
	# 03:8442 60          RTS
	return
end

# Calculates the offset (from 0) for item inDEX $E5, in a list delimited by #$FFs. AY is the list offset
# Input: A, Y, $E5
# Output: Y
def _8443(a, y)
	# 03:8443 85 82       STA $82
	# 03:8445 84 80       STY $80
	mem8[0x82] = a
	mem16[0x80] = y

	# 03:8447 7B          TDC
	# 03:8448 A8          TAY
	# 03:8449 A5 E5       LDA $E5
	# 03:844B F0 10       BEQ $845D (+16 B)
	if mem8[0xE5] != 0
		# 03:844D B7 80       LDA [$80],y
		# 03:844F C9 FF       CMP #$FF
		# 03:8451 D0 06       BNE $8459 (+6 B)
		# 03:8453 C6 E5       DEC $E5
		# 03:8455 A5 E5       LDA $E5
		# 03:8457 F0 03       BEQ $845C (+3 B)
		# 03:8459 C8          INY
		# 03:845A 80 F1       BRA $844D (-15 B)
		# 03:845C C8          INY
		until mem8[0xE5] == 0 do
			if mem8[mem24[0x80] + y] == 0xFF
				mem8[0xE5] -= 1
			end

			y += 1
		end
	end

	# 03:845D 60          RTS
	return
end

# Copies byte data of length a to $289C-..., taken from [$80]'s inDEX $E5
# Input: a, $E5, $80-82
# Output: $289C-...
def _845E(a)
	# 03:845E 85 E1       STA $E1
	# 03:8460 A5 E5       LDA $E5
	# 03:8462 85 DF       STA $DF
	# 03:8464 A5 E1       LDA $E1
	# 03:8466 85 E5       STA $E5
	# 03:8468 20 E0 83    JSR $83E0
	# 03:846B A4 E3       LDY $E3
	y = mem8[0xE5] * a

	# 03:846D 7B          TDC
	# 03:846E AA          TAX
	# 03:846F B7 80       LDA [$80],y
	# 03:8471 9D 9C 28    STA $289C,x
	# 03:8474 C8          INY
	# 03:8475 E8          INX
	# 03:8476 E4 E5       CPX $E5
	# 03:8478 D0 F5       BNE $846F (-11 B)
	a.times do |x|
		mem8[0x289C + x] = mem8[mem24[0x80] + y]
		y += 1
	end

	# 03:847A 60          RTS
	return
end

# 03:847B 0A          ASL a
# 03:847C 0A          ASL a
# 03:847D 0A          ASL a
# 03:847E 0A          ASL a
# 03:847F 0A          ASL a
# 03:8480 0A          ASL a
# 03:8481 60          RTS
def _847B(a); a *= 64; end
def _847C(a); a *= 32; end
def _847D(a); a *= 16; end
def _847E(a); a *= 8; end
def _847F(a); a *= 4; end
def _8480(a); a *= 2; end

# 03:8482 4A          LSR a
# 03:8483 4A          LSR a
# 03:8484 4A          LSR a
# 03:8485 4A          LSR a
# 03:8486 4A          LSR a
# 03:8487 4A          LSR a
# 03:8488 60          RTS
def _8482(a); a /= 64; end
def _8483(a); a /= 32; end
def _8484(a); a /= 16; end
def _8485(a); a /= 8; end
def _8486(a); a /= 4; end
def _8487(a); a /= 2; end

def _8489(a)
	# 03:8489 8D 2F 35    STA $352F
	# 03:848C 85 DF       STA $DF
	# 03:848E A9 80       LDA #$80
	# 03:8490 85 E1       STA $E1
	# 03:8492 20 E0 83    JSR $83E0
	# 03:8495 A6 E3       LDX $E3
	# 03:8497 86 A6       STX $A6
	mem16[0xA6] = a * 0x80

	# 03:8499 AD 2F 35    LDA $352F
	# 03:849C 85 DF       STA $DF
	# 03:849E A9 15       LDA #$15
	# 03:84A0 85 E1       STA $E1
	# 03:84A2 20 E0 83    JSR $83E0
	# 03:84A5 A6 E3       LDX $E3
	# 03:84A7 8E 30 35    STX $3530
	mem16[0x3530] = a * 0x15

	# 03:84AA AD 2F 35    LDA $352F
	# 03:84AD 85 DF       STA $DF
	# 03:84AF A9 37       LDA #$37
	# 03:84B1 85 E1       STA $E1
	# 03:84B3 20 E0 83    JSR $83E0
	# 03:84B6 A6 E3       LDX $E3
	# 03:84B8 8E 32 35    STX $3532
	mem16[0x3532] = a * 0x37

	# 03:84BB AD 2F 35    LDA $352F
	# 03:84BE 85 DF       STA $DF
	# 03:84C0 A9 1C       LDA #$1C
	# 03:84C2 85 E1       STA $E1
	# 03:84C4 20 E0 83    JSR $83E0
	# 03:84C7 A6 E3       LDX $E3
	# 03:84C9 8E 34 35    STX $3534
	mem16[0x3534] = a * 0x1C

	# 03:84CC AD 2F 35    LDA $352F
	# 03:84CF AA          TAX
	# 03:84D0 8E 3D 39    STX $393D
	# 03:84D3 A2 20 01    LDX #$0120
	# 03:84D6 8E 3F 39    STX $393F
	# 03:84D9 20 B9 83    JSR $83B9
	# 03:84DC AE 41 39    LDX $3941
	# 03:84DF 8E 36 35    STX $3536
	mem16[0x3536] = a * 0x0120

	# 03:84E2 60          RTS
	return
end


# $395A-395D = $3956-3957 + $3958-3959
def _84e3
	# 03:84E3 C2 20       REP #$20
	# 03:84E5 18          CLC
	# 03:84E6 AD 56 39    LDA $3956
	# 03:84E9 6D 58 39    ADC $3958
	# 03:84EC 8D 5A 39    STA $395A
	# 03:84EF A9 00 00    LDA #$0000
	# 03:84F2 69 00 00    ADC #$0000
	# 03:84F5 8D 5C 39    STA $395C
	mem32[0x395A] = mem16[0x3956] + mem16[0x3958]

	# 03:84F8 7B          TDC
	# 03:84F9 E2 20       SEP #$20
	# 03:84FB 60          RTS
	return
end

# $3962-3963 = $395E-395F - $3960-$3961
def _84fc
	# 03:84FC C2 20       REP #$20
	# 03:84FE 38          SEC
	# 03:84FF AD 5E 39    LDA $395E
	# 03:8502 ED 60 39    SBC $3960
	# 03:8505 8D 62 39    STA $3962
	mem16[0x3962] = mem16[0x395E] - mem16[0x3960]

	# 03:8508 7B          TDC
	# 03:8509 E2 20       SEP #$20
	# 03:850B 60          RTS
	return
end

# Returns X, which = the number ofBITs set in A
def _850c(a)
	# 03:850C A2 00 00    LDX #$0000
	# 03:850F A0 08 00    LDY #$0008
	x = 0x0000
	y = 0x0008

	0x0008.times do
		# 03:8512 0A          ASL a
		# 03:8513 90 01       BCC $8516 (+1 B)
		if a % 2 == 1
			# 03:8515 E8          INX
			x += 1
		end

		a /= 2
		# 03:8516 88          DEY
		# 03:8517 D0 F9       BNE $8512 (-7 B)
	end

	# 03:8519 60          RTS
	return
end

# Copies theSTAts from $2680AND $2700 to the slots
# designated by $CDAND $CE (if b7 set, unsetsAND add 5)
def _851a
	# 03:851A AD 53 35    LDA $3553
	# 03:851D D0 1D       BNE $853C (+29 B)
	if mem8[0x3553] == 0
		# 03:851F A5 CD       LDA $CD
		# 03:8521 10 05       BPL $8528 (+5 B)
		a = mem8[0xCD]
		if a & 0x80 != 0
			# 03:8523 29 7F       AND #$7F
			# 03:8525 18          CLC
			# 03:8526 69 05       ADC #$05
			a = (a & 0x7F) + 0x05
		end

		# 03:8528 20 89 84    JSR $8489
		# 03:852B A6 A6       LDX $A6
		# 03:852D 7B          TDC
		# 03:852E A8          TAY
		x = a * 0x80
		0x0080.times do |y|
			# 03:852F B9 80 26    LDA $2680,y
			# 03:8532 9D 00 20    STA $2000,x
			# 03:8535 E8          INX
			# 03:8536 C8          INY
			# 03:8537 C0 80 00    CPY #$0080
			# 03:853A D0 F3       BNE $852F (-13 B)
			mem8[0x2000 + x] = mem8[0x2680 + y]
		end
	end

	# 03:853C A5 CE       LDA $CE
	# 03:853E 10 05       BPL $8545 (+5 B)
	a = mem8[0xCE]
	if a & 0x80 != 0
		# 03:8540 29 7F       AND #$7F
		# 03:8542 18          CLC
		# 03:8543 69 05       ADC #$05
		a = (a & 0x7F) + 0x05
	end

	# 03:8545 20 89 84    JSR $8489
	# 03:8548 A6 A6       LDX $A6
	# 03:854A 7B          TDC
	# 03:854B A8          TAY
	x = a * 0x80
	0x0080.times do
		# 03:854C B9 00 27    LDA $2700,y
		# 03:854F 9D 00 20    STA $2000,x
		# 03:8552 E8          INX
		# 03:8553 C8          INY
		# 03:8554 C0 80 00    CPY #$0080
		# 03:8557 D0 F3       BNE $854C (-13 B)
		mem8[0x2000 + x] = mem8[0x2700 + y]
	end

	# 03:8559 60          RTS
	return
end

# 13:FEF6                    7F BF  DF EF F7 FB FD FE 80 40
# 13:FF00 20 10 08 04  02 01 00

#ANDs with one 0
def _855a(a, x)
	# 03:855A 3F F6 FE 13 AND $13FEF6,x
	a &= mem8[0x13FEF6 + x]

	# 03:855E 60          RTS
	return
end

# ORs with one 1
def _855f(a, x)
	# 03:855F 1F FE FE 13 ORA $13FEFE,x
	a |= mem8[0x13FEFE + x]

	# 03:8563 60          RTS
	return
end

#ANDs with one 1
def _8564(a, x)
	# 03:8564 3F FE FE 13 AND $13FEFE,x
	a &= mem8[0x13FEFE + x]

	# 03:8568 60          RTS
	return
end

def _8569(a)
	# 03:8569 18          CLC
	# 03:856A 6D 30 35    ADC $3530
	# 03:856D 8D 98 35    STA $3598
	# 03:8570 AD 31 35    LDA $3531
	# 03:8573 69 00       ADC #$00
	# 03:8575 8D 99 35    STA $3599
	mem16[0x3598] = mem16[0x3530] + a

	# 03:8578 60          RTS
	return
end

# Returns a value between 0AND 7
def _8579
	# 03:8579 A2 00 00    LDX #$0000
	# 03:857C A9 07       LDA #$07
	# 03:857E 20 79 83    JSR $8379
	_8379(0x0000, 0x07)

	# 03:8581 60          RTS
	return
end

# Returns a value between 0AND 4
def _8582
	# 03:8582 A2 00 00    LDX #$0000
	# 03:8585 A9 04       LDA #$04
	# 03:8587 20 79 83    JSR $8379
	_8379(0x0000, 0x04)

	# 03:858A 60          RTS
	return
end

def _858b
	# Returns a value between 0AND 98
	# 03:858B 7B          TDC
	# 03:858C AA          TAX
	# 03:858D A9 62       LDA #$62
	# 03:858F 20 79 83    JSR $8379
	_8379(0x0000, 0x62)

	# 03:8592 60          RTS
	return
end

def _8593
	# Returns a value between 0AND 255
	# 03:8593 7B          TDC
	# 03:8594 AA          TAX
	# 03:8595 A9 FF       LDA #$FF
	# 03:8597 20 79 83    JSR $8379
	_8379(0x0000, 0xFF)

	# 03:859A 60          RTS
	return
end

def _859b
	# 03:859B A9 F8       LDA #$F8
	# 03:859D 8D C2 33    STA $33C2
	# 03:85A0 A9 03       LDA #$03
	# 03:85A2 8D C3 33    STA $33C3
	mem8[0x33C2] = 0xF8
	mem8[0x33C3] = 0x03

	# 03:85A5 60          RTS
	return
end

def _85a6
	# 03:85A6 A9 F8       LDA #$F8
	# 03:85A8 8D C6 33    STA $33C6
	# 03:85AB A9 03       LDA #$03
	# 03:85AD 8D C7 33    STA $33C7
	mem8[0x33C6] = 0xF8
	mem8[0x33C7] = 0x03

	# 03:85B0 60          RTS
	return
end

def _85b1
	# 03:85B1 A9 F8       LDA #$F8
	# 03:85B3 8D C8 33    STA $33C8
	# 03:85B6 A9 03       LDA #$03
	# 03:85B8 8D C9 33    STA $33C9
	mem8[0x33C8] = 0xF8
	mem8[0x33C9] = 0x03

	# 03:85BB 60          RTS
	return
end

def _85bc(x)
	# 03:85BC C2 20       REP #$20
	# 03:85BE 8A          TXA
	# 03:85BF 18          CLC
	# 03:85C0 69 80 00    ADC #$0080
	# 03:85C3 AA          TAX
	x += 0x80

	# 03:85C4 7B          TDC
	# 03:85C5 E2 20       SEP #$20
	# 03:85C7 60          RTS
	return
end

def _85c8(a)
	# 03:85C8 20 69 85    JSR $8569
	# 03:85CB AE 98 35    LDX $3598
	# 03:85CE A5 D4       LDA $D4
	# 03:85D0 9D 04 2A    STA $2A04,x
	# 03:85D3 A5 D5       LDA $D5
	# 03:85D5 9D 05 2A    STA $2A05,x
	x = mem16[0x3530] + a
	mem16[0x2A04 + x] = mem16[0xD4]

	# 03:85D8 60          RTS
	return
end

def _85d9
	# 03:85D9 AD AC 16    LDA $16AC
	# 03:85DC AA          TAX
	# 03:85DD BF 06 FF 13 LDA $13FF06,x
	# 03:85E1 1A          INC a
	# 03:85E2 8D 38 35    STA $3538
	# 03:85E5 9C E6 38    STZ $38E6
	mem8[0x3538] = mem8[0x13FF06 + mem8[0x16AC]] + 1
	mem8[0x38E6] = 0

	loop do
		# 03:85E8 A9 02       LDA #$02
		# 03:85EA 20 85 80    JSR $8085  Continue with code from bank 2
		_8085(0x02)

		# 03:85ED AD D9 38    LDA $38D9
		# 03:85F0 0D DA 38    ORA $38DA
		# 03:85F3 D0 F3       BNE $85E8 (-13 B)
		if mem8[0x38D9] | mem8[0x38DA] == 0
			next
		end

		# 03:85F5 CE 38 35DEC $3538
		# 03:85F8 AD 38 35    LDA $3538
		# 03:85FB D0 EB       BNE $85E8 (-21 B)
		mem8[0x3538] -= 1
		if mem8[0x3538] == 0
			next
		end

		# 03:85FD AD D7 38    LDA $38D7
		# 03:8600 D0 07       BNE $8609 (+7 B)
		if mem8[0x38D7] == 0
			# 03:8602 AD 81 35    LDA $3581
			# 03:8605 29 08       AND #$08
			# 03:8607 F0 2F       BEQ $8638 (+47 B)
		end

	# LOOOOOP

	# 03:8609 AD 81 35    LDA $3581
	# 03:860C 29 08       AND #$08
	# 03:860E F0 04       BEQ $8614 (+4 B)
	if mem8[0x3581] & 0x08 != 0
		# 03:8610 A9 04       LDA #$04
		# 03:8612 80 0B       BRA $861F (+11 B)
		a = 0x04
	else
		# 03:8614 AD D7 38    LDA $38D7
		# 03:8617 30 04       BMI $861D (+4 B)
		if mem8[0x38D7] >= 0
			# 03:8619 A9 02       LDA #$02
			# 03:861B 80 02       BRA $861F (+2 B)
			a = 0x02
		else
			# 03:861D A9 03       LDA #$03
			a = 0x03
		end
	end

	# 03:861F 8D CA 34    STA $34CA
	# 03:8622 A9 FF       LDA #$FF
	# 03:8624 8D CB 34    STA $34CB
	# 03:8627 8D C4 33    STA $33C4
	# 03:862A 20 9B 85    JSR $859B  Puts #$03F8 in $33C2-33C3
	mem8[0x34CA] = a
	mem8[0x34CB] = 0xFF
	mem8[0x34C4] = 0xFF
	_859b

	# 03:862D A9 05       LDA #$05
	# 03:862F 20 85 80    JSR $8085  Code from bank 2
	_8085(0x05)

	# 03:8632 9C D7 38    STZ $38D7
	# 03:8635 9C 81 35    STZ $3581
	mem8[0x38D7] = 0
	mem8[0x3581] = 0

	# 03:8638 AD 82 22    LDA $2282  If first enemy's level is 99,
	# 03:863B C9 63       CMP #$63
	# 03:863D D0 0A       BNE $8649 (+10 B)
	# 03:863F AD 2D 35    LDA $352D
	# 03:8642 D0 05       BNE $8649 (+5 B)
	if mem8[0x2282] == 0x63 && mem8[0x352D] == 0
		# 03:8644 A9 0F       LDA #$0F
		# 03:8646 8D D6 38    STA $38D6
		mem8[0x38D6] = 0x0F
	end

	# 03:8649 20 77 96    JSR $9677
	_9677

	# 03:864C AD 01 36    LDA $3601
	# 03:864F C9 FF       CMP #$FF
	# 03:8651 D0 06       BNE $8659 (+6 B)
	if mem8[0x3601] == 0xFF
		# 03:8653 20 5F 80    JSR $805F
		# 03:8656 20 FF A9    JSR $A9FF
		_805f
		_a9ff
	end

	# 03:8659 20 15 A0    JSR $A015
	_a015

	# 03:865C A5 A8       LDA $A8
	# 03:865E D0 3F       BNE $869F (+63 B)
	if mem8[0xA8] == 0
		# 03:8660 AD 01 36    LDA $3601
		# 03:8663 C9 FF       CMP #$FF
		# 03:8665 D0 06       BNE $866D (+6 B)
		if mem8[0x3601] == 0xFF
			# 03:8667 20 E2 AA    JSR $AAE2
			# 03:866A 20 E3 A3    JSR $A3E3
			_aae2
			_a3e3
		end

		# 03:866D 20 41 97    JSR $9741
		_9741

		# 03:8670 A5 D1       LDA $D1
		# 03:8672 F0 10       BEQ $8684 (+16 B)
		if mem8[0xD1] != 0
			# 03:8674 20 B3 97    JSR $97B3
			# 03:8677 20 87 86    JSR $8687
			_97b3
			_8687

			# 03:867A AD 2E 35    LDA $352E
			# 03:867D C9 02       CMP #$02
			# 03:867F D0 03       BNE $8684 (+3 B)
			if mem8[0x352E] == 0x02
				# 03:8681 20 6C C1    JSR $C16C
				_c16c
			end
		end

		# 03:8684 4C D9 85    JMP $85D9
		_85d9 # reSTArt the method
		return

		def _8687
			# 03:8920    71 A3 B6  B3 57 AD 56  A8
			# mem8[0x353E] address
			# 0            $03A371
			# 1            $03B3B6
			# 2            $03AD57
			# 3            $03A856

			# 03:8687 AD 2E 35    LDA $352E
			# 03:868A 0A          ASL a
			# 03:868B AA          TAX
			# 03:868C BF 21 89 03 LDA $038921,x
			# 03:8690 85 80       STA $80
			# 03:8692 BF 22 89 03 LDA $038922,x
			# 03:8696 85 81       STA $81
			# 03:8698 A9 03       LDA #$03
			# 03:869A 85 82       STA $82
			# 03:869C DC 80 00    JML ($0080)
			x = mem8[0x353E] * 2
			mem8[0x80] = mem8[0x038921 + x]
			mem8[0x81] = mem8[0x038922 + x]
			mem8[0x82] = 0x03
			# JUMP to ($0080)
			return
		end
	end

	# 03:869F 22 3A FF 13  JSL $13FF3A
	_13ff3a

	# 03:86A3 A9 85       LDA #$85
	# 03:86A5 8D F3 35    STA $35F3
	mem8[0x35F3] = 0x85

	# 03:86A8 A5 D7       LDA $D7
	# 03:86AA F0 05       BEQ $86B1 (+5 B)
	if mem8[0xD7] != 0
		# 03:86AC A9 01       LDA #$01
		# 03:86AE 20 85 80    JSR $8085
		_8085(0x01)
	end

	# 03:86B1 A5 A8       LDA $A8
	# 03:86B3 8D 03 18    STA $1803
	# 03:86B6 29 60       AND #$60
	# 03:86B8 D0 55       BNE $870F (+85 B)
	a = mem8[0xA8]
	mem8[0x1803] = a
	if a & 0x60 == 0
		# 03:86BA A5 A8       LDA $A8
		# 03:86BC 29 04       AND #$04
		# 03:86BE F0 0E       BEQ $86CE (+14 B)
		if mem8[0xA8] & 0x04 != 0
			# 03:86C0 A9 15       LDA #$15
			# 03:86C2 20 85 80    JSR $8085
			_8085(0x15)

			# 03:86C5 20 0F 88    JSR $880F
			# 03:86C8 20 8B F1    JSR $F18B
			# 03:86CB 4C 66 87    JMP $8766
			_880f
			_f18b
		else
			# 03:86CE AD E5 38    LDA $38E5
			# 03:86D1 29 0C       AND #$0C
			# 03:86D3 C9 0C       CMP #$0C
			# 03:86D5 F0 05       BEQ $86DC (+5 B)
			if mem8[0x38E5] & 0x0C != 0x0C
				# 03:86D7 A9 8B       LDA #$8B
				# 03:86D9 8D F3 35    STA $35F3
				mem8[0x35F3] = 0x8B
			end

			# 03:86DC A9 0C       LDA #$0C
			# 03:86DE 20 85 80    JSR $8085
			_8085(0x0C)

			# 03:86E1 A9 02       LDA #$02
			# 03:86E3 20 85 80    JSR $8085
			_8085(0x02)

			# 03:86E6 A9 15       LDA #$15
			# 03:86E8 20 85 80    JSR $8085
			_8085(0x15)

			# 03:86EB 20 9B 85    JSR $859B
			_859b

			# 03:86EE A9 24       LDA #$24
			# 03:86F0 8D CA 34    STA $34CA
			# 03:86F3 A9 FF       LDA #$FF
			# 03:86F5 8D CB 34    STA $34CB
			# 03:86F8 8D C4 33    STA $33C4
			# 03:86FB 9C 9A 35    STZ $359A
			# 03:86FE A9 40       LDA #$40
			# 03:8700 8D C2 34    STA $34C2
			mem8[0x34CA] = 0x24
			mem8[0x34CB] = 0xFF
			mem8[0x33C4] = 0xFF
			mem8[0x359A] = 0
			mem8[0x34C2] = 0x40

			# 03:8703 AD 8B 38    LDA $388B
			# 03:8706 D0 05       BNE $870D (+5 B)
			if mem8[0x388B] == 0
				# 03:8708 A9 05       LDA #$05
				# 03:870A 20 85 80    JSR $8085
				_8085(0x05)
			end
		end

		# 03:870D 80 57       BRA $8766 (+87 B)
	elsif a & 0x40 != 0
		# 03:870F 29 40       AND #$40
		# 03:8711 F0 15       BEQ $8728 (+21 B)
		# 03:8713 A9 13       LDA #$13
		# 03:8715 20 85 80    JSR $8085
		_8085(0x13)

		# 03:8718 20 0F 88    JSR $880F
		# 03:871B 20 8B F1    JSR $F18B
		_880f
		_f18b

		# 03:871E AD F3 38    LDA $38F3
		# 03:8721 D0 43       BNE $8766 (+67 B)
		# 03:8723 20 40 88    JSR $8840
		if mem8[0x38f3] == 0
			_8840
		end

		# 03:8726 80 3E       BRA $8766 (+62 B)
	else
		# 03:8728 20 D8 87    JSR $87D8
		_87d8

		# 03:872B A5 A9       LDA $A9
		# 03:872D D0 09       BNE $8738 (+9 B)
		# 03:872F AD E5 38    LDA $38E5
		# 03:8732 29 0C       AND #$0C
		# 03:8734 C9 0C       CMP #$0C
		# 03:8736 F0 08       BEQ $8740 (+8 B)
		if mem8[0xA9] != 0 || mem8[0x38E5] & 0x0C != 0x0C
			# 03:8738 A9 08       LDA #$08
			# 03:873A 8D BE 38    STA $38BE
			# 03:873D EE BD 38    INC $38BD
			mem8[0x38BE] = 0x08
			mem8[0x38BD] += 1
		end

		# 03:8740 20 0F 88    JSR $880F
		_880f

		# 03:8743 A9 02       LDA #$02
		# 03:8745 20 85 80    JSR $8085
		_8085(0x02)

		# 03:8748 20 03 88    JSR $8803
		_8803

		# 03:874B A5 A9       LDA $A9
		# 03:874D D0 05       BNE $8754 (+5 B)
		if mem8[0xA9] == 0
			# 03:874F A9 12       LDA #$12
			# 03:8751 20 85 80    JSR $8085
			_8085(0x12)
		end

		# 03:8754 20 8B F1    JSR $F18B
		_f18b

		# 03:8757 A5 A8       LDA $A8
		# 03:8759 29 10       AND #$10
		# 03:875B F0 09       BEQ $8766 (+9 B)
		if mem8[0xA8] & 0x10 != 0
			# 03:875D 20 72 EC    JSR $EC72
			# 03:8760 20 0F 88    JSR $880F
			# 03:8763 20 8B F1    JSR $F18B
			_ec72
			_880f
			_f18b
		end
	end

	# 03:8766 7B          TDC
	# 03:8767 AA          TAX
	# 03:8768 CA          DEX
	# 03:8769 D0 FD       BNE $8768 (-3 B)
	# Does nothing? Except doing 255 timesDEX?

	# 03:876B 64 AB       STZ $AB
	mem8[0xAB] = 0

	# 03:876D AE 00 18    LDX $1800
	# 03:8770 E0 C0 01    CPX #$01C0
	# 03:8773 90 02       BCC $8777 (+2 B)
	if mem16[0x1800] < 0x01C0
		# 03:8775 E6 AB       INC $AB
		mem8[0xAB] += 1
	end

	# 03:8777 A2 07 00    LDX #$0007
	0x0007.times do |x|
		# 03:877A BD 04 18    LDA $1804,x
		# 03:877D 05 AB       ORA $AB
		# 03:877F 85 AB       STA $AB
		# 03:8781 CA          DEX
		# 03:8782 10 F6       BPL $877A (-10 B)
		mem8[0xAB] = mem8[0x1804 + x] | mem8[0xAB]
	end

	# 03:8784 E8          INX
	# 03:8785 CA          DEX
	# 03:8786 D0 FD       BNE $8785 (-3 B)
	# Does nothing?
	x = 0

	# 03:8788 AD E5 38    LDA $38E5
	# 03:878B 29 0C       AND #$0C
	# 03:878D C9 0C       CMP #$0C
	# 03:878F F0 1A       BEQ $87AB (+26 B)
	# 03:8791 A5 AB       LDA $AB
	# 03:8793 D0 16       BNE $87AB (+22 B)
	if mem8[0x38E5] & 0x0C != 0x0C && mem8[0xAB] == 0
		# 03:8795 AD 00 18    LDA $1800
		# 03:8798 C9 B7       CMP #$B7
		# 03:879A D0 05       BNE $87A1 (+5 B)
		if mem8[0x1800] == 0xB7
			# 03:879C AD 01 18    LDA $1801
			# 03:879F D0 0A       BNE $87AB (+10 B)
			a = mem8[0x1801]
		else
			# 03:87A1 AD F3 35    LDA $35F3
			# 03:87A4 8D 00 1E    STA $1E00
			mem8[0x1E00] = mem8[0x35F3]

			# 03:87A7 22 04 80 04  JSL $048004
			_048004
		end
	end

	# 03:87AB A9 FF       LDA #$FF
	# 03:87AD 85 A9       STA $A9
	# 03:87AF A9 10       LDA #$10
	# 03:87B1 85 AA       STA $AA
	mem8[0xA9] = 0xFF
	mem8[0xAA] = 0x10

	loop do
		# 03:87B3 E6 A9       INC $A9
		# 03:87B5 C6 AA       DEC $AA
		# 03:87B7 A5 AA       LDA $AA
		# 03:87B9 F0 16       BEQ $87D1 (+22 B)
		mem8[0xA9] += 1
		mem8[0xAA] -= 1
		if mem8[0xAA] == 0
			break
		end

		# 03:87BB A5 A9       LDA $A9
		# 03:87BD 20 7E 84    JSR $847E
		# 03:87C0 09 03       ORA #$03
		# 03:87C2 8D C2 6C    STA $6CC2
		# 03:87C5 A5 AA       LDA $AA
		# 03:87C7 8D C1 6C    STA $6CC1
		mem8[0x6CC2] = (mem8[0xA9] * 8) | 0x03
		mem8[0x6CC1] = mem8[0xAA]

		# 03:87CA A9 02       LDA #$02
		# 03:87CC 20 85 80    JSR $8085
		_8085(0x02)

		# 03:87CF 80 E2       BRA $87B3 (-30 B)
	end

	# 03:87D1 A9 00       LDA #$00
	# 03:87D3 8F 06 21 00 STA $002106
	mem8[0x002106] = 0

	# 03:87D7 60          RTS
	return
end

def _87d8
	# 03:87D8 A2 67 FE    LDX #$FE67
	# 03:87DB 86 AB       STX $AB
	# 03:87DD A9 13       LDA #$13
	# 03:87DF 85 AD       STA $AD
	mem16[0xAB] = mem16[0xFE67]
	mem8[0xAD] = 0x13

	# 03:87E1 4C E4 87    JMP $87E4
	_87e4
end

def _87e4
	# 03:87E4 64 A9       STZ $A9
	# 03:87E6 7B          TDC
	# 03:87E7 A8          TAY
	mem8[0xA9] = 0
	y = 0

	loop do
		# 03:87E8 B7 AB       LDA [$AB],y
		# 03:87EA C9 FF       CMP #$FF
		# 03:87EC F0 14       BEQ $8802 (+20 B)
		a = mem8[mem8[0xAB] + y]
		if a == 0xFF
			break
		end

		# 03:87EE CD 00 18    CMP $1800
		# 03:87F1 D0 0B       BNE $87FE (+11 B)
		if a == mem8[0x1800]
			# 03:87F3 C8          INY
			# 03:87F4 B7 AB       LDA [$AB],y
			# 03:87F6 CD 01 18    CMP $1801
			# 03:87F9 D0 04       BNE $87FF (+4 B)
			# 03:87FB E6 A9       INC $A9
			# 03:87FD 60          RTS
			y += 1
			a = mem8[mem8[0xAB] + y]
			if a == mem8[0x1801]
				mem8[0xA9] += 1
				return
			else
				y += 1
			end
		else
			y += 2
		end

		# 03:87FE C8          INY
		# 03:87FF C8          INY
		# 03:8800 80 E6       BRA $87E8 (-26 B)
	end

	# 03:8802 60          RTS
	return
end

def _8803
	# 03:8803 A2 76 FE    LDX #$FE76
	# 03:8806 86 AB       STX $AB
	# 03:8808 A9 13       LDA #$13
	# 03:880A 85 AD       STA $AD
	# 03:880C 4C E4 87    JMP $87E4
	mem16[0xAB] = mem16[0xFE76]
	mem8[0xAD] = 0x13
	_87e4
end

def _880f
	# 03:880F 7B          TDC
	# 03:8810 AA          TAX
	# 03:8811 A8          TAY
	x = 0
	y = 0
	begin
		# 03:8812 BD 03 20    LDA $2003,x
		# 03:8815 99 BF 38    STA $38BF,y
		# 03:8818 29 F8       AND #$F8
		# 03:881A 9D 03 20    STA $2003,x
		a = mem8[0x2003 + x]
		mem8[0x38BF + y] = a
		mem8[0x2003 + x] = a & 0xF8

		# 03:881D BD 04 20    LDA $2004,x
		# 03:8820 99 C0 38    STA $38C0,y
		# 03:8823 29 40       AND #$40
		# 03:8825 9D 04 20    STA $2004,x
		a = mem8[0x2004 + x]
		mem8[0x38C0 + y] = a
		mem8[0x2004 + x] = a & 0x40

		# 03:8828 9E 05 20    STZ $2005,x
		# 03:882B BD 06 20    LDA $2006,x
		# 03:882E 99 C1 38    STA $38C1,y
		# 03:8831 9E 06 20    STZ $2006,x
		mem8[0x2005 + x] = 0
		mem8[0x38C1 + y] = mem8[0x2006 + x]
		mem8[0x2006 + x] = 0

		# 03:8834 20 BC 85    JSR $85BC
		# 03:8837 C8          INY
		# 03:8838 C8          INY
		# 03:8839 C8          INY
		# 03:883A C0 0F 00    CPY #$000F
		# 03:883D D0 D3       BNE $8812 (-45 B)
		x += 0x80
		y -= 3
	end until y == 0

	# 03:883F 60          RTS
	return
end

def _8840
	# 03:8840 20 8B 85    JSR $858B
	# 03:8843 C9 32       CMP #$32
	# 03:8845 B0 03       BCS $884A (+3 B)
	if (0..0x62).to_a.sample >= 0x32 # 1/2 chances
		# 03:8847 4C 20 89    JMP $8920
		return
	end

	# 03:884A A0 00 00    LDY #$0000
	# 03:884D 84 AB       STY $AB
	# 03:884F 8C 9C 28    STY $289C
	# 03:8852 8C 9E 28    STY $289E
	mem32[0x289C] = 0
	3.times do |y|
		# 03:8855 A4 AB       LDY $AB
		# 03:8857 B9 88 35    LDA $3588,y
		# 03:885A AA          TAX
		# 03:885B 86 A9       STX $A9
		# 03:885D 06 A9       ASL $A9
		# 03:885F 26 AA       ROL $AA
		# 03:8861 A6 A9       LDX $A9
		x = mem8[0x3588 + y] * 2

		# 03:8863 BF 00 A0 0E LDA $0EA000,x  Enemy Gil
		# 03:8867 8D 3D 39    STA $393D
		# 03:886A BF 01 A0 0E LDA $0EA001,x
		# 03:886E 8D 3E 39    STA $393E
		# 03:8871 B9 F0 38    LDA $38F0,y
		# 03:8874 AA          TAX
		# 03:8875 8E 3F 39    STX $393F
		# 03:8878 20 B9 83    JSR $83B9
		# 03:887B 18          CLC
		# 03:887C AD 41 39    LDA $3941
		# 03:887F 6D 9C 28    ADC $289C
		# 03:8882 8D 9C 28    STA $289C
		# 03:8885 AD 42 39    LDA $3942
		# 03:8888 6D 9D 28    ADC $289D
		# 03:888B 8D 9D 28    STA $289D
		# 03:888E AD 43 39    LDA $3943
		# 03:8891 6D 9E 28    ADC $289E
		# 03:8894 8D 9E 28    STA $289E
		mem24[0x289C] += mem16[0x0EA000 + x] * mem8[0x38F0 + y]

		# 03:8897 E6 AB       INC $AB
		# 03:8899 A5 AB       LDA $AB
		# 03:889B C9 03       CMP #$03
		# 03:889D D0 B6       BNE $8855 (-74 B)
	end

	# 03:889F 4E 9E 28    LSR $289E
	# 03:88A2 6E 9D 28    ROR $289D
	# 03:88A5 6E 9C 28    ROR $289C
	# 03:88A8 4E 9E 28    LSR $289E
	# 03:88AB 6E 9D 28    ROR $289D
	# 03:88AE 6E 9C 28    ROR $289C
	mem24[0x289C] /= 4

	# 03:88B1 AD 9C 28    LDA $289C
	# 03:88B4 0D 9D 28    ORA $289D
	# 03:88B7 F0 67       BEQ $8920 (+103 B)
	# 03:88B9 AD A0 16    LDA $16A0
	# 03:88BC 0D A1 16    ORA $16A1
	# 03:88BF 0D A2 16    ORA $16A2
	# 03:88C2 F0 5C       BEQ $8920 (+92 B)
	# 0x289E isn't important in this condition?
	if mem16[0x289C] != 0 && mem24[0x16A0] != 0
		# 03:88C4 AD A0 16    LDA $16A0
		# 03:88C7 85 A9       STA $A9
		# 03:88C9 AD A1 16    LDA $16A1
		# 03:88CC 85 AA       STA $AA
		mem16[0xA9] = mem16[0x16A0]

		# 03:88CE 38          SEC
		# 03:88CF AD A0 16    LDA $16A0
		# 03:88D2 ED 9C 28    SBC $289C
		# 03:88D5 8D A0 16    STA $16A0
		# 03:88D8 AD A1 16    LDA $16A1
		# 03:88DB ED 9D 28    SBC $289D
		# 03:88DE 8D A1 16    STA $16A1
		# 03:88E1 AD A2 16    LDA $16A2
		# 03:88E4 ED 9E 28    SBC $289E
		# 03:88E7 8D A2 16    STA $16A2
		# 03:88EA B0 13       BCS $88FF (+19 B)
		mem24[0x16A0] -= mem24[0x289C]
		if mem24[0x16A0] >= 0
			# 03:88EC A5 A9       LDA $A9
			# 03:88EE 8D 9C 28    STA $289C
			# 03:88F1 A5 AA       LDA $AA
			# 03:88F3 8D 9D 28    STA $289D
			# 03:88F6 9C A0 16    STZ $16A0
			# 03:88F9 9C A1 16    STZ $16A1
			# 03:88FC 9C A2 16    STZ $16A2
			mem16[0x289C] = mem16[0xA9]
			mem24[0x16A0] = 0
		end

		# 03:88FF AD 9C 28    LDA $289C
		# 03:8902 8D 9A 35    STA $359A
		# 03:8905 AD 9D 28    LDA $289D
		# 03:8908 8D 9B 35    STA $359B
		# 03:890B 9C 9C 35    STZ $359C
		mem24[0x359A] = mem16[0x289C]

		# 03:890E A9 37       LDA #$37
		# 03:8910 8D CA 34    STA $34CA
		# 03:8913 20 9B 85    JSR $859B
		# 03:8916 A9 FF       LDA #$FF
		# 03:8918 8D CC 34    STA $34CC
		mem8[0x34CA] = 0x37
		mem8[0x33C2] = 0xF8
		mem8[0x33C3] = 0x03
		mem8[0x34CC] = 0xFF

		# 03:891B A9 05       LDA #$05
		# 03:891D 20 85 80    JSR $8085
		_8085(0x05)
	end

	# 03:8920 60          RTS
	return
end

# 03:8920    71 A3 B6  B3 57 AD 56  A8

def _8929
	# If Current Encounter == #$01B6 or #$01B7,INC $3881
	# (Golbez/ShadowDragon, or Zeromus/Zeromus)
	# 03:8929 AD 00 18    LDA $1800
	# 03:892C C9 B6       CMP #$B6
	# 03:892E F0 04       BEQ $8934 (+4 B)
	# 03:8930 C9 B7       CMP #$B7
	# 03:8932 D0 08       BNE $893C (+8 B)
	# 03:8934 AD 01 18    LDA $1801
	# 03:8937 F0 03       BEQ $893C (+3 B)
	if mem16[0x1800] == 0x01B6 || mem16[0x1800] == 0x01B7
		# 03:8939 EE 81 38    INC $3881
		mem8[0x3881] += 1
	end

	# 03:893C 20 A8 94    JSR $94A8  PREPares the characters for the battle
	# 03:893F 20 25 96    JSR $9625  Writes data about poisoned characters
	# 03:8942 20 CE 95    JSR $95CE  Set FrontAND Back RowBITs
	_94a8
	_9625
	_95ce

	# If Auto-Battle, calculates the offset for correct seq.
	# 03:8945 AD A2 29    LDA $29A2
	# 03:8948 8D E5 38    STA $38E5
	# 03:894B 29 20       AND #$20
	# 03:894D 8D 8B 38    STA $388B
	# 03:8950 F0 5D       BEQ $89AF (+93 B)
	a = mem8[0x29A2]
	mem8[0x38E5] = a
	mem8[0x388B] = a & 0x20
	if a & 0x20 != 0
		# 03:8952 7B          TDC
		# 03:8953 AA          TAX
		# 03:8954 BF 0D FE 13 LDA $13FE0D,x
		# 03:8958 CD 00 18    CMP $1800
		# 03:895B F0 09       BEQ $8966 (+9 B)
		# 03:895D E8          INX
		# 03:895E E8          INX
		# 03:895F E0 10 00    CPX #$0010
		# 03:8962 D0 F0       BNE $8954 (-16 B)
		# 03:8964 80 49       BRA $89AF (+73 B)
		# 03:8966 86 84       STX $84
		# 03:8968 BF 0E FE 13 LDA $13FE0E,x
		# 03:896C CD 01 18    CMP $1801
		# 03:896F D0 3E       BNE $89AF (+62 B)
		# 03:8971 8A          TXA
		# 03:8972 BF 1D FE 13 LDA $13FE1D,x
		# 03:8976 85 80       STA $80
		# 03:8978 BF 1E FE 13 LDA $13FE1E,x
		# 03:897C 85 81       STA $81
		# 03:897E A9 13       LDA #$13
		# 03:8980 85 82       STA $82

		# Data from $13FE0D,xAND $13FE1D,x. They result in an offset to



		# 03:8982 7B          TDC   Copies the Auto-Battle Sequence to $388C-...
		# 03:8983 A8          TAY
		# 03:8984 AA          TAX
		# 03:8985 B7 80       LDA [$80],y
		# 03:8987 9D 8C 38    STA $388C,x
		# 03:898A C8          INY
		# 03:898B E8          INX
		# 03:898C C9 FF       CMP #$FF
		# 03:898E D0 F5       BNE $8985 (-11 B)

		# 03:8990 A5 84       LDA $84  If the fight is with FuSoYa/Golbez, copies theSECond
		# 03:8992 C9 0C       CMP #$0C  auto-battle sequence
		# 03:8994 F0 04       BEQ $899A (+4 B)
		# 03:8996 C9 0E       CMP #$0E
		# 03:8998 D0 15       BNE $89AF (+21 B)
		# 03:899A 7B          TDC
		# 03:899B AA          TAX
		# 03:899C B7 80       LDA [$80],y
		# 03:899E 9D 9A 38    STA $389A,x
		# 03:89A1 C8          INY
		# 03:89A2 E8          INX
		# 03:89A3 C9 FF       CMP #$FF
		# 03:89A5 D0 F5       BNE $899C (-11 B)
		# 03:89A7 A9 63       LDA #$63
		# 03:89A9 8D 90 21    STA $2190
		# 03:89AC 8D 10 22    STA $2210
	end

	# 03:89AF 20 CC 8E    JSR $8ECC  PREPares the Equipped Items for the Party
  _8ecc

	# 03:89B2 9C 75 39    STZ $3975
  5.times do |x|
  	# 03:89B5 AD 75 39    LDA $3975
  	# 03:89B8 AA          TAX
  	# 03:89B9 BD 40 35    LDA $3540,x
  	# 03:89BC D0 03       BNE $89C1 (+3 B)
    if mem8[0x3540 + x] == 0
      # Prepares the current char's stats relative to his equipped items
  	  # 03:89BE 20 37 98    JSR $9837
      _9837
    end

  	# 03:89C1 EE 75 39    INC $3975
  	# 03:89C4 AD 75 39    LDA $3975
  	# 03:89C7 C9 05       CMP #$05
  	# 03:89C9 D0 EA       BNE $89B5 (-22 B)
  end

  # Copies the active chars' magic banks to $2C7B-321A
	# 03:89CB 7B          TDC
	# 03:89CC AA          TAX
	# 03:89CD A8          TAY
	# 03:89CE 86 A9       STX $A9
	# 03:89D0 84 AB       STY $AB
	# 03:89D2 A5 A9       LDA $A9
	# 03:89D4 20 89 84    JSR $8489
	# 03:89D7 A6 A6       LDX $A6
	# 03:89D9 BD 01 20    LDA $2001,x
	# 03:89DC 29 0F       AND #$0F
	# 03:89DE 85 B7       STA $B7
  a = mem8[0x2001 + mem8[0xA9] * 0x80]
  mem8[0xB7] = a
	# 03:89E0 0A          ASL a
	# 03:89E1 18          CLC
	# 03:89E2 65 B7       ADC $B7
	# 03:89E4 85 B7       STA $B7
	# 03:89E6 64 B8       STZ $B8
	# 03:89E8 64 B5       STZ $B5
	# 03:89EA A6 B7       LDX $B7
	# 03:89EC BF DD FD 13 LDA $13FDDD,x
	# 03:89F0 C9 FF       CMP #$FF
	# 03:89F2 D0 0F       BNE $8A03 (+15 B)

	# 03:89F4 18          CLC
	# 03:89F5 A5 AB       LDA $AB
	# 03:89F7 69 60       ADC #$60
	# 03:89F9 85 AB       STA $AB
	# 03:89FB A5 AC       LDA $AC
	# 03:89FD 69 00       ADC #$00
	# 03:89FF 85 AC       STA $AC
	# 03:8A01 80 25       BRA $8A28 (+37 B)

	# 03:8A03 85 DF       STA $DF
	# 03:8A05 A9 18       LDA #$18
	# 03:8A07 85 E1       STA $E1
	# 03:8A09 20 E0 83    JSR $83E0
	# 03:8A0C A6 E3       LDX $E3
	# 03:8A0E 64 AD       STZ $AD
	# 03:8A10 A4 AB       LDY $AB

	# 03:8A12 BF 60 15 00 LDA $001560,x
	# 03:8A16 99 7B 2C    STA $2C7B,y
	# 03:8A19 C8          INY
	# 03:8A1A C8          INY
	# 03:8A1B C8          INY
	# 03:8A1C C8          INY
	# 03:8A1D E8          INX
	# 03:8A1E E6 AD       INC $AD
	# 03:8A20 A5 AD       LDA $AD
	# 03:8A22 C9 18       CMP #$18
	# 03:8A24 D0 EC       BNE $8A12 (-20 B)
	# 03:8A26 84 AB       STY $AB

	# 03:8A28 E6 B7       INC $B7
	# 03:8A2A E6 B5       INC $B5
	# 03:8A2C A5 B5       LDA $B5
	# 03:8A2E C9 03       CMP #$03
	# 03:8A30 F0 03       BEQ $8A35 (+3 B)

	# 03:8A32 4C EA 89    JMP $89EA

	# 03:8A35 E6 A9       INC $A9
	# 03:8A37 A5 A9       LDA $A9
	# 03:8A39 C9 05       CMP #$05
	# 03:8A3B F0 03       BEQ $8A40 (+3 B)

	# 03:8A3D 4C D2 89    JMP $89D2

  # Fills the $2C7A-3219 range with data from valid magics
	# 03:8A40 7B          TDC
	# 03:8A41 AA          TAX
	# 03:8A42 86 A9       STX $A9
  x = 0
  loop do
  	# 03:8A44 A6 A9       LDX $A9
  	# 03:8A46 BD 7B 2C    LDA $2C7B,x
  	# 03:8A49 F0 24       BEQ $8A6F (+36 B)
    a = mem8[0x2C7B + x]
    if a != 0
    	# 03:8A4B AA          TAX
    	# 03:8A4C 86 E5       STX $E5
    	# 03:8A4E A2 A0 97    LDX #$97A0
    	# 03:8A51 86 80       STX $80
    	# 03:8A53 A9 0F       LDA #$0F
    	# 03:8A55 85 82       STA $82
    	# 03:8A57 A9 06       LDA #$06
    	# 03:8A59 20 5E 84    JSR $845E
      mem16[0xE5] = a
      mem24[0x80] = 0x0F97A0
      _845e(0x06)

    	# 03:8A5C A6 A9       LDX $A9
    	# 03:8A5E AD 9C 28    LDA $289C
    	# 03:8A61 29 E0       AND #$E0
    	# 03:8A63 4A          LSR a
    	# 03:8A64 9D 7A 2C    STA $2C7A,x
    	# 03:8A67 AD A1 28    LDA $28A1
    	# 03:8A6A 29 7F       AND #$7F
    	# 03:8A6C 9D 7D 2C    STA $2C7D,x
      mem8[0x2C7A + x] = (mem8[0x289C] & 0xE0) / 2
      mem8[0x2C7D + x] = mem8[0x28A1] & 0x7F
    end

  	# 03:8A6F E8          INX
  	# 03:8A70 E8          INX
  	# 03:8A71 E8          INX
  	# 03:8A72 E8          INX
  	# 03:8A73 86 A9       STX $A9
  	# 03:8A75 E0 A0 05    CPX #$05A0
  	# 03:8A78 D0 CA       BNE $8A44 (-54 B)
    x += 4

    break if x == 0x5A0
  end

  # Copies the party items to the $321A-32D9 range
	# 03:8A7A 7B          TDC
	# 03:8A7B AA          TAX
	# 03:8A7C A8          TAY
  x = 0
  y = 0
  loop do
  	# 03:8A7D BD 40 14    LDA $1440,x
  	# 03:8A80 99 1B 32    STA $321B,y
  	# 03:8A83 BD 41 14    LDA $1441,x
  	# 03:8A86 99 1C 32    STA $321C,y
  	# 03:8A89 E8          INX
  	# 03:8A8A E8          INX
  	# 03:8A8B C8          INY
  	# 03:8A8C C8          INY
  	# 03:8A8D C8          INY
  	# 03:8A8E C8          INY
  	# 03:8A8F E0 60 00    CPX #$0060
  	# 03:8A92 D0 E9       BNE $8A7D (-23 B)
    mem16[0x321B + y] = mem16[0x1440 + x]
    x += 2
    y += 4

    break if x == 0x0060
  end

	# 03:8A94 7B          TDC   Fills data in the $321A-32D9 range concerning items
	# 03:8A95 AA          TAX
	# 03:8A96 86 A9       STX $A9
	# 03:8A98 64 C7       STZ $C7
	# 03:8A9A A6 A9       LDX $A9
	# 03:8A9C BD 1B 32    LDA $321B,x
	# 03:8A9F F0 73       BEQ $8B14 (+115 B)
	# 03:8AA1 C9 DE       CMP #$DE
	# 03:8AA3 B0 6F       BCS $8B14 (+111 B)
	# 03:8AA5 C9 B0       CMP #$B0
	# 03:8AA7 B0 44       BCS $8AED (+68 B)
	# 03:8AA9 C9 6D       CMP #$6D
	# 03:8AAB B0 67       BCS $8B14 (+103 B)
	# 03:8AAD C9 61       CMP #$61
	# 03:8AAF 90 04       BCC $8AB5 (+4 B)
	# 03:8AB1 A9 00       LDA #$00
	# 03:8AB3 F0 5C       BEQ $8B11 (+92 B)
	# 03:8AB5 AA          TAX   [Weapon]
	# 03:8AB6 86 E5       STX $E5  Fills these items in $321A-32D9
	# 03:8AB8 A2 00 91    LDX #$9100
	# 03:8ABB 86 80       STX $80
	# 03:8ABD A9 0F       LDA #$0F
	# 03:8ABF 85 82       STA $82
	# 03:8AC1 A9 08       LDA #$08
	# 03:8AC3 20 5E 84    JSR $845E
	# 03:8AC6 AD 9C 28    LDA $289C
	# 03:8AC9 20 85 84    JSR $8485
	# 03:8ACC 29 08       AND #$08  <-- throwable?
	# 03:8ACE 85 C7       STA $C7
	# 03:8AD0 AD 9F 28    LDA $289F
	# 03:8AD3 48          PHA
	# 03:8AD4 AA          TAX
	# 03:8AD5 86 E5       STX $E5
	# 03:8AD7 A2 A0 97    LDX #$97A0
	# 03:8ADA 86 80       STX $80
	# 03:8ADC A9 0F       LDA #$0F
	# 03:8ADE 85 82       STA $82
	# 03:8AE0 A9 06       LDA #$06
	# 03:8AE2 20 5E 84    JSR $845E
	# 03:8AE5 A6 A9       LDX $A9
	# 03:8AE7 68          PLA
	# 03:8AE8 9D 1D 32    STA $321D,x
	# 03:8AEB 80 1C       BRA $8B09 (+28 B)
	# 03:8AED 38          SEC   [Usable items in combat]
	# 03:8AEE E9 B0       SBC #$B0  Fills these items in $321A-32D9
	# 03:8AF0 AA          TAX
	# 03:8AF1 86 E5       STX $E5
	# 03:8AF3 A2 80 96    LDX #$9680
	# 03:8AF6 86 80       STX $80
	# 03:8AF8 A9 0F       LDA #$0F
	# 03:8AFA 85 82       STA $82
	# 03:8AFC A9 06       LDA #$06
	# 03:8AFE 20 5E 84    JSR $845E
	# 03:8B01 A6 A9       LDX $A9
	# 03:8B03 AD 9F 28    LDA $289F
	# 03:8B06 9D 1D 32    STA $321D,x
	# 03:8B09 AD 9C 28    LDA $289C
	# 03:8B0C 29 E0       AND #$E0
	# 03:8B0E 05 C7       ORA $C7  <-- ?
	# 03:8B10 4A          LSR a
	# 03:8B11 9D 1A 32    STA $321A,x
	# 03:8B14 E8          INX
	# 03:8B15 E8          INX
	# 03:8B16 E8          INX
	# 03:8B17 E8          INX
	# 03:8B18 86 A9       STX $A9
	# 03:8B1A E0 C0 00    CPX #$00C0
	# 03:8B1D F0 03       BEQ $8B22 (+3 B)
	# 03:8B1F 4C 98 8A    JMP $8A98

	# 03:8B22 7B          TDC   Fills data in the $32DA-3301 range concerning
	# 03:8B23 AA          TAX   equipped weapons
	# 03:8B24 86 A9       STX $A9
	# 03:8B26 64 C7       STZ $C7
	# 03:8B28 A6 A9       LDX $A9
	# 03:8B2A BD DB 32    LDA $32DB,x
	# 03:8B2D F0 41       BEQ $8B70 (+65 B)
	# 03:8B2F AA          TAX
	# 03:8B30 86 E5       STX $E5
	# 03:8B32 A2 00 91    LDX #$9100
	# 03:8B35 86 80       STX $80
	# 03:8B37 A9 0F       LDA #$0F
	# 03:8B39 85 82       STA $82
	# 03:8B3B A9 08       LDA #$08
	# 03:8B3D 20 5E 84    JSR $845E
	# 03:8B40 AD 9C 28    LDA $289C  <-- throwable ?
	# 03:8B43 20 85 84    JSR $8485
	# 03:8B46 29 08       AND #$08
	# 03:8B48 85 C7       STA $C7
	# 03:8B4A A6 A9       LDX $A9
	# 03:8B4C AD 9F 28    LDA $289F
	# 03:8B4F 9D DD 32    STA $32DD,x
	# 03:8B52 AA          TAX
	# 03:8B53 86 E5       STX $E5
	# 03:8B55 A2 A0 97    LDX #$97A0
	# 03:8B58 86 80       STX $80
	# 03:8B5A A9 0F       LDA #$0F
	# 03:8B5C 85 82       STA $82
	# 03:8B5E A9 06       LDA #$06
	# 03:8B60 20 5E 84    JSR $845E
	# 03:8B63 A6 A9       LDX $A9
	# 03:8B65 AD 9C 28    LDA $289C
	# 03:8B68 29 E0       AND #$E0
	# 03:8B6A 05 C7       ORA $C7  <-- ?
	# 03:8B6C 4A          LSR a
	# 03:8B6D 9D DA 32    STA $32DA,x
	# 03:8B70 E8          INX
	# 03:8B71 E8          INX
	# 03:8B72 E8          INX
	# 03:8B73 E8          INX
	# 03:8B74 86 A9       STX $A9
	# 03:8B76 E0 28 00    CPX #$0028
	# 03:8B79 D0 AB       BNE $8B26 (-85 B)

	# 03:8B7B 7B          TDC   Copies the BattleCommAND data for each char's 7 commANDs
	# 03:8B7C A8          TAY   to the $3302-338D range,INCluding the ChangeAND Defend
	# 03:8B7D 84 A9       STY $A9
	# 03:8B7F A6 A9       LDX $A9
	# 03:8B81 BD 00 20    LDA $2000,x
	# 03:8B84 29 1F       AND #$1F
	# 03:8B86 D0 01       BNE $8B89 (+1 B)
	# 03:8B88 1A          INC a
	# 03:8B89 3A          DEC a
	# 03:8B8A 85 DF       STA $DF
	# 03:8B8C A9 05       LDA #$05
	# 03:8B8E 85 E1       STA $E1
	# 03:8B90 20 E0 83    JSR $83E0
	# 03:8B93 A9 05       LDA #$05
	# 03:8B95 85 AB       STA $AB
	# 03:8B97 A6 E3       LDX $E3
	# 03:8B99 BF 55 FD 13 LDA $13FD55,x
	# 03:8B9D 99 03 33    STA $3303,y
	# 03:8BA0 C9 FF       CMP #$FF
	# 03:8BA2 D0 03       BNE $8BA7 (+3 B)
	# 03:8BA4 DA          PHX
	# 03:8BA5 80 06       BRA $8BAD (+6 B)
	# 03:8BA7 DA          PHX
	# 03:8BA8 AA          TAX
	# 03:8BA9 BF C3 FD 13 LDA $13FDC3,x
	# 03:8BAD 99 02 33    STA $3302,y
	# 03:8BB0 FA          PLX
	# 03:8BB1 E8          INX
	# 03:8BB2 C8          INY
	# 03:8BB3 C8          INY
	# 03:8BB4 C8          INY
	# 03:8BB5 C8          INY
	# 03:8BB6 C6 AB       DEC $AB
	# 03:8BB8 A5 AB       LDA $AB
	# 03:8BBA D0 DD       BNE $8B99 (-35 B)
	# 03:8BBC A9 1A       LDA #$1A
	# 03:8BBE 99 03 33    STA $3303,y
	# 03:8BC1 C8          INY
	# 03:8BC2 C8          INY
	# 03:8BC3 C8          INY
	# 03:8BC4 C8          INY
	# 03:8BC5 A9 1B       LDA #$1B
	# 03:8BC7 99 03 33    STA $3303,y
	# 03:8BCA C8          INY
	# 03:8BCB C8          INY
	# 03:8BCC C8          INY
	# 03:8BCD C8          INY
	# 03:8BCE 08          PHP
	# 03:8BCF C2 20       REP #$20
	# 03:8BD1 18          CLC
	# 03:8BD2 A5 A9       LDA $A9
	# 03:8BD4 69 80 00    ADC #$0080
	# 03:8BD7 85 A9       STA $A9
	# 03:8BD9 7B          TDC
	# 03:8BDA 28          PLP
	# 03:8BDB C0 8C 00    CPY #$008C
	# 03:8BDE D0 9F       BNE $8B7F (-97 B)

	# 03:8BE0 7B          TDC   If current enemy is empty, puts the correct byte
	# 03:8BE1 AA          TAX   in the $3540-354C range
	# 03:8BE2 86 B9       STX $B9
	# 03:8BE4 64 BD       STZ $BD
	# 03:8BE6 A6 B9       LDX $B9
	# 03:8BE8 BD B5 29    LDA $29B5,x
	# 03:8BEB C9 FF       CMP #$FF
	# 03:8BED D0 0A       BNE $8BF9 (+10 B)
	# 03:8BEF 8A          TXA
	# 03:8BF0 18          CLC
	# 03:8BF1 69 05       ADC #$05
	# 03:8BF3 AA          TAX
	# 03:8BF4 FE 40 35    INC $3540,x
	# 03:8BF7 80 15       BRA $8C0E (+21 B)

	# 03:8BF9 AA          TAX   If current enemy is not empty, calculates itsSTAts
	# 03:8BFA AD 9C 29    LDA $299CAND attack/counter sequences
	# 03:8BFD 20 64 85    JSR $8564
	# 03:8C00 F0 02       BEQ $8C04 (+2 B)
	# 03:8C02 E6 BD       INC $BD
	# 03:8C04 BD AD 29    LDA $29AD,x
	# 03:8C07 85 BB       STA $BB
	# 03:8C09 64 BC       STZ $BC
	# 03:8C0B 20 CA 8F    JSR $8FCA
	# 03:8C0E E6 B9       INC $B9
	# 03:8C10 A5 B9       LDA $B9
	# 03:8C12 C9 08       CMP #$08
	# 03:8C14 D0 CE       BNE $8BE4 (-50 B)

	# 03:8C16 AD A0 29    LDA $29A0
	# 03:8C19 29 03       AND #$03
	# 03:8C1B F0 49       BEQ $8C66 (+73 B)
	# 03:8C1D C9 01       CMP #$01  Encounter B0b0-1 = 1, kill first enemy from group
	# 03:8C1F D0 11       BNE $8C32 (+17 B) inDEX #$01
	# 03:8C21 7B          TDC
	# 03:8C22 AA          TAX
	# 03:8C23 BD B5 29    LDA $29B5,x
	# 03:8C26 C9 01       CMP #$01
	# 03:8C28 F0 03       BEQ $8C2D (+3 B)
	# 03:8C2A E8          INX
	# 03:8C2B 80 F6       BRA $8C23 (-10 B)
	# 03:8C2D 20 99 8E    JSR $8E99
	# 03:8C30 80 34       BRA $8C66 (+52 B)
	# 03:8C32 C9 02       CMP #$02  If Encounter B0b0-1 = 2, kill all enemies that are not
	# 03:8C34 D0 16       BNE $8C4C (+22 B) from group inDEX #$00
	# 03:8C36 7B          TDC
	# 03:8C37 AA          TAX
	# 03:8C38 86 C7       STX $C7
	# 03:8C3A A6 C7       LDX $C7
	# 03:8C3C BD B5 29    LDA $29B5,x
	# 03:8C3F F0 07       BEQ $8C48 (+7 B)
	# 03:8C41 C9 FF       CMP #$FF
	# 03:8C43 F0 21       BEQ $8C66 (+33 B)
	# 03:8C45 20 99 8E    JSR $8E99
	# 03:8C48 E6 C7       INC $C7
	# 03:8C4A 80 EE       BRA $8C3A (-18 B)
	# 03:8C4C 7B          TDC   If Encounter B0b0-1 = 3, kill all enemies that are from
	# 03:8C4D AA          TAX   group inDEX #$02
	# 03:8C4E 86 C7       STX $C7
	# 03:8C50 A6 C7       LDX $C7
	# 03:8C52 BD B5 29    LDA $29B5,x
	# 03:8C55 C9 FF       CMP #$FF
	# 03:8C57 F0 0D       BEQ $8C66 (+13 B)
	# 03:8C59 C9 02       CMP #$02
	# 03:8C5B D0 05       BNE $8C62 (+5 B)
	# 03:8C5D F0 00       BEQ $8C5F (+0 B)
	# 03:8C5F 20 99 8E    JSR $8E99
	# 03:8C62 E6 C7       INC $C7
	# 03:8C64 80 EA       BRA $8C50 (-22 B)

	# 03:8C66 AD 81 35    LDA $3581  If Boss Death, puts #$80 in $38D8
	# 03:8C69 29 08       AND #$08
	# 03:8C6B F0 08       BEQ $8C75 (+8 B)
	# 03:8C6D A9 80       LDA #$80
	# 03:8C6F 8D D8 38    STA $38D8
	# 03:8C72 4C 44 8D    JMP $8D44
	# 03:8C75 AD 82 35    LDA $3582
	# 03:8C78 D0 F8       BNE $8C72 (-8 B)
	# 03:8C7A AD E5 38    LDA $38E5
	# 03:8C7D 29 01       AND #$01
	# 03:8C7F D0 F1       BNE $8C72 (-15 B)
	# 03:8C81 AD EF 38    LDA $38EF
	# 03:8C84 D0 EC       BNE $8C72 (-20 B)

	# 03:8C86 7B          TDC   If not boss death, no enemy boss, can run away,AND
	# 03:8C87 AA          TAX   enemies take their attack from attack bank 1 (?),
	# 03:8C88 A8          TAY   - Calculates the average levels of active characters
	# 03:8C89 86 A9       STX $A9  - Calculates the average levels of active enemies
	# 03:8C8B 86 AB       STX $AB  - Does some rANDomizations between these values
	# 03:8C8D 86 AD       STX $AD
	# 03:8C8F B9 40 35    LDA $3540,y
	# 03:8C92 D0 10       BNE $8CA4 (+16 B)
	# 03:8C94 18          CLC
	# 03:8C95 BD 02 20    LDA $2002,x
	# 03:8C98 65 A9       ADC $A9
	# 03:8C9A 85 A9       STA $A9
	# 03:8C9C A9 00       LDA #$00
	# 03:8C9E 65 AA       ADC $AA
	# 03:8CA0 85 AA       STA $AA
	# 03:8CA2 E6 AD       INC $AD
	# 03:8CA4 20 BC 85    JSR $85BC
	# 03:8CA7 C8          INY
	# 03:8CA8 C0 05 00    CPY #$0005
	# 03:8CAB D0 E2       BNE $8C8F (-30 B)
	# 03:8CAD B9 40 35    LDA $3540,y
	# 03:8CB0 D0 10       BNE $8CC2 (+16 B)
	# 03:8CB2 18          CLC
	# 03:8CB3 BD 02 20    LDA $2002,x
	# 03:8CB6 65 AB       ADC $AB
	# 03:8CB8 85 AB       STA $AB
	# 03:8CBA A9 00       LDA #$00
	# 03:8CBC 65 AC       ADC $AC
	# 03:8CBE 85 AC       STA $AC
	# 03:8CC0 E6 AE       INC $AE
	# 03:8CC2 20 BC 85    JSR $85BC
	# 03:8CC5 C8          INY
	# 03:8CC6 C0 08 00    CPY #$0008
	# 03:8CC9 D0 E2       BNE $8CAD (-30 B)
	# 03:8CCB A6 A9       LDX $A9
	# 03:8CCD 8E 45 39    STX $3945
	# 03:8CD0 A5 AD       LDA $AD
	# 03:8CD2 AA          TAX
	# 03:8CD3 8E 47 39    STX $3947
	# 03:8CD6 20 07 84    JSR $8407
	# 03:8CD9 AE 49 39    LDX $3949
	# 03:8CDC 8A          TXA
	# 03:8CDD 8D D4 38    STA $38D4
	# 03:8CE0 7B          TDC
	# 03:8CE1 A6 AB       LDX $AB
	# 03:8CE3 8E 45 39    STX $3945
	# 03:8CE6 A5 AE       LDA $AE
	# 03:8CE8 AA          TAX
	# 03:8CE9 8E 47 39    STX $3947
	# 03:8CEC 20 07 84    JSR $8407
	# 03:8CEF AE 49 39    LDX $3949
	# 03:8CF2 8A          TXA
	# 03:8CF3 8D D5 38    STA $38D5
	# 03:8CF6 7B          TDC
	# 03:8CF7 7B          TDC
	# 03:8CF8 AA          TAX
	# 03:8CF9 86 A9       STX $A9
	# 03:8CFB E6 A9       INC $A9
	# 03:8CFD E6 AA       INC $AA
	# 03:8CFF A9 63       LDA #$63
	# 03:8D01 20 79 83    JSR $8379  Returns a value between 0AND 99
	# 03:8D04 CD D4 38    CMP $38D4
	# 03:8D07 B0 02       BCS $8D0B (+2 B)
	# 03:8D09 E6 A9       INC $A9
	# 03:8D0B 20 8B 85    JSR $858B  Returns a value between 0AND 98
	# 03:8D0E CD D5 38    CMP $38D5
	# 03:8D11 B0 02       BCS $8D15 (+2 B)
	# 03:8D13 E6 AA       INC $AA
	# 03:8D15 A5 A9       LDA $A9
	# 03:8D17 C5 AA       CMP $AA
	# 03:8D19 F0 29       BEQ $8D44 (+41 B)
	# 03:8D1B 90 08       BCC $8D25 (+8 B)
	# 03:8D1D EE D7 38    INC $38D7
	# 03:8D20 EE D8 38    INC $38D8
	# 03:8D23 80 1F       BRA $8D44 (+31 B)
	# 03:8D25 A9 80       LDA #$80
	# 03:8D27 8D D7 38    STA $38D7
	# 03:8D2A 8D D8 38    STA $38D8
	# 03:8D2D AD D5 38    LDA $38D5
	# 03:8D30 4A          LSR a
	# 03:8D31 85 A9       STA $A9
	# 03:8D33 7B          TDC
	# 03:8D34 AA          TAX
	# 03:8D35 AD D4 38    LDA $38D4
	# 03:8D38 20 79 83    JSR $8379
	# 03:8D3B C5 A9       CMP $A9
	# 03:8D3D B0 05       BCS $8D44 (+5 B)
	# 03:8D3F A9 08       LDA #$08
	# 03:8D41 8D 81 35    STA $3581

	# 03:8D44 7B          TDC   Locates Cecil's position in the party, or selects the
	# 03:8D45 AA          TAX   first character if Cecil is not present
	# 03:8D46 A8          TAY
	# 03:8D47 86 C7       STX $C7
	# 03:8D49 B9 40 35    LDA $3540,y
	# 03:8D4C D0 11       BNE $8D5F (+17 B)
	# 03:8D4E A5 C7       LDA $C7
	# 03:8D50 D0 28       BNE $8D7A (+40 B)
	# 03:8D52 BD 00 20    LDA $2000,x
	# 03:8D55 29 1F       AND #$1F
	# 03:8D57 C9 01       CMP #$01
	# 03:8D59 F0 1F       BEQ $8D7A (+31 B)
	# 03:8D5B C9 0B       CMP #$0B
	# 03:8D5D F0 18       BEQ $8D77 (+24 B)
	# 03:8D5F C2 20       REP #$20
	# 03:8D61 8A          TXA
	# 03:8D62 18          CLC
	# 03:8D63 69 80 00    ADC #$0080
	# 03:8D66 AA          TAX
	# 03:8D67 7B          TDC
	# 03:8D68 E2 20       SEP #$20
	# 03:8D6A C8          INY
	# 03:8D6B C0 05 00    CPY #$0005
	# 03:8D6E D0 D9       BNE $8D49 (-39 B)
	# 03:8D70 E6 C7       INC $C7
	# 03:8D72 7B          TDC
	# 03:8D73 AA          TAX
	# 03:8D74 A8          TAY
	# 03:8D75 80 D2       BRA $8D49 (-46 B)
	# 03:8D77 8C 5E 35    STY $355E  If Paladin Cecil present, puts the position in $355E

	# 03:8D7A BD 15 20    LDA $2015,x  Multiplies Cecil's agility by 50, then divides that
	# 03:8D7D AA          TAX   number by (Agility*10) for every charAND enemy. Puts the
	# 03:8D7E 8E 3D 39    STX $393D  result in theSTAts
	# 03:8D81 A2 32 00    LDX #$0032
	# 03:8D84 8E 3F 39    STX $393F
	# 03:8D87 20 B9 83    JSR $83B9
	# 03:8D8A AE 41 39    LDX $3941
	# 03:8D8D 86 CB       STX $CB
	# 03:8D8F 7B          TDC
	# 03:8D90 AA          TAX
	# 03:8D91 86 C7       STX $C7
	# 03:8D93 86 C9       STX $C9
	# 03:8D95 A6 C9       LDX $C9
	# 03:8D97 BD 40 35    LDA $3540,x
	# 03:8D9A D0 31       BNE $8DCD (+49 B)
	# 03:8D9C A6 C7       LDX $C7
	# 03:8D9E BD 15 20    LDA $2015,x
	# 03:8DA1 85 DF       STA $DF
	# 03:8DA3 A9 0A       LDA #$0A
	# 03:8DA5 85 E1       STA $E1
	# 03:8DA7 20 E0 83    JSR $83E0
	# 03:8DAA A6 CB       LDX $CB
	# 03:8DAC 8E 45 39    STX $3945
	# 03:8DAF A6 E3       LDX $E3
	# 03:8DB1 8E 47 39    STX $3947
	# 03:8DB4 20 07 84    JSR $8407
	# 03:8DB7 AE 49 39    LDX $3949
	# 03:8DBA D0 03       BNE $8DBF (+3 B)
	# 03:8DBC EE 49 39    INC $3949
	# 03:8DBF A6 C7       LDX $C7
	# 03:8DC1 AD 49 39    LDA $3949
	# 03:8DC4 9D 60 20    STA $2060,x
	# 03:8DC7 AD 4A 39    LDA $394A
	# 03:8DCA 9D 61 20    STA $2061,x
	# 03:8DCD C2 20       REP #$20
	# 03:8DCF 18          CLC
	# 03:8DD0 A5 C7       LDA $C7
	# 03:8DD2 69 80 00    ADC #$0080
	# 03:8DD5 85 C7       STA $C7
	# 03:8DD7 7B          TDC
	# 03:8DD8 E2 20       SEP #$20
	# 03:8DDA E6 C9       INC $C9
	# 03:8DDC A5 C9       LDA $C9
	# 03:8DDE C9 0D       CMP #$0D
	# 03:8DE0 D0 B3       BNE $8D95 (-77 B)

	# 03:8DE2 7B          TDC
	# 03:8DE3 AA          TAX
	# 03:8DE4 86 C7       STX $C7
	# 03:8DE6 A6 C7       LDX $C7
	# 03:8DE8 BD 40 35    LDA $3540,x
	# 03:8DEB D0 66       BNE $8E53 (+102 B)
	# 03:8DED 8A          TXA
	# 03:8DEE 85 DF       STA $DF
	# 03:8DF0 A9 80       LDA #$80
	# 03:8DF2 85 E1       STA $E1
	# 03:8DF4 20 E0 83    JSR $83E0
	# 03:8DF7 A6 E3       LDX $E3
	# 03:8DF9 BD 03 20    LDA $2003,x
	# 03:8DFC 30 55       BMI $8E53 (+85 B)
	# 03:8DFE A5 C7       LDA $C7
	# 03:8E00 C9 05       CMP #$05
	# 03:8E02 B0 19       BCS $8E1D (+25 B)
	# 03:8E04 AD 81 35    LDA $3581  if EnemyAvgLvl/2 < rnd(0, CharAvgLvl), switch all chars
	# 03:8E07 29 08       AND #$08  back rows
	# 03:8E09 F0 12       BEQ $8E1D (+18 B)
	# 03:8E0B BD 01 20    LDA $2001,x
	# 03:8E0E 48          PHA
	# 03:8E0F 29 7F       AND #$7F
	# 03:8E11 85 A9       STA $A9
	# 03:8E13 68          PLA
	# 03:8E14 29 80       AND #$80
	# 03:8E16 49 80       EOR #$80
	# 03:8E18 05 A9       ORA $A9
	# 03:8E1A 9D 01 20    STA $2001,x

	# 03:8E1D 64 D6       STZ $D6
	# 03:8E1F A6 C7       LDX $C7
	# 03:8E21 8A          TXA
	# 03:8E22 20 2C 9E    JSR $9E2C  $D4 = (speed * relSpeed) \ #$10, min 1
	# 03:8E25 AD D8 38    LDA $38D8
	# 03:8E28 F0 24       BEQ $8E4E (+36 B)
	# 03:8E2A 30 16       BMI $8E42 (+22 B)
	# 03:8E2C A5 C7       LDA $C7  If won the level contest, $D4 *= 2, min 2
	# 03:8E2E C9 05       CMP #$05
	# 03:8E30 90 16       BCC $8E48 (+22 B)
	# 03:8E32 06 D4       ASL $D4
	# 03:8E34 26 D5       ROL $D5
	# 03:8E36 A5 D4       LDA $D4
	# 03:8E38 05 D5       ORA $D5
	# 03:8E3A D0 12       BNE $8E4E (+18 B)
	# 03:8E3C 1A          INC a
	# 03:8E3D 1A          INC a
	# 03:8E3E 85 D4       STA $D4
	# 03:8E40 80 0C       BRA $8E4E (+12 B)
	# 03:8E42 A5 C7       LDA $C7
	# 03:8E44 C9 05       CMP #$05
	# 03:8E46 90 EA       BCC $8E32 (-22 B)
	# 03:8E48 A9 01       LDA #$01  If didn't win the level contest, $D4 = 1
	# 03:8E4A 85 D4       STA $D4
	# 03:8E4C 64 D5       STZ $D5
	# 03:8E4E A9 03       LDA #$03
	# 03:8E50 20 C8 85    JSR $85C8  Puts $D4 in the $2A04-2B15 range
	# 03:8E53 E6 C7       INC $C7
	# 03:8E55 A5 C7       LDA $C7
	# 03:8E57 C9 0D       CMP #$0D
	# 03:8E59 D0 8B       BNE $8DE6 (-117 B)

	# 03:8E5B C2 20       REP #$20  Normalize the charAND enemy speeds so that the slowest
	# 03:8E5D A9 FF FF    LDA #$FFFF  has a speed of 1
	# 03:8E60 85 A9       STA $A9
	# 03:8E62 A0 0D 00    LDY #$000D
	# 03:8E65 7B          TDC
	# 03:8E66 AA          TAX
	# 03:8E67 BD 07 2A    LDA $2A07,x
	# 03:8E6A F0 06       BEQ $8E72 (+6 B)
	# 03:8E6C C5 A9       CMP $A9
	# 03:8E6E B0 02       BCS $8E72 (+2 B)
	# 03:8E70 85 A9       STA $A9
	# 03:8E72 8A          TXA
	# 03:8E73 18          CLC
	# 03:8E74 69 15 00    ADC #$0015
	# 03:8E77 AA          TAX
	# 03:8E78 88          DEY
	# 03:8E79 D0 EC       BNE $8E67 (-20 B)
	# 03:8E7B C6 A9       DEC $A9
	# 03:8E7D BB          TYX
	# 03:8E7E 38          SEC
	# 03:8E7F BD 07 2A    LDA $2A07,x
	# 03:8E82 F0 05       BEQ $8E89 (+5 B)
	# 03:8E84 E5 A9       SBC $A9
	# 03:8E86 9D 07 2A    STA $2A07,x
	# 03:8E89 8A          TXA
	# 03:8E8A 18          CLC
	# 03:8E8B 69 15 00    ADC #$0015
	# 03:8E8E AA          TAX
	# 03:8E8F C8          INY
	# 03:8E90 C0 0D 00    CPY #$000D
	# 03:8E93 D0 E9       BNE $8E7E (-23 B)
	# 03:8E95 7B          TDC
	# 03:8E96 E2 20       SEP #$20
	# 03:8E98 60          RTS
end



# Kills enemy inDEX X
#
# Inputs: X
# Outputs: X?
#
# Called from:
#   $8C2D
#   $8C45
#   $8C5F
def _8e99(x)
	# 03:8E99 8A          TXA
	# 03:8E9A 18          CLC
	# 03:8E9B 69 05       ADC #$05
	# 03:8E9D 85 8A       STA $8A
	mem8[0x8A] = x + 5

	# 03:8E9F BD B5 29    LDA $29B5,x
	# 03:8EA2 85 A9       STA $A9
	mem8[0xA9] = mem8[0x29B5 + x]

	# 03:8EA4 A9 FF       LDA #$FF
	# 03:8EA6 9D B5 29    STA $29B5,x
	mem8[0x29B5 + x] = 0xFF

	# 03:8EA9 A5 A9       LDA $A9
	# 03:8EAB AA          TAX
	x = mem8[0xA9]

	# 03:8EAC DE CA 29DEC $29CA,x
	# 03:8EAF CE CD 29DEC $29CD
	mem8[0x29CA + x] -= 1
	mem8[0x29CD] -= 1

	# 03:8EB2 A5 8A       LDA $8A
	# 03:8EB4 85 DF       STA $DF
	mem8[0xDF] = mem8[0x8A]

	# 03:8EB6 A9 80       LDA #$80
	# 03:8EB8 85 E1       STA $E1
	mem8[0xE1] = 0x80

	# 03:8EBA 20 E0 83    JSR $83E0
	_83e0

	# 03:8EBD A6 E3       LDX $E3
	# 03:8EBF A9 80       LDA #$80
	# 03:8EC1 9D 03 20    STA $2003,x
	x = mem16[0xE3]
	mem8[0x2003 + x] = mem8[0x80]

	# 03:8EC4 A5 8A       LDA $8A
	# 03:8EC6 0A          ASL a
	# 03:8EC7 AA          TAX
	# 03:8EC8 9E EB 29    STZ $29EB,x
	x = mem8[0x8A] * 2
	mem8[0x29EB + x] = 0

	# 03:8ECB 60          RTS
	return
end

# Copies all chars' items data.
# Called from:
#   $89AF
#   $980B
#   $A5C2
#   $A84F
def _8ecc
	# 03:8ECC A0 30 20    LDY #$2030
	# 03:8ECF 84 86       STY $86
	mem16[0x86] = 0x2030

	# 03:8ED1 A0 80 27    LDY #$2780
	# 03:8ED4 84 84       STY $84
	mem16[0x84] = 0x2780

	# 03:8ED6 64 A9       STZ $A9
	mem8[0xA9] = 0

	(0..4).each do |k|
		# mem16[0xA9] = current char inDEX

		# 03:8ED8 7B          TDC
		# 03:8ED9 AA          TAX
		# 03:8EDA 86 AB       STX $AB
		# 03:8EDC 9B          TXY
		mem16[0xAB] = 0

		# Copies the armor equipments, with a default ox 0x60 (NoArmor).
		# 03:8EDD B1 86       LDA ($86),y
		# 03:8EDF D0 02       BNE $8EE3 (+2 B)
		# 03:8EE1 A9 60       LDA #$60
		# 03:8EE3 95 AD       STA $AD,x
		# 03:8EE5 C8          INY
		# 03:8EE6 E8          INX
		# 03:8EE7 E0 03 00    CPX #$0003
		# 03:8EEA D0 F1       BNE $8EDD (-15 B)
		(0..2).each do |i|
			a = mem8[mem16[0x86] + i]
			mem8[0xAD + i] = a == 0 ? 0x60 : a
		end

		# Copies the weapon equipments.
		# 03:8EEC B1 86       LDA ($86),y
		# 03:8EEE 95 AD       STA $AD,x
		# 03:8EF0 C8          INY
		# 03:8EF1 E8          INX
		# 03:8EF2 E0 07 00    CPX #$0007
		# 03:8EF5 D0 F5       BNE $8EEC (-11 B)
		(3..6).each do |i|
			mem8[0xAD + i] = mem8[mem16[0x86] + i]
		end

		# Copies the Item InDEXAND Item Count for RHANDAND LHAND
		# 03:8EF7 A5 A9       LDA $A9
		# 03:8EF9 20 7E 84    JSR $847E
		# 03:8EFC AA          TAX
		# 03:8EFD A5 B0       LDA $B0
		# 03:8EFF 9D DB 32    STA $32DB,x
		# 03:8F02 A5 B1       LDA $B1
		# 03:8F04 9D DC 32    STA $32DC,x
		# 03:8F07 A5 B2       LDA $B2
		# 03:8F09 9D DF 32    STA $32DF,x
		# 03:8F0C A5 B3       LDA $B3
		# 03:8F0E 9D E0 32    STA $32E0,x
		# 03:8F11 A5 B2       LDA $B2
		# 03:8F13 85 B1       STA $B1
		x = mem8[0xA9] * 8
		mem8[0x32DB + x] = mem8[0xB0]
		mem8[0x32DC + x] = mem8[0xB1]
		mem8[0x32DF + x] = mem8[0xB2]
		mem8[0x32E0 + x] = mem8[0xB3]
		mem8[0xB1] = mem8[0xB2]

		# 03:8F15 64 B7       STZ $B7
		# 03:8F17 64 B8       STZ $B8
		mem8[0xB7] = 0
		mem8[0xB8] = 0

		# If Bow in RHAND, set $B7.7
		# If Arrow in RHAND, set $B7.6
		# 03:8F19 A5 B0       LDA $B0
		# 03:8F1B F0 16       BEQ $8F33 (+22 B)
		# 03:8F1D C9 4D       CMP #$4D
		# 03:8F1F 90 12       BCC $8F33 (+18 B)
		# 03:8F21 C9 54       CMP #$54
		# 03:8F23 B0 06       BCS $8F2B (+6 B)
		# 03:8F25 A9 80       LDA #$80
		# 03:8F27 85 B7       STA $B7
		# 03:8F29 80 08       BRA $8F33 (+8 B)
		# 03:8F2B C9 61       CMP #$61
		# 03:8F2D B0 04       BCS $8F33 (+4 B)
		# 03:8F2F A9 40       LDA #$40
		# 03:8F31 85 B7       STA $B7
		case mem8[0xB0]
		when (0x4D..0x54)
			mem8[0xB7] = 0x80
		when (0x55..0x61)
			mem8[0xB7] = 0x40
		end

		# If Bow in LHAND, set $B8.7
		# If Arrow in LHAND, set $B8.6
		# 03:8F33 A5 B1       LDA $B1
		# 03:8F35 F0 16       BEQ $8F4D (+22 B)
		# 03:8F37 C9 4D       CMP #$4D
		# 03:8F39 90 12       BCC $8F4D (+18 B)
		# 03:8F3B C9 54       CMP #$54
		# 03:8F3D B0 06       BCS $8F45 (+6 B)
		# 03:8F3F A9 80       LDA #$80
		# 03:8F41 85 B8       STA $B8
		# 03:8F43 80 08       BRA $8F4D (+8 B)
		# 03:8F45 C9 61       CMP #$61
		# 03:8F47 B0 04       BCS $8F4D (+4 B)
		# 03:8F49 A9 40       LDA #$40
		# 03:8F4B 85 B8       STA $B8
		case mem8[0xB1]
		when (0x4D..0x54)
			mem8[0xB8] = 0x80
		when (0x55..0x61)
			mem8[0xB8] = 0x40
		end

		# If only has a bow *OR* arrow, not good. Sets both to 0
		# 03:8F4D A5 B7       LDA $B7
		# 03:8F4F 05 B8       ORA $B8
		# 03:8F51 F0 08       BEQ $8F5B (+8 B)
		# 03:8F53 49 C0       EOR #$C0
		# 03:8F55 F0 04       BEQ $8F5B (+4 B)
		# 03:8F57 64 B0       STZ $B0
		# 03:8F59 64 B1       STZ $B1
		if (mem8[0xB7] | mem8[0xB8]) ^ 0xC0 != 0
			mem8[0xB0] = 0
			mem8[0xB1] = 1
		end

		(0..4).each do |j|
			# mem16[0xAB] = current item inDEX

			# Copies the current item's data to $289C-...
			# 03:8F5B A6 AB       LDX $AB
			# 03:8F5D B5 AD       LDA $AD,x
			# 03:8F5F AA          TAX
			# 03:8F60 86 E5       STX $E5
			# 03:8F62 A2 00 91    LDX #$9100
			# 03:8F65 86 80       STX $80
			# 03:8F67 A9 0F       LDA #$0F
			# 03:8F69 85 82       STA $82
			# 03:8F6B A9 08       LDA #$08
			# 03:8F6D 20 5E 84    JSR $845E
			mem16[0xE5] = mem8[0xAD + mem16[0xAB]]
			mem16[0x80] = 0x9100
			mem8[0x82] = 0x0F
			_845e(0x80)

			# Copies the item's data to $2780+...
			# 03:8F70 7B          TDC
			# 03:8F71 AA          TAX
			# 03:8F72 A8          TAY
			# 03:8F73 BD 9C 28    LDA $289C,x
			# 03:8F76 91 84       STA ($84),y
			# 03:8F78 C8          INY
			# 03:8F79 E8          INX
			# 03:8F7A E0 08 00    CPX #$0008
			# 03:8F7D D0 F4       BNE $8F73 (-12 B)
			(0..7).each do |i|
				mem8[mem16[0x84] + i] = mem8[0x289C + i]
			end

			# Appends the Elem/STAtus Bytes
			# 03:8F7F AD A0 28    LDA $28A0
			# 03:8F82 29 7F       AND #$7F
			# 03:8F84 85 DF       STA $DF
			# 03:8F86 A9 03       LDA #$03
			# 03:8F88 85 E1       STA $E1
			# 03:8F8A 20 E0 83    JSR $83E0
			# 03:8F8D A6 E3       LDX $E3
			# 03:8F8F BF 90 A5 0F LDA $0FA590,x
			# 03:8F93 91 84       STA ($84),y
			# 03:8F95 C8          INY
			# 03:8F96 E8          INX
			# 03:8F97 C0 0B 00    CPY #$000B
			# 03:8F9A D0 F3       BNE $8F8F (-13 B)
			x = (mem8[0x28A0] & 0x7F) * 3
			(0x08..0x0A).each do |y|
				mem8[mem16[0x84] + y] = mem8[0x0FA590 + x]
			end

			# Adjusts the offsets for the next equipped item
			# 03:8F9C 18          CLC
			# 03:8F9D A5 84       LDA $84
			# 03:8F9F 69 0B       ADC #$0B
			# 03:8FA1 85 84       STA $84
			# 03:8FA3 A5 85       LDA $85
			# 03:8FA5 69 00       ADC #$00
			# 03:8FA7 85 85       STA $85
			mem16[0x84] += 0x0B

			# 03:8FA9 E6 AB       INC $AB
			# 03:8FAB A5 AB       LDA $AB
			# 03:8FAD C9 05       CMP #$05
			# 03:8FAF D0 AA       BNE $8F5B (-86 B)
		end


		# Adjusts the offsets for the next character
		# 03:8FB1 18          CLC
		# 03:8FB2 A5 86       LDA $86
		# 03:8FB4 69 80       ADC #$80
		# 03:8FB6 85 86       STA $86
		# 03:8FB8 A5 87       LDA $87
		# 03:8FBA 69 00       ADC #$00
		# 03:8FBC 85 87       STA $87
		mem16[0x86] += 0x80

		# 03:8FBE E6 A9       INC $A9
		# 03:8FC0 A5 A9       LDA $A9
		# 03:8FC2 C9 05       CMP #$05
		# 03:8FC4 F0 03       BEQ $8FC9 (+3 B)
		# 03:8FC6 4C D8 8E    JMP $8ED8
	end

	# 03:8FC9 60          RTS
	return
end

# Sets the enemy as active
def _8fca
	# 03:8FCA A5 B9       LDA $B9
	# 03:8FCC 8D A0 35    STA $35A0
	# 03:8FCF 0A          ASL a
	# 03:8FD0 18          CLC
	# 03:8FD1 69 0A       ADC #$0A
	# 03:8FD3 AA          TAX
	# 03:8FD4 A9 40       LDA #$40
	# 03:8FD6 9D EB 29    STA $29EB,x
	a = mem8[0xB9]
	mem8[0x35A0] = a

	mem8[0x29EB + (a * 2 + 0x0A)] = 0x40
end


# Calculates a lot of stuff related to enemies
# Called from: ?
def _8fd9
	# Calculates the current enemy'sSTAts offset
	# 03:8FD9 C2 20       REP #$20
	# 03:8FDB A5 BB       LDA $BB
	# 03:8FDD 0A          ASL a
	# 03:8FDE AA          TAX
	# 03:8FDF 38          SEC
	# 03:8FE0 BF A0 A6 0E LDA $0EA6A0,x  [Enemy Data Pointers]
	# 03:8FE4 E9 60 A8    SBC #$A860
	# 03:8FE7 AA          TAX
	# 03:8FE8 7B          TDC
	# 03:8FE9 E2 20       SEP #$20
	x = mem16[0x0EA6A0 + mem16[0xBB] * 2] - 0xA860

	# 03:8FEB 7B          TDC
	# 03:8FEC A8          TAY

	# Copies the 20 Permanent Bytes from the Enemy Data
	(0..0x13).each do |i|
		# 03:8FED BF 60 A8 0E LDA $0EA860,x  [Enemy Data]
		# 03:8FF1 99 9C 28    STA $289C,y
		# 03:8FF4 E8          INX      to $289C-28AF
		# 03:8FF5 C8          INY
		# 03:8FF6 C0 14 00    CPY #$0014
		# 03:8FF9 D0 F2       BNE $8FED (-14 B)
		mem8[0x289C + i] = mem8[0x0EA860 + x + i]
	end

	# Recalculates common offsets for current enemy inDEX
	# 03:8FFB 18          CLC
	# 03:8FFC AD A0 35    LDA $35A0
	# 03:8FFF 69 05       ADC #$05
	# 03:9001 20 89 84    JSR $8489
	# 03:9004 A6 A6       LDX $A6
	_8489(mem8[0x35A0] + 0x05)
	x = mem16[0xA6] # (mem8[0x35A0] + 0x05) * 0x80

	# If current enemy is an egg, sets the condition,AND puts #$01 in $38D0,x
	# 03:9006 A5 BD       LDA $BD
	# 03:9008 F0 14       BEQ $901E (+20 B)
	if mem8[0xBD] != 0
		# 03:900A A9 20       LDA #$20
		# 03:900C 9D 05 20    STA $2005,x
		# 03:900F DA          PHX
		# 03:9010 AD A0 35    LDA $35A0
		# 03:9013 AA          TAX
		# 03:9014 BD B5 29    LDA $29B5,x
		# 03:9017 AA          TAX
		# 03:9018 A9 01       LDA #$01
		# 03:901A 9D D0 38    STA $38D0,x
		# 03:901D FA          PLX
		mem8[0x2005 + x] = 0x20
		mem8[0x38D0 + mem8[0x29B5 + mem8[0x35A0]]] = 0x01
	end

	# Enemy level-related.
	# 03:901E AD 9C 28    LDA $289C
	# 03:9021 9D 70 20    STA $2070,x
	# 03:9024 10 03       BPL $9029 (+3 B)
	# 03:9026 EE 82 35    INC $3582
	# 03:9029 29 7F       AND #$7F
	# 03:902B 9D 02 20    STA $2002,x
	# 03:902E 18          CLC
	# 03:902F 69 0A       ADC #$0A
	# 03:9031 9D 2F 20    STA $202F,x
	a = mem8[0x289C]
	mem8[0x2070 + x] = a

	if a >= 128 # if enemy is a boss
		mem8[0x3582] += 1
	end

	mem8[0x2002 + x] = a & 0x7F # enemy level
	mem8[0x202F + x] = a + 0x0A # enemy speed

	# Puts Enemy’s HP to $2007AND $2009 (CurAND Max)
	# 03:9034 AD 9D 28    LDA $289D
	# 03:9037 9D 07 20    STA $2007,x
	# 03:903A 9D 09 20    STA $2009,x
	# 03:903D AD 9E 28    LDA $289E
	# 03:9040 9D 08 20    STA $2008,x
	# 03:9043 9D 0A 20    STA $200A,x
	a = mem8[0x289D]
	mem8[0x2007 + x] = a
	mem8[0x2009 + x] = a
	a = mem8[0x289E]
	mem8[0x2008 + x] = a
	mem8[0x200A + x] = a

	# Puts Enemy’s HP / 16 to $200BAND $200D
	# 03:9046 C2 20       REP #$20
	# 03:9048 AD 9D 28    LDA $289D
	# 03:904B 20 84 84    JSR $8484
	# 03:904E 9D 0B 20    STA $200B,x
	# 03:9051 9D 0D 20    STA $200D,x
	# 03:9054 7B          TDC
	# 03:9055 E2 20       SEP #$20
	a = mem16[0x289D] / 16
	mem16[0x200B + x] = a
	mem16[0x200D + x] = a

	# RANDomizes the Enemy’s Agility between the UpperAND Lower Limits of the Agility Range
	# 03:9057 DA          PHX
	# 03:9058 AD A2 28    LDA $28A2
	# 03:905B 29 3F       AND #$3F
	# 03:905D 0A          ASL a
	# 03:905E AA          TAX
	# 03:905F BF 21 A6 0E LDA $0EA621,x  [Enemy Agility Range Table – Upper Limit]
	# 03:9063 85 A9       STA $A9
	# 03:9065 BF 20 A6 0E LDA $0EA620,x  [Enemy Agility Range Table – Lower Limit]
	# 03:9069 AA          TAX
	# 03:906A A5 A9       LDA $A9
	# 03:906C 20 79 83    JSR $8379
	# 03:906F FA          PLX
	# 03:9070 9D 15 20    STA $2015,x
	x2 = (mem8[0x28A2] & 0x3F) * 2
	mem8[0x2015 + x] = (mem8[0x0EA620 + x2]..mem8[0x0EA621 + x2]).to_a.sample

	# If Auto-Battle, Enemy's speed = 1, else = 0x10
	# 03:9073 AD A2 29    LDA $29A2
	# 03:9076 29 20       AND #$20
	# 03:9078 F0 07       BEQ $9081 (+7 B)
	if mem8[0x29A2] & 0x20 != mem8[0x9081]
		# 03:907A A9 01       LDA #$01
		# 03:907C 9D 3B 20    STA $203B,x
		# 03:907F 80 05       BRA $9086 (+5 B)
		mem8[0x203B + x] = 0x01
	else
		# 03:9081 A9 10       LDA #$10
		# 03:9083 9D 3B 20    STA $203B,x
		mem8[0x203B + x] = 0x10
	end

	# 03:9086 AD A4 28    LDA $28A4
	# 03:9089 8D A1 35    STA $35A1
	mem8[0x35A1] = mem8[0x28A4]

	# Puts the Attack Seq. for current group inDEX in $3588-358AAND $358B-358D
	# Enemy Items Dropped InDEX/Freq in $358E-3590AND on current enemy'sSTAts
	# 03:908C DA          PHX
	# 03:908D A5 B9       LDA $B9
	# 03:908F AA          TAX
	# 03:9090 BD B5 29    LDA $29B5,x
	# 03:9093 AA          TAX
	# 03:9094 A5 BB       LDA $BB
	# 03:9096 9D 8B 35    STA $358B,x
	# 03:9099 9D 88 35    STA $3588,x
	# 03:909C AD A3 28    LDA $28A3
	# 03:909F 9D 8E 35    STA $358E,x
	# 03:90A2 FA          PLX
	# 03:90A3 9D 73 20    STA $2073,x
	x2 = mem8[0x29B5 + mem8[0xB9]]
	a = mem8[0xBB]
	mem[0x358B + x2] = a
	mem[0x3588 + x2] = a
	a = mem8[0x28A3]
	mem8[0x358E + x2] = a
	mem8[0x2073 + x] = a

	# Copies the Enemy'sPHYsical Offense to itsSTAts
	# 03:90A6 AD 9F 28    LDA $289F
	# 03:90A9 20 89 94    JSR $9489
	# 03:90AC AD 1C 29    LDA $291C
	# 03:90AF 9D 1B 20    STA $201B,x
	# 03:90B2 AD 1D 29    LDA $291D
	# 03:90B5 9D 1C 20    STA $201C,x
	# 03:90B8 AD 1E 29    LDA $291E
	# 03:90BB 9D 1D 20    STA $201D,x
	offset = mem8[0x289F] * 0x03
	mem8[0x201B + x] = mem8[0x0EA380 + offset]
	mem8[0x201C + x] = mem8[0x0EA380 + offset + 1]
	mem8[0x201D + x] = mem8[0x0EA380 + offset + 2]

	# Copies the Enemy'sPHYsical Defense to itsSTAts
	# 03:90BE AD A0 28    LDA $28A0
	# 03:90C1 20 89 94    JSR $9489
	# 03:90C4 AD 1C 29    LDA $291C
	# 03:90C7 9D 28 20    STA $2028,x
	# 03:90CA AD 1D 29    LDA $291D
	# 03:90CD 9D 2A 20    STA $202A,x
	# 03:90D0 AD 1E 29    LDA $291E
	# 03:90D3 9D 2A 20    STA $202A,x
	offset = mem8[0x28A0] * 0x03
	mem8[0x2028 + x] = mem8[0x0EA380 + offset]
	mem8[0x202A + x] = mem8[0x0EA380 + offset + 1] # Why not 0x2029? ErROR?
	mem8[0x202A + x] = mem8[0x0EA380 + offset + 2]

	# Copies the Enemy's Magical Defense to itsSTAts
	# 03:90D6 AD A1 28    LDA $28A1
	# 03:90D9 20 89 94    JSR $9489
	# 03:90DC AD 1C 29    LDA $291C
	# 03:90DF 9D 22 20    STA $2022,x
	# 03:90E2 AD 1D 29    LDA $291D
	# 03:90E5 9D 24 20    STA $2024,x
	# 03:90E8 AD 1E 29    LDA $291E
	# 03:90EB 9D 24 20    STA $2024,x
	offset = mem8[0x28A1] * 0x03
	mem8[0x2022 + x] = mem8[0x0EA380 + offset]
	mem8[0x2024 + x] = mem8[0x0EA380 + offset + 1] # Why not 0x2023? ErROR?
	mem8[0x2024 + x] = mem8[0x0EA380 + offset + 2]

	# Enemy OptionalBITs
	# 03:90EE AD A5 28    LDA $28A5
	# 03:90F1 D0 03       BNE $90F6 (+3 B)
	# 03:90F3 4C E5 92    JMP $92E5
	if mem8[0x28A5] != mem8[0x90F6]
		# 03:90F6 7B          TDC
		# 03:90F7 A8          TAY
		y = 0

		# Elemental/STAtus Offense
		# 03:90F8 AD A5 28    LDA $28A5
		# 03:90FB 29 80       AND #$80
		# 03:90FD F0 15       BEQ $9114 (+21 B)
		if mem8[0x28A5] & 0x80 != 0
			# 03:90FF B9 A6 28    LDA $28A6,y
			# 03:9102 9D 19 20    STA $2019,x
			# 03:9105 C8          INY
			mem8[0x2019 + x] = mem8[0x28A6 + y]
			y += 1

			# 03:9106 B9 A6 28    LDA $28A6,y
			# 03:9109 9D 1E 20    STA $201E,x
			# 03:910C C8          INY
			mem8[0x201E + x] = mem8[0x28A6 + y]
			y += 1

			# 03:910D B9 A6 28    LDA $28A6,y
			# 03:9110 9D 1F 20    STA $201F,x
			# 03:9113 C8          INY
			mem8[0x201F + x] = mem8[0x28A6 + y]
			y += 1
		end

		# Elemental/STAtus Defense
		# 03:9114 AD A5 28    LDA $28A5
		# 03:9117 29 40       AND #$40
		# 03:9119 F0 1C       BEQ $9137 (+28 B)
		if mem8[0x28A5] & 0x40 != 0
			# 03:911B B9 A6 28    LDA $28A6,y
			# 03:911E 10 05       BPL $9125 (+5 B)
			a = mem8[0x28A6 + y]
			if a < 0
				# 03:9120 9D 26 20    STA $2026,x
				# 03:9123 80 03       BRA $9128 (+3 B)
				mem8[0x2026 + x] = a
			else
				# 03:9125 9D 25 20    STA $2025,x
				mem8[0x2025 + x] = a
			end

			# 03:9128 C8          INY
			y += 1

			# 03:9129 B9 A6 28    LDA $28A6,y
			# 03:912C 9D 2B 20    STA $202B,x
			# 03:912F C8          INY
			mem8[0x202B + x] = mem8[0x28A6 + y]
			y += 1

			# 03:9130 B9 A6 28    LDA $28A6,y
			# 03:9133 9D 2C 20    STA $202C,x
			# 03:9136 C8          INY
			mem8[0x202C + x] = mem8[0x28A6 + y]
			y += 1
		end

		# Elemental Weakness
		# 03:9137 AD A5 28    LDA $28A5
		# 03:913A 29 20       AND #$20
		# 03:913C F0 15       BEQ $9153 (+21 B)
		if mem8[0x28A5] & 0x20 != 0
			# 03:913E B9 A6 28    LDA $28A6,y
			# 03:9141 10 03       BPL $9146 (+3 B)
			a = mem8[0x28A6 + y]
			if a >= 128
				# 03:9143 9D 21 20    STA $2021,x  <-- why notSEParate elem very-weakness totally?
				mem8[0x2021 + x] = a
			end

			# 03:9146 9D 20 20    STA $2020,x
			mem8[0x2020 + x] = a

			# 03:9149 C8          INY
			y += 1

			# If Weak (or Very Weak) to Air, adds Float Condition (??)
			# 03:914A 29 20       AND #$20
			# 03:914C F0 05       BEQ $9153 (+5 B)
			if a & 0x20 != 0
				# 03:914E A9 40       LDA #$40
				# 03:9150 9D 04 20    STA $2004,x
				mem8[0x2004 + x] = 0x40
			end
		end

		# Wisdom/Will
		# 03:9153 AD A5 28    LDA $28A5
		# 03:9156 29 10       AND #$10
		# 03:9158 F0 10       BEQ $916A (+16 B)
		if mem8[0x28A5] & 0x10 != 0
			# 03:915A B9 A6 28    LDA $28A6,y
			# 03:915D 9D 12 20    STA $2012,x
			# 03:9160 9D 13 20    STA $2013,x
			# 03:9163 9D 17 20    STA $2017,x
			# 03:9166 9D 18 20    STA $2018,x
			# 03:9169 C8          INY
			a = mem8[0x28A6 + y]
			mem8[0x2012 + x] = a
			mem8[0x2013 + x] = a
			mem8[0x2017 + x] = a
			mem8[0x2018 + x] = a
			y += 1
		end

		# Creature Type
		# 03:916A AD A5 28    LDA $28A5
		# 03:916D 29 08       AND #$08
		# 03:916F F0 07       BEQ $9178 (+7 B)
		if mem8[0x28A5] & 0x08 != 0
			# 03:9171 B9 A6 28    LDA $28A6,y
			# 03:9174 9D 40 20    STA $2040,x
			# 03:9177 C8          INY
			mem8[0x2040 + x] = mem8[0x28A6 + y]
			y += 1
		end

		# Counter Sequence Group InDEX
		# 03:9178 AD A5 28    LDA $28A5
		# 03:917B 29 04       AND #$04
		# 03:917D D0 03       BNE $9182 (+3 B)
		# 03:917F 4C E5 92    JMP $92E5
		if mem8[0x28A5] & 0x04 != 0
			# 03:9182 AD A0 35    LDA $35A0
			# 03:9185 AA          TAX
			# 03:9186 FE AA 38    INC $38AA,x
			# 03:9189 B9 A6 28    LDA $28A6,y
			# 03:918C 8D A2 35    STA $35A2
			mem8[0x38AA + mem8[0x35A0]] += 1
			mem8[0x35A2] = mem8[0x28A6 + y]

			# 03:918F AD A0 35    LDA $35A0
			# 03:9192 85 DF       STA $DF
			# 03:9194 A9 14       LDA #$14
			# 03:9196 85 E1       STA $E1
			# 03:9198 20 E0 83    JSR $83E0
			# 03:919B AD A2 35    LDA $35A2
			# 03:919E 85 E5       STA $E5
			# 03:91A0 A0 30 E0    LDY #$E030
			# 03:91A3 A9 0E       LDA #$0E
			# 03:91A5 20 43 84    JSR $8443
			# 03:91A8 BB          TYX
			# 03:91A9 A4 E3       LDY $E3
			# 03:91AB 84 98       STY $98
			# 03:91AD 84 9C       STY $9C
			mem8[0xE5] = mem8[0x35A2]
			x = _8443(0x0E, 0xE030) # Calculates the offset for target Counter InDEX; $0EE030 => [Enemy Attack Seq Group Table]
			y = mem8[0x35A0] * 0x14
			mem16[0x98] = y
			mem16[0x9C] = y

			# Copies the Sequence Group until #$FF
			# 03:91AF BF 30 E0 0E LDA $0EE030,x [Enemy Attack Sequence Group Table] (Counter)
			# 03:91B3 99 1F 53    STA $531F,y
			# 03:91B6 E8          INX
			# 03:91B7 C8          INY
			# 03:91B8 C9 FF       CMP #$FF
			# 03:91BA D0 F3       BNE $91AF (-13 B)
			begin
				a = mem8[0x0EE030 + x]
				mem8[0x531F + y] = a
				x += 1
				y += 1
			end until a == 0xFF

			# Multiplies $enemy inDEX by #$28, to $9AAND $9E
			# 03:91BC AD A0 35    LDA $35A0
			# 03:91BF 85 DF       STA $DF
			# 03:91C1 A9 28       LDA #$28
			# 03:91C3 85 E1       STA $E1
			# 03:91C5 20 E0 83    JSR $83E0
			# 03:91C8 A4 E3       LDY $E3
			# 03:91CA 84 9A       STY $9A
			# 03:91CC 84 9E       STY $9E
			y = mem8[0x35A0] * 0x28
			mem16[0x9A] = y
			mem16[0x9E] = y

			loop do
				# 03:91CE A6 98       LDX $98
				# 03:91D0 BD 1F 53    LDA $531F,x
				# 03:91D3 C9 FF       CMP #$FF
				# 03:91D5 F0 35       BEQ $920C (+53 B)
				a = mem8[0x531F + mem16[0x98]]
				break if a == 0xFF

				# 03:91D7 85 E5       STA $E5
				mem[0xE5] = a

				# 03:91D9 A0 00 E6    LDY #$E600
				# 03:91DC A9 0E       LDA #$0E
				# 03:91DE 20 43 84    JSR $8443
				# 03:91E1 BB          TYX
				x = _8443(0x0E, 0xE600) # Calculates the offset for target Counter Cond Set InDEX; $0EE600 => [Enemy Attack Cond Set Table]

				# 03:91E2 A4 9A       LDY $9A
				# 03:91E4 A9 04       LDA #$04   Normalize the condition sets to 4B
				# 03:91E6 85 A9       STA $A9
				y = mem16[0x9A]
				mem8[0xA9] = 0x04

				begin
					# 03:91E8 BF 00 E6 0E LDA $0EE600,x  [Enemy Attack Condition Set Table] (Counter)
					# 03:91EC 99 BF 53    STA $53BF,y
					# 03:91EF C9 FF       CMP #$FF
					# 03:91F1 F0 01       BEQ $91F4 (+1 B)
					a = mem8[0x0EE600 + x]
					mem8[0x53BF + y] = a
					if a != 0xFF
						# 03:91F3 E8          INX
						x += 1
					end

					# 03:91F4 C8          INY
					# 03:91F5 C6 A9       DEC $A9
					y += 1
					mem8[0xA9] -= 1

					# 03:91F7 A5 A9       LDA $A9
					# 03:91F9 D0 ED       BNE $91E8 (-19 B)
				end until mem8[0xA9] == 0

				# 03:91FB 84 9A       STY $9A
				mem16[0x9A] = y

				# 03:91FD 18          CLC
				# 03:91FE A5 98       LDA $98
				# 03:9200 69 02       ADC #$02   Each Attack Seq Group has 2 Bytes
				# 03:9202 85 98       STA $98
				# 03:9204 A5 99       LDA $99
				# 03:9206 69 00       ADC #$00
				# 03:9208 85 99       STA $99
				mem16[0x98] += 2

				# 03:920A 80 C2       BRA $91CE (-62 B)
			end

			# 03:920C AD A0 35    LDA $35A0
			# 03:920F 85 DF       STA $DF
			# 03:9211 A9 A0       LDA #$A0
			# 03:9213 85 E1       STA $E1
			# 03:9215 20 E0 83    JSR $83E0
			# 03:9218 A4 E3       LDY $E3
			# 03:921A 84 9A       STY $9A
			mem16[0x9A] = mem8[0x35A0] * 0xA0

			# 03:921C A9 0A       LDA #$0A
			# 03:921E 85 AB       STA $AB
			(0..9).each do
				# 03:9220 A9 04       LDA #$04
				# 03:9222 85 A9       STA $A9
				(0..3).each do
					# 03:9224 A6 9E       LDX $9E
					# 03:9226 BD BF 53    LDA $53BF,x
					# 03:9229 C9 FF       CMP #$FF
					# 03:922B F0 20       BEQ $924D (+32 B)
					a = mem8[0x53BF + mem16[0x9E]]
					if a != 0xFF
						# 03:922D 85 DF       STA $DF
						# 03:922F A9 04       LDA #$04
						# 03:9231 85 E1       STA $E1
						# 03:9233 20 E0 83    JSR $83E0
						# 03:9236 A6 E3       LDX $E3
						x = a * 0x04

						# 03:9238 A4 9A       LDY $9A
						y = mem16[0x9A]

						# 03:923A A9 04       LDA #$04
						# 03:923C 85 AA       STA $AA
						# 03:923E BF 00 E7 0E LDA $0EE700,x  [Enemy Attack Condition Table] (Counter)
						# 03:9242 99 FF 54    STA $54FF,y
						# 03:9245 E8          INX
						# 03:9246 C8          INY
						# 03:9247 C6 AA       DEC $AA
						# 03:9249 A5 AA       LDA $AA
						# 03:924B D0 F1       BNE $923E (-15 B)
						(0..3).each do |i|
							mem8[0x54FF + y] = mem8[0x0EE700 + x]
							x += 1
							y += 1
						end
					end

					# 03:924D 18          CLC
					# 03:924E A5 9A       LDA $9A
					# 03:9250 69 04       ADC #$04
					# 03:9252 85 9A       STA $9A
					# 03:9254 A5 9B       LDA $9B
					# 03:9256 69 00       ADC #$00
					# 03:9258 85 9B       STA $9B
					mem16[0x9A] += 0x04

					# 03:925A 18          CLC
					# 03:925B A5 9E       LDA $9E
					# 03:925D 69 01       ADC #$01
					# 03:925F 85 9E       STA $9E
					# 03:9261 A5 9F       LDA $9F
					# 03:9263 69 00       ADC #$00
					# 03:9265 85 9F       STA $9F
					mem16[0x9E] += 0x01

					# 03:9267 C6 A9       DEC $A9
					# 03:9269 A5 A9       LDA $A9
					# 03:926B D0 B7       BNE $9224 (-73 B)
				end

				# 03:926D C6 AB       DEC $AB
				# 03:926F A5 AB       LDA $AB
				# 03:9271 D0 AD       BNE $9220 (-83 B)
			end

			# 03:9273 AD A0 35    LDA $35A0
			# 03:9276 AA          TAX
			# 03:9277 8E 3D 39    STX $393D
			# 03:927A A2 58 02    LDX #$0258
			# 03:927D 8E 3F 39    STX $393F
			# 03:9280 20 B9 83    JSR $83B9
			# 03:9283 AC 41 39    LDY $3941
			# 03:9286 84 9A       STY $9A
			mem16[0x9A] = mem8[0x35A0] * 0x0258

			# 03:9288 A6 9C       LDX $9C
			# 03:928A BD 1F 53    LDA $531F,x
			# 03:928D C9 FF       CMP #$FF
			# 03:928F F0 54       BEQ $92E5 (+84 B)
			until mem8[0x531F + mem16[0x9C]] == 0xFF
				# 03:9291 E8          INX
				# 03:9292 BD 1F 53    LDA $531F,x
				# 03:9295 85 E5       STA $E5
				x = mem16[0x9C] + 1
				mem8[0xE5] = mem8[0x531F + x]

				# 03:9297 A0 00 E9    LDY #$E900
				y = 0xE900

				# 03:929A AD EF 38    LDA $38EF   <-- Determines which attack bank to use
				# 03:929D F0 03       BEQ $92A2 (+3 B)
				if mem8[0x38EF] != 0
					# 03:929F A0 C0 B6    LDY #$B6C0
					y = 0xB6C0
				end

				# 03:92A2 A9 0E       LDA #$0E
				# 03:92A4 20 43 84    JSR $8443
				x = _8443(0x0E, y) # because of theTYX later.

				# 03:92A7 AD EF 38    LDA $38EF
				# 03:92AA D0 12       BNE $92BE (+18 B)
				if mem8[0x38EF] == 0
					# 03:92AC BB          TYX
					# 03:92AD A4 9A       LDY $9A
					y = mem16[0x9A]

					begin
						# 03:92AF BF 00 E9 0E LDA $0EE900,x  [Enemy Attack Sequence Table] (Counter)
						# 03:92B3 99 FF 59    STA $59FF,y
						mem8[0x59FF + y] = mem8[0x0EE900 + x]

						# 03:92B6 E8          INX
						# 03:92B7 C8          INY
						# 03:92B8 C9 FF       CMP #$FF
						# 03:92BA D0 F3       BNE $92AF (-13 B)
						# 03:92BC 80 10       BRA $92CE (+16 B)
						x += 1
						y += 1
					end until y == 0xFF
				else
					# 03:92BE BB          TYX
					# 03:92BF A4 9A       LDY $9A
					y = mem16[0x9A]

					begin
						# 03:92C1 BF C0 B6 0E LDA $0EB6C0,x  [Enemy Attack Sequence #2 Table] (Counter)
						# 03:92C5 99 FF 59    STA $59FF,y
						mem8[0x59FF + y] = mem8[0x0EB6C0 + x]

						# 03:92C8 E8          INX
						# 03:92C9 C8          INY
						# 03:92CA C9 FF       CMP #$FF
						# 03:92CC D0 F3       BNE $92C1 (-13 B)
						x += 1
						y += 1
					end until y == 0xFF
				end

				# 03:92CE C2 20       REP #$20
				# 03:92D0 18          CLC
				# 03:92D1 A5 9A       LDA $9A
				# 03:92D3 69 3C 00    ADC #$003C
				# 03:92D6 85 9A       STA $9A
				# 03:92D8 18          CLC
				# 03:92D9 A5 9C       LDA $9C
				# 03:92DB 69 02 00    ADC #$0002
				# 03:92DE 85 9C       STA $9C
				# 03:92E0 7B          TDC
				# 03:92E1 E2 20       SEP #$20
				mem16[0x9A] += 0x003C
				mem16[0x9C] += 0x0002

				# 03:92E3 80 A3       BRA $9288 (-93 B)
			end
		end
	end

	# 03:92E5 AD A0 35    LDA $35A0
	# 03:92E8 85 DF       STA $DF
	# 03:92EA A9 14       LDA #$14
	# 03:92EC 85 E1       STA $E1
	# 03:92EE 20 E0 83    JSR $83E0
	y = mem8[0x35A0] * 0x14

	# 03:92F1 AD A1 35    LDA $35A1
	# 03:92F4 85 E5       STA $E5
	# 03:92F6 A0 30 E0    LDY #$E030
	# 03:92F9 A9 0E       LDA #$0E
	# 03:92FB 20 43 84    JSR $8443
	# 03:92FE BB          TYX
	mem8[0xE5] = mem8[0x35A1]
	x = _8443(0x0E, 0xE030)

	# 03:92FF A4 E3       LDY $E3
	# 03:9301 84 98       STY $98
	# 03:9303 84 9C       STY $9C
	mem16[0x98] = y
	mem16[0x9C] = y

	# 03:9305 BF 30 E0 0E LDA $0EE030,x  [Enemy Attack Sequence Group Table]
	# 03:9309 99 7F 39    STA $397F,y
	# 03:930C E8          INX
	# 03:930D C8          INY
	# 03:930E C9 FF       CMP #$FF
	# 03:9310 D0 F3       BNE $9305 (-13 B)
	begin
		mem8[0x397F + y] = mem8[0x0EE030 + x]
		x += 1
		y += 1
	end until y == 0xFF

	# 03:9312 AD A0 35    LDA $35A0
	# 03:9315 85 DF       STA $DF
	# 03:9317 A9 28       LDA #$28
	# 03:9319 85 E1       STA $E1
	# 03:931B 20 E0 83    JSR $83E0
	# 03:931E A4 E3       LDY $E3
	# 03:9320 84 9A       STY $9A
	# 03:9322 84 9E       STY $9E
	y = mem8[0x35A0] * 0x28
	mem16[0x9A] = y
	mem16[0x9E] = y

	# 03:9324 A6 98       LDX $98
	# 03:9326 BD 7F 39    LDA $397F,x
	# 03:9329 C9 FF       CMP #$FF
	# 03:932B F0 35       BEQ $9362 (+53 B)
	until mem8[0x397F + mem8[0x98]] == 0xFF
		a = mem8[0x397F + mem8[0x98]]

		# 03:932D 85 E5       STA $E5
		# 03:932F A0 00 E6    LDY #$E600
		# 03:9332 A9 0E       LDA #$0E
		# 03:9334 20 43 84    JSR $8443
		# 03:9337 BB          TYX
		mem8[0xE5] = a
		x = _8443(0x0E, 0xE600)

		# 03:9338 A4 9A       LDY $9A
		y = mem16[0x9A]

		# 03:933A A9 04       LDA #$04
		# 03:933C 85 A9       STA $A9
		(0..3).each do
			# 03:933E BF 00 E6 0E LDA $0EE600,x  [Enemy Attack Condition Set Table]
			# 03:9342 99 1F 3A    STA $3A1F,y
			# 03:9345 C9 FF       CMP #$FF
			# 03:9347 F0 01       BEQ $934A (+1 B)
			a = mem8[0x0EE600 + x]
			mem8[0x3A1F + y] = a
			if a != 0xFF
				# 03:9349 E8          INX
				x += 1
			end

			# 03:934A C8          INY
			# 03:934B C6 A9       DEC $A9
			# 03:934D A5 A9       LDA $A9
			# 03:934F D0 ED       BNE $933E (-19 B)
			y += 1
		end

		# 03:9351 84 9A       STY $9A
		mem16[0x9A] = y

		# 03:9353 18          CLC
		# 03:9354 A5 98       LDA $98
		# 03:9356 69 02       ADC #$02
		# 03:9358 85 98       STA $98
		# 03:935A A5 99       LDA $99
		# 03:935C 69 00       ADC #$00
		# 03:935E 85 99       STA $99
		mem16[0x98] += 2

		# 03:9360 80 C2       BRA $9324 (-62 B)
	end

	# 03:9362 AD A0 35    LDA $35A0
	# 03:9365 85 DF       STA $DF
	# 03:9367 A9 A0       LDA #$A0
	# 03:9369 85 E1       STA $E1
	# 03:936B 20 E0 83    JSR $83E0
	# 03:936E A4 E3       LDY $E3
	# 03:9370 84 9A       STY $9A
	mem16[0x9A] = mem8[0x35A0] * 0xA0

	# 03:9372 A9 0A       LDA #$0A
	# 03:9374 85 AB       STA $AB
	0x0A.times do
		# 03:9376 A9 04       LDA #$04
		# 03:9378 85 A9       STA $A9
		0x04.times do
			# 03:937A A6 9E       LDX $9E
			# 03:937C BD 1F 3A    LDA $3A1F,x
			# 03:937F C9 FF       CMP #$FF
			# 03:9381 F0 20       BEQ $93A3 (+32 B)
			x = mem16[0x9E]
			a = mem8[0x3A1F + x]
			if a != 0xFF
				# 03:9383 85 DF       STA $DF
				# 03:9385 A9 04       LDA #$04
				# 03:9387 85 E1       STA $E1
				# 03:9389 20 E0 83    JSR $83E0
				# 03:938C A6 E3       LDX $E3
				# 03:938E A4 9A       LDY $9A
				# 03:9390 A9 04       LDA #$04
				# 03:9392 85 AA       STA $AA
				x = a * 0x04
				y = mem16[0x9A]

				0x04.times do
					# 03:9394 BF 00 E7 0E LDA $0EE700,x  [Enemy Attack Condition table]
					# 03:9398 99 5F 3B    STA $3B5F,y
					# 03:939B E8          INX
					# 03:939C C8          INY
					# 03:939D C6 AA       DEC $AA
					# 03:939F A5 AA       LDA $AA
					# 03:93A1 D0 F1       BNE $9394 (-15 B)
					mem8[0x3B5F + y] = mem8[0x0EE700 + x]
					x += 1
					y += 1
				end
			end

			# 03:93A3 18          CLC
			# 03:93A4 A5 9A       LDA $9A
			# 03:93A6 69 04       ADC #$04
			# 03:93A8 85 9A       STA $9A
			# 03:93AA A5 9B       LDA $9B
			# 03:93AC 69 00       ADC #$00
			# 03:93AE 85 9B       STA $9B
			mem16[0x9A] += 4

			# 03:93B0 18          CLC
			# 03:93B1 A5 9E       LDA $9E
			# 03:93B3 69 01       ADC #$01
			# 03:93B5 85 9E       STA $9E
			# 03:93B7 A5 9F       LDA $9F
			# 03:93B9 69 00       ADC #$00
			# 03:93BB 85 9F       STA $9F
			mem16[0x9E] += 1

			# 03:93BD C6 A9       DEC $A9
			# 03:93BF A5 A9       LDA $A9
			# 03:93C1 D0 B7       BNE $937A (-73 B)
		end

		# 03:93C3 C6 AB       DEC $AB
		# 03:93C5 A5 AB       LDA $AB
		# 03:93C7 D0 AD       BNE $9376 (-83 B)
	end

	# 03:93C9 AD A0 35    LDA $35A0
	# 03:93CC AA          TAX
	# 03:93CD 8E 3D 39    STX $393D
	# 03:93D0 A2 58 02    LDX #$0258
	# 03:93D3 8E 3F 39    STX $393F
	# 03:93D6 20 B9 83    JSR $83B9
	# 03:93D9 AC 41 39    LDY $3941
	# 03:93DC 84 9A       STY $9A
	# 03:93DE 8C 96 28    STY $2896
	y = mem8[0x35A0] * 0x0258
	mem16[0x9A] = y
	mem16[0x2896] = y

	# 03:93E1 A6 9C       LDX $9C
	# 03:93E3 BD 7F 39    LDA $397F,x
	# 03:93E6 C9 FF       CMP #$FF
	# 03:93E8 F0 54       BEQ $943E (+84 B)
	if mem8[0x397F + mem16[0x9C]] != 0xFF
		# 03:93EA E8          INX
		x += 1

		# 03:93EB BD 7F 39    LDA $397F,x
		# 03:93EE 85 E5       STA $E5
		# 03:93F0 A0 00 E9    LDY #$E900
		mem8[0xE5] = mem8]0x397F + x]
		y = 0xE900

		# 03:93F3 AD EF 38    LDA $38EF   <-- Determines which attack bank to use
		# 03:93F6 F0 03       BEQ $93FB (+3 B)
		if mem8[0x38EF] != 0
			# 03:93F8 A0 C0 B6    LDY #$B6C0
			y = 0xB6C0
		end

		# 03:93FB A9 0E       LDA #$0E
		# 03:93FD 20 43 84    JSR $8443
		x = _8443(0x0E, y)

		# 03:9400 AD EF 38    LDA $38EF
		# 03:9403 D0 12       BNE $9417 (+18 B)
		if mem8[0x38EF] == 0
			# 03:9405 BB          TYX
			# 03:9406 A4 9A       LDY $9A
			y = mem16[0x9A]

			begin
				# 03:9408 BF 00 E9 0E LDA $0EE900,x  [Enemy Attack Sequence Table]
				# 03:940C 99 5F 40    STA $405F,y
				# 03:940F E8          INX
				# 03:9410 C8          INY
				# 03:9411 C9 FF       CMP #$FF
				# 03:9413 D0 F3       BNE $9408 (-13 B)
				# 03:9415 80 10       BRA $9427 (+16 B)
				mem8[0x405F + y] = mem8[0x0EE900 + x]
				x += 1
				y += 1
			end until y == 0xFF
		else
			# 03:9417 BB          TYX
			# 03:9418 A4 9A       LDY $9A
			y = mem16[0x9A]

			begin
				# 03:941A BF C0 B6 0E LDA $0EB6C0,x  [Enemy Attack Sequence #2 Table]
				# 03:941E 99 5F 40    STA $405F,y
				# 03:9421 E8          INX
				# 03:9422 C8          INY
				# 03:9423 C9 FF       CMP #$FF
				# 03:9425 D0 F3       BNE $941A (-13 B)
				mem8[0x405F + y] = mem8[0x0EB6C0 + x]
				x += 1
				y += 1
			end until y == 0xFF
		end

		# 03:9427 C2 20       REP #$20
		# 03:9429 18          CLC
		# 03:942A A5 9A       LDA $9A
		# 03:942C 69 3C 00    ADC #$003C
		# 03:942F 85 9A       STA $9A
		# 03:9431 18          CLC
		# 03:9432 A5 9C       LDA $9C
		# 03:9434 69 02 00    ADC #$0002
		# 03:9437 85 9C       STA $9C
		# 03:9439 7B          TDC
		# 03:943A E2 20       SEP #$20
		# 03:943C 80 A3       BRA $93E1 (-93 B)
		mem16[0x9A] += 0x003C
		mem16[0x9C] += 0x0002
	end

	# Calculates the offsets for the attack seq associated with the ALWAYS condition, for easy referal
	# 03:943E AD A0 35    LDA $35A0
	# 03:9441 85 DF       STA $DF
	# 03:9443 A9 14       LDA #$14
	# 03:9445 85 E1       STA $E1
	# 03:9447 20 E0 83    JSR $83E0
	# 03:944A A6 E3       LDX $E3
	x = mem8[0x35A0] * 0x14

	# 03:944C 7B          TDC
	# 03:944D A8          TAY
	# 03:944E 84 A9       STY $A9
	mem16[0xA9] = 0

	until mem8[0x397F + x] == 0 do
		# 03:9450 BD 7F 39    LDA $397F,x
		# 03:9453 F0 08       BEQ $945D (+8 B)
		# 03:9455 E8          INX
		# 03:9456 E8          INX
		# 03:9457 E6 A9       INC $A9
		# 03:9459 E6 A9       INC $A9
		# 03:945B 80 F3       BRA $9450 (-13 B)
		x += 2
		mem8[0xA9] += 2
	end

	# 03:945D A5 A9       LDA $A9
	# 03:945F 4A          LSR a
	# 03:9460 48          PHA
	# 03:9461 AD A0 35    LDA $35A0
	# 03:9464 AA          TAX
	# 03:9465 68          PLA
	# 03:9466 9D 04 36    STA $3604,x
	# 03:9469 85 DF       STA $DF
	x = mem8[0x35A0]
	a = mem8[0xA9] * 2
	mem8[0x3604 + x] = a

	# 03:946B A9 3C       LDA #$3C
	# 03:946D 85 E1       STA $E1
	# 03:946F 20 E0 83    JSR $83E0
	# 03:9472 AD A0 35    LDA $35A0
	# 03:9475 0A          ASL a
	# 03:9476 AA          TAX
	# 03:9477 18          CLC
	# 03:9478 AD 96 28    LDA $2896
	# 03:947B 65 E3       ADC $E3
	# 03:947D 9D 0C 36    STA $360C,x
	# 03:9480 AD 97 28    LDA $2897
	# 03:9483 65 E4       ADC $E4
	# 03:9485 9D 0D 36    STA $360D,x
	mem16[0x360C + mem8[0x35A0] * 2] += a * 0x3C

	# 03:9488 60          RTS
	return
end


# Used to copy the bytes targeted by a Combat Ability InDEX
# Input: A
# Output: $291C, $291D, $291E
def _9489(a)
	# 03:9489 DA          PHX
	# 03:948A 85 DF       STA $DF
	# 03:948C A9 03       LDA #$03
	# 03:948E 85 E1       STA $E1
	# 03:9490 20 E0 83    JSR $83E0
	# 03:9493 A6 E3       LDX $E3   Copies the 3 bytes to $291C-291E
	x = a * 0x03

	# 03:9495 A0 80 00    LDY #$0080
	# 03:9498 BF 80 A3 0E LDA $0EA380,x  Enemy Combat Abilities Table
	# 03:949C 99 9C 28    STA $289C,y
	# 03:949F E8          INX
	# 03:94A0 C8          INY
	# 03:94A1 C0 83 00    CPY #$0083
	# 03:94A4 D0 F2       BNE $9498 (-14 B)
	y = 0x0080
	(0..2).each do |i|
		mem8[0x289C + y + i] = mem8[0x0EA380 + x + i]
	end

	# 03:94A6 FA          PLX
	# 03:94A7 60          RTS
	return
end

# 03:94A8 A2 00 20    LDX #$2000
# 03:94AB 86 80       STX $80
# 03:94AD 7B          TDC
# 03:94AE AA          TAX
# 03:94AF 64 A9       STZ $A9
# 03:94B1 7B          TDC   Copies the current char’sSTAts to its correct slot
# 03:94B2 A8          TAY
# 03:94B3 BD 00 10    LDA $1000,x
# 03:94B6 91 80       STA ($80),y
# 03:94B8 E8          INX
# 03:94B9 C8          INY
# 03:94BA C0 40 00    CPY #$0040
# 03:94BD D0 F4       BNE $94B3 (-12 B)

# 03:94BF DA          PHX
# 03:94C0 7B          TDC
# 03:94C1 A8          TAY
# 03:94C2 B1 80       LDA ($80),y  Loads current char’s ID (BITs 0-4)
# 03:94C4 29 1F       AND #$1F
# 03:94C6 D0 18       BNE $94E0 (+24 B)
# 03:94C8 A5 A9       LDA $A9  If empty slot, puts all conditions to 0, next char
# 03:94CA AA          TAX
# 03:94CB FE 40 35    INC $3540,x
# 03:94CE A0 03 00    LDY #$0003
# 03:94D1 7B          TDC
# 03:94D2 91 80       STA ($80),y
# 03:94D4 C8          INY
# 03:94D5 91 80       STA ($80),y
# 03:94D7 C8          INY
# 03:94D8 91 80       STA ($80),y
# 03:94DA C8          INY
# 03:94DB 91 80       STA ($80),y
# 03:94DD 4C 66 95    JMP $9566

# 03:94E0 A0 03 00    LDY #$0003  If current char is swonned or stoned, next char
# 03:94E3 B1 80       LDA ($80),y
# 03:94E5 29 C0       AND #$C0
# 03:94E7 D0 7D       BNE $9566 (+125 B)

# 03:94E9 A5 A9       LDA $A9  If current char is active, sets thatBIT
# 03:94EB 0A          ASL a
# 03:94EC AA          TAX
# 03:94ED A9 40       LDA #$40
# 03:94EF 9D EB 29    STA $29EB,x

# 03:94F2 C2 20       REP #$20  If  Current Char MaxHP / 4 is >= CurHP, sets the
# 03:94F4 A0 09 00    LDY #$0009  Weak condition
# 03:94F7 B1 80       LDA ($80),y
# 03:94F9 20 86 84    JSR $8486
# 03:94FC A0 07 00    LDY #$0007
# 03:94FF D1 80       CMP ($80),y
# 03:9501 90 0C       BCC $950F (+12 B)
# 03:9503 A0 05 00    LDY #$0005
# 03:9506 B1 80       LDA ($80),y
# 03:9508 09 00 01    ORA #$0100
# 03:950B 91 80       STA ($80),y
# 03:950D 80 0A       BRA $9519 (+10 B)
# 03:950F A0 05 00    LDY #$0005
# 03:9512 B1 80       LDA ($80),y
# 03:9514 29 FF FE    AND #$FEFF
# 03:9517 91 80       STA ($80),y

# 03:9519 7B          TDC   Puts #$10 at the character’s Relative Speed
# 03:951A E2 20       SEP #$20
# 03:951C A0 3B 00    LDY #$003B
# 03:951F A9 10       LDA #$10
# 03:9521 91 80       STA ($80),y

# 03:9523 AD A2 29    LDA $29A2  Character’s Relative Speed = 1 if Auto-Battle
# 03:9526 29 20       AND #$20
# 03:9528 F0 04       BEQ $952E (+4 B)
# 03:952A A9 01       LDA #$01
# 03:952C 91 80       STA ($80),y

# 03:952E A0 2D 00    LDY #$002D  Copies char slot’s $2D-2E to slot’s $41-42
# 03:9531 B1 80       LDA ($80),y
# 03:9533 A0 41 00    LDY #$0041
# 03:9536 91 80       STA ($80),y
# 03:9538 A0 2E 00    LDY #$002E
# 03:953B B1 80       LDA ($80),y
# 03:953D A0 42 00    LDY #$0042
# 03:9540 91 80       STA ($80),y

# 03:9542 A0 04 00    LDY #$0004  Takes every condition off, except Float, Jump, Magnetize,
# 03:9545 B1 80       LDA ($80),yAND Weak
# 03:9547 29 40       AND #$40
# 03:9549 91 80       STA ($80),y
# 03:954B C8          INY
# 03:954C B1 80       LDA ($80),y
# 03:954E 29 82       AND #$82
# 03:9550 91 80       STA ($80),y
# 03:9552 C8          INY
# 03:9553 B1 80       LDA ($80),y
# 03:9555 29 01       AND #$01
# 03:9557 91 80       STA ($80),y

# 03:9559 A0 02 00    LDY #$0002  If the char’s level is greater than $3583,REPLAce that
# 03:955C B1 80       LDA ($80),y  value with the char’s level
# 03:955E CD 83 35    CMP $3583
# 03:9561 B0 03       BCS $9566 (+3 B)
# 03:9563 8D 83 35    STA $3583

# 03:9566 FA          PLX
# 03:9567 A0 07 00    LDY #$0007
# 03:956A B1 80       LDA ($80),y
# 03:956C A0 08 00    LDY #$0008
# 03:956F 11 80       ORA ($80),y
# 03:9571 D0 09       BNE $957C (+9 B)
# 03:9573 A0 03 00    LDY #$0003  If current char has no remaining CurHP,
# 03:9576 B1 80       LDA ($80),y  adds the Swoon Condition
# 03:9578 09 80       ORA #$80
# 03:957A 91 80       STA ($80),y

# 03:957C 18          CLC
# 03:957D A5 80       LDA $80  Adds #$80 to the current enemy offset
# 03:957F 69 80       ADC #$80
# 03:9581 85 80       STA $80
# 03:9583 A5 81       LDA $81
# 03:9585 69 00       ADC #$00
# 03:9587 85 81       STA $81
# 03:9589 E6 A9       INC $A9  Next Enemy
# 03:958B A5 A9       LDA $A9
# 03:958D C9 05       CMP #$05  5 Chars
# 03:958F F0 03       BEQ $9594 (+3 B)
# 03:9591 4C B1 94    JMP $94B1

# 03:9594 A2 00 20    LDX #$2000  Puts the current char number in $3539-(353D?) if
# 03:9597 86 80       STX $80  char is a twin-caster
# 03:9599 7B          TDC
# 03:959A A8          TAY
# 03:959B AA          TAX
# 03:959C 84 A9       STY $A9
# 03:959E B1 80       LDA ($80),y
# 03:95A0 29 1F       AND #$1F
# 03:95A2 C9 08       CMP #$08
# 03:95A4 F0 0C       BEQ $95B2 (+12 B)
# 03:95A6 C9 09       CMP #$09
# 03:95A8 F0 08       BEQ $95B2 (+8 B)
# 03:95AA C9 13       CMP #$13
# 03:95AC F0 04       BEQ $95B2 (+4 B)
# 03:95AE C9 15       CMP #$15
# 03:95B0 D0 06       BNE $95B8 (+6 B)
# 03:95B2 A5 A9       LDA $A9
# 03:95B4 9D 39 35    STA $3539,x
# 03:95B7 E8          INX
# 03:95B8 C2 20       REP #$20
# 03:95BA 18          CLC
# 03:95BB A5 80       LDA $80
# 03:95BD 69 80 00    ADC #$0080
# 03:95C0 85 80       STA $80
# 03:95C2 7B          TDC
# 03:95C3 E2 20       SEP #$20
# 03:95C5 E6 A9       INC $A9
# 03:95C7 A5 A9       LDA $A9
# 03:95C9 C9 05       CMP #$05
# 03:95CB D0 D1       BNE $959E (-47 B)
# 03:95CD 60          RTS


# 03:95CE AD A8 16    LDA $16A8   Sets FrontAND Back RowBITs
# 03:95D1 D0 29       BNE $95FC (+41 B)

# 03:95D3 AD 01 20    LDA $2001
# 03:95D6 29 7F       AND #$7F
# 03:95D8 8D 01 20    STA $2001
# 03:95DB AD 81 20    LDA $2081
# 03:95DE 29 7F       AND #$7F
# 03:95E0 8D 81 20    STA $2081
# 03:95E3 AD 01 21    LDA $2101
# 03:95E6 29 7F       AND #$7F
# 03:95E8 8D 01 21    STA $2101
# 03:95EB AD 81 21    LDA $2181
# 03:95EE 09 80       ORA #$80
# 03:95F0 8D 81 21    STA $2181
# 03:95F3 AD 01 22    LDA $2201
# 03:95F6 09 80       ORA #$80
# 03:95F8 8D 01 22    STA $2201
# 03:95FB 60          RTS

# 03:95FC AD 01 20    LDA $2001
# 03:95FF 09 80       ORA #$80
# 03:9601 8D 01 20    STA $2001
# 03:9604 AD 81 20    LDA $2081
# 03:9607 09 80       ORA #$80
# 03:9609 8D 81 20    STA $2081
# 03:960C AD 01 21    LDA $2101
# 03:960F 09 80       ORA #$80
# 03:9611 8D 01 21    STA $2101
# 03:9614 AD 81 21    LDA $2181
# 03:9617 29 7F       AND #$7F
# 03:9619 8D 81 21    STA $2181
# 03:961C AD 01 22    LDA $2201
# 03:961F 29 7F       AND #$7F
# 03:9621 8D 01 22    STA $2201
# 03:9624 60          RTS


# 03:9625 7B          TDC
# 03:9626 AA          TAX
# 03:9627 86 CD       STX $CD

# 03:9629 A6 CD       LDX $CD
# 03:962B BD 40 35    LDA $3540,x   If empty slot, next char
# 03:962E D0 3E       BNE $966E (+62 B)
# 03:9630 A6 CD       LDX $CD
# 03:9632 86 DF       STX $DF   Multiplies $CD by #$0080
# 03:9634 A2 80 00    LDX #$0080
# 03:9637 86 E1       STX $E1
# 03:9639 20 E0 83    JSR $83E0

# 03:963C A6 E3       LDX $E3
# 03:963E BD 03 20    LDA $2003,x
# 03:9641 29 01       AND #$01
# 03:9643 F0 29       BEQ $966E (+41 B)  If current char doesn’t have poison, next char

# 03:9645 A9 06       LDA #$06
# 03:9647 85 D6       STA $D6
# 03:9649 A5 CD       LDA $CD
# 03:964B 20 2C 9E    JSR $9E2C   $D4 = (CurVit+14)*relSpeed \ #$10

# 03:964E A9 09       LDA #$09   Computes $2A04-2A06 (??)
# 03:9650 20 C8 85    JSR $85C8
# 03:9653 A9 40       LDA #$40
# 03:9655 9D 06 2A    STA $2A06,x

# 03:9658 A5 CD       LDA $CD   Writes data about current character being poisoned
# 03:965A 0A          ASL a
# 03:965B AA          TAX
# 03:965C BD EB 29    LDA $29EB,x
# 03:965F 09 10       ORA #$10
# 03:9661 9D EB 29    STA $29EB,x
# 03:9664 A5 D4       LDA $D4
# 03:9666 9D 2A 2B    STA $2B2A,x
# 03:9669 A5 D5       LDA $D5
# 03:966B 9D 2B 2B    STA $2B2B,x

# 03:966E E6 CD       INC $CD
# 03:9670 A5 CD       LDA $CD
# 03:9672 C9 05       CMP #$05
# 03:9674 D0 B3       BNE $9629 (-77 B)
# 03:9676 60          RTS


# 03:9677 9C EE 38    STZ $38EE
# 03:967A AD EE 38    LDA $38EE
# 03:967D F0 06       BEQ $9685 (+6 B)
# 03:967F CE EE 38DEC $38EE
# 03:9682 4C 40 97    JMP $9740

# 03:9685 AD AC 16    LDA $16AC
# 03:9688 8D EE 38    STA $38EE
# 03:968B C2 20       REP #$20
# 03:968D 7B          TDC
# 03:968E AA          TAX
# 03:968F A8          TAY
# 03:9690 AD 01 36    LDA $3601
# 03:9693 C9 FF FF    CMP #$FFFF
# 03:9696 F0 0A       BEQ $96A2 (+10 B)
# 03:9698 85 80       STA $80
# 03:969A 98          TYA
# 03:969B 4A          LSR a
# 03:969C C5 80       CMP $80
# 03:969E F0 02       BEQ $96A2 (+2 B)
# 03:96A0 80 2C       BRA $96CE (+44 B)
# 03:96A2 B9 EA 29    LDA $29EA,y
# 03:96A5 8D 96 28    STA $2896
# 03:96A8 0E 96 28    ASL $2896
# 03:96AB 90 29       BCC $96D6 (+41 B)
# 03:96AD 7B          TDC
# 03:96AE E2 20       SEP #$20

# 03:96B0 BD 06 2A    LDA $2A06,x
# 03:96B3 29 01       AND #$01
# 03:96B5 C2 20       REP #$20
# 03:96B7 D0 1D       BNE $96D6 (+29 B)
# 03:96B9 DE 04 2ADEC $2A04,x
# 03:96BC BD 04 2A    LDA $2A04,x
# 03:96BF D0 0D       BNE $96CE (+13 B)
# 03:96C1 7B          TDC
# 03:96C2 E2 20       SEP #$20

# 03:96C4 BD 06 2A    LDA $2A06,x
# 03:96C7 09 01       ORA #$01
# 03:96C9 9D 06 2A    STA $2A06,x
# 03:96CC C2 20       REP #$20
# 03:96CE 8A          TXA
# 03:96CF 18          CLC
# 03:96D0 69 15 00    ADC #$0015
# 03:96D3 AA          TAX
# 03:96D4 80 5D       BRA $9733 (+93 B)
# 03:96D6 E8          INX
# 03:96D7 E8          INX
# 03:96D8 E8          INX
# 03:96D9 A9 06 00    LDA #$0006
# 03:96DC 8D 9A 28    STA $289A
# 03:96DF 0E 96 28    ASL $2896
# 03:96E2 90 44       BCC $9728 (+68 B)
# 03:96E4 7B          TDC
# 03:96E5 E2 20       SEP #$20
# 03:96E7 BD 06 2A    LDA $2A06,x
# 03:96EA 29 81       AND #$81
# 03:96EC C2 20       REP #$20
# 03:96EE D0 38       BNE $9728 (+56 B)
# 03:96F0 AD 9A 28    LDA $289A
# 03:96F3 C9 01 00    CMP #$0001
# 03:96F6 D0 16       BNE $970E (+22 B)
# 03:96F8 DA          PHX
# 03:96F9 98          TYA
# 03:96FA 0A          ASL a
# 03:96FB AA          TAX
# 03:96FC DE A4 35DEC $35A4,x
# 03:96FF BD A4 35    LDA $35A4,x
# 03:9702 F0 03       BEQ $9707 (+3 B)
# 03:9704 FA          PLX
# 03:9705 80 21       BRA $9728 (+33 B)
# 03:9707 A9 02 00    LDA #$0002
# 03:970A 9D A4 35    STA $35A4,x
# 03:970D FA          PLX
# 03:970E BD 04 2A    LDA $2A04,x
# 03:9711 F0 08       BEQ $971B (+8 B)
# 03:9713 DE 04 2ADEC $2A04,x
# 03:9716 BD 04 2A    LDA $2A04,x
# 03:9719 D0 0D       BNE $9728 (+13 B)
# 03:971B 7B          TDC
# 03:971C E2 20       SEP #$20
# 03:971E BD 06 2A    LDA $2A06,x
# 03:9721 09 81       ORA #$81
# 03:9723 9D 06 2A    STA $2A06,x
# 03:9726 C2 20       REP #$20
# 03:9728 E8          INX
# 03:9729 E8          INX
# 03:972A E8          INX
# 03:972B CE 9A 28DEC $289A
# 03:972E AD 9A 28    LDA $289A
# 03:9731 D0 AC       BNE $96DF (-84 B)
# 03:9733 C8          INY
# 03:9734 C8          INY
# 03:9735 C0 1A 00    CPY #$001A
# 03:9738 F0 03       BEQ $973D (+3 B)
# 03:973A 4C 90 96    JMP $9690
# 03:973D 7B          TDC
# 03:973E E2 20       SEP #$20
# 03:9740 60          RTS


# 03:9741 64 D1       STZ $D1
# 03:9743 64 00       STZ $00
# 03:9745 AD F6 38    LDA $38F6
# 03:9748 85 A9       STA $A9
# 03:974A AD 01 36    LDA $3601
# 03:974D C9 FF       CMP #$FF
# 03:974F F0 04       BEQ $9755 (+4 B)
# 03:9751 C5 A9       CMP $A9
# 03:9753 D0 20       BNE $9775 (+32 B)
# 03:9755 64 AD       STZ $AD
# 03:9757 64 AE       STZ $AE
# 03:9759 A5 A9       LDA $A9
# 03:975B 0A          ASL a
# 03:975C AA          TAX
# 03:975D BD EB 29    LDA $29EB,x
# 03:9760 85 AB       STA $AB
# 03:9762 06 AB       ASL $AB
# 03:9764 90 07       BCC $976D (+7 B)
# 03:9766 20 88 97    JSR $9788
# 03:9769 A5 D1       LDA $D1
# 03:976B D0 1A       BNE $9787 (+26 B)
# 03:976D E6 AD       INC $AD
# 03:976F A5 AD       LDA $AD
# 03:9771 C9 07       CMP #$07
# 03:9773 D0 ED       BNE $9762 (-19 B)
# 03:9775 E6 A9       INC $A9
# 03:9777 A5 A9       LDA $A9
# 03:9779 C9 0D       CMP #$0D
# 03:977B D0 02       BNE $977F (+2 B)
# 03:977D 64 A9       STZ $A9
# 03:977F E6 00       INC $00
# 03:9781 A5 00       LDA $00
# 03:9783 C9 0D       CMP #$0D
# 03:9785 D0 C3       BNE $974A (-61 B)
# 03:9787 60          RTS

# 03:9788 A5 A9       LDA $A9
# 03:978A 85 D2       STA $D2
# 03:978C 20 89 84    JSR $8489
# 03:978F A5 AD       LDA $AD
# 03:9791 85 D3       STA $D3
# 03:9793 0A          ASL a
# 03:9794 18          CLC
# 03:9795 65 AD       ADC $AD
# 03:9797 85 AF       STA $AF
# 03:9799 A5 AF       LDA $AF
# 03:979B 20 69 85    JSR $8569
# 03:979E AE 98 35    LDX $3598
# 03:97A1 BD 04 2A    LDA $2A04,x
# 03:97A4 1D 05 2A    ORA $2A05,x
# 03:97A7 D0 09       BNE $97B2 (+9 B)
# 03:97A9 BD 06 2A    LDA $2A06,x
# 03:97AC 29 01       AND #$01
# 03:97AE F0 02       BEQ $97B2 (+2 B)
# 03:97B0 E6 D1       INC $D1
# 03:97B2 60          RTS

# 03:97B3 A5 D2       LDA $D2
# 03:97B5 8D F6 38    STA $38F6
# 03:97B8 20 89 84    JSR $8489
# 03:97BB EE F6 38    INC $38F6
# 03:97BE AD F6 38    LDA $38F6
# 03:97C1 C9 0D       CMP #$0D
# 03:97C3 D0 03       BNE $97C8 (+3 B)
# 03:97C5 9C F6 38    STZ $38F6
# 03:97C8 A5 D3       LDA $D3
# 03:97CA 0A          ASL a
# 03:97CB 18          CLC
# 03:97CC 65 D3       ADC $D3
# 03:97CE 85 A9       STA $A9
# 03:97D0 A5 A9       LDA $A9
# 03:97D2 20 69 85    JSR $8569
# 03:97D5 AE 98 35    LDX $3598
# 03:97D8 BD 06 2A    LDA $2A06,x
# 03:97DB 29 7E       AND #$7E
# 03:97DD D0 0E       BNE $97ED (+14 B)
# 03:97DF A5 D2       LDA $D2
# 03:97E1 C9 05       CMP #$05
# 03:97E3 90 04       BCC $97E9 (+4 B)
# 03:97E5 A9 01       LDA #$01
# 03:97E7 80 0E       BRA $97F7 (+14 B)
# 03:97E9 A9 00       LDA #$00
# 03:97EB 80 0A       BRA $97F7 (+10 B)
# 03:97ED 29 08       AND #$08
# 03:97EF F0 04       BEQ $97F5 (+4 B)
# 03:97F1 A9 02       LDA #$02
# 03:97F3 80 02       BRA $97F7 (+2 B)
# 03:97F5 A9 03       LDA #$03
# 03:97F7 8D 2E 35    STA $352E
# 03:97FA 64 D1       STZ $D1
# 03:97FC 60          RTS


# 03:97FD AD 75 39    LDA $3975
# 03:9800 48          PHA
# 03:9801 20 35 82    JSR $8235  Clears most the of the RAM
# 03:9804 68          PLA
# 03:9805 8D 75 39    STA $3975
# 03:9808 20 A8 94    JSR $94A8  Copies the char'sSTAts,AND sets the conditions
# 03:980B 20 CC 8E    JSR $8ECC  Calculates stuff with the equipped items
# 03:980E 20 37 98    JSR $9837  Calculates battle properties for the characters

# 03:9811 46 A7       LSR $A7
# 03:9813 66 A6       ROR $A6
# 03:9815 A6 A6       LDX $A6

# 03:9817 7B          TDC   Copies the char'sSTAts back to $1000
# 03:9818 A8          TAY
# 03:9819 B1 80       LDA ($80),y
# 03:981B 9D 00 10    STA $1000,x
# 03:981E E8          INX
# 03:981F C8          INY
# 03:9820 C0 40 00    CPY #$0040
# 03:9823 D0 F4       BNE $9819 (-12 B)

# 03:9825 A0 41 00    LDY #$0041  CopiesSTAts's $41-42 back to $2D-2E
# 03:9828 B1 80       LDA ($80),y
# 03:982A A6 A6       LDX $A6
# 03:982C 9D 2D 10    STA $102D,x
# 03:982F C8          INY
# 03:9830 E8          INX
# 03:9831 B1 80       LDA ($80),y
# 03:9833 9D 2D 10    STA $102D,x

# 03:9836 60          RTS


# 03:9837 AD 75 39    LDA $3975
# 03:983A 20 89 84    JSR $8489  Recalculates some offsets based on A
# 03:983D 18          CLC
# 03:983E A5 A6       LDA $A6
# 03:9840 69 00       ADC #$00
# 03:9842 85 80       STA $80
# 03:9844 A5 A7       LDA $A7
# 03:9846 69 20       ADC #$20
# 03:9848 85 81       STA $81

# 03:984A C2 20       REP #$20  Puts the offset for current char's first eq.item in $82-83
# 03:984C 18          CLC
# 03:984D AD 32 35    LDA $3532
# 03:9850 69 80 27    ADC #$2780
# 03:9853 85 82       STA $82
# 03:9855 7B          TDC
# 03:9856 E2 20       SEP #$20

# 03:9858 AD 2C 35    LDA $352C  If in a magnetic location,AND current character has
# 03:985B F0 26       BEQ $9883 (+38 B) a metallic equipped item, character is magnetized
# 03:985D 7B          TDC
# 03:985E A8          TAY
# 03:985F 84 A9       STY $A9
# 03:9861 84 AB       STY $AB
# 03:9863 B1 82       LDA ($82),y
# 03:9865 10 04       BPL $986B (+4 B)
# 03:9867 E6 A9       INC $A9
# 03:9869 80 0D       BRA $9878 (+13 B)
# 03:986B 98          TYA
# 03:986C 18          CLC
# 03:986D 69 0B       ADC #$0B
# 03:986F A8          TAY
# 03:9870 E6 AB       INC $AB
# 03:9872 A5 AB       LDA $AB
# 03:9874 C9 05       CMP #$05
# 03:9876 D0 EB       BNE $9863 (-21 B)
# 03:9878 A5 A9       LDA $A9
# 03:987A F0 07       BEQ $9883 (+7 B)
# 03:987C A0 05 00    LDY #$0005
# 03:987F A9 80       LDA #$80
# 03:9881 91 80       STA ($80),y

# 03:9883 A0 14 00    LDY #$0014  Resets current char's currentSTAts,AND adds all the
# 03:9886 7B          TDC   items for ability modifiers
# 03:9887 91 80       STA ($80),y
# 03:9889 C8          INY
# 03:988A C0 19 00    CPY #$0019
# 03:988D D0 F8       BNE $9887 (-8 B)
# 03:988F 64 AF       STZ $AF
# 03:9891 A0 07 00    LDY #$0007
# 03:9894 B1 82       LDA ($82),y
# 03:9896 48          PHA
# 03:9897 29 F8       AND #$F8
# 03:9899 85 A9       STA $A9
# 03:989B 68          PLA
# 03:989C 29 07       AND #$07
# 03:989E 0A          ASL a
# 03:989F AA          TAX
# 03:98A0 BF AC FE 13 LDA $13FEAC,x
# 03:98A4 85 AB       STA $AB
# 03:98A6 E8          INX
# 03:98A7 BF AC FE 13 LDA $13FEAC,x
# 03:98AB 85 AC       STA $AC
# 03:98AD 5A          PHY
# 03:98AE A0 14 00    LDY #$0014
# 03:98B1 06 A9       ASL $A9
# 03:98B3 90 07       BCC $98BC (+7 B)
# 03:98B5 18          CLC
# 03:98B6 B1 80       LDA ($80),y
# 03:98B8 65 AB       ADC $AB
# 03:98BA 80 05       BRA $98C1 (+5 B)
# 03:98BC 18          CLC
# 03:98BD B1 80       LDA ($80),y
# 03:98BF 65 AC       ADC $AC
# 03:98C1 91 80       STA ($80),y
# 03:98C3 C8          INY
# 03:98C4 C0 19 00    CPY #$0019
# 03:98C7 D0 E8       BNE $98B1 (-24 B)
# 03:98C9 7A          PLY
# 03:98CA 98          TYA
# 03:98CB 18          CLC
# 03:98CC 69 0B       ADC #$0B
# 03:98CE A8          TAY
# 03:98CF E6 AF       INC $AF
# 03:98D1 A5 AF       LDA $AF
# 03:98D3 C9 05       CMP #$05
# 03:98D5 D0 BD       BNE $9894 (-67 B)
# 03:98D7 A0 0F 00    LDY #$000F  Adds the baseSTAts to get the currentSTAt
# 03:98DA 84 A9       STY $A9
# 03:98DC A0 14 00    LDY #$0014
# 03:98DF 84 AB       STY $AB
# 03:98E1 A4 A9       LDY $A9
# 03:98E3 18          CLC
# 03:98E4 B1 80       LDA ($80),y
# 03:98E6 A4 AB       LDY $AB
# 03:98E8 71 80       ADC ($80),y
# 03:98EA C9 B6       CMP #$B6  If currentSTAt >= 182, puts currentSTAt as 1 (???)
# 03:98EC B0 08       BCS $98F6 (+8 B)
# 03:98EE C9 63       CMP #$63
# 03:98F0 90 06       BCC $98F8 (+6 B)
# 03:98F2 A9 63       LDA #$63
# 03:98F4 80 02       BRA $98F8 (+2 B)
# 03:98F6 A9 01       LDA #$01
# 03:98F8 91 80       STA ($80),y
# 03:98FA E6 A9       INC $A9
# 03:98FC E6 AB       INC $AB
# 03:98FE A5 A9       LDA $A9
# 03:9900 C9 14       CMP #$14
# 03:9902 D0 DD       BNE $98E1 (-35 B)

# 03:9904 A0 02 00    LDY #$0002  Puts level in $3965,AND currentSTAts in $3966-396A
# 03:9907 B1 80       LDA ($80),y
# 03:9909 8D 65 39    STA $3965
# 03:990C 7B          TDC
# 03:990D AA          TAX
# 03:990E A0 14 00    LDY #$0014
# 03:9911 B1 80       LDA ($80),y
# 03:9913 9D 66 39    STA $3966,x
# 03:9916 C8          INY
# 03:9917 E8          INX
# 03:9918 E0 05 00    CPX #$0005
# 03:991B D0 F4       BNE $9911 (-12 B)

# 03:991D 9C 6B 39    STZ $396B  Resets $396B-3974 to 0 -> used to add shields together
# 03:9920 9C 6C 39    STZ $396C
# 03:9923 9C 6D 39    STZ $396D
# 03:9926 9C 6E 39    STZ $396E
# 03:9929 9C 6F 39    STZ $396F
# 03:992C 9C 70 39    STZ $3970
# 03:992F 9C 71 39    STZ $3971
# 03:9932 9C 72 39    STZ $3972
# 03:9935 9C 73 39    STZ $3973
# 03:9938 9C 74 39    STZ $3974

# 03:993B A0 25 00    LDY #$0025  SetsAND adds defenseBITs for equipped shields in
# 03:993E B1 82       LDA ($82),y  rightAND left hANDs
# 03:9940 10 0C       BPL $994E (+12 B)
# 03:9942 A0 21 00    LDY #$0021
# 03:9945 20 AB 9D    JSR $9DAB
# 03:9948 AD 6D 39    LDA $396D
# 03:994B 8D 6E 39    STA $396E
# 03:994E A0 30 00    LDY #$0030
# 03:9951 B1 82       LDA ($82),y
# 03:9953 10 06       BPL $995B (+6 B)
# 03:9955 A0 2C 00    LDY #$002C
# 03:9958 20 AB 9D    JSR $9DAB

# 03:995B 7B          TDC   Puts the elemental defenseBITs for equipped items
# 03:995C AA          TAX   in $AD-B1
# 03:995D 86 A9       STX $A9
# 03:995F A0 08 00    LDY #$0008
# 03:9962 B1 82       LDA ($82),y
# 03:9964 95 AD       STA $AD,x
# 03:9966 98          TYA
# 03:9967 18          CLC
# 03:9968 69 0B       ADC #$0B
# 03:996A A8          TAY
# 03:996B E8          INX
# 03:996C E0 03 00    CPX #$0003
# 03:996F D0 F1       BNE $9962 (-15 B)
# 03:9971 AD 6D 39    LDA $396D
# 03:9974 85 B0       STA $B0
# 03:9976 AD 6E 39    LDA $396E
# 03:9979 85 B1       STA $B1

# 03:997B 7B          TDC   Adds together all the elementalBITs in $A9,SEParating
# 03:997C AA          TAX   the immuneBITs in $AA, then puts them in the current
# 03:997D B5 AD       LDA $AD,x  char'sSTAts
# 03:997F 30 06       BMI $9987 (+6 B)
# 03:9981 05 A9       ORA $A9
# 03:9983 85 A9       STA $A9
# 03:9985 80 04       BRA $998B (+4 B)
# 03:9987 05 AA       ORA $AA
# 03:9989 85 AA       STA $AA
# 03:998B E8          INX
# 03:998C E0 05 00    CPX #$0005
# 03:998F D0 EC       BNE $997D (-20 B)
# 03:9991 A0 25 00    LDY #$0025
# 03:9994 A5 A9       LDA $A9
# 03:9996 91 80       STA ($80),y
# 03:9998 C8          INY
# 03:9999 A5 AA       LDA $AA
# 03:999B 91 80       STA ($80),y

# 03:999D A0 05 00    LDY #$0005  Adds together all the racial defenseBITs, then puts them
# 03:99A0 7B          TDC   in the current char'sSTAts
# 03:99A1 AA          TAX
# 03:99A2 64 A9       STZ $A9
# 03:99A4 B1 82       LDA ($82),y
# 03:99A6 05 A9       ORA $A9
# 03:99A8 85 A9       STA $A9
# 03:99AA 98          TYA
# 03:99AB 18          CLC
# 03:99AC 69 0B       ADC #$0B
# 03:99AE A8          TAY
# 03:99AF E8          INX
# 03:99B0 E0 03 00    CPX #$0003
# 03:99B3 D0 EF       BNE $99A4 (-17 B)
# 03:99B5 A0 27 00    LDY #$0027
# 03:99B8 A5 A9       LDA $A9
# 03:99BA 0D 74 39    ORA $3974
# 03:99BD 91 80       STA ($80),y

# 03:99BF A0 28 00    LDY #$0028  Defense Multiplier = (lvl/16)*(shieldCount) + (Agi/8)
# 03:99C2 AD 65 39    LDA $3965
# 03:99C5 20 84 84    JSR $8484
# 03:99C8 85 DF       STA $DF
# 03:99CA AD 6B 39    LDA $396B
# 03:99CD 85 E1       STA $E1
# 03:99CF 20 E0 83    JSR $83E0
# 03:99D2 AD 67 39    LDA $3967
# 03:99D5 20 85 84    JSR $8485
# 03:99D8 18          CLC
# 03:99D9 65 E3       ADC $E3
# 03:99DB 91 80       STA ($80),y

# 03:99DD A0 02 00    LDY #$0002  (*****BUG!?!?*****)
# 03:99E0 7B          TDC   Adds the defense rates together for defensive eq.items,
# 03:99E1 AA          TAX   but forgets to reset $AA to 0 before. Currently contains
# 03:99E2 18          CLC   the elemental immuneBITs,INCludingBIT 7, making it
# 03:99E3 B1 82       LDA ($82),y  negative by at least 128. Then makes sure the value is
# 03:99E5 29 7F       AND #$7F  maximum 99,AND sets the value in the current char'sSTAts
# 03:99E7 65 AA       ADC $AA
# 03:99E9 85 AA       STA $AA
# 03:99EB 98          TYA
# 03:99EC 18          CLC
# 03:99ED 69 0B       ADC #$0B
# 03:99EF A8          TAY
# 03:99F0 E8          INX
# 03:99F1 E0 03 00    CPX #$0003
# 03:99F4 D0 EC       BNE $99E2 (-20 B)
# 03:99F6 A0 29 00    LDY #$0029
# 03:99F9 18          CLC
# 03:99FA A5 AA       LDA $AA
# 03:99FC 6D 6C 39    ADC $396C
# 03:99FF 20 20 9E    JSR $9E20
# 03:9A02 91 80       STA ($80),y

# 03:9A04 AD 68 39    LDA $3968  Defense Base = (vit/2) + sum(protection's defBase)
# 03:9A07 4A          LSR a   with a maximum of 255
# 03:9A08 85 A9       STA $A9
# 03:9A0A 7B          TDC
# 03:9A0B AA          TAX
# 03:9A0C A8          TAY
# 03:9A0D C8          INY
# 03:9A0E 18          CLC
# 03:9A0F B1 82       LDA ($82),y
# 03:9A11 65 A9       ADC $A9
# 03:9A13 85 A9       STA $A9
# 03:9A15 98          TYA
# 03:9A16 18          CLC
# 03:9A17 69 0B       ADC #$0B
# 03:9A19 A8          TAY
# 03:9A1A E8          INX
# 03:9A1B E0 03 00    CPX #$0003
# 03:9A1E D0 EE       BNE $9A0E (-18 B)
# 03:9A20 A0 2A 00    LDY #$002A
# 03:9A23 18          CLC
# 03:9A24 A5 A9       LDA $A9
# 03:9A26 6D 6F 39    ADC $396F
# 03:9A29 20 27 9E    JSR $9E27
# 03:9A2C 91 80       STA ($80),y

# 03:9A2E 7B          TDC   Adds theSTAtus defenseBITs together for protection items
# 03:9A2F AA          TAXAND stores it in the current char'sSTAts
# 03:9A30 86 A9       STX $A9
# 03:9A32 A0 09 00    LDY #$0009
# 03:9A35 B1 82       LDA ($82),y
# 03:9A37 05 A9       ORA $A9
# 03:9A39 85 A9       STA $A9
# 03:9A3B C8          INY
# 03:9A3C B1 82       LDA ($82),y
# 03:9A3E 05 AA       ORA $AA
# 03:9A40 85 AA       STA $AA
# 03:9A42 98          TYA
# 03:9A43 18          CLC
# 03:9A44 69 0A       ADC #$0A
# 03:9A46 A8          TAY
# 03:9A47 E8          INX
# 03:9A48 E0 03 00    CPX #$0003
# 03:9A4B D0 E8       BNE $9A35 (-24 B)
# 03:9A4D A0 2B 00    LDY #$002B
# 03:9A50 A5 A9       LDA $A9
# 03:9A52 0D 70 39    ORA $3970
# 03:9A55 91 80       STA ($80),y
# 03:9A57 C8          INY
# 03:9A58 A5 AA       LDA $AA
# 03:9A5A 0D 71 39    ORA $3971
# 03:9A5D 91 80       STA ($80),y

# 03:9A5F A0 25 00    LDY #$0025  Calculates the elemental weaknesses. ElemRes(x0.5) results
# 03:9A62 B1 80       LDA ($80),y  in ElemRes(x2), while ElemRes(x0) results in ElemRes(x4)
# 03:9A64 85 A9       STA $A9
# 03:9A66 20 9E 9D    JSR $9D9E
# 03:9A69 A0 20 00    LDY #$0020
# 03:9A6C 91 80       STA ($80),y
# 03:9A6E A0 26 00    LDY #$0026
# 03:9A71 B1 80       LDA ($80),y
# 03:9A73 85 A9       STA $A9
# 03:9A75 20 9E 9D    JSR $9D9E
# 03:9A78 F0 07       BEQ $9A81 (+7 B)
# 03:9A7A A0 21 00    LDY #$0021
# 03:9A7D 09 80       ORA #$80
# 03:9A7F 91 80       STA ($80),y

# 03:9A81 A0 22 00    LDY #$0022  Magical Defense Multiplier = ((wis+will)/32) + (vit/32)
# 03:9A84 18          CLC
# 03:9A85 AD 69 39    LDA $3969
# 03:9A88 6D 6A 39    ADC $396A
# 03:9A8B 85 AA       STA $AA
# 03:9A8D 20 83 84    JSR $8483
# 03:9A90 85 A9       STA $A9
# 03:9A92 AD 67 39    LDA $3967
# 03:9A95 20 83 84    JSR $8483
# 03:9A98 18          CLC
# 03:9A99 65 A9       ADC $A9
# 03:9A9B 91 80       STA ($80),y

# 03:9A9D A5 AA       LDA $AA  MagDef Rate = ((wis+will)/8) + sum(prot's magDefRate)
# 03:9A9F 20 85 84    JSR $8485  Max 99
# 03:9AA2 85 A9       STA $A9
# 03:9AA4 7B          TDC
# 03:9AA5 A8          TAY
# 03:9AA6 AA          TAX
# 03:9AA7 18          CLC
# 03:9AA8 B1 82       LDA ($82),y
# 03:9AAA 29 7F       AND #$7F
# 03:9AAC 65 A9       ADC $A9
# 03:9AAE 85 A9       STA $A9
# 03:9AB0 98          TYA
# 03:9AB1 18          CLC
# 03:9AB2 69 0B       ADC #$0B
# 03:9AB4 A8          TAY
# 03:9AB5 E8          INX
# 03:9AB6 E0 03 00    CPX #$0003
# 03:9AB9 D0 EC       BNE $9AA7 (-20 B)
# 03:9ABB A0 23 00    LDY #$0023
# 03:9ABE 18          CLC
# 03:9ABF A5 A9       LDA $A9
# 03:9AC1 6D 72 39    ADC $3972
# 03:9AC4 20 20 9E    JSR $9E20
# 03:9AC7 91 80       STA ($80),y

# 03:9AC9 A0 03 00    LDY #$0003  MagDef Base = sum(prot's magdefbase)
# 03:9ACC 7B          TDC
# 03:9ACD AA          TAX
# 03:9ACE 86 A9       STX $A9
# 03:9AD0 18          CLC
# 03:9AD1 B1 82       LDA ($82),y
# 03:9AD3 65 A9       ADC $A9
# 03:9AD5 85 A9       STA $A9
# 03:9AD7 98          TYA
# 03:9AD8 18          CLC
# 03:9AD9 69 0B       ADC #$0B
# 03:9ADB A8          TAY
# 03:9ADC E8          INX
# 03:9ADD E0 03 00    CPX #$0003
# 03:9AE0 D0 EE       BNE $9AD0 (-18 B)
# 03:9AE2 A0 24 00    LDY #$0024
# 03:9AE5 18          CLC
# 03:9AE6 A5 A9       LDA $A9
# 03:9AE8 6D 73 39    ADC $3973
# 03:9AEB 20 27 9E    JSR $9E27
# 03:9AEE 91 80       STA ($80),y

# 03:9AF0 A2 15 00    LDX #$0015  Clears the temp bytes used for the head, body,AND
# 03:9AF3 9E 9C 28    STZ $289C,x  hANDs' items dataAND elem/STAtus bytes
# 03:9AF6 CA          DEX
# 03:9AF7 10 FA       BPL $9AF3 (-6 B)

# 03:9AF9 9C 77 39    STZ $3977  Sets $3977.0 if current char is a Monk
# 03:9AFC A0 01 00    LDY #$0001
# 03:9AFF B1 80       LDA ($80),y
# 03:9B01 29 0F       AND #$0F
# 03:9B03 C9 06       CMP #$06
# 03:9B05 D0 03       BNE $9B0A (+3 B)
# 03:9B07 EE 77 39    INC $3977

# 03:9B0A A0 33 00    LDY #$0033  Sets $3977.7 if RHAND has a weapon
# 03:9B0D B1 80       LDA ($80),y
# 03:9B0F F0 20       BEQ $9B31 (+32 B)
# 03:9B11 A0 25 00    LDY #$0025
# 03:9B14 B1 82       LDA ($82),y
# 03:9B16 30 19       BMI $9B31 (+25 B)
# 03:9B18 AD 77 39    LDA $3977
# 03:9B1B 09 80       ORA #$80
# 03:9B1D 8D 77 39    STA $3977
# 03:9B20 A0 21 00    LDY #$0021
# 03:9B23 7B          TDC
# 03:9B24 AA          TAX
# 03:9B25 B1 82       LDA ($82),y
# 03:9B27 9D 9C 28    STA $289C,x
# 03:9B2A C8          INY
# 03:9B2B E8          INX
# 03:9B2C E0 0B 00    CPX #$000B
# 03:9B2F D0 F4       BNE $9B25 (-12 B)

# 03:9B31 A0 35 00    LDY #$0035   Sets $3977.6 if LHAND has a weapon
# 03:9B34 B1 80       LDA ($80),y
# 03:9B36 F0 21       BEQ $9B59 (+33 B)
# 03:9B38 A0 30 00    LDY #$0030
# 03:9B3B B1 82       LDA ($82),y
# 03:9B3D 30 1A       BMI $9B59 (+26 B)
# 03:9B3F AD 77 39    LDA $3977
# 03:9B42 09 40       ORA #$40
# 03:9B44 8D 77 39    STA $3977
# 03:9B47 A0 2C 00    LDY #$002C
# 03:9B4A A2 0B 00    LDX #$000B
# 03:9B4D B1 82       LDA ($82),y
# 03:9B4F 9D 9C 28    STA $289C,x
# 03:9B52 C8          INY
# 03:9B53 E8          INX
# 03:9B54 E0 16 00    CPX #$0016
# 03:9B57 D0 F4       BNE $9B4D (-12 B)

# 03:9B59 AD 77 39    LDA $3977   Determines what kind of weapon configuration
# 03:9B5C 29 C0       AND #$C0   the current character has
# 03:9B5E F0 09       BEQ $9B69 (+9 B)
# 03:9B60 49 C0       EOR #$C0
# 03:9B62 D0 55       BNE $9BB9 (+85 B)
# 03:9B64 AD A2 28    LDA $28A2
# 03:9B67 29 C0       AND #$C0
# 03:9B69 F0 4C       BEQ $9BB7 (+76 B)

# 03:9B6B A9 80       LDA #$80   [Bow/Arrow]
# 03:9B6D 8D 78 39    STA $3978   If character has a bow in the LHAND, switch
# 03:9B70 AD A2 28    LDA $28A2   the items' data so that the bow is in the RHAND
# 03:9B73 29 80       AND #$80AND set $3977.5
# 03:9B75 D0 36       BNE $9BAD (+54 B)
# 03:9B77 A9 40       LDA #$40
# 03:9B79 8D 78 39    STA $3978
# 03:9B7C 7B          TDC
# 03:9B7D AA          TAX
# 03:9B7E A0 16 00    LDY #$0016
# 03:9B81 BD 9C 28    LDA $289C,x
# 03:9B84 99 9C 28    STA $289C,y
# 03:9B87 E8          INX
# 03:9B88 C8          INY
# 03:9B89 E0 0B 00    CPX #$000B
# 03:9B8C D0 F3       BNE $9B81 (-13 B)
# 03:9B8E 7B          TDC
# 03:9B8F A8          TAY
# 03:9B90 BD 9C 28    LDA $289C,x
# 03:9B93 99 9C 28    STA $289C,y
# 03:9B96 E8          INX
# 03:9B97 C8          INY
# 03:9B98 E0 16 00    CPX #$0016
# 03:9B9B D0 F3       BNE $9B90 (-13 B)
# 03:9B9D A0 0B 00    LDY #$000B
# 03:9BA0 BD 9C 28    LDA $289C,x
# 03:9BA3 99 9C 28    STA $289C,y
# 03:9BA6 E8          INX
# 03:9BA7 C8          INY
# 03:9BA8 C0 16 00    CPY #$0016
# 03:9BAB D0 F3       BNE $9BA0 (-13 B)
# 03:9BAD AD 77 39    LDA $3977
# 03:9BB0 29 3F       AND #$3F
# 03:9BB2 09 A0       ORA #$A0
# 03:9BB4 8D 77 39    STA $3977
# 03:9BB7 80 2B       BRA $9BE4 (+43 B)

# 03:9BB9 AD 77 39    LDA $3977   [Only one weapon]
# 03:9BBC 30 26       BMI $9BE4 (+38 B)  If weapon is in LHAND, puts it in the RHAND,AND
# 03:9BBE A2 0B 00    LDX #$000B   resets the LHAND to 0
# 03:9BC1 7B          TDC
# 03:9BC2 A8          TAY
# 03:9BC3 BD 9C 28    LDA $289C,x
# 03:9BC6 99 9C 28    STA $289C,y
# 03:9BC9 E8          INX
# 03:9BCA C8          INY
# 03:9BCB C0 0B 00    CPY #$000B
# 03:9BCE D0 F3       BNE $9BC3 (-13 B)
# 03:9BD0 BB          TYX
# 03:9BD1 9E 9C 28    STZ $289C,x
# 03:9BD4 E8          INX
# 03:9BD5 E0 16 00    CPX #$0016
# 03:9BD8 D0 F7       BNE $9BD1 (-9 B)
# 03:9BDA AD 77 39    LDA $3977
# 03:9BDD 29 3F       AND #$3F
# 03:9BDF 09 80       ORA #$80
# 03:9BE1 8D 77 39    STA $3977

# 03:9BE4 7B          TDC   [One or Two Weapons]
# 03:9BE5 AA          TAX   Puts the elemental offenseBITs of weapons in curChar's
# 03:9BE6 86 A9       STX $A9STAts
# 03:9BE8 AD A0 28    LDA $28A0
# 03:9BEB 30 05       BMI $9BF2 (+5 B)
# 03:9BED AD A4 28    LDA $28A4
# 03:9BF0 85 A9       STA $A9
# 03:9BF2 AD AB 28    LDA $28AB
# 03:9BF5 30 05       BMI $9BFC (+5 B)
# 03:9BF7 AD AF 28    LDA $28AF
# 03:9BFA 85 AA       STA $AA
# 03:9BFC A5 A9       LDA $A9
# 03:9BFE 05 AA       ORA $AA
# 03:9C00 A0 19 00    LDY #$0019
# 03:9C03 91 80       STA ($80),y

# 03:9C05 7B          TDC   Puts the racial offenseBITs of weapons in curChar's
# 03:9C06 AA          TAXSTAts
# 03:9C07 86 A9       STX $A9
# 03:9C09 AD A0 28    LDA $28A0
# 03:9C0C 30 05       BMI $9C13 (+5 B)
# 03:9C0E AD A1 28    LDA $28A1
# 03:9C11 85 A9       STA $A9
# 03:9C13 AD AB 28    LDA $28AB
# 03:9C16 30 05       BMI $9C1D (+5 B)
# 03:9C18 AD AC 28    LDA $28AC
# 03:9C1B 85 AA       STA $AA
# 03:9C1D A5 A9       LDA $A9
# 03:9C1F 05 AA       ORA $AA
# 03:9C21 A0 1A 00    LDY #$001A
# 03:9C24 91 80       STA ($80),y

# 03:9C26 AD 66 39    LDA $3966PHYsicalOffense Multiplier = (Str/8) + (Agi/16) + 1
# 03:9C29 20 85 84    JSR $8485
# 03:9C2C 85 A9       STA $A9
# 03:9C2E AD 67 39    LDA $3967
# 03:9C31 20 84 84    JSR $8484
# 03:9C34 18          CLC
# 03:9C35 65 A9       ADC $A9
# 03:9C37 1A          INC a
# 03:9C38 A0 1B 00    LDY #$001B
# 03:9C3B 91 80       STA ($80),y

# 03:9C3D AD 65 39    LDA $3965PHYsical Offense Rate = (lvl/16) +
# 03:9C40 20 86 84    JSR $8486  If two weapons equipped, + RHAND's Rate + (lvl/16) +
# 03:9C43 85 A9       STA $A9    LHAND's Rate
# 03:9C45 AD 77 39    LDA $3977  If no weapon equipped, + Weapon(0).Rate
# 03:9C48 29 C0       AND #$C0  If only one weapon equipped, + RHAND's Rate
# 03:9C4A F0 21       BEQ $9C6D (+33 B) Max 99
# 03:9C4C 49 C0       EOR #$C0
# 03:9C4E D0 26       BNE $9C76 (+38 B)
# 03:9C50 18          CLC
# 03:9C51 AD 9E 28    LDA $289E
# 03:9C54 29 7F       AND #$7F
# 03:9C56 65 A9       ADC $A9
# 03:9C58 85 AA       STA $AA
# 03:9C5A 18          CLC
# 03:9C5B AD A9 28    LDA $28A9
# 03:9C5E 29 7F       AND #$7F
# 03:9C60 65 A9       ADC $A9
# 03:9C62 65 AA       ADC $AA
# 03:9C64 4A          LSR a
# 03:9C65 C9 63       CMP #$63
# 03:9C67 90 18       BCC $9C81 (+24 B)
# 03:9C69 A9 63       LDA #$63
# 03:9C6B 80 14       BRA $9C81 (+20 B)
# 03:9C6D 18          CLC
# 03:9C6E AF 02 91 0F LDA $0F9102
# 03:9C72 65 A9       ADC $A9
# 03:9C74 80 08       BRA $9C7E (+8 B)
# 03:9C76 18          CLC
# 03:9C77 AD 9E 28    LDA $289E
# 03:9C7A 29 7F       AND #$7F
# 03:9C7C 65 A9       ADC $A9
# 03:9C7E 20 20 9E    JSR $9E20
# 03:9C81 A0 1C 00    LDY #$001C
# 03:9C84 91 80       STA ($80),y

# 03:9C86 AD 66 39    LDA $3966PHYsical Offense Base = (Str/16) +
# 03:9C89 20 86 84    JSR $8486  If char is a monk, + (lvl*2) + 2
# 03:9C8C 85 AA       STA $AA  If two weapons equipped, + RHAND's Base + 2*(lvl/16) +
# 03:9C8E AD 77 39    LDA $3977    (Str/16) + LHAND's Base
# 03:9C91 29 01       AND #$01  If only one weapon equipped, + RHAND's Base
# 03:9C93 F0 0B       BEQ $9CA0 (+11 B) If RHAND has bow, (RHAND's base/2) + LHAND's base
# 03:9C95 AD 65 39    LDA $3965    (If wrong bow/arrow config), - (((RHB/2)+(LHB))/5)
# 03:9C98 0A          ASL a   If no weapon equipped, + Weapon(0).Base
# 03:9C99 18          CLC   Max 255
# 03:9C9A 65 AA       ADC $AA
# 03:9C9C 69 02       ADC #$02
# 03:9C9E 80 66       BRA $9D06 (+102 B)
# 03:9CA0 AD 77 39    LDA $3977
# 03:9CA3 29 C0       AND #$C0
# 03:9CA5 F0 57       BEQ $9CFE (+87 B)
# 03:9CA7 49 C0       EOR #$C0
# 03:9CA9 D0 17       BNE $9CC2 (+23 B)
# 03:9CAB 18          CLC
# 03:9CAC AD 9D 28    LDA $289D
# 03:9CAF 65 A9       ADC $A9
# 03:9CB1 65 AA       ADC $AA
# 03:9CB3 85 AB       STA $AB
# 03:9CB5 18          CLC
# 03:9CB6 AD A8 28    LDA $28A8
# 03:9CB9 65 A9       ADC $A9
# 03:9CBB 65 AA       ADC $AA
# 03:9CBD 18          CLC
# 03:9CBE 65 AB       ADC $AB
# 03:9CC0 80 44       BRA $9D06 (+68 B)
# 03:9CC2 AD 77 39    LDA $3977
# 03:9CC5 29 20       AND #$20
# 03:9CC7 F0 35       BEQ $9CFE (+53 B)
# 03:9CC9 AD 9D 28    LDA $289D
# 03:9CCC 4A          LSR a
# 03:9CCD 18          CLC
# 03:9CCE 6D A8 28    ADC $28A8
# 03:9CD1 65 AA       ADC $AA
# 03:9CD3 85 BF       STA $BF
# 03:9CD5 A0 00 00    LDY #$0000
# 03:9CD8 B1 80       LDA ($80),y
# 03:9CDA 29 C0       AND #$C0
# 03:9CDC 2D 78 39    AND $3978
# 03:9CDF F0 19       BEQ $9CFA (+25 B)
# 03:9CE1 A5 BF       LDA $BF
# 03:9CE3 AA          TAX
# 03:9CE4 8E 45 39    STX $3945
# 03:9CE7 A2 05 00    LDX #$0005
# 03:9CEA 8E 47 39    STX $3947
# 03:9CED 20 07 84    JSR $8407
# 03:9CF0 38          SEC
# 03:9CF1 A5 BF       LDA $BF
# 03:9CF3 ED 49 39    SBC $3949
# 03:9CF6 85 BF       STA $BF
# 03:9CF8 80 0F       BRA $9D09 (+15 B)
# 03:9CFA A5 BF       LDA $BF
# 03:9CFC 80 0B       BRA $9D09 (+11 B)
# 03:9CFE 18          CLC
# 03:9CFF AD 9D 28    LDA $289D
# 03:9D02 65 A9       ADC $A9
# 03:9D04 65 AA       ADC $AA
# 03:9D06 20 27 9E    JSR $9E27
# 03:9D09 A0 1D 00    LDY #$001D
# 03:9D0C 91 80       STA ($80),y

# 03:9D0E AD A5 28    LDA $28A5  Puts theSTAtus OffenseBITs of eq.weapons inSTAts
# 03:9D11 0D B0 28    ORA $28B0
# 03:9D14 A0 1E 00    LDY #$001E
# 03:9D17 91 80       STA ($80),y
# 03:9D19 AD A6 28    LDA $28A6
# 03:9D1C 0D B1 28    ORA $28B1
# 03:9D1F C8          INY
# 03:9D20 91 80       STA ($80),y

# 03:9D22 A0 41 00    LDY #$0041  What are $2DAND $2E??
# 03:9D25 B1 80       LDA ($80),y
# 03:9D27 85 A9       STA $A9
# 03:9D29 C8          INY
# 03:9D2A B1 80       LDA ($80),y
# 03:9D2C 85 AA       STA $AA
# 03:9D2E AD 77 39    LDA $3977
# 03:9D31 29 C0       AND #$C0
# 03:9D33 F0 51       BEQ $9D86 (+81 B) If no weapon or two weapons, don't chance $2DAND $2E
# 03:9D35 49 C0       EOR #$C0
# 03:9D37 F0 4D       BEQ $9D86 (+77 B)
# 03:9D39 AD 77 39    LDA $3977
# 03:9D3C 29 20       AND #$20
# 03:9D3E F0 22       BEQ $9D62 (+34 B)
# 03:9D40 A5 A9       LDA $A9  If using bow/arrow,
# 03:9D42 C9 21       CMP #$21  $2D = ($41 max #$21) * 3
# 03:9D44 90 02       BCC $9D48 (+2 B) $2E = $42 + (arrow's attack base), max 255
# 03:9D46 A9 21       LDA #$21
# 03:9D48 85 DF       STA $DF
# 03:9D4A A9 03       LDA #$03
# 03:9D4C 85 E1       STA $E1
# 03:9D4E 20 E0 83    JSR $83E0
# 03:9D51 A5 E3       LDA $E3
# 03:9D53 85 A9       STA $A9
# 03:9D55 18          CLC
# 03:9D56 A5 AA       LDA $AA
# 03:9D58 6D A8 28    ADC $28A8
# 03:9D5B 20 27 9E    JSR $9E27
# 03:9D5E 85 AA       STA $AA
# 03:9D60 80 18       BRA $9D7A (+24 B)
# 03:9D62 06 A9       ASL $A9  If only one weapon,
# 03:9D64 90 04       BCC $9D6A (+4 B) $2D = $41*2, max 99
# 03:9D66 A9 63       LDA #$63  $2E = $42 + (weapon's attack base / 2), max 255
# 03:9D68 80 02       BRA $9D6C (+2 B)
# 03:9D6A A5 A9       LDA $A9
# 03:9D6C 85 A9       STA $A9
# 03:9D6E AD 9D 28    LDA $289D
# 03:9D71 4A          LSR a
# 03:9D72 18          CLC
# 03:9D73 65 AA       ADC $AA
# 03:9D75 20 27 9E    JSR $9E27
# 03:9D78 85 AA       STA $AA
# 03:9D7A A0 2D 00    LDY #$002D
# 03:9D7D A5 A9       LDA $A9
# 03:9D7F 91 80       STA ($80),y
# 03:9D81 C8          INY
# 03:9D82 A5 AA       LDA $AA
# 03:9D84 91 80       STA ($80),y

# 03:9D86 AD 77 39    LDA $3977
# 03:9D89 29 20       AND #$20
# 03:9D8B D0 07       BNE $9D94 (+7 B)
# 03:9D8D AD 9C 28    LDA $289C  If not bow/arrow, only keep RHAND's b5
# 03:9D90 29 20       AND #$20
# 03:9D92 F0 09       BEQ $9D9D (+9 B)
# 03:9D94 A0 01 00    LDY #$0001  If bow/arrow, set B1b5 in current char'sSTAts
# 03:9D97 B1 80       LDA ($80),y
# 03:9D99 09 20       ORA #$20
# 03:9D9B 91 80       STA ($80),y

# 03:9D9D 60          RTS


# Calculates the elemental weaknesses of fire, ice, darkness,AND sacred. aAND $A9 are essentially the same elemental defenseBITs, a is the elemental weakness result.
# Input: a, $A9
# Output: a

# 03:9D9E 29 12       AND #$12
# 03:9DA0 4A          LSR a
# 03:9DA1 85 AA       STA $AA
# 03:9DA3 A5 A9       LDA $A9
# 03:9DA5 29 09       AND #$09
# 03:9DA7 0A          ASL a
# 03:9DA8 05 AA       ORA $AA
# 03:9DAA 60          RTS


# 03:9DAB EE 6B 39    INC $396B  Adds bytes for current char's equipped items (defense)
# 03:9DAE 18          CLC   ($82) is offset for current item's data + elem/STAtus data
# 03:9DAF B1 82       LDA ($82),y
# 03:9DB1 29 7F       AND #$7F
# 03:9DB3 6D 72 39    ADC $3972
# 03:9DB6 8D 72 39    STA $3972
# 03:9DB9 C8          INY
# 03:9DBA 18          CLC
# 03:9DBB B1 82       LDA ($82),y
# 03:9DBD 6D 6F 39    ADC $396F
# 03:9DC0 8D 6F 39    STA $396F
# 03:9DC3 C8          INY
# 03:9DC4 18          CLC
# 03:9DC5 B1 82       LDA ($82),y
# 03:9DC7 29 7F       AND #$7F
# 03:9DC9 6D 6C 39    ADC $396C
# 03:9DCC 8D 6C 39    STA $396C
# 03:9DCF C8          INY
# 03:9DD0 18          CLC
# 03:9DD1 B1 82       LDA ($82),y
# 03:9DD3 6D 73 39    ADC $3973
# 03:9DD6 8D 73 39    STA $3973
# 03:9DD9 C8          INY
# 03:9DDA C8          INY
# 03:9DDB B1 82       LDA ($82),y
# 03:9DDD 0D 74 39    ORA $3974
# 03:9DE0 8D 74 39    STA $3974
# 03:9DE3 C8          INY
# 03:9DE4 C8          INY
# 03:9DE5 C8          INY
# 03:9DE6 B1 82       LDA ($82),y
# 03:9DE8 8D 6D 39    STA $396D
# 03:9DEB C8          INY
# 03:9DEC B1 82       LDA ($82),y
# 03:9DEE 0D 70 39    ORA $3970
# 03:9DF1 8D 70 39    STA $3970
# 03:9DF4 C8          INY
# 03:9DF5 B1 82       LDA ($82),y
# 03:9DF7 0D 71 39    ORA $3971
# 03:9DFA 8D 71 39    STA $3971
# 03:9DFD 60          RTS


# 03:9DFE AD 75 39    LDA $3975
# 03:9E01 20 7E 84    JSR $847E
# 03:9E04 A8          TAY
# 03:9E05 A6 A6       LDX $A6
# 03:9E07 B9 DB 32    LDA $32DB,y
# 03:9E0A 9D 33 20    STA $2033,x
# 03:9E0D B9 DC 32    LDA $32DC,y
# 03:9E10 9D 34 20    STA $2034,x
# 03:9E13 B9 DF 32    LDA $32DF,y
# 03:9E16 9D 35 20    STA $2035,x
# 03:9E19 B9 E0 32    LDA $32E0,y
# 03:9E1C 9D 36 20    STA $2036,x
# 03:9E1F 60          RTS


# Makes sure a value is less than or equal to 99.
# Input: a
# Output: a

# 03:9E20 C9 63       CMP #$63
# 03:9E22 90 02       BCC $9E26 (+2 B)
# 03:9E24 A9 63       LDA #$63
# 03:9E26 60          RTS


# If the carry is set, puts a to 255.
# Input: a
# Output: a

# 03:9E27 90 02       BCC $9E2B (+2 B)
# 03:9E29 A9 FF       LDA #$FF
# 03:9E2B 60          RTS


# 03:9E2C 9C 58 35    STZ $3558
# 03:9E2F C9 05       CMP #$05   If analyzing an enemy, 3558 = 1
# 03:9E31 90 03       BCC $9E36 (+3 B)
# 03:9E33 EE 58 35    INC $3558
# 03:9E36 20 89 84    JSR $8489   Recalculates common offsets for current char/enemy

# 03:9E39 A6 A6       LDX $A6   Copies the relative speed to $A9-AA
# 03:9E3B BD 60 20    LDA $2060,x
# 03:9E3E 85 A9       STA $A9
# 03:9E40 BD 61 20    LDA $2061,x
# 03:9E43 85 AA       STA $AA
# 03:9E45 BD 3B 20    LDA $203B,x
# 03:9E48 A8          TAY
# 03:9E49 8C 79 39    STY $3979

# 03:9E4C DA          PHX    Goes to a target subroutine depending on $D6
# 03:9E4D A5 D6       LDA $D6   (From $9625, $D6 = #$06)
# 03:9E4F 0A          ASL a    (From $8E22, $D6 = #$00)
# 03:9E50 AA          TAX
# 03:9E51 BF FB 9F 03 LDA $039FFB,x
# 03:9E55 85 80       STA $80
# 03:9E57 BF FC 9F 03 LDA $039FFC,x
# 03:9E5B 85 81       STA $81
# 03:9E5D A9 03       LDA #$03
# 03:9E5F 85 82       STA $82
# 03:9E61 FA          PLX
# 03:9E62 DC 80 00    JML ($0080)

#JML Target, depending on $D6


# 03:9E65 20 D8 9F    JSR $9FD8
# 03:9E68 A4 AB       LDY $AB
# 03:9E6A D0 02       BNE $9E6E (+2 B)
# 03:9E6C E6 AB       INC $AB
# 03:9E6E 4C CF 9F    JMP $9FCF

# 03:9E71 AD 58 35    LDA $3558
# 03:9E74 D0 04       BNE $9E7A (+4 B)
# 03:9E76 7B          TDC
# 03:9E77 AA          TAX
# 03:9E78 80 03       BRA $9E7D (+3 B)
# 03:9E7A A2 01 00    LDX #$0001
# 03:9E7D 86 A9       STX $A9
# 03:9E7F 20 D8 9F    JSR $9FD8
# 03:9E82 4C CF 9F    JMP $9FCF

# 03:9E85 AD 7B 39    LDA $397B
# 03:9E88 85 DF       STA $DF
# 03:9E8A A9 06       LDA #$06
# 03:9E8C 85 E1       STA $E1
# 03:9E8E 20 E0 83    JSR $83E0
# 03:9E91 A6 E3       LDX $E3
# 03:9E93 BF 80 96 0F LDA $0F9680,x
# 03:9E97 80 12       BRA $9EAB (+18 B)

# 03:9E99 AD 7B 39    LDA $397B
# 03:9E9C 85 DF       STA $DF
# 03:9E9E A9 06       LDA #$06
# 03:9EA0 85 E1       STA $E1
# 03:9EA2 20 E0 83    JSR $83E0
# 03:9EA5 A6 E3       LDX $E3
# 03:9EA7 BF A0 97 0F LDA $0F97A0,x
# 03:9EAB 29 1F       AND #$1F
# 03:9EAD AA          TAX
# 03:9EAE 86 A9       STX $A9
# 03:9EB0 06 A9       ASL $A9
# 03:9EB2 26 AA       ROL $AA
# 03:9EB4 AD 8B 38    LDA $388B
# 03:9EB7 F0 04       BEQ $9EBD (+4 B)
# 03:9EB9 7B          TDC
# 03:9EBA AA          TAX
# 03:9EBB 86 A9       STX $A9
# 03:9EBD 20 D8 9F    JSR $9FD8
# 03:9EC0 4C CF 9F    JMP $9FCF

# 03:9EC3 AD 58 35    LDA $3558
# 03:9EC6 F0 05       BEQ $9ECD (+5 B)
# 03:9EC8 BD 2F 20    LDA $202F,x
# 03:9ECB 80 03       BRA $9ED0 (+3 B)
# 03:9ECD BD 18 20    LDA $2018,x
# 03:9ED0 85 AD       STA $AD
# 03:9ED2 06 AD       ASL $AD
# 03:9ED4 06 AD       ASL $AD
# 03:9ED6 38          SEC
# 03:9ED7 A9 2C       LDA #$2C
# 03:9ED9 E5 AD       SBC $AD
# 03:9EDB 85 A9       STA $A9
# 03:9EDD A9 01       LDA #$01
# 03:9EDF E9 00       SBC #$00
# 03:9EE1 85 AA       STA $AA
# 03:9EE3 B0 05       BCS $9EEA (+5 B)
# 03:9EE5 A2 01 00    LDX #$0001
# 03:9EE8 86 A9       STX $A9
# 03:9EEA 20 D8 9F    JSR $9FD8
# 03:9EED A6 AB       LDX $AB
# 03:9EEF 8E 45 39    STX $3945
# 03:9EF2 A2 06 00    LDX #$0006
# 03:9EF5 8E 47 39    STX $3947
# 03:9EF8 20 07 84    JSR $8407
# 03:9EFB AE 49 39    LDX $3949
# 03:9EFE 86 AB       STX $AB
# 03:9F00 4C CF 9F    JMP $9FCF

# 03:9F03 AD 58 35    LDA $3558   If char, takes Current Vitality, if enemy,
# 03:9F06 F0 05       BEQ $9F0D (+5 B)  takes slot's 2F. Adds #$14 to it, stores in $A9.
# 03:9F08 BD 2F 20    LDA $202F,x
# 03:9F0B 80 03       BRA $9F10 (+3 B)
# 03:9F0D BD 16 20    LDA $2016,x
# 03:9F10 18          CLC
# 03:9F11 69 14       ADC #$14
# 03:9F13 AA          TAX
# 03:9F14 86 A9       STX $A9
# 03:9F16 20 D8 9F    JSR $9FD8
# 03:9F19 4C CF 9F    JMP $9FCF

# 03:9F1C AD 7B 39    LDA $397B
# 03:9F1F 85 AD       STA $AD
# 03:9F21 64 AE       STZ $AE
# 03:9F23 06 AD       ASL $AD
# 03:9F25 26 AE       ROL $AE
# 03:9F27 18          CLC
# 03:9F28 A5 AD       LDA $AD
# 03:9F2A 69 1E       ADC #$1E
# 03:9F2C 85 A9       STA $A9
# 03:9F2E A5 AE       LDA $AE
# 03:9F30 69 00       ADC #$00
# 03:9F32 85 AA       STA $AA
# 03:9F34 20 D8 9F    JSR $9FD8
# 03:9F37 4C CF 9F    JMP $9FCF

# 03:9F3A AD 58 35    LDA $3558
# 03:9F3D F0 0B       BEQ $9F4A (+11 B)
# 03:9F3F BD 2F 20    LDA $202F,x
# 03:9F42 85 AD       STA $AD
# 03:9F44 A9 04       LDA #$04
# 03:9F46 85 AE       STA $AE
# 03:9F48 80 0D       BRA $9F57 (+13 B)
# 03:9F4A 18          CLC
# 03:9F4B BD 17 20    LDA $2017,x
# 03:9F4E 7D 18 20    ADC $2018,x
# 03:9F51 85 AD       STA $AD
# 03:9F53 A9 02       LDA #$02
# 03:9F55 85 AE       STA $AE
# 03:9F57 A5 AD       LDA $AD
# 03:9F59 85 DF       STA $DF
# 03:9F5B A5 AE       LDA $AE
# 03:9F5D 85 E2       STA $E2
# 03:9F5F 20 E0 83    JSR $83E0
# 03:9F62 18          CLC
# 03:9F63 A5 E3       LDA $E3
# 03:9F65 69 1E       ADC #$1E
# 03:9F67 85 A9       STA $A9
# 03:9F69 A5 E4       LDA $E4
# 03:9F6B 69 00       ADC #$00
# 03:9F6D 85 AA       STA $AA
# 03:9F6F 20 D8 9F    JSR $9FD8
# 03:9F72 4C CF 9F    JMP $9FCF

# 03:9F75 AD 7B 39    LDA $397B
# 03:9F78 85 DF       STA $DF
# 03:9F7A A9 03       LDA #$03
# 03:9F7C 85 E1       STA $E1
# 03:9F7E 20 E0 83    JSR $83E0
# 03:9F81 A6 E3       LDX $E3
# 03:9F83 86 A9       STX $A9
# 03:9F85 20 D8 9F    JSR $9FD8
# 03:9F88 4C CF 9F    JMP $9FCF

# 03:9F8B AD 7B 39    LDA $397B
# 03:9F8E C9 06       CMP #$06
# 03:9F90 D0 05       BNE $9F97 (+5 B)
# 03:9F92 A2 04 00    LDX #$0004
# 03:9F95 80 33       BRA $9FCA (+51 B)
# 03:9F97 AA          TAX
# 03:9F98 BF 89 FE 13 LDA $13FE89,x
# 03:9F9C D0 03       BNE $9FA1 (+3 B)
# 03:9F9E 4C 71 9E    JMP $9E71
# 03:9FA1 10 15       BPL $9FB8 (+21 B)
# 03:9FA3 29 7F       AND #$7F
# 03:9FA5 AA          TAX
# 03:9FA6 8E 3F 39    STX $393F
# 03:9FA9 A6 A9       LDX $A9
# 03:9FAB 8E 3D 39    STX $393D
# 03:9FAE 20 B9 83    JSR $83B9
# 03:9FB1 AE 41 39    LDX $3941
# 03:9FB4 86 A9       STX $A9
# 03:9FB6 80 14       BRA $9FCC (+20 B)
# 03:9FB8 AA          TAX
# 03:9FB9 8E 47 39    STX $3947
# 03:9FBC A6 A9       LDX $A9
# 03:9FBE 8E 45 39    STX $3945
# 03:9FC1 20 07 84    JSR $8407
# 03:9FC4 AE 49 39    LDX $3949
# 03:9FC7 D0 01       BNE $9FCA (+1 B)
# 03:9FC9 E8          INX
# 03:9FCA 86 A9       STX $A9
# 03:9FCC 20 D8 9F    JSR $9FD8

# 03:9FCF A4 AB       LDY $AB   Makes sure $AB is >= 0, puts it in $D4
# 03:9FD1 10 02       BPL $9FD5 (+2 B)
# 03:9FD3 7B          TDC
# 03:9FD4 A8          TAY
# 03:9FD5 84 D4       STY $D4
# 03:9FD7 60          RTS


# 03:9FD8 A6 A9       LDX $A9  Multiplies $A9 by relative speed (?) (slot's $3B)
# 03:9FDA 8E 3D 39    STX $393D
# 03:9FDD AE 79 39    LDX $3979
# 03:9FE0 8E 3F 39    STX $393F
# 03:9FE3 20 B9 83    JSR $83B9

# 03:9FE6 AE 41 39    LDX $3941  Divides $A9 by #$10. IntegerDivision in $AB
# 03:9FE9 8E 45 39    STX $3945
# 03:9FEC A2 10 00    LDX #$0010
# 03:9FEF 8E 47 39    STX $3947
# 03:9FF2 20 07 84    JSR $8407
# 03:9FF5 AE 49 39    LDX $3949
# 03:9FF8 86 AB       STX $AB
# 03:9FFA 60          RTS





# 03:A015 AD A8 00    LDA $00A8
# 03:A018 D0 63       BNE $A07D (+99 B)
# 03:A01A A2 04 00    LDX #$0004
# 03:A01D 86 A9       STX $A9
# 03:A01F A6 A9       LDX $A9
# 03:A021 BD 40 35    LDA $3540,x
# 03:A024 D0 13       BNE $A039 (+19 B)
# 03:A026 A5 A9       LDA $A9
# 03:A028 20 89 84    JSR $8489
# 03:A02B A6 A6       LDX $A6
# 03:A02D BD 03 20    LDA $2003,x
# 03:A030 29 C0       AND #$C0
# 03:A032 D0 05       BNE $A039 (+5 B)
# 03:A034 BD 05 20    LDA $2005,x
# 03:A037 10 18       BPL $A051 (+24 B)
# 03:A039 C6 A9       DEC $A9
# 03:A03B A5 A9       LDA $A9
# 03:A03D 10 E0       BPL $A01F (-32 B)
# 03:A03F AD E5 38    LDA $38E5
# 03:A042 29 02       AND #$02
# 03:A044 F0 04       BEQ $A04A (+4 B)
# 03:A046 A9 08       LDA #$08
# 03:A048 80 02       BRA $A04C (+2 B)
# 03:A04A A9 80       LDA #$80
# 03:A04C 85 A8       STA $A8
# 03:A04E 4C F6 A0    JMP $A0F6
# 03:A051 AD CD 29    LDA $29CD
# 03:A054 D0 28       BNE $A07E (+40 B)
# 03:A056 A9 30       LDA #$30
# 03:A058 85 A8       STA $A8
# 03:A05A 7B          TDC
# 03:A05B AA          TAX
# 03:A05C BF 00 FD 13 LDA $13FD00,x
# 03:A060 C9 FF       CMP #$FF
# 03:A062 F0 19       BEQ $A07D (+25 B)
# 03:A064 CD 00 18    CMP $1800
# 03:A067 D0 10       BNE $A079 (+16 B)
# 03:A069 BF 01 FD 13 LDA $13FD01,x
# 03:A06D CD 01 18    CMP $1801
# 03:A070 D0 07       BNE $A079 (+7 B)
# 03:A072 A5 A8       LDA $A8
# 03:A074 29 EF       AND #$EF
# 03:A076 85 A8       STA $A8
# 03:A078 60          RTS
# 03:A079 E8          INX
# 03:A07A E8          INX
# 03:A07B 80 DF       BRA $A05C (-33 B)
# 03:A07D 60          RTS
# 03:A07E AD 8B 38    LDA $388B
# 03:A081 D0 73       BNE $A0F6 (+115 B)
# 03:A083 AD D3 38    LDA $38D3
# 03:A086 D0 05       BNE $A08D (+5 B)
# 03:A088 AD 2D 35    LDA $352D
# 03:A08B F0 69       BEQ $A0F6 (+105 B)
# 03:A08D AD D6 38    LDA $38D6
# 03:A090 C9 FF       CMP #$FF
# 03:A092 F0 62       BEQ $A0F6 (+98 B)
# 03:A094 AD D6 38    LDA $38D6
# 03:A097 F0 0A       BEQ $A0A3 (+10 B)
# 03:A099 AD F3 38    LDA $38F3
# 03:A09C D0 05       BNE $A0A3 (+5 B)
# 03:A09E CE D6 38DEC $38D6
# 03:A0A1 80 53       BRA $A0F6 (+83 B)
# 03:A0A3 AD E5 38    LDA $38E5
# 03:A0A6 29 01       AND #$01
# 03:A0A8 D0 34       BNE $A0DE (+52 B)
# 03:A0AA 7B          TDC
# 03:A0AB AA          TAX
# 03:A0AC 86 A9       STX $A9
# 03:A0AE A6 A9       LDX $A9
# 03:A0B0 BD 40 35    LDA $3540,x
# 03:A0B3 D0 21       BNE $A0D6 (+33 B)
# 03:A0B5 A5 A9       LDA $A9
# 03:A0B7 20 89 84    JSR $8489
# 03:A0BA A6 A6       LDX $A6
# 03:A0BC BD 03 20    LDA $2003,x
# 03:A0BF 29 C0       AND #$C0
# 03:A0C1 D0 13       BNE $A0D6 (+19 B)
# 03:A0C3 BD 04 20    LDA $2004,x
# 03:A0C6 29 30       AND #$30
# 03:A0C8 D0 0C       BNE $A0D6 (+12 B)
# 03:A0CA BD 05 20    LDA $2005,x
# 03:A0CD 29 C2       AND #$C2
# 03:A0CF D0 05       BNE $A0D6 (+5 B)
# 03:A0D1 A9 40       LDA #$40
# 03:A0D3 85 A8       STA $A8
# 03:A0D5 60          RTS
# 03:A0D6 E6 A9       INC $A9
# 03:A0D8 A5 A9       LDA $A9
# 03:A0DA C9 05       CMP #$05
# 03:A0DC D0 D0       BNE $A0AE (-48 B)
# 03:A0DE 9C 2D 35    STZ $352D
# 03:A0E1 20 9B 85    JSR $859B
# 03:A0E4 A9 FF       LDA #$FF
# 03:A0E6 8D C4 33    STA $33C4
# 03:A0E9 A9 22       LDA #$22
# 03:A0EB 8D CA 34    STA $34CA
# 03:A0EE A9 05       LDA #$05
# 03:A0F0 20 85 80    JSR $8085
# 03:A0F3 20 FD B2    JSR $B2FD
# 03:A0F6 60          RTS
# 03:A0F7 64 A9       STZ $A9
# 03:A0F9 64 AA       STZ $AA
# 03:A0FB AD 22 18    LDA $1822
# 03:A0FE 20 89 84    JSR $8489
# 03:A101 A6 A6       LDX $A6
# 03:A103 BD 01 20    LDA $2001,x
# 03:A106 29 0F       AND #$0F
# 03:A108 A8          TAY
# 03:A109 C8          INY
# 03:A10A 38          SEC
# 03:A10B 26 A9       ROL $A9
# 03:A10D 26 AA       ROL $AA
# 03:A10F 88          DEY
# 03:A110 D0 F9       BNE $A10B (-7 B)
# 03:A112 7B          TDC
# 03:A113 AA          TAX
# 03:A114 86 AF       STX $AF
# 03:A116 A6 AF       LDX $AF
# 03:A118 BD 1B 32    LDA $321B,x
# 03:A11B F0 2F       BEQ $A14C (+47 B)
# 03:A11D C9 C8       CMP #$C8
# 03:A11F D0 0D       BNE $A12E (+13 B)
# 03:A121 A6 A6       LDX $A6
# 03:A123 BD 00 20    LDA $2000,x
# 03:A126 29 1F       AND #$1F
# 03:A128 C9 0B       CMP #$0B
# 03:A12A D0 0A       BNE $A136 (+10 B)
# 03:A12C F0 14       BEQ $A142 (+20 B)
# 03:A12E 20 58 A1    JSR $A158
# 03:A131 AD 3D 35    LDA $353D
# 03:A134 F0 0C       BEQ $A142 (+12 B)
# 03:A136 A6 AF       LDX $AF
# 03:A138 BD 1A 32    LDA $321A,x
# 03:A13B 09 80       ORA #$80
# 03:A13D 9D 1A 32    STA $321A,x
# 03:A140 80 0A       BRA $A14C (+10 B)
# 03:A142 A6 AF       LDX $AF
# 03:A144 BD 1A 32    LDA $321A,x
# 03:A147 29 7F       AND #$7F
# 03:A149 9D 1A 32    STA $321A,x
# 03:A14C 18          CLC
# 03:A14D A5 AF       LDA $AF
# 03:A14F 69 04       ADC #$04
# 03:A151 85 AF       STA $AF
# 03:A153 C9 C0       CMP #$C0
# 03:A155 D0 BF       BNE $A116 (-65 B)
# 03:A157 60          RTS
# 03:A158 9C 3D 35    STZ $353D
# 03:A15B C9 6D       CMP #$6D
# 03:A15D 90 0A       BCC $A169 (+10 B)
# 03:A15F C9          DECMP #$DE
# 03:A161 B0 34       BCS $A197 (+52 B)
# 03:A163 C9 B0       CMP #$B0
# 03:A165 90 30       BCC $A197 (+48 B)
# 03:A167 B0 31       BCS $A19A (+49 B)
# 03:A169 AA          TAX
# 03:A16A 86 E5       STX $E5
# 03:A16C A2 00 91    LDX #$9100
# 03:A16F 86 80       STX $80
# 03:A171 A9 0F       LDA #$0F
# 03:A173 85 82       STA $82
# 03:A175 A9 08       LDA #$08
# 03:A177 20 5E 84    JSR $845E
# 03:A17A AD A2 28    LDA $28A2
# 03:A17D 29 1F       AND #$1F
# 03:A17F 0A          ASL a
# 03:A180 AA          TAX
# 03:A181 BF 50 A5 0F LDA $0FA550,x
# 03:A185 85 AB       STA $AB
# 03:A187 BF 51 A5 0F LDA $0FA551,x
# 03:A18B 85 AC       STA $AC
# 03:A18D C2 20       REP #$20
# 03:A18F A5 AB       LDA $AB
# 03:A191 25 A9       AND $A9
# 03:A193 E2 20       SEP #$20
# 03:A195 D0 03       BNE $A19A (+3 B)
# 03:A197 EE 3D 35    INC $353D
# 03:A19A C2 20       REP #$20
# 03:A19C 7B          TDC
# 03:A19D E2 20       SEP #$20
# 03:A19F 60          RTS
# 03:A1A0 64 A9       STZ $A9
# 03:A1A2 64 AA       STZ $AA
# 03:A1A4 64 AB       STZ $AB
# 03:A1A6 64 AC       STZ $AC
# 03:A1A8 A6 A6       LDX $A6
# 03:A1AA BD 03 20    LDA $2003,x
# 03:A1AD 29 04       AND #$04
# 03:A1AF F0 04       BEQ $A1B5 (+4 B)
# 03:A1B1 A9 80       LDA #$80
# 03:A1B3 85 A9       STA $A9
# 03:A1B5 BD 03 20    LDA $2003,x
# 03:A1B8 29 20       AND #$20
# 03:A1BA F0 04       BEQ $A1C0 (+4 B)
# 03:A1BC A9 80       LDA #$80
# 03:A1BE 85 AB       STA $AB
# 03:A1C0 BD 03 20    LDA $2003,x
# 03:A1C3 29 08       AND #$08
# 03:A1C5 F0 04       BEQ $A1CB (+4 B)
# 03:A1C7 A9 80       LDA #$80
# 03:A1C9 85 AC       STA $AC
# 03:A1CB BD 0B 20    LDA $200B,x
# 03:A1CE 85 AD       STA $AD
# 03:A1D0 BD 0C 20    LDA $200C,x
# 03:A1D3 F0 04       BEQ $A1D9 (+4 B)
# 03:A1D5 A9 FF       LDA #$FF
# 03:A1D7 85 AD       STA $AD
# 03:A1D9 A6 A6       LDX $A6
# 03:A1DB BD 06 20    LDA $2006,x
# 03:A1DE 30 17       BMI $A1F7 (+23 B)
# 03:A1E0 A0 05 00    LDY #$0005
# 03:A1E3 AE 34 35    LDX $3534
# 03:A1E6 BD 03 33    LDA $3303,x
# 03:A1E9 C9 FF       CMP #$FF
# 03:A1EB F0 03       BEQ $A1F0 (+3 B)
# 03:A1ED 20 7E A2    JSR $A27E
# 03:A1F0 E8          INX
# 03:A1F1 E8          INX
# 03:A1F2 E8          INX
# 03:A1F3 E8          INX
# 03:A1F4 88          DEY
# 03:A1F5 D0 EF       BNE $A1E6 (-17 B)
# 03:A1F7 A5 AA       LDA $AA
# 03:A1F9 F0 0A       BEQ $A205 (+10 B)
# 03:A1FB AD 22 18    LDA $1822
# 03:A1FE 85 00       STA $00
# 03:A200 A9 09       LDA #$09
# 03:A202 20 85 80    JSR $8085
# 03:A205 AE 36 35    LDX $3536
# 03:A208 86 AF       STX $AF
# 03:A20A 7B          TDC
# 03:A20B AA          TAX
# 03:A20C 86 B1       STX $B1
# 03:A20E A6 AF       LDX $AF
# 03:A210 BD 7B 2C    LDA $2C7B,x
# 03:A213 F0 4B       BEQ $A260 (+75 B)
# 03:A215 BD 7A 2C    LDA $2C7A,x
# 03:A218 29 7F       AND #$7F
# 03:A21A 05 A9       ORA $A9
# 03:A21C 05 AB       ORA $AB
# 03:A21E 05 AC       ORA $AC
# 03:A220 9D 7A 2C    STA $2C7A,x
# 03:A223 A5 A9       LDA $A9
# 03:A225 D0 28       BNE $A24F (+40 B)
# 03:A227 A5 AB       LDA $AB
# 03:A229 10 11       BPL $A23C (+17 B)
# 03:A22B BD 7B 2C    LDA $2C7B,x
# 03:A22E C9 19       CMP #$19
# 03:A230 D0 1D       BNE $A24F (+29 B)
# 03:A232 BD 7A 2C    LDA $2C7A,x
# 03:A235 29 7F       AND #$7F
# 03:A237 9D 7A 2C    STA $2C7A,x
# 03:A23A 80 13       BRA $A24F (+19 B)
# 03:A23C A5 AC       LDA $AC
# 03:A23E 10 0F       BPL $A24F (+15 B)
# 03:A240 BD 7B 2C    LDA $2C7B,x
# 03:A243 C9 1A       CMP #$1A
# 03:A245 D0 08       BNE $A24F (+8 B)
# 03:A247 BD 7A 2C    LDA $2C7A,x
# 03:A24A 29 7F       AND #$7F
# 03:A24C 9D 7A 2C    STA $2C7A,x
# 03:A24F BD 7D 2C    LDA $2C7D,x
# 03:A252 C5          ADCMP $AD
# 03:A254 F0 0A       BEQ $A260 (+10 B)
# 03:A256 90 08       BCC $A260 (+8 B)
# 03:A258 BD 7A 2C    LDA $2C7A,x
# 03:A25B 09 80       ORA #$80
# 03:A25D 9D 7A 2C    STA $2C7A,x
# 03:A260 A6 AF       LDX $AF
# 03:A262 E8          INX
# 03:A263 E8          INX
# 03:A264 E8          INX
# 03:A265 E8          INX
# 03:A266 86 AF       STX $AF
# 03:A268 E6 B1       INC $B1
# 03:A26A A5 B1       LDA $B1
# 03:A26C C9 48       CMP #$48
# 03:A26E F0 03       BEQ $A273 (+3 B)
# 03:A270 4C 0E A2    JMP $A20E
# 03:A273 AD 22 18    LDA $1822
# 03:A276 85 00       STA $00
# 03:A278 A9 0E       LDA #$0E
# 03:A27A 20 85 80    JSR $8085
# 03:A27D 60          RTS
# 03:A27E 64 B5       STZ $B5
# 03:A280 DA          PHX
# 03:A281 BD 03 33    LDA $3303,x
# 03:A284 C9 05       CMP #$05
# 03:A286 F0 0C       BEQ $A294 (+12 B)
# 03:A288 C9 08       CMP #$08
# 03:A28A F0 08       BEQ $A294 (+8 B)
# 03:A28C C9 0C       CMP #$0C
# 03:A28E F0 04       BEQ $A294 (+4 B)
# 03:A290 C9 10       CMP #$10
# 03:A292 D0 07       BNE $A29B (+7 B)
# 03:A294 20 D9 A2    JSR $A2D9
# 03:A297 A5 B3       LDA $B3
# 03:A299 D0 23       BNE $A2BE (+35 B)
# 03:A29B FA          PLX
# 03:A29C DA          PHX
# 03:A29D BD 03 33    LDA $3303,x
# 03:A2A0 0A          ASL a
# 03:A2A1 AA          TAX
# 03:A2A2 BF 19 FD 13 LDA $13FD19,x
# 03:A2A6 85 B3       STA $B3
# 03:A2A8 BF 1A FD 13 LDA $13FD1A,x
# 03:A2AC 85 B4       STA $B4
# 03:A2AE A6 A6       LDX $A6
# 03:A2B0 BD 03 20    LDA $2003,x
# 03:A2B3 25 B3       AND $B3
# 03:A2B5 D0 07       BNE $A2BE (+7 B)
# 03:A2B7 BD 04 20    LDA $2004,x
# 03:A2BA 25 B4       AND $B4
# 03:A2BC F0 04       BEQ $A2C2 (+4 B)
# 03:A2BE A9 80       LDA #$80
# 03:A2C0 85 B5       STA $B5
# 03:A2C2 FA          PLX
# 03:A2C3 BD 02 33    LDA $3302,x
# 03:A2C6 48          PHA
# 03:A2C7 29 7F       AND #$7F
# 03:A2C9 05 B5       ORA $B5
# 03:A2CB 9D 02 33    STA $3302,x
# 03:A2CE 68          PLA
# 03:A2CF DD 02 33    CMP $3302,x
# 03:A2D2 F0 04       BEQ $A2D8 (+4 B)
# 03:A2D4 A9 01       LDA #$01
# 03:A2D6 85 AA       STA $AA
# 03:A2D8 60          RTS
# 03:A2D9 A6 A6       LDX $A6
# 03:A2DB 64 B3       STZ $B3
# 03:A2DD C9 08       CMP #$08
# 03:A2DF F0 1E       BEQ $A2FF (+30 B)
# 03:A2E1 C9 0C       CMP #$0C
# 03:A2E3 F0 36       BEQ $A31B (+54 B)
# 03:A2E5 C9 10       CMP #$10
# 03:A2E7 F0 46       BEQ $A32F (+70 B)
# 03:A2E9 BD 33 20    LDA $2033,x
# 03:A2EC F0 07       BEQ $A2F5 (+7 B)
# 03:A2EE C9 61       CMP #$61
# 03:A2F0 B0 03       BCS $A2F5 (+3 B)
# 03:A2F2 4C 70 A3    JMP $A370
# 03:A2F5 BD 35 20    LDA $2035,x
# 03:A2F8 F0 74       BEQ $A36E (+116 B)
# 03:A2FA C9 61       CMP #$61
# 03:A2FC B0 70       BCS $A36E (+112 B)
# 03:A2FE 60          RTS
# 03:A2FF BD 33 20    LDA $2033,x
# 03:A302 F0 09       BEQ $A30D (+9 B)
# 03:A304 C9 44       CMP #$44
# 03:A306 90 66       BCC $A36E (+102 B)
# 03:A308 C9 4D       CMP #$4D
# 03:A30A B0 62       BCS $A36E (+98 B)
# 03:A30C 60          RTS
# 03:A30D BD 35 20    LDA $2035,x
# 03:A310 F0 5C       BEQ $A36E (+92 B)
# 03:A312 C9 44       CMP #$44
# 03:A314 90 58       BCC $A36E (+88 B)
# 03:A316 C9 4D       CMP #$4D
# 03:A318 B0 54       BCS $A36E (+84 B)
# 03:A31A 60          RTS
# 03:A31B BD 33 20    LDA $2033,x
# 03:A31E F0 4E       BEQ $A36E (+78 B)
# 03:A320 C9 4D       CMP #$4D
# 03:A322 90 4A       BCC $A36E (+74 B)
# 03:A324 C9 61       CMP #$61
# 03:A326 B0 46       BCS $A36E (+70 B)
# 03:A328 BD 35 20    LDA $2035,x
# 03:A32B F0 41       BEQ $A36E (+65 B)
# 03:A32D D0 41       BNE $A370 (+65 B)
# 03:A32F DA          PHX
# 03:A330 AE 36 35    LDX $3536
# 03:A333 DA          PHX
# 03:A334 A6 A6       LDX $A6
# 03:A336 BD 03 20    LDA $2003,x
# 03:A339 29 3C       AND #$3C
# 03:A33B D0 2A       BNE $A367 (+42 B)
# 03:A33D A2 01 00    LDX #$0001
# 03:A340 A5 D0       LDA $D0
# 03:A342 CD 39 35    CMP $3539
# 03:A345 F0 01       BEQ $A348 (+1 B)
# 03:A347 CA          DEX
# 03:A348 BD 39 35    LDA $3539,x
# 03:A34B 20 89 84    JSR $8489
# 03:A34E A6 A6       LDX $A6
# 03:A350 BD 03 20    LDA $2003,x
# 03:A353 29 FC       AND #$FC
# 03:A355 D0 10       BNE $A367 (+16 B)
# 03:A357 BD 04 20    LDA $2004,x
# 03:A35A 29 3C       AND #$3C
# 03:A35C D0 09       BNE $A367 (+9 B)
# 03:A35E BD 05 20    LDA $2005,x
# 03:A361 29 40       AND #$40
# 03:A363 D0 02       BNE $A367 (+2 B)
# 03:A365 C6 B3       DEC $B3
# 03:A367 FA          PLX
# 03:A368 8E 36 35    STX $3536
# 03:A36B FA          PLX
# 03:A36C 86 A6       STX $A6
# 03:A36E E6 B3       INC $B3
# 03:A370 60          RTS
# 03:A371 AD 00 18    LDA $1800
# 03:A374 C9 B7       CMP #$B7
# 03:A376 D0 0A       BNE $A382 (+10 B)
# 03:A378 AD 01 18    LDA $1801
# 03:A37B F0 05       BEQ $A382 (+5 B)
# 03:A37D AD F7 38    LDA $38F7
# 03:A380 F0 60       BEQ $A3E2 (+96 B)
# 03:A382 AD F3 38    LDA $38F3
# 03:A385 D0 0F       BNE $A396 (+15 B)
# 03:A387 AD 82 22    LDA $2282
# 03:A38A C9 63       CMP #$63
# 03:A38C F0 0B       BEQ $A399 (+11 B)
# 03:A38E C9 62       CMP #$62
# 03:A390 F0 07       BEQ $A399 (+7 B)
# 03:A392 C9 61       CMP #$61
# 03:A394 F0 03       BEQ $A399 (+3 B)
# 03:A396 9C D6 38    STZ $38D6
# 03:A399 7B          TDC
# 03:A39A AA          TAX
# 03:A39B BD 29 39    LDA $3929,x
# 03:A39E C5 D2       CMP $D2
# 03:A3A0 F0 40       BEQ $A3E2 (+64 B)
# 03:A3A2 E8          INX
# 03:A3A3 E0 05 00    CPX #$0005
# 03:A3A6 D0 F3       BNE $A39B (-13 B)
# 03:A3A8 A5 D0       LDA $D0
# 03:A3AA 85 8C       STA $8C
# 03:A3AC A5 D2       LDA $D2
# 03:A3AE 20 89 84    JSR $8489
# 03:A3B1 A9 03       LDA #$03
# 03:A3B3 20 69 85    JSR $8569
# 03:A3B6 AE 98 35    LDX $3598
# 03:A3B9 BD 06 2A    LDA $2A06,x
# 03:A3BC 29 FE       AND #$FE
# 03:A3BE 9D 06 2A    STA $2A06,x
# 03:A3C1 A5 D2       LDA $D2
# 03:A3C3 85 D0       STA $D0
# 03:A3C5 20 8D A5    JSR $A58D
# 03:A3C8 A5 D0       LDA $D0
# 03:A3CA 85 AA       STA $AA
# 03:A3CC A5 8C       LDA $8C
# 03:A3CE 85 D0       STA $D0
# 03:A3D0 A5 AA       LDA $AA
# 03:A3D2 C9 FF       CMP #$FF
# 03:A3D4 F0 0C       BEQ $A3E2 (+12 B)
# 03:A3D6 AD 2F 39    LDA $392F
# 03:A3D9 AA          TAX
# 03:A3DA A5 D2       LDA $D2
# 03:A3DC 9D 29 39    STA $3929,x
# 03:A3DF EE 2F 39    INC $392F
# 03:A3E2 60          RTS
# 03:A3E3 A5 D7       LDA $D7
# 03:A3E5 F0 52       BEQ $A439 (+82 B)
# 03:A3E7 A5 D0       LDA $D0
# 03:A3E9 C9 FF       CMP #$FF
# 03:A3EB F0 4B       BEQ $A438 (+75 B)
# 03:A3ED 20 8D A5    JSR $A58D
# 03:A3F0 A5 D0       LDA $D0
# 03:A3F2 C9 FF       CMP #$FF
# 03:A3F4 F0 39       BEQ $A42F (+57 B)
# 03:A3F6 A6 A6       LDX $A6
# 03:A3F8 BD 00 20    LDA $2000,x
# 03:A3FB 29 1F       AND #$1F
# 03:A3FD C9 05       CMP #$05
# 03:A3FF D0 14       BNE $A415 (+20 B)
# 03:A401 AD 82 35    LDA $3582
# 03:A404 D0 0F       BNE $A415 (+15 B)
# 03:A406 AD DB 38    LDA $38DB
# 03:A409 3A          DEC a
# 03:A40A F0 09       BEQ $A415 (+9 B)
# 03:A40C BD 06 20    LDA $2006,x
# 03:A40F 30 04       BMI $A415 (+4 B)
# 03:A411 29 01       AND #$01
# 03:A413 D0 1A       BNE $A42F (+26 B)
# 03:A415 20 A0 A1    JSR $A1A0
# 03:A418 A5 D0       LDA $D0
# 03:A41A 20 7E 84    JSR $847E
# 03:A41D AA          TAX
# 03:A41E BD DB 32    LDA $32DB,x
# 03:A421 C9 4C       CMP #$4C
# 03:A423 F0 07       BEQ $A42C (+7 B)
# 03:A425 BD DF 32    LDA $32DF,x
# 03:A428 C9 4C       CMP #$4C
# 03:A42A D0 0C       BNE $A438 (+12 B)
# 03:A42C 20 C8 A4    JSR $A4C8
# 03:A42F A9 FF       LDA #$FF
# 03:A431 85 D0       STA $D0
# 03:A433 A9 01       LDA #$01
# 03:A435 20 85 80    JSR $8085
# 03:A438 60          RTS
# 03:A439 A5 D0       LDA $D0
# 03:A43B C9 FF       CMP #$FF
# 03:A43D F0 24       BEQ $A463 (+36 B)
# 03:A43F 20 8D A5    JSR $A58D
# 03:A442 A5 D0       LDA $D0
# 03:A444 C9 FF       CMP #$FF
# 03:A446 F0 1A       BEQ $A462 (+26 B)
# 03:A448 AD 8B 38    LDA $388B
# 03:A44B F0 03       BEQ $A450 (+3 B)
# 03:A44D 20 D9 A4    JSR $A4D9
# 03:A450 A2 05 00    LDX #$0005
# 03:A453 B5 D7       LDA $D7,x
# 03:A455 9D 37 39    STA $3937,x
# 03:A458 CA          DEX
# 03:A459 10 F8       BPL $A453 (-8 B)
# 03:A45B 20 B2 A5    JSR $A5B2
# 03:A45E A9 FF       LDA #$FF
# 03:A460 85 D0       STA $D0
# 03:A462 60          RTS
# 03:A463 AD 2D 35    LDA $352D
# 03:A466 D0 5F       BNE $A4C7 (+95 B)
# 03:A468 AD 29 39    LDA $3929
# 03:A46B C9 FF       CMP #$FF
# 03:A46D F0 58       BEQ $A4C7 (+88 B)
# 03:A46F 48          PHA
# 03:A470 7B          TDC
# 03:A471 AA          TAX
# 03:A472 BD 2A 39    LDA $392A,x
# 03:A475 9D 29 39    STA $3929,x
# 03:A478 E8          INX
# 03:A479 E0 05 00    CPX #$0005
# 03:A47C D0 F4       BNE $A472 (-12 B)
# 03:A47E CE 2F 39DEC $392F
# 03:A481 68          PLA
# 03:A482 8D 22 18    STA $1822
# 03:A485 85 D0       STA $D0
# 03:A487 20 1B A8    JSR $A81B
# 03:A48A 20 8D A5    JSR $A58D
# 03:A48D A5 D0       LDA $D0
# 03:A48F C9 FF       CMP #$FF
# 03:A491 F0 34       BEQ $A4C7 (+52 B)
# 03:A493 20 F7 A0    JSR $A0F7
# 03:A496 20 A0 A1    JSR $A1A0
# 03:A499 A6 A6       LDX $A6
# 03:A49B BD 05 20    LDA $2005,x
# 03:A49E 29 EF       AND #$EF
# 03:A4A0 9D 05 20    STA $2005,x
# 03:A4A3 AD 7B 35    LDA $357B
# 03:A4A6 C5 D0       CMP $D0
# 03:A4A8 F0 1D       BEQ $A4C7 (+29 B)
# 03:A4AA BD 33 20    LDA $2033,x
# 03:A4AD C9 4C       CMP #$4C
# 03:A4AF F0 07       BEQ $A4B8 (+7 B)
# 03:A4B1 BD 35 20    LDA $2035,x
# 03:A4B4 C9 4C       CMP #$4C
# 03:A4B6 D0 03       BNE $A4BB (+3 B)
# 03:A4B8 4C C8 A4    JMP $A4C8
# 03:A4BB AD 8B 38    LDA $388B
# 03:A4BE F0 03       BEQ $A4C3 (+3 B)
# 03:A4C0 64 D7       STZ $D7
# 03:A4C2 60          RTS
# 03:A4C3 7B          TDC
# 03:A4C4 20 85 80    JSR $8085
# 03:A4C7 60          RTS
# 03:A4C8 A6 A6       LDX $A6
# 03:A4CA BD 04 20    LDA $2004,x
# 03:A4CD 09 04       ORA #$04
# 03:A4CF 9D 04 20    STA $2004,x
# 03:A4D2 A5 D0       LDA $D0
# 03:A4D4 AA          TAX
# 03:A4D5 9E 60 35    STZ $3560,x
# 03:A4D8 60          RTS
# 03:A4D9 AD E8 38    LDA $38E8
# 03:A4DC D0 FB       BNE $A4D9 (-5 B)
# 03:A4DE 64 DB       STZ $DB
# 03:A4E0 64 D9       STZ $D9
# 03:A4E2 A5 D0       LDA $D0
# 03:A4E4 8D E9 38    STA $38E9
# 03:A4E7 20 89 84    JSR $8489
# 03:A4EA A6 A6       LDX $A6
# 03:A4EC BD 00 20    LDA $2000,x
# 03:A4EF 29 1F       AND #$1F
# 03:A4F1 C9 15       CMP #$15
# 03:A4F3 D0 1D       BNE $A512 (+29 B)
# 03:A4F5 AD A9 38    LDA $38A9
# 03:A4F8 0A          ASL a
# 03:A4F9 AA          TAX
# 03:A4FA BD 9A 38    LDA $389A,x
# 03:A4FD C9 FF       CMP #$FF
# 03:A4FF F0 0C       BEQ $A50D (+12 B)
# 03:A501 85 A9       STA $A9
# 03:A503 BD 9B 38    LDA $389B,x
# 03:A506 85 AA       STA $AA
# 03:A508 EE A9 38    INC $38A9
# 03:A50B 80 22       BRA $A52F (+34 B)
# 03:A50D 9C A9 38    STZ $38A9
# 03:A510 80 E3       BRA $A4F5 (-29 B)
# 03:A512 AD A8 38    LDA $38A8
# 03:A515 0A          ASL a
# 03:A516 AA          TAX
# 03:A517 BD 8C 38    LDA $388C,x
# 03:A51A C9 FF       CMP #$FF
# 03:A51C F0 0C       BEQ $A52A (+12 B)
# 03:A51E 85 A9       STA $A9
# 03:A520 BD 8D 38    LDA $388D,x
# 03:A523 85 AA       STA $AA
# 03:A525 EE A8 38    INC $38A8
# 03:A528 80 05       BRA $A52F (+5 B)
# 03:A52A 9C A8 38    STZ $38A8
# 03:A52D 80 E3       BRA $A512 (-29 B)
# 03:A52F A5 A9       LDA $A9
# 03:A531 C9 C0       CMP #$C0
# 03:A533 90 31       BCC $A566 (+49 B)
# 03:A535 38          SEC
# 03:A536 E9 C0       SBC #$C0
# 03:A538 85 DC       STA $DC
# 03:A53A 8D EA 38    STA $38EA
# 03:A53D A5 A9       LDA $A9
# 03:A53F C9 CE       CMP #$CE
# 03:A541 D0 06       BNE $A549 (+6 B)
# 03:A543 A9 FF       LDA #$FF
# 03:A545 85 DA       STA $DA
# 03:A547 80 17       BRA $A560 (+23 B)
# 03:A549 20 79 85    JSR $8579
# 03:A54C 85 A9       STA $A9
# 03:A54E 18          CLC
# 03:A54F 69 05       ADC #$05
# 03:A551 AA          TAX
# 03:A552 BD 40 35    LDA $3540,x
# 03:A555 D0 F2       BNE $A549 (-14 B)
# 03:A557 A5 A9       LDA $A9
# 03:A559 AA          TAX
# 03:A55A 7B          TDC
# 03:A55B 20 5F 85    JSR $855F
# 03:A55E 85 DA       STA $DA
# 03:A560 A9 80       LDA #$80
# 03:A562 85 D8       STA $D8
# 03:A564 80 23       BRA $A589 (+35 B)
# 03:A566 C9 01       CMP #$01
# 03:A568 F0 13       BEQ $A57D (+19 B)
# 03:A56A A9 02       LDA #$02
# 03:A56C 8D EA 38    STA $38EA
# 03:A56F A5 AA       LDA $AA
# 03:A571 85 DC       STA $DC
# 03:A573 A9 20       LDA #$20
# 03:A575 85 D8       STA $D8
# 03:A577 A9 FF       LDA #$FF
# 03:A579 85 DA       STA $DA
# 03:A57B 80 0C       BRA $A589 (+12 B)
# 03:A57D A5 AA       LDA $AA
# 03:A57F 85 DC       STA $DC
# 03:A581 A9 40       LDA #$40
# 03:A583 85 D8       STA $D8
# 03:A585 A9 FF       LDA #$FF
# 03:A587 85 DA       STA $DA
# 03:A589 EE E8 38    INC $38E8
# 03:A58C 60          RTS
# 03:A58D A5 D0       LDA $D0
# 03:A58F C9 FF       CMP #$FF
# 03:A591 F0 1A       BEQ $A5AD (+26 B)
# 03:A593 20 89 84    JSR $8489
# 03:A596 A6 A6       LDX $A6
# 03:A598 BD 03 20    LDA $2003,x
# 03:A59B 29 C0       AND #$C0
# 03:A59D D0 0E       BNE $A5AD (+14 B)
# 03:A59F BD 04 20    LDA $2004,x
# 03:A5A2 29 3C       AND #$3C
# 03:A5A4 D0 07       BNE $A5AD (+7 B)
# 03:A5A6 BD 05 20    LDA $2005,x
# 03:A5A9 29 C6       AND #$C6
# 03:A5AB F0 04       BEQ $A5B1 (+4 B)
# 03:A5AD A9 FF       LDA #$FF
# 03:A5AF 85 D0       STA $D0
# 03:A5B1 60          RTS
# 03:A5B2 A5 D0       LDA $D0
# 03:A5B4 8D 75 39    STA $3975
# 03:A5B7 20 89 84    JSR $8489
# 03:A5BA AD 2B 35    LDA $352B
# 03:A5BD F0 0C       BEQ $A5CB (+12 B)
# 03:A5BF 20 FE 9D    JSR $9DFE
# 03:A5C2 20 CC 8E    JSR $8ECC
# 03:A5C5 20 37 98    JSR $9837
# 03:A5C8 9C 2B 35    STZ $352B
# 03:A5CB AD 3A 39    LDA $393A
# 03:A5CE 0D 3B 39    ORA $393B
# 03:A5D1 D0 0A       BNE $A5DD (+10 B)
# 03:A5D3 A5 D0       LDA $D0
# 03:A5D5 AA          TAX
# 03:A5D6 7B          TDC
# 03:A5D7 20 5F 85    JSR $855F
# 03:A5DA 8D 3B 39    STA $393B
# 03:A5DD A6 A6       LDX $A6
# 03:A5DF 9E 52 20    STZ $2052,x
# 03:A5E2 AD 38 39    LDA $3938
# 03:A5E5 9D 50 20    STA $2050,x
# 03:A5E8 10 44       BPL $A62E (+68 B)
# 03:A5EA A2 1C 00    LDX #$001C
# 03:A5ED 86 AB       STX $AB
# 03:A5EF A2 02 33    LDX #$3302
# 03:A5F2 86 AD       STX $AD
# 03:A5F4 20 78 A7    JSR $A778
# 03:A5F7 AD 8B 38    LDA $388B
# 03:A5FA F0 04       BEQ $A600 (+4 B)
# 03:A5FC A5 DC       LDA $DC
# 03:A5FE 80 05       BRA $A605 (+5 B)
# 03:A600 A0 01 00    LDY #$0001
# 03:A603 B1 80       LDA ($80),y
# 03:A605 8D 7B 39    STA $397B
# 03:A608 48          PHA
# 03:A609 C9 10       CMP #$10
# 03:A60B D0 13       BNE $A620 (+19 B)
# 03:A60D A2 01 00    LDX #$0001
# 03:A610 A5 D0       LDA $D0
# 03:A612 CD 39 35    CMP $3539
# 03:A615 F0 01       BEQ $A618 (+1 B)
# 03:A617 CA          DEX
# 03:A618 BD 39 35    LDA $3539,x
# 03:A61B 8D 7B 35    STA $357B
# 03:A61E 80 07       BRA $A627 (+7 B)
# 03:A620 C9 0A       CMP #$0A
# 03:A622 D0 03       BNE $A627 (+3 B)
# 03:A624 20 D6 A7    JSR $A7D6
# 03:A627 A9 0C       LDA #$0C
# 03:A629 85 D6       STA $D6
# 03:A62B 68          PLA
# 03:A62C 80 71       BRA $A69F (+113 B)
# 03:A62E 29 40       AND #$40
# 03:A630 F0 6F       BEQ $A6A1 (+111 B)
# 03:A632 7B          TDC
# 03:A633 AA          TAX
# 03:A634 86 AB       STX $AB
# 03:A636 A2 1A 32    LDX #$321A
# 03:A639 86 AD       STX $AD
# 03:A63B 20 78 A7    JSR $A778
# 03:A63E A6 A6       LDX $A6
# 03:A640 BD 50 20    LDA $2050,x
# 03:A643 29 08       AND #$08
# 03:A645 F0 1A       BEQ $A661 (+26 B)
# 03:A647 A9 02       LDA #$02
# 03:A649 85 D6       STA $D6
# 03:A64B A0 01 00    LDY #$0001
# 03:A64E B1 80       LDA ($80),y
# 03:A650 9D 52 20    STA $2052,x
# 03:A653 C8          INY
# 03:A654 B1 80       LDA ($80),y
# 03:A656 3A          DEC a
# 03:A657 91 80       STA ($80),y
# 03:A659 20 C1 A7    JSR $A7C1
# 03:A65C A9 16       LDA #$16
# 03:A65E 4C F0 A6    JMP $A6F0
# 03:A661 AD 8B 38    LDA $388B
# 03:A664 F0 04       BEQ $A66A (+4 B)
# 03:A666 A5 DC       LDA $DC
# 03:A668 80 05       BRA $A66F (+5 B)
# 03:A66A A0 01 00    LDY #$0001
# 03:A66D B1 80       LDA ($80),y
# 03:A66F C9 B0       CMP #$B0
# 03:A671 B0 09       BCS $A67C (+9 B)
# 03:A673 9D 52 20    STA $2052,x
# 03:A676 A9 02       LDA #$02
# 03:A678 85 D6       STA $D6
# 03:A67A 80 21       BRA $A69D (+33 B)
# 03:A67C 9D 52 20    STA $2052,x
# 03:A67F 8D 7B 39    STA $397B
# 03:A682 A9 0B       LDA #$0B
# 03:A684 85 D6       STA $D6
# 03:A686 AD 8B 38    LDA $388B
# 03:A689 D0 12       BNE $A69D (+18 B)
# 03:A68B AD 7B 39    LDA $397B
# 03:A68E C9 C8       CMP #$C8
# 03:A690 F0 0B       BEQ $A69D (+11 B)
# 03:A692 A0 02 00    LDY #$0002
# 03:A695 B1 80       LDA ($80),y
# 03:A697 3A          DEC a
# 03:A698 91 80       STA ($80),y
# 03:A69A 20 C1 A7    JSR $A7C1
# 03:A69D A9 01       LDA #$01
# 03:A69F 80 4F       BRA $A6F0 (+79 B)
# 03:A6A1 AD 38 39    LDA $3938
# 03:A6A4 29 20       AND #$20
# 03:A6A6 F0 2B       BEQ $A6D3 (+43 B)
# 03:A6A8 A9 03       LDA #$03
# 03:A6AA 85 D6       STA $D6
# 03:A6AC A2 20 01    LDX #$0120
# 03:A6AF 86 AB       STX $AB
# 03:A6B1 A2 7A 2C    LDX #$2C7A
# 03:A6B4 86 AD       STX $AD
# 03:A6B6 20 78 A7    JSR $A778
# 03:A6B9 A6 A6       LDX $A6
# 03:A6BB AD 8B 38    LDA $388B
# 03:A6BE F0 04       BEQ $A6C4 (+4 B)
# 03:A6C0 A5 DC       LDA $DC
# 03:A6C2 80 05       BRA $A6C9 (+5 B)
# 03:A6C4 A0 01 00    LDY #$0001
# 03:A6C7 B1 80       LDA ($80),y
# 03:A6C9 8D 7B 39    STA $397B
# 03:A6CC 9D 52 20    STA $2052,x
# 03:A6CF A9 02       LDA #$02
# 03:A6D1 80 1D       BRA $A6F0 (+29 B)
# 03:A6D3 A9 02       LDA #$02
# 03:A6D5 85 D6       STA $D6
# 03:A6D7 A2 08 00    LDX #$0008
# 03:A6DA 86 AB       STX $AB
# 03:A6DC A2 DA 32    LDX #$32DA
# 03:A6DF 86 AD       STX $AD
# 03:A6E1 20 78 A7    JSR $A778
# 03:A6E4 A6 A6       LDX $A6
# 03:A6E6 A0 01 00    LDY #$0001
# 03:A6E9 B1 80       LDA ($80),y
# 03:A6EB 9D 52 20    STA $2052,x
# 03:A6EE A9 01       LDA #$01
# 03:A6F0 A6 A6       LDX $A6
# 03:A6F2 9D 51 20    STA $2051,x
# 03:A6F5 20 14 A7    JSR $A714
# 03:A6F8 AD 3A 39    LDA $393A
# 03:A6FB 9D 53 20    STA $2053,x
# 03:A6FE AD 3B 39    LDA $393B
# 03:A701 9D 54 20    STA $2054,x
# 03:A704 A5 D0       LDA $D0
# 03:A706 20 2C 9E    JSR $9E2C
# 03:A709 A9 03       LDA #$03
# 03:A70B 20 C8 85    JSR $85C8
# 03:A70E A9 08       LDA #$08
# 03:A710 9D 06 2A    STA $2A06,x
# 03:A713 60          RTS
# 03:A714 DA          PHX
# 03:A715 AD 3B 39    LDA $393B
# 03:A718 0D 3A 39    ORA $393A
# 03:A71B 20 0C 85    JSR $850C
# 03:A71E CA          DEX
# 03:A71F F0 2A       BEQ $A74B (+42 B)
# 03:A721 7B          TDC
# 03:A722 A8          TAY
# 03:A723 AD 3B 39    LDA $393B
# 03:A726 85 A9       STA $A9
# 03:A728 85 AD       STA $AD
# 03:A72A A2 05 00    LDX #$0005
# 03:A72D 86 AB       STX $AB
# 03:A72F 20 4D A7    JSR $A74D
# 03:A732 A5 AD       LDA $AD
# 03:A734 8D 3B 39    STA $393B
# 03:A737 AD 3A 39    LDA $393A
# 03:A73A 85 A9       STA $A9
# 03:A73C 85 AD       STA $AD
# 03:A73E A2 0D 00    LDX #$000D
# 03:A741 86 AB       STX $AB
# 03:A743 20 4D A7    JSR $A74D
# 03:A746 A5 AD       LDA $AD
# 03:A748 8D 3A 39    STA $393A
# 03:A74B FA          PLX
# 03:A74C 60          RTS
# 03:A74D 64 AA       STZ $AA
# 03:A74F 06 A9       ASL $A9
# 03:A751 90 11       BCC $A764 (+17 B)
# 03:A753 B9 03 20    LDA $2003,y
# 03:A756 29 C0       AND #$C0
# 03:A758 F0 0A       BEQ $A764 (+10 B)
# 03:A75A A5 AA       LDA $AA
# 03:A75C AA          TAX
# 03:A75D A5 AD       LDA $AD
# 03:A75F 20 5A 85    JSR $855A
# 03:A762 85 AD       STA $AD
# 03:A764 C2 20       REP #$20
# 03:A766 98          TYA
# 03:A767 18          CLC
# 03:A768 69 80 00    ADC #$0080
# 03:A76B A8          TAY
# 03:A76C 7B          TDC
# 03:A76D E2 20       SEP #$20
# 03:A76F E6 AA       INC $AA
# 03:A771 A5 AA       LDA $AA
# 03:A773 C5 AB       CMP $AB
# 03:A775 D0 D8       BNE $A74F (-40 B)
# 03:A777 60          RTS
# 03:A778 A5 D0       LDA $D0
# 03:A77A AA          TAX
# 03:A77B 8E 3D 39    STX $393D
# 03:A77E A6 AB       LDX $AB
# 03:A780 8E 3F 39    STX $393F
# 03:A783 20 B9 83    JSR $83B9
# 03:A786 AE 41 39    LDX $3941
# 03:A789 8E 56 39    STX $3956
# 03:A78C A6 AD       LDX $AD
# 03:A78E 8E 58 39    STX $3958
# 03:A791 20 E3 84    JSR $84E3
# 03:A794 AD 39 39    LDA $3939
# 03:A797 85 DF       STA $DF
# 03:A799 A9 04       LDA #$04
# 03:A79B 85 E1       STA $E1
# 03:A79D 20 E0 83    JSR $83E0
# 03:A7A0 A6 E3       LDX $E3
# 03:A7A2 8E 56 39    STX $3956
# 03:A7A5 AE 5A 39    LDX $395A
# 03:A7A8 8E 58 39    STX $3958
# 03:A7AB 20 E3 84    JSR $84E3
# 03:A7AE A6 A6       LDX $A6
# 03:A7B0 AD 5A 39    LDA $395A
# 03:A7B3 9D 55 20    STA $2055,x
# 03:A7B6 85 80       STA $80
# 03:A7B8 AD 5B 39    LDA $395B
# 03:A7BB 9D 56 20    STA $2056,x
# 03:A7BE 85 81       STA $81
# 03:A7C0 60          RTS
# 03:A7C1 D0 09       BNE $A7CC (+9 B)
# 03:A7C3 88          DEY
# 03:A7C4 7B          TDC
# 03:A7C5 91 80       STA ($80),y
# 03:A7C7 88          DEY
# 03:A7C8 A9 80       LDA #$80
# 03:A7CA 91 80       STA ($80),y
# 03:A7CC AD 39 39    LDA $3939
# 03:A7CF 85 01       STA $01
# 03:A7D1 A9 06       LDA #$06
# 03:A7D3 4C 85 80    JMP $8085
# 03:A7D6 7B          TDC
# 03:A7D7 AA          TAX
# 03:A7D8 A8          TAY
# 03:A7D9 BD 1B 32    LDA $321B,x
# 03:A7DC C9 CE       CMP #$CE
# 03:A7DE F0 0B       BEQ $A7EB (+11 B)
# 03:A7E0 C8          INY
# 03:A7E1 E8          INX
# 03:A7E2 E8          INX
# 03:A7E3 E8          INX
# 03:A7E4 E8          INX
# 03:A7E5 E0 C0 00    CPX #$00C0
# 03:A7E8 D0 EF       BNE $A7D9 (-17 B)
# 03:A7EA 60          RTS
# 03:A7EB BD 1C 32    LDA $321C,x
# 03:A7EE C9 01       CMP #$01
# 03:A7F0 90 28       BCC $A81A (+40 B)
# 03:A7F2 38          SEC
# 03:A7F3 BD 1C 32    LDA $321C,x
# 03:A7F6 48          PHA
# 03:A7F7 DA          PHX
# 03:A7F8 E9 01       SBC #$01
# 03:A7FA 9D 1C 32    STA $321C,x
# 03:A7FD D0 0E       BNE $A80D (+14 B)
# 03:A7FF 9E 1C 32    STZ $321C,x
# 03:A802 9E 1B 32    STZ $321B,x
# 03:A805 BD 1A 32    LDA $321A,x
# 03:A808 09 80       ORA #$80
# 03:A80A 9D 1A 32    STA $321A,x
# 03:A80D 98          TYA
# 03:A80E 85 01       STA $01
# 03:A810 A9 06       LDA #$06
# 03:A812 20 85 80    JSR $8085
# 03:A815 FA          PLX
# 03:A816 68          PLA
# 03:A817 9D 1C 32    STA $321C,x
# 03:A81A 60          RTS
# 03:A81B AD 22 18    LDA $1822
# 03:A81E A8          TAY
# 03:A81F B9 DC 38    LDA $38DC,y
# 03:A822 F0 31       BEQ $A855 (+49 B)
# 03:A824 AD 22 18    LDA $1822
# 03:A827 8D 75 39    STA $3975
# 03:A82A 20 7E 84    JSR $847E
# 03:A82D AA          TAX
# 03:A82E B9 DC 38    LDA $38DC,y
# 03:A831 10 04       BPL $A837 (+4 B)
# 03:A833 E8          INX
# 03:A834 E8          INX
# 03:A835 E8          INX
# 03:A836 E8          INX
# 03:A837 7B          TDC
# 03:A838 99 DC 38    STA $38DC,y
# 03:A83B 9E DB 32    STZ $32DB,x
# 03:A83E 9E DC 32    STZ $32DC,x
# 03:A841 A9 80       LDA #$80
# 03:A843 9D DA 32    STA $32DA,x
# 03:A846 AD 75 39    LDA $3975
# 03:A849 20 89 84    JSR $8489
# 03:A84C 20 FE 9D    JSR $9DFE
# 03:A84F 20 CC 8E    JSR $8ECC
# 03:A852 20 37 98    JSR $9837
# 03:A855 60          RTS
# 03:A856 20 FD B2    JSR $B2FD
# 03:A859 A5 D2       LDA $D2
# 03:A85B 20 89 84    JSR $8489
# 03:A85E A5 D3       LDA $D3
# 03:A860 85 AB       STA $AB
# 03:A862 0A          ASL a
# 03:A863 18          CLC
# 03:A864 65 AB       ADC $AB
# 03:A866 20 69 85    JSR $8569
# 03:A869 AE 98 35    LDX $3598
# 03:A86C 8E 55 35    STX $3555
# 03:A86F A5 D3       LDA $D3
# 03:A871 0A          ASL a
# 03:A872 AA          TAX
# 03:A873 BF 49 AD 03 LDA $03AD49,x
# 03:A877 85 80       STA $80
# 03:A879 BF 4A AD 03 LDA $03AD4A,x
# 03:A87D 85 81       STA $81
# 03:A87F A9 03       LDA #$03
# 03:A881 85 82       STA $82
# 03:A883 DC 80 00    JML ($0080)
# 03:A886 60          RTS
# 03:A887 A6 A6       LDX $A6
# 03:A889 BD 05 20    LDA $2005,x
# 03:A88C 29 BF       AND #$BF
# 03:A88E 9D 05 20    STA $2005,x
# 03:A891 AE 30 35    LDX $3530
# 03:A894 9E 06 2A    STZ $2A06,x
# 03:A897 A9 03       LDA #$03
# 03:A899 20 69 85    JSR $8569
# 03:A89C AE 98 35    LDX $3598
# 03:A89F BD 06 2A    LDA $2A06,x
# 03:A8A2 10 0B       BPL $A8AF (+11 B)
# 03:A8A4 9E 06 2A    STZ $2A06,x
# 03:A8A7 A9 01       LDA #$01
# 03:A8A9 9D 04 2A    STA $2A04,x
# 03:A8AC 9E 05 2A    STZ $2A05,x
# 03:A8AF A5 D2       LDA $D2
# 03:A8B1 0A          ASL a
# 03:A8B2 AA          TAX
# 03:A8B3 BD EB 29    LDA $29EB,x
# 03:A8B6 29 7F       AND #$7F
# 03:A8B8 9D EB 29    STA $29EB,x
# 03:A8BB 60          RTS
# 03:A8BC A6 A6       LDX $A6
# 03:A8BE BD 04 20    LDA $2004,x
# 03:A8C1 29 CF       AND #$CF
# 03:A8C3 9D 04 20    STA $2004,x
# 03:A8C6 64 D6       STZ $D6
# 03:A8C8 A5 D2       LDA $D2
# 03:A8CA 20 2C 9E    JSR $9E2C
# 03:A8CD A9 03       LDA #$03
# 03:A8CF 20 C8 85    JSR $85C8
# 03:A8D2 9E 06 2A    STZ $2A06,x
# 03:A8D5 60          RTS
# 03:A8D6 A6 A6       LDX $A6
# 03:A8D8 BD 06 20    LDA $2006,x
# 03:A8DB 29 BF       AND #$BF
# 03:A8DD 9D 06 20    STA $2006,x
# 03:A8E0 A5 D2       LDA $D2
# 03:A8E2 0A          ASL a
# 03:A8E3 AA          TAX
# 03:A8E4 BD EB 29    LDA $29EB,x
# 03:A8E7 29 DF       AND #$DF
# 03:A8E9 9D EB 29    STA $29EB,x
# 03:A8EC 60          RTS
# 03:A8ED A6 A6       LDX $A6
# 03:A8EF BD 05 20    LDA $2005,x
# 03:A8F2 29 02       AND #$02
# 03:A8F4 D0 71       BNE $A967 (+113 B)
# 03:A8F6 BD 06 20    LDA $2006,x
# 03:A8F9 30 6C       BMI $A967 (+108 B)
# 03:A8FB A5 D2       LDA $D2
# 03:A8FD 0A          ASL a
# 03:A8FE AA          TAX
# 03:A8FF BD 2A 2B    LDA $2B2A,x
# 03:A902 85 A9       STA $A9
# 03:A904 BD 2B 2B    LDA $2B2B,x
# 03:A907 85 AA       STA $AA
# 03:A909 AE 55 35    LDX $3555
# 03:A90C A5 A9       LDA $A9
# 03:A90E 9D 04 2A    STA $2A04,x
# 03:A911 A5 AA       LDA $AA
# 03:A913 9D 05 2A    STA $2A05,x
# 03:A916 A9 40       LDA #$40
# 03:A918 9D 06 2A    STA $2A06,x
# 03:A91B C2 20       REP #$20
# 03:A91D A6 A6       LDX $A6
# 03:A91F BD 09 20    LDA $2009,x
# 03:A922 20 85 84    JSR $8485
# 03:A925 85 A9       STA $A9
# 03:A927 A5 A9       LDA $A9
# 03:A929 D0 02       BNE $A92D (+2 B)
# 03:A92B E6 A9       INC $A9
# 03:A92D 7B          TDC
# 03:A92E E2 20       SEP #$20
# 03:A930 A5 D2       LDA $D2
# 03:A932 0A          ASL a
# 03:A933 AA          TAX
# 03:A934 A5 A9       LDA $A9
# 03:A936 9D D4 34    STA $34D4,x
# 03:A939 A5 AA       LDA $AA
# 03:A93B 9D D5 34    STA $34D5,x
# 03:A93E 20 6E CA    JSR $CA6E
# 03:A941 A9 F8       LDA #$F8
# 03:A943 8D C2 33    STA $33C2
# 03:A946 A9 03       LDA #$03
# 03:A948 8D C3 33    STA $33C3
# 03:A94B A9 35       LDA #$35
# 03:A94D 8D CA 34    STA $34CA
# 03:A950 A9 05       LDA #$05
# 03:A952 20 85 80    JSR $8085
# 03:A955 20 A0 B1    JSR $B1A0
# 03:A958 A9 11       LDA #$11
# 03:A95A 20 85 80    JSR $8085
# 03:A95D A9 0C       LDA #$0C
# 03:A95F 20 85 80    JSR $8085
# 03:A962 A9 10       LDA #$10
# 03:A964 20 85 80    JSR $8085
# 03:A967 60          RTS
# 03:A968 A6 A6       LDX $A6
# 03:A96A BD 05 20    LDA $2005,x
# 03:A96D 29 02       AND #$02
# 03:A96F D0 56       BNE $A9C7 (+86 B)
# 03:A971 BD 06 20    LDA $2006,x
# 03:A974 30 51       BMI $A9C7 (+81 B)
# 03:A976 BD 04 20    LDA $2004,x
# 03:A979 29 03       AND #$03
# 03:A97B 1A          INC a
# 03:A97C 85 A9       STA $A9
# 03:A97E C9 04       CMP #$04
# 03:A980 D0 1D       BNE $A99F (+29 B)
# 03:A982 BD 04 20    LDA $2004,x
# 03:A985 29 FC       AND #$FC
# 03:A987 9D 04 20    STA $2004,x
# 03:A98A BD 03 20    LDA $2003,x
# 03:A98D 09 40       ORA #$40
# 03:A98F 9D 03 20    STA $2003,x
# 03:A992 A5 D2       LDA $D2
# 03:A994 0A          ASL a
# 03:A995 AA          TAX
# 03:A996 AD EB 29    LDA $29EB
# 03:A999 29 F7       AND #$F7
# 03:A99B 8D EB 29    STA $29EB
# 03:A99E 60          RTS
# 03:A99F BD 04 20    LDA $2004,x
# 03:A9A2 05 A9       ORA $A9
# 03:A9A4 9D 04 20    STA $2004,x
# 03:A9A7 A5 D2       LDA $D2
# 03:A9A9 0A          ASL a
# 03:A9AA AA          TAX
# 03:A9AB BD 44 2B    LDA $2B44,x
# 03:A9AE 85 A9       STA $A9
# 03:A9B0 BD 45 2B    LDA $2B45,x
# 03:A9B3 85 AA       STA $AA
# 03:A9B5 AE 55 35    LDX $3555
# 03:A9B8 A5 A9       LDA $A9
# 03:A9BA 9D 04 2A    STA $2A04,x
# 03:A9BD A5 AA       LDA $AA
# 03:A9BF 9D 05 2A    STA $2A05,x
# 03:A9C2 A9 40       LDA #$40
# 03:A9C4 9D 06 2A    STA $2A06,x
# 03:A9C7 60          RTS
# 03:A9C8 A6 A6       LDX $A6
# 03:A9CA BD 06 20    LDA $2006,x
# 03:A9CD 29 DF       AND #$DF
# 03:A9CF 9D 06 20    STA $2006,x
# 03:A9D2 A5 D2       LDA $D2
# 03:A9D4 0A          ASL a
# 03:A9D5 AA          TAX
# 03:A9D6 BD EB 29    LDA $29EB,x
# 03:A9D9 29 FB       AND #$FB
# 03:A9DB 9D EB 29    STA $29EB,x
# 03:A9DE 60          RTS
# 03:A9DF A6 A6       LDX $A6
# 03:A9E1 BD 05 20    LDA $2005,x
# 03:A9E4 29 02       AND #$02
# 03:A9E6 D0 16       BNE $A9FE (+22 B)
# 03:A9E8 BD 06 20    LDA $2006,x
# 03:A9EB 30 11       BMI $A9FE (+17 B)
# 03:A9ED A6 A6       LDX $A6
# 03:A9EF BD 03 20    LDA $2003,x
# 03:A9F2 09 80       ORA #$80
# 03:A9F4 9D 03 20    STA $2003,x
# 03:A9F7 A5 D2       LDA $D2
# 03:A9F9 0A          ASL a
# 03:A9FA AA          TAX
# 03:A9FB 9E EB 29    STZ $29EB,x
# 03:A9FE 60          RTS
# 03:A9FF EE 57 35    INC $3557
# 03:AA02 AD 57 35    LDA $3557
# 03:AA05 C9 01       CMP #$01
# 03:AA07 D0 7C       BNE $AA85 (+124 B)
# 03:AA09 9C 57 35    STZ $3557
# 03:AA0C 20 FD B2    JSR $B2FD
# 03:AA0F A9 00       LDA #$00
# 03:AA11 85 8A       STA $8A
# 03:AA13 A5 8A       LDA $8A
# 03:AA15 AA          TAX
# 03:AA16 BD 40 35    LDA $3540,x
# 03:AA19 D0 31       BNE $AA4C (+49 B)
# 03:AA1B A5 8A       LDA $8A
# 03:AA1D 0A          ASL a
# 03:AA1E AA          TAX
# 03:AA1F BD EB 29    LDA $29EB,x
# 03:AA22 29 20       AND #$20
# 03:AA24 F0 26       BEQ $AA4C (+38 B)
# 03:AA26 A5 8A       LDA $8A
# 03:AA28 20 89 84    JSR $8489
# 03:AA2B A6 A6       LDX $A6
# 03:AA2D BD 03 20    LDA $2003,x
# 03:AA30 29 C0       AND #$C0
# 03:AA32 D0 18       BNE $AA4C (+24 B)
# 03:AA34 BD 05 20    LDA $2005,x
# 03:AA37 29 42       AND #$42
# 03:AA39 D0 11       BNE $AA4C (+17 B)
# 03:AA3B BD 06 20    LDA $2006,x
# 03:AA3E 30 0C       BMI $AA4C (+12 B)
# 03:AA40 A5 8A       LDA $8A
# 03:AA42 0A          ASL a
# 03:AA43 AA          TAX
# 03:AA44 A9 02       LDA #$02
# 03:AA46 9D D4 34    STA $34D4,x
# 03:AA49 9E D5 34    STZ $34D5,x
# 03:AA4C E6 8A       INC $8A
# 03:AA4E A5 8A       LDA $8A
# 03:AA50 C9 0D       CMP #$0D
# 03:AA52 D0 BF       BNE $AA13 (-65 B)
# 03:AA54 9C 07 39    STZ $3907
# 03:AA57 20 6E CA    JSR $CA6E
# 03:AA5A A9 FF       LDA #$FF
# 03:AA5C 8D C2 33    STA $33C2
# 03:AA5F AD 07 39    LDA $3907
# 03:AA62 F0 21       BEQ $AA85 (+33 B)
# 03:AA64 A9 F8       LDA #$F8
# 03:AA66 8D C2 33    STA $33C2
# 03:AA69 A9 03       LDA #$03
# 03:AA6B 8D C3 33    STA $33C3
# 03:AA6E A9 38       LDA #$38
# 03:AA70 8D CA 34    STA $34CA
# 03:AA73 A9 05       LDA #$05
# 03:AA75 20 85 80    JSR $8085
# 03:AA78 20 A0 B1    JSR $B1A0
# 03:AA7B A9 10       LDA #$10
# 03:AA7D 20 85 80    JSR $8085
# 03:AA80 A9 02       LDA #$02
# 03:AA82 20 85 80    JSR $8085
# 03:AA85 20 FD B2    JSR $B2FD
# 03:AA88 22 B4 FF 13  JSL $13FFB4
# 03:AA8C AD 7C 35    LDA $357C
# 03:AA8F C9 FF       CMP #$FF
# 03:AA91 F0 45       BEQ $AAD8 (+69 B)
# 03:AA93 EE 7C 35    INC $357C
# 03:AA96 C9 05       CMP #$05
# 03:AA98 D0 3E       BNE $AAD8 (+62 B)
# 03:AA9A 9C 7C 35    STZ $357C
# 03:AA9D 20 FD B2    JSR $B2FD
# 03:AAA0 7B          TDC
# 03:AAA1 AA          TAX
# 03:AAA2 9B          TXY
# 03:AAA3 AD 40 35    LDA $3540
# 03:AAA6 D0 19       BNE $AAC1 (+25 B)
# 03:AAA8 BD 03 20    LDA $2003,x
# 03:AAAB 29 C0       AND #$C0
# 03:AAAD D0 12       BNE $AAC1 (+18 B)
# 03:AAAF DA          PHX
# 03:AAB0 5A          PHY
# 03:AAB1 98          TYA
# 03:AAB2 0A          ASL a
# 03:AAB3 AA          TAX
# 03:AAB4 AD 7D 35    LDA $357D
# 03:AAB7 9D D4 34    STA $34D4,x
# 03:AABA A9 80       LDA #$80
# 03:AABC 9D D5 34    STA $34D5,x
# 03:AABF 7A          PLY
# 03:AAC0 FA          PLX
# 03:AAC1 C2 20       REP #$20
# 03:AAC3 8A          TXA
# 03:AAC4 18          CLC
# 03:AAC5 69 80 00    ADC #$0080
# 03:AAC8 AA          TAX
# 03:AAC9 7B          TDC
# 03:AACA E2 20       SEP #$20
# 03:AACC C8          INY
# 03:AACD C0 05 00    CPY #$0005
# 03:AAD0 D0 D1       BNE $AAA3 (-47 B)
# 03:AAD2 20 6E CA    JSR $CA6E
# 03:AAD5 20 A0 B1    JSR $B1A0
# 03:AAD8 A9 11       LDA #$11
# 03:AADA 20 85 80    JSR $8085
# 03:AADD A9 0C       LDA #$0C
# 03:AADF 4C 85 80    JMP $8085
# 03:AAE2 7B          TDC
# 03:AAE3 AA          TAX
# 03:AAE4 86 8E       STX $8E
# 03:AAE6 A6 8E       LDX $8E
# 03:AAE8 BD 40 35    LDA $3540,x
# 03:AAEB D0 05       BNE $AAF2 (+5 B)
# 03:AAED BD 60 35    LDA $3560,x
# 03:AAF0 F0 03       BEQ $AAF5 (+3 B)
# 03:AAF2 4C 91 AB    JMP $AB91
# 03:AAF5 8A          TXA
# 03:AAF6 20 89 84    JSR $8489
# 03:AAF9 A6 A6       LDX $A6
# 03:AAFB BD 03 20    LDA $2003,x
# 03:AAFE 29 C0       AND #$C0
# 03:AB00 D0 05       BNE $AB07 (+5 B)
# 03:AB02 BD 04 20    LDA $2004,x
# 03:AB05 29 30       AND #$30
# 03:AB07 D0 07       BNE $AB10 (+7 B)
# 03:AB09 BD 05 20    LDA $2005,x
# 03:AB0C 29 40       AND #$40
# 03:AB0E F0 03       BEQ $AB13 (+3 B)
# 03:AB10 4C 91 AB    JMP $AB91
# 03:AB13 BD 04 20    LDA $2004,x
# 03:AB16 29 0C       AND #$0C
# 03:AB18 F0 20       BEQ $AB3A (+32 B)
# 03:AB1A BD 05 20    LDA $2005,x
# 03:AB1D 29 FB       AND #$FB
# 03:AB1F 9D 05 20    STA $2005,x
# 03:AB22 BD 04 20    LDA $2004,x
# 03:AB25 29 04       AND #$04
# 03:AB27 F0 05       BEQ $AB2E (+5 B)
# 03:AB29 20 8C AC    JSR $AC8C
# 03:AB2C 80 4D       BRA $AB7B (+77 B)
# 03:AB2E BD 04 20    LDA $2004,x
# 03:AB31 29 08       AND #$08
# 03:AB33 F0 05       BEQ $AB3A (+5 B)
# 03:AB35 20 9D AB    JSR $AB9D
# 03:AB38 80 41       BRA $AB7B (+65 B)
# 03:AB3A BD 06 20    LDA $2006,x
# 03:AB3D 29 01       AND #$01
# 03:AB3F F0 50       BEQ $AB91 (+80 B)
# 03:AB41 BD 00 20    LDA $2000,x
# 03:AB44 29 1F       AND #$1F
# 03:AB46 C9 05       CMP #$05
# 03:AB48 D0 47       BNE $AB91 (+71 B)
# 03:AB4A AD 82 35    LDA $3582
# 03:AB4D D0 42       BNE $AB91 (+66 B)
# 03:AB4F A9 03       LDA #$03
# 03:AB51 20 69 85    JSR $8569
# 03:AB54 AE 98 35    LDX $3598
# 03:AB57 BD 06 2A    LDA $2A06,x
# 03:AB5A 29 08       AND #$08
# 03:AB5C D0 33       BNE $AB91 (+51 B)
# 03:AB5E A6 A6       LDX $A6
# 03:AB60 BD 06 20    LDA $2006,x
# 03:AB63 10 0B       BPL $AB70 (+11 B)
# 03:AB65 20 23 AD    JSR $AD23
# 03:AB68 3A          DEC a
# 03:AB69 D0 26       BNE $AB91 (+38 B)
# 03:AB6B 20 02 AD    JSR $AD02
# 03:AB6E 80 12       BRA $AB82 (+18 B)
# 03:AB70 20 23 AD    JSR $AD23
# 03:AB73 3A          DEC a
# 03:AB74 F0 1B       BEQ $AB91 (+27 B)
# 03:AB76 20 F9 AC    JSR $ACF9
# 03:AB79 80 07       BRA $AB82 (+7 B)
# 03:AB7B 64 D6       STZ $D6
# 03:AB7D A5 8E       LDA $8E
# 03:AB7F 20 2C 9E    JSR $9E2C
# 03:AB82 A9 03       LDA #$03
# 03:AB84 20 C8 85    JSR $85C8
# 03:AB87 A9 08       LDA #$08
# 03:AB89 9D 06 2A    STA $2A06,x
# 03:AB8C A6 8E       LDX $8E
# 03:AB8E FE 60 35    INC $3560,x
# 03:AB91 E6 8E       INC $8E
# 03:AB93 A5 8E       LDA $8E
# 03:AB95 C9 05       CMP #$05
# 03:AB97 F0 03       BEQ $AB9C (+3 B)
# 03:AB99 4C E6 AA    JMP $AAE6
# 03:AB9C 60          RTS
# 03:AB9D A6 A6       LDX $A6
# 03:AB9F 9E 53 20    STZ $2053,x
# 03:ABA2 9E 54 20    STZ $2054,x
# 03:ABA5 20 8B 85    JSR $858B
# 03:ABA8 C9 46       CMP #$46
# 03:ABAA B0 24       BCS $ABD0 (+36 B)
# 03:ABAC AE 34 35    LDX $3534
# 03:ABAF A0 05 00    LDY #$0005
# 03:ABB2 BD 03 33    LDA $3303,x
# 03:ABB5 C9 02       CMP #$02
# 03:ABB7 F0 0D       BEQ $ABC6 (+13 B)
# 03:ABB9 C9 03       CMP #$03
# 03:ABBB F0 09       BEQ $ABC6 (+9 B)
# 03:ABBD E8          INX
# 03:ABBE E8          INX
# 03:ABBF E8          INX
# 03:ABC0 E8          INX
# 03:ABC1 88          DEY
# 03:ABC2 D0 EE       BNE $ABB2 (-18 B)
# 03:ABC4 80 0A       BRA $ABD0 (+10 B)
# 03:ABC6 64 90       STZ $90
# 03:ABC8 20 ED AB    JSR $ABED
# 03:ABCB A5 90       LDA $90
# 03:ABCD D0 01       BNE $ABD0 (+1 B)
# 03:ABCF 60          RTS
# 03:ABD0 A6 A6       LDX $A6
# 03:ABD2 A9 80       LDA #$80
# 03:ABD4 9D 50 20    STA $2050,x
# 03:ABD7 9E 51 20    STZ $2051,x
# 03:ABDA 20 82 85    JSR $8582
# 03:ABDD AA          TAX
# 03:ABDE BD 40 35    LDA $3540,x
# 03:ABE1 D0 F7       BNE $ABDA (-9 B)
# 03:ABE3 7B          TDC
# 03:ABE4 20 5F 85    JSR $855F
# 03:ABE7 A6 A6       LDX $A6
# 03:ABE9 9D 54 20    STA $2054,x
# 03:ABEC 60          RTS
# 03:ABED AE 36 35    LDX $3536
# 03:ABF0 86 A9       STX $A9
# 03:ABF2 A0 30 00    LDY #$0030
# 03:ABF5 BD 7A 2C    LDA $2C7A,x
# 03:ABF8 10 0C       BPL $AC06 (+12 B)
# 03:ABFA E8          INX
# 03:ABFB E8          INX
# 03:ABFC E8          INX
# 03:ABFD E8          INX
# 03:ABFE 88          DEY
# 03:ABFF D0 F4       BNE $ABF5 (-12 B)
# 03:AC01 E6 90       INC $90
# 03:AC03 4C 8B AC    JMP $AC8B
# 03:AC06 7B          TDC
# 03:AC07 AA          TAX
# 03:AC08 A9 2F       LDA #$2F
# 03:AC0A 20 79 83    JSR $8379
# 03:AC0D AA          TAX
# 03:AC0E 8E 3D 39    STX $393D
# 03:AC11 A2 04 00    LDX #$0004
# 03:AC14 8E 3F 39    STX $393F
# 03:AC17 20 B9 83    JSR $83B9
# 03:AC1A 18          CLC
# 03:AC1B A5 A9       LDA $A9
# 03:AC1D 6D 41 39    ADC $3941
# 03:AC20 85 AB       STA $AB
# 03:AC22 A5 AA       LDA $AA
# 03:AC24 6D 42 39    ADC $3942
# 03:AC27 85 AC       STA $AC
# 03:AC29 A6 AB       LDX $AB
# 03:AC2B BD 7A 2C    LDA $2C7A,x
# 03:AC2E 85 AD       STA $AD
# 03:AC30 30 D4       BMI $AC06 (-44 B)
# 03:AC32 BD 7B 2C    LDA $2C7B,x
# 03:AC35 A6 A6       LDX $A6
# 03:AC37 9D 52 20    STA $2052,x
# 03:AC3A A9 20       LDA #$20
# 03:AC3C 9D 50 20    STA $2050,x
# 03:AC3F A9 02       LDA #$02
# 03:AC41 9D 51 20    STA $2051,x
# 03:AC44 A5 AD       LDA $AD
# 03:AC46 29 40       AND #$40
# 03:AC48 D0 25       BNE $AC6F (+37 B)
# 03:AC4A A5 AD       LDA $AD
# 03:AC4C 29 10       AND #$10
# 03:AC4E F0 17       BEQ $AC67 (+23 B)
# 03:AC50 20 79 85    JSR $8579
# 03:AC53 85 AB       STA $AB
# 03:AC55 18          CLC
# 03:AC56 69 05       ADC #$05
# 03:AC58 AA          TAX
# 03:AC59 BD 40 35    LDA $3540,x
# 03:AC5C D0 F2       BNE $AC50 (-14 B)
# 03:AC5E A5 AB       LDA $AB
# 03:AC60 AA          TAX
# 03:AC61 7B          TDC
# 03:AC62 20 5F 85    JSR $855F
# 03:AC65 80 02       BRA $AC69 (+2 B)
# 03:AC67 A9 FF       LDA #$FF
# 03:AC69 A6 A6       LDX $A6
# 03:AC6B 9D 53 20    STA $2053,x
# 03:AC6E 60          RTS
# 03:AC6F A5 AD       LDA $AD
# 03:AC71 29 10       AND #$10
# 03:AC73 F0 0F       BEQ $AC84 (+15 B)
# 03:AC75 20 82 85    JSR $8582
# 03:AC78 AA          TAX
# 03:AC79 BD 40 35    LDA $3540,x
# 03:AC7C D0 F7       BNE $AC75 (-9 B)
# 03:AC7E 7B          TDC
# 03:AC7F 20 5F 85    JSR $855F
# 03:AC82 80 02       BRA $AC86 (+2 B)
# 03:AC84 A9 F8       LDA #$F8
# 03:AC86 A6 A6       LDX $A6
# 03:AC88 9D 54 20    STA $2054,x
# 03:AC8B 60          RTS
# 03:AC8C A6 A6       LDX $A6
# 03:AC8E A9 80       LDA #$80
# 03:AC90 9D 50 20    STA $2050,x
# 03:AC93 9E 51 20    STZ $2051,x
# 03:AC96 9E 54 20    STZ $2054,x
# 03:AC99 9E 53 20    STZ $2053,x
# 03:AC9C BD 04 20    LDA $2004,x
# 03:AC9F 29 08       AND #$08
# 03:ACA1 F0 3B       BEQ $ACDE (+59 B)
# 03:ACA3 A2 00 00    LDX #$0000
# 03:ACA6 A9 04       LDA #$04
# 03:ACA8 20 79 83    JSR $8379
# 03:ACAB 85 A9       STA $A9
# 03:ACAD 85 DF       STA $DF
# 03:ACAF AA          TAX
# 03:ACB0 BD 40 35    LDA $3540,x
# 03:ACB3 D0 EE       BNE $ACA3 (-18 B)
# 03:ACB5 A9 80       LDA #$80
# 03:ACB7 85 AB       STA $AB
# 03:ACB9 20 E0 83    JSR $83E0
# 03:ACBC A6 E3       LDX $E3
# 03:ACBE BD 03 20    LDA $2003,x
# 03:ACC1 29 C0       AND #$C0
# 03:ACC3 D0 DE       BNE $ACA3 (-34 B)
# 03:ACC5 BD 05 20    LDA $2005,x
# 03:ACC8 29 82       AND #$82
# 03:ACCA D0 D7       BNE $ACA3 (-41 B)
# 03:ACCC BD 06 20    LDA $2006,x
# 03:ACCF 30 D2       BMI $ACA3 (-46 B)
# 03:ACD1 A5 A9       LDA $A9
# 03:ACD3 AA          TAX
# 03:ACD4 7B          TDC
# 03:ACD5 20 5F 85    JSR $855F
# 03:ACD8 A6 A6       LDX $A6
# 03:ACDA 9D 54 20    STA $2054,x
# 03:ACDD 60          RTS
# 03:ACDE 20 79 85    JSR $8579
# 03:ACE1 85 A9       STA $A9
# 03:ACE3 18          CLC
# 03:ACE4 69 05       ADC #$05
# 03:ACE6 AA          TAX
# 03:ACE7 BD 40 35    LDA $3540,x
# 03:ACEA D0 F2       BNE $ACDE (-14 B)
# 03:ACEC A5 A9       LDA $A9
# 03:ACEE AA          TAX
# 03:ACEF 7B          TDC
# 03:ACF0 20 5F 85    JSR $855F
# 03:ACF3 A6 A6       LDX $A6
# 03:ACF5 9D 53 20    STA $2053,x
# 03:ACF8 60          RTS
# 03:ACF9 A6 A6       LDX $A6
# 03:ACFB A9 09       LDA #$09
# 03:ACFD 9D 51 20    STA $2051,x
# 03:AD00 80 07       BRA $AD09 (+7 B)
# 03:AD02 A6 A6       LDX $A6
# 03:AD04 A9 1C       LDA #$1C
# 03:AD06 9D 51 20    STA $2051,x
# 03:AD09 A9 80       LDA #$80
# 03:AD0B 9D 50 20    STA $2050,x
# 03:AD0E 9E 53 20    STZ $2053,x
# 03:AD11 A5 8E       LDA $8E
# 03:AD13 AA          TAX
# 03:AD14 7B          TDC
# 03:AD15 20 5F 85    JSR $855F
# 03:AD18 A6 A6       LDX $A6
# 03:AD1A 9D 54 20    STA $2054,x
# 03:AD1D A2 01 00    LDX #$0001
# 03:AD20 86 D4       STX $D4
# 03:AD22 60          RTS
# 03:AD23 DA          PHX
# 03:AD24 5A          PHY
# 03:AD25 7B          TDC
# 03:AD26 AA          TAX
# 03:AD27 9B          TXY
# 03:AD28 86 A9       STX $A9
# 03:AD2A B9 40 35    LDA $3540,y
# 03:AD2D D0 09       BNE $AD38 (+9 B)
# 03:AD2F BD 03 20    LDA $2003,x
# 03:AD32 29 C0       AND #$C0
# 03:AD34 D0 02       BNE $AD38 (+2 B)
# 03:AD36 E6 A9       INC $A9
# 03:AD38 20 BC 85    JSR $85BC
# 03:AD3B C8          INY
# 03:AD3C C0 05 00    CPY #$0005
# 03:AD3F D0 E9       BNE $AD2A (-23 B)
# 03:AD41 7A          PLY
# 03:AD42 FA          PLX
# 03:AD43 A5 A9       LDA $A9
# 03:AD45 8D DB 38    STA $38DB
# 03:AD48 60          RTS
# 03:AD49 87 A8       STA [$A8]
# 03:AD4B BC A8 D6    LDY $D6A8,x
# 03:AD4E A8          TAY
# 03:AD4F ED A8 68    SBC $68A8
# 03:AD52 A9 C8       LDA #$C8
# 03:AD54 A9 DF       LDA #$DF
# 03:AD56 A9 A9       LDA #$A9
# 03:AD58 14 20       TRB $20
# 03:AD5A 85 80       STA $80
# 03:AD5C A2 80 02    LDX #$0280
# 03:AD5F 7B          TDC
# 03:AD60 A8          TAY
# 03:AD61 BD 01 20    LDA $2001,x
# 03:AD64 29 7F       AND #$7F
# 03:AD66 19 EB 35    ORA $35EB,y
# 03:AD69 9D 01 20    STA $2001,x
# 03:AD6C 20 BC 85    JSR $85BC
# 03:AD6F C8          INY
# 03:AD70 C0 08 00    CPY #$0008
# 03:AD73 D0 EC       BNE $AD61 (-20 B)
# 03:AD75 9C 0A 39    STZ $390A
# 03:AD78 9C E7 38    STZ $38E7
# 03:AD7B 9C 07 39    STZ $3907
# 03:AD7E 9C 5B 35    STZ $355B
# 03:AD81 9C 5C 35    STZ $355C
# 03:AD84 9C 53 35    STZ $3553
# 03:AD87 9C 5D 35    STZ $355D
# 03:AD8A 9C 2A 35    STZ $352A
# 03:AD8D 9C 84 35    STZ $3584
# 03:AD90 9C E2 38    STZ $38E2
# 03:AD93 9C EB 38    STZ $38EB
# 03:AD96 9C ED 38    STZ $38ED
# 03:AD99 A9 04       LDA #$04
# 03:AD9B 8D 78 2C    STA $2C78
# 03:AD9E 9C 79 2C    STZ $2C79
# 03:ADA1 A5 D2       LDA $D2
# 03:ADA3 20 89 84    JSR $8489
# 03:ADA6 20 FD B2    JSR $B2FD
# 03:ADA9 A5 D2       LDA $D2
# 03:ADAB C9 05       CMP #$05
# 03:ADAD 90 26       BCC $ADD5 (+38 B)
# 03:ADAF 38          SEC
# 03:ADB0 A5 D2       LDA $D2
# 03:ADB2 E9 05       SBC #$05
# 03:ADB4 85 DF       STA $DF
# 03:ADB6 A9 3C       LDA #$3C
# 03:ADB8 85 E1       STA $E1
# 03:ADBA 20 E0 83    JSR $83E0
# 03:ADBD 7B          TDC
# 03:ADBE A8          TAY
# 03:ADBF A6 E3       LDX $E3
# 03:ADC1 BD 59 36    LDA $3659,x
# 03:ADC4 99 C2 33    STA $33C2,y
# 03:ADC7 E8          INX
# 03:ADC8 C8          INY
# 03:ADC9 C0 3C 00    CPY #$003C
# 03:ADCC D0 F3       BNE $ADC1 (-13 B)
# 03:ADCE A5 D2       LDA $D2
# 03:ADD0 38          SEC
# 03:ADD1 E9 05       SBC #$05
# 03:ADD3 09 80       ORA #$80
# 03:ADD5 85 CD       STA $CD
# 03:ADD7 48          PHA
# 03:ADD8 48          PHA
# 03:ADD9 29 7F       AND #$7F
# 03:ADDB 8D C3 34    STA $34C3
# 03:ADDE 68          PLA
# 03:ADDF 29 80       AND #$80
# 03:ADE1 8D C2 34    STA $34C2
# 03:ADE4 68          PLA
# 03:ADE5 20 63 B1    JSR $B163
# 03:ADE8 A0 7F 00    LDY #$007F
# 03:ADEB B1 80       LDA ($80),y
# 03:ADED 99 80 26    STA $2680,y
# 03:ADF0 88          DEY
# 03:ADF1 10 F8       BPL $ADEB (-8 B)
# 03:ADF3 7B          TDC
# 03:ADF4 AA          TAX
# 03:ADF5 BD 40 35    LDA $3540,x
# 03:ADF8 9D 9C 28    STA $289C,x
# 03:ADFB E8          INX
# 03:ADFC E0 0D 00    CPX #$000D
# 03:ADFF D0 F4       BNE $ADF5 (-12 B)
# 03:AE01 A6 A6       LDX $A6
# 03:AE03 BD 53 20    LDA $2053,x
# 03:AE06 1D 54 20    ORA $2054,x
# 03:AE09 85 8A       STA $8A
# 03:AE0B D0 25       BNE $AE32 (+37 B)
# 03:AE0D A9 21       LDA #$21
# 03:AE0F 9D 51 20    STA $2051,x
# 03:AE12 A5 CD       LDA $CD
# 03:AE14 85 CE       STA $CE
# 03:AE16 10 4B       BPL $AE63 (+75 B)
# 03:AE18 20 48 B3    JSR $B348
# 03:AE1B A5 A9       LDA $A9
# 03:AE1D D0 0F       BNE $AE2E (+15 B)
# 03:AE1F A6 A6       LDX $A6
# 03:AE21 A9 E1       LDA #$E1
# 03:AE23 9D 51 20    STA $2051,x
# 03:AE26 8D C2 33    STA $33C2
# 03:AE29 A9 FF       LDA #$FF
# 03:AE2B 8D C3 33    STA $33C3
# 03:AE2E A5 CD       LDA $CD
# 03:AE30 80 31       BRA $AE63 (+49 B)
# 03:AE32 A2 00 00    LDX #$0000
# 03:AE35 0A          ASL a
# 03:AE36 B0 03       BCS $AE3B (+3 B)
# 03:AE38 E8          INX
# 03:AE39 D0 FA       BNE $AE35 (-6 B)
# 03:AE3B 8A          TXA
# 03:AE3C 85 CE       STA $CE
# 03:AE3E A6 A6       LDX $A6
# 03:AE40 BD 53 20    LDA $2053,x
# 03:AE43 F0 13       BEQ $AE58 (+19 B)
# 03:AE45 A9 80       LDA #$80
# 03:AE47 8D C4 34    STA $34C4
# 03:AE4A BD 53 20    LDA $2053,x
# 03:AE4D 20 8E B1    JSR $B18E
# 03:AE50 A5 CE       LDA $CE
# 03:AE52 09 80       ORA #$80
# 03:AE54 85 CE       STA $CE
# 03:AE56 80 0B       BRA $AE63 (+11 B)
# 03:AE58 9C C4 34    STZ $34C4
# 03:AE5B BD 54 20    LDA $2054,x
# 03:AE5E 20 8E B1    JSR $B18E
# 03:AE61 A5 CE       LDA $CE
# 03:AE63 20 63 B1    JSR $B163
# 03:AE66 A0 7F 00    LDY #$007F
# 03:AE69 B1 80       LDA ($80),y
# 03:AE6B 99 00 27    STA $2700,y
# 03:AE6E 88          DEY
# 03:AE6F 10 F8       BPL $AE69 (-8 B)
# 03:AE71 AD D2 26    LDA $26D2
# 03:AE74 C9 BA       CMP #$BA
# 03:AE76 D0 06       BNE $AE7E (+6 B)
# 03:AE78 EE E7 38    INC $38E7
# 03:AE7B 4C 6F AF    JMP $AF6F
# 03:AE7E A5 8A       LDA $8A
# 03:AE80 20 0C 85    JSR $850C
# 03:AE83 CA          DEX
# 03:AE84 F0 03       BEQ $AE89 (+3 B)
# 03:AE86 4C EB AF    JMP $AFEB
# 03:AE89 A5 CD       LDA $CD
# 03:AE8B 30 0F       BMI $AE9C (+15 B)
# 03:AE8D AD D1 26    LDA $26D1
# 03:AE90 85 A9       STA $A9
# 03:AE92 A5 CE       LDA $CE
# 03:AE94 30 0E       BMI $AEA4 (+14 B)
# 03:AE96 A5 A9       LDA $A9
# 03:AE98 D0 3D       BNE $AED7 (+61 B)
# 03:AE9A 80 08       BRA $AEA4 (+8 B)
# 03:AE9C 38          SEC
# 03:AE9D AD D1 26    LDA $26D1
# 03:AEA0 E9 C0       SBC #$C0
# 03:AEA2 85 A9       STA $A9
# 03:AEA4 AD 03 27    LDA $2703
# 03:AEA7 29 80       AND #$80
# 03:AEA9 F0 32       BEQ $AEDD (+50 B)
# 03:AEAB A5 A9       LDA $A9
# 03:AEAD F0 63       BEQ $AF12 (+99 B)
# 03:AEAF C9 03       CMP #$03
# 03:AEB1 B0 5F       BCS $AF12 (+95 B)
# 03:AEB3 C9 02       CMP #$02
# 03:AEB5 D0 19       BNE $AED0 (+25 B)
# 03:AEB7 AD D2 26    LDA $26D2
# 03:AEBA C9 13       CMP #$13
# 03:AEBC F0 19       BEQ $AED7 (+25 B)
# 03:AEBE C9 14       CMP #$14
# 03:AEC0 F0 15       BEQ $AED7 (+21 B)
# 03:AEC2 C9 AB       CMP #$AB
# 03:AEC4 F0 11       BEQ $AED7 (+17 B)
# 03:AEC6 C9 AC       CMP #$AC
# 03:AEC8 F0 0D       BEQ $AED7 (+13 B)
# 03:AECA C9 8F       CMP #$8F
# 03:AECC F0 09       BEQ $AED7 (+9 B)
# 03:AECE 80 42       BRA $AF12 (+66 B)
# 03:AED0 AD D2 26    LDA $26D2
# 03:AED3 C9 D4       CMP #$D4
# 03:AED5 D0 3B       BNE $AF12 (+59 B)
# 03:AED7 EE E7 38    INC $38E7
# 03:AEDA 4C 6F AF    JMP $AF6F
# 03:AEDD AD 03 27    LDA $2703
# 03:AEE0 29 40       AND #$40
# 03:AEE2 F0 22       BEQ $AF06 (+34 B)
# 03:AEE4 A5 A9       LDA $A9
# 03:AEE6 F0 2A       BEQ $AF12 (+42 B)
# 03:AEE8 C9 03       CMP #$03
# 03:AEEA B0 26       BCS $AF12 (+38 B)
# 03:AEEC C9 02       CMP #$02
# 03:AEEE D0 09       BNE $AEF9 (+9 B)
# 03:AEF0 AD D2 26    LDA $26D2
# 03:AEF3 C9 12       CMP #$12
# 03:AEF5 F0 78       BEQ $AF6F (+120 B)
# 03:AEF7 80 19       BRA $AF12 (+25 B)
# 03:AEF9 AD D2 26    LDA $26D2
# 03:AEFC C9 D5       CMP #$D5
# 03:AEFE F0 6F       BEQ $AF6F (+111 B)
# 03:AF00 C9 DD       CMP #$DD
# 03:AF02 F0 6B       BEQ $AF6F (+107 B)
# 03:AF04 80 0C       BRA $AF12 (+12 B)
# 03:AF06 AD 05 27    LDA $2705
# 03:AF09 29 02       AND #$02
# 03:AF0B D0 05       BNE $AF12 (+5 B)
# 03:AF0D AD 06 27    LDA $2706
# 03:AF10 10 5D       BPL $AF6F (+93 B)
# 03:AF12 A5 CE       LDA $CE
# 03:AF14 10 10       BPL $AF26 (+16 B)
# 03:AF16 A2 05 00    LDX #$0005
# 03:AF19 BD 9C 28    LDA $289C,x
# 03:AF1C F0 41       BEQ $AF5F (+65 B)
# 03:AF1E E8          INX
# 03:AF1F E0 0D 00    CPX #$000D
# 03:AF22 D0 F5       BNE $AF19 (-11 B)
# 03:AF24 80 0D       BRA $AF33 (+13 B)
# 03:AF26 7B          TDC
# 03:AF27 AA          TAX
# 03:AF28 BD 9C 28    LDA $289C,x
# 03:AF2B F0 32       BEQ $AF5F (+50 B)
# 03:AF2D E8          INX
# 03:AF2E E0 05 00    CPX #$0005
# 03:AF31 D0 F5       BNE $AF28 (-11 B)
# 03:AF33 A6 A6       LDX $A6
# 03:AF35 E0 80 02    CPX #$0280
# 03:AF38 90 14       BCC $AF4E (+20 B)
# 03:AF3A 20 48 B3    JSR $B348
# 03:AF3D A5 A9       LDA $A9
# 03:AF3F F0 09       BEQ $AF4A (+9 B)
# 03:AF41 A6 A6       LDX $A6
# 03:AF43 A9 E1       LDA #$E1
# 03:AF45 9D 51 20    STA $2051,x
# 03:AF48 80 25       BRA $AF6F (+37 B)
# 03:AF4A A9 E1       LDA #$E1
# 03:AF4C 80 02       BRA $AF50 (+2 B)
# 03:AF4E A9 21       LDA #$21
# 03:AF50 A6 A6       LDX $A6
# 03:AF52 9D 51 20    STA $2051,x
# 03:AF55 8D C2 33    STA $33C2
# 03:AF58 A9 FF       LDA #$FF
# 03:AF5A 8D C3 33    STA $33C3
# 03:AF5D 80 10       BRA $AF6F (+16 B)
# 03:AF5F 20 9E B0    JSR $B09E
# 03:AF62 48          PHA
# 03:AF63 A5 8B       LDA $8B
# 03:AF65 AA          TAX
# 03:AF66 A9 01       LDA #$01
# 03:AF68 9D 9C 28    STA $289C,x
# 03:AF6B 68          PLA
# 03:AF6C 4C 32 AE    JMP $AE32
# 03:AF6F A5 CE       LDA $CE
# 03:AF71 30 78       BMI $AFEB (+120 B)
# 03:AF73 AD 06 27    LDA $2706
# 03:AF76 29 02       AND #$02
# 03:AF78 D0 07       BNE $AF81 (+7 B)
# 03:AF7A AD 06 27    LDA $2706
# 03:AF7D 29 01       AND #$01
# 03:AF7F F0 6A       BEQ $AFEB (+106 B)
# 03:AF81 AD D1 26    LDA $26D1
# 03:AF84 C9 C0       CMP #$C0
# 03:AF86 D0 63       BNE $AFEB (+99 B)
# 03:AF88 AD 5E 35    LDA $355E
# 03:AF8B 30 5E       BMI $AFEB (+94 B)
# 03:AF8D A5 CD       LDA $CD
# 03:AF8F C5 CE       CMP $CE
# 03:AF91 F0 58       BEQ $AFEB (+88 B)
# 03:AF93 CD 5E 35    CMP $355E
# 03:AF96 F0 53       BEQ $AFEB (+83 B)
# 03:AF98 AD 5E 35    LDA $355E
# 03:AF9B C5 CE       CMP $CE
# 03:AF9D F0 4C       BEQ $AFEB (+76 B)
# 03:AF9F AD 5E 35    LDA $355E
# 03:AFA2 20 89 84    JSR $8489
# 03:AFA5 A6 A6       LDX $A6
# 03:AFA7 BD 03 20    LDA $2003,x
# 03:AFAA 29 C0       AND #$C0
# 03:AFAC D0 38       BNE $AFE6 (+56 B)
# 03:AFAE BD 04 20    LDA $2004,x
# 03:AFB1 29 3C       AND #$3C
# 03:AFB3 D0 31       BNE $AFE6 (+49 B)
# 03:AFB5 BD 05 20    LDA $2005,x
# 03:AFB8 29 50       AND #$50
# 03:AFBA D0 2A       BNE $AFE6 (+42 B)
# 03:AFBC BD 06 20    LDA $2006,x
# 03:AFBF 29 01       AND #$01
# 03:AFC1 D0 23       BNE $AFE6 (+35 B)
# 03:AFC3 AD C5 34    LDA $34C5
# 03:AFC6 8D C6 34    STA $34C6
# 03:AFC9 AE 5E 35    LDX $355E
# 03:AFCC 8A          TXA
# 03:AFCD 85 CE       STA $CE
# 03:AFCF 7B          TDC
# 03:AFD0 20 5F 85    JSR $855F
# 03:AFD3 8D C5 34    STA $34C5
# 03:AFD6 A5 CE       LDA $CE
# 03:AFD8 20 63 B1    JSR $B163
# 03:AFDB A0 7F 00    LDY #$007F
# 03:AFDE B1 80       LDA ($80),y
# 03:AFE0 99 00 27    STA $2700,y
# 03:AFE3 88          DEY
# 03:AFE4 10 F8       BPL $AFDE (-8 B)
# 03:AFE6 A5 D2       LDA $D2
# 03:AFE8 20 89 84    JSR $8489
# 03:AFEB A5 CD       LDA $CD
# 03:AFED 10 05       BPL $AFF4 (+5 B)
# 03:AFEF 29 7F       AND #$7F
# 03:AFF1 18          CLC
# 03:AFF2 69 05       ADC #$05
# 03:AFF4 AA          TAX
# 03:AFF5 9E 60 35    STZ $3560,x
# 03:AFF8 A5 D2       LDA $D2
# 03:AFFA 20 89 84    JSR $8489
# 03:AFFD A6 A6       LDX $A6
# 03:AFFF BD 03 20    LDA $2003,x
# 03:B002 29 C0       AND #$C0
# 03:B004 D0 07       BNE $B00D (+7 B)
# 03:B006 BD 04 20    LDA $2004,x
# 03:B009 29 30       AND #$30
# 03:B00B F0 0F       BEQ $B01C (+15 B)
# 03:B00D A5 D2       LDA $D2
# 03:B00F C9 05       CMP #$05
# 03:B011 90 04       BCC $B017 (+4 B)
# 03:B013 A9 E1       LDA #$E1
# 03:B015 80 02       BRA $B019 (+2 B)
# 03:B017 A9 21       LDA #$21
# 03:B019 9D 51 20    STA $2051,x
# 03:B01C 20 FF B0    JSR $B0FF
# 03:B01F 7B          TDC
# 03:B020 AD B3 38    LDA $38B3
# 03:B023 D0 1D       BNE $B042 (+29 B)
# 03:B025 AD CD 00    LDA $00CD
# 03:B028 10 18       BPL $B042 (+24 B)
# 03:B02A 7B          TDC
# 03:B02B AA          TAX
# 03:B02C BD C2 33    LDA $33C2,x
# 03:B02F C9 FF       CMP #$FF
# 03:B031 F0 0F       BEQ $B042 (+15 B)
# 03:B033 C9 FC       CMP #$FC
# 03:B035 F0 03       BEQ $B03A (+3 B)
# 03:B037 E8          INX
# 03:B038 80 F2       BRA $B02C (-14 B)
# 03:B03A A9 FF       LDA #$FF
# 03:B03C 8D 01 36    STA $3601
# 03:B03F 8D 02 36    STA $3602
# 03:B042 AD E4 38    LDA $38E4
# 03:B045 D0 03       BNE $B04A (+3 B)
# 03:B047 20 88 B2    JSR $B288
# 03:B04A 20 A0 B1    JSR $B1A0
# 03:B04D 9C E4 38    STZ $38E4
# 03:B050 7B          TDC
# 03:B051 AA          TAX
# 03:B052 BD C2 33    LDA $33C2,x
# 03:B055 9D 9C 28    STA $289C,x
# 03:B058 C9 FF       CMP #$FF
# 03:B05A F0 03       BEQ $B05F (+3 B)
# 03:B05C E8          INX
# 03:B05D 80 F3       BRA $B052 (-13 B)
# 03:B05F A9 05       LDA #$05
# 03:B061 20 85 80    JSR $8085
# 03:B064 A9 0C       LDA #$0C
# 03:B066 20 85 80    JSR $8085
# 03:B069 A9 0B       LDA #$0B
# 03:B06B 20 85 80    JSR $8085
# 03:B06E AD 0A 39    LDA $390A
# 03:B071 D0 2A       BNE $B09D (+42 B)
# 03:B073 64 D6       STZ $D6
# 03:B075 A5 CD       LDA $CD
# 03:B077 10 05       BPL $B07E (+5 B)
# 03:B079 29 7F       AND #$7F
# 03:B07B 18          CLC
# 03:B07C 69 05       ADC #$05
# 03:B07E 20 2C 9E    JSR $9E2C
# 03:B081 AE 30 35    LDX $3530
# 03:B084 BD 09 2A    LDA $2A09,x
# 03:B087 29 76       AND #$76
# 03:B089 9D 09 2A    STA $2A09,x
# 03:B08C A6 A6       LDX $A6
# 03:B08E BD 04 20    LDA $2004,x
# 03:B091 29 30       AND #$30
# 03:B093 D0 08       BNE $B09D (+8 B)
# 03:B095 A9 03       LDA #$03
# 03:B097 20 C8 85    JSR $85C8
# 03:B09A 9E 06 2A    STZ $2A06,x
# 03:B09D 60          RTS
# 03:B09E A2 04 00    LDX #$0004
# 03:B0A1 A5 CE       LDA $CE
# 03:B0A3 10 03       BPL $B0A8 (+3 B)
# 03:B0A5 A2 07 00    LDX #$0007
# 03:B0A8 8A          TXA
# 03:B0A9 A2 00 00    LDX #$0000
# 03:B0AC 20 79 83    JSR $8379
# 03:B0AF 85 AA       STA $AA
# 03:B0B1 85 8B       STA $8B
# 03:B0B3 A5 CE       LDA $CE
# 03:B0B5 10 07       BPL $B0BE (+7 B)
# 03:B0B7 18          CLC
# 03:B0B8 A5 8B       LDA $8B
# 03:B0BA 69 05       ADC #$05
# 03:B0BC 85 8B       STA $8B
# 03:B0BE A5 8B       LDA $8B
# 03:B0C0 AA          TAX
# 03:B0C1 BD 40 35    LDA $3540,x
# 03:B0C4 D0 D8       BNE $B09E (-40 B)
# 03:B0C6 A5 CE       LDA $CE
# 03:B0C8 10 12       BPL $B0DC (+18 B)
# 03:B0CA A5 8B       LDA $8B
# 03:B0CC 85 DF       STA $DF
# 03:B0CE A9 80       LDA #$80
# 03:B0D0 85 E1       STA $E1
# 03:B0D2 20 E0 83    JSR $83E0
# 03:B0D5 A6 E3       LDX $E3
# 03:B0D7 BD 01 20    LDA $2001,x
# 03:B0DA 30 C2       BMI $B09E (-62 B)
# 03:B0DC A5 AA       LDA $AA
# 03:B0DE AA          TAX
# 03:B0DF 7B          TDC
# 03:B0E0 20 5F 85    JSR $855F
# 03:B0E3 48          PHA
# 03:B0E4 A5 D2       LDA $D2
# 03:B0E6 20 89 84    JSR $8489
# 03:B0E9 A6 A6       LDX $A6
# 03:B0EB A5 CE       LDA $CE
# 03:B0ED 30 08       BMI $B0F7 (+8 B)
# 03:B0EF 68          PLA
# 03:B0F0 9D 54 20    STA $2054,x
# 03:B0F3 8D D4 26    STA $26D4
# 03:B0F6 60          RTS
# 03:B0F7 68          PLA
# 03:B0F8 9D 53 20    STA $2053,x
# 03:B0FB 8D D3 26    STA $26D3
# 03:B0FE 60          RTS
# 03:B0FF A5 D2       LDA $D2
# 03:B101 C9 05       CMP #$05
# 03:B103 90 17       BCC $B11C (+23 B)
# 03:B105 A6 A6       LDX $A6
# 03:B107 BD 51 20    LDA $2051,x
# 03:B10A C9 C0       CMP #$C0
# 03:B10C 90 04       BCC $B112 (+4 B)
# 03:B10E C9 E1       CMP #$E1
# 03:B110 90 02       BCC $B114 (+2 B)
# 03:B112 A9 E1       LDA #$E1
# 03:B114 8D FF 35    STA $35FF
# 03:B117 38          SEC
# 03:B118 E9 C0       SBC #$C0
# 03:B11A 80 32       BRA $B14E (+50 B)
# 03:B11C A9 F8       LDA #$F8
# 03:B11E 8D C2 33    STA $33C2
# 03:B121 A9 02       LDA #$02
# 03:B123 8D C3 33    STA $33C3
# 03:B126 A6 A6       LDX $A6
# 03:B128 BD 51 20    LDA $2051,x
# 03:B12B C9 02       CMP #$02
# 03:B12D F0 08       BEQ $B137 (+8 B)
# 03:B12F C9 07       CMP #$07
# 03:B131 F0 04       BEQ $B137 (+4 B)
# 03:B133 C9 20       CMP #$20
# 03:B135 D0 03       BNE $B13A (+3 B)
# 03:B137 EE 5D 35    INC $355D
# 03:B13A 48          PHA
# 03:B13B 18          CLC
# 03:B13C 69 C0       ADC #$C0
# 03:B13E 8D C4 33    STA $33C4
# 03:B141 C9 C1       CMP #$C1
# 03:B143 D0 02       BNE $B147 (+2 B)
# 03:B145 A9 C2       LDA #$C2
# 03:B147 8D FF 35    STA $35FF
# 03:B14A 9C C5 33    STZ $33C5
# 03:B14D 68          PLA
# 03:B14E 0A          ASL a
# 03:B14F AA          TAX
# 03:B150 BF 6C B3 03 LDA $03B36C,x
# 03:B154 85 80       STA $80
# 03:B156 BF 6D B3 03 LDA $03B36D,x
# 03:B15A 85 81       STA $81
# 03:B15C A9 03       LDA #$03
# 03:B15E 85 82       STA $82
# 03:B160 DC 80 00    JML ($0080)
# 03:B163 48          PHA
# 03:B164 29 7F       AND #$7F
# 03:B166 85 DF       STA $DF
# 03:B168 A9 80       LDA #$80
# 03:B16A 85 E1       STA $E1
# 03:B16C 20 E0 83    JSR $83E0
# 03:B16F 68          PLA
# 03:B170 30 0E       BMI $B180 (+14 B)
# 03:B172 18          CLC
# 03:B173 A5 E3       LDA $E3
# 03:B175 69 00       ADC #$00
# 03:B177 85 80       STA $80
# 03:B179 A5 E4       LDA $E4
# 03:B17B 69 20       ADC #$20
# 03:B17D 85 81       STA $81
# 03:B17F 60          RTS
# 03:B180 18          CLC
# 03:B181 A5 E3       LDA $E3
# 03:B183 69 80       ADC #$80
# 03:B185 85 80       STA $80
# 03:B187 A5 E4       LDA $E4
# 03:B189 69 22       ADC #$22
# 03:B18B 85 81       STA $81
# 03:B18D 60          RTS
# 03:B18E 8D C5 34    STA $34C5
# 03:B191 20 0C 85    JSR $850C
# 03:B194 CA          DEX
# 03:B195 F0 08       BEQ $B19F (+8 B)
# 03:B197 AD C4 34    LDA $34C4
# 03:B19A 09 40       ORA #$40
# 03:B19C 8D C4 34    STA $34C4
# 03:B19F 60          RTS
# 03:B1A0 7B          TDC
# 03:B1A1 AA          TAX
# 03:B1A2 86 A9       STX $A9
# 03:B1A4 A5 A9       LDA $A9
# 03:B1A6 20 89 84    JSR $8489
# 03:B1A9 A6 A6       LDX $A6
# 03:B1AB A5 A9       LDA $A9
# 03:B1AD 20 7F 84    JSR $847F
# 03:B1B0 A8          TAY
# 03:B1B1 B9 8E 33    LDA $338E,y
# 03:B1B4 1D 03 20    ORA $2003,x
# 03:B1B7 9D 03 20    STA $2003,x
# 03:B1BA 29 C0       AND #$C0
# 03:B1BC F0 76       BEQ $B234 (+118 B)
# 03:B1BE 29 80       AND #$80
# 03:B1C0 F0 06       BEQ $B1C8 (+6 B)
# 03:B1C2 9E 07 20    STZ $2007,x
# 03:B1C5 9E 08 20    STZ $2008,x
# 03:B1C8 DA          PHX
# 03:B1C9 A5 A9       LDA $A9
# 03:B1CB 0A          ASL a
# 03:B1CC AA          TAX
# 03:B1CD 7B          TDC
# 03:B1CE 9D EB 29    STA $29EB,x
# 03:B1D1 A5 A9       LDA $A9
# 03:B1D3 C9 05       CMP #$05
# 03:B1D5 90 4F       BCC $B226 (+79 B)
# 03:B1D7 38          SEC
# 03:B1D8 E9 05       SBC #$05
# 03:B1DA AA          TAX
# 03:B1DB BD BD 29    LDA $29BD,x
# 03:B1DE 85 AA       STA $AA
# 03:B1E0 BD B5 29    LDA $29B5,x
# 03:B1E3 30 34       BMI $B219 (+52 B)
# 03:B1E5 A9 FF       LDA #$FF
# 03:B1E7 9D B5 29    STA $29B5,x
# 03:B1EA A5 AA       LDA $AA
# 03:B1EC AA          TAX
# 03:B1ED BD CA 29    LDA $29CA,x
# 03:B1F0 F0 03       BEQ $B1F5 (+3 B)
# 03:B1F2 DE CA 29DEC $29CA,x
# 03:B1F5 FA          PLX
# 03:B1F6 DA          PHX
# 03:B1F7 BD 51 20    LDA $2051,x
# 03:B1FA C9 E4       CMP #$E4
# 03:B1FC F0 0B       BEQ $B209 (+11 B)
# 03:B1FE AD 82 38    LDA $3882
# 03:B201 D0 06       BNE $B209 (+6 B)
# 03:B203 A5 AA       LDA $AA
# 03:B205 AA          TAX
# 03:B206 FE 85 35    INC $3585,x
# 03:B209 A5 AA       LDA $AA
# 03:B20B AA          TAX
# 03:B20C BD CA 29    LDA $29CA,x
# 03:B20F D0 08       BNE $B219 (+8 B)
# 03:B211 A5 AA       LDA $AA
# 03:B213 AA          TAX
# 03:B214 A9 FF       LDA #$FF
# 03:B216 9D AD 29    STA $29AD,x
# 03:B219 18          CLC
# 03:B21A AD CA 29    LDA $29CA
# 03:B21D 6D CB 29    ADC $29CB
# 03:B220 6D CC 29    ADC $29CC
# 03:B223 8D CD 29    STA $29CD
# 03:B226 FA          PLX
# 03:B227 BD 03 20    LDA $2003,x
# 03:B22A 29 80       AND #$80
# 03:B22C F0 06       BEQ $B234 (+6 B)
# 03:B22E 9E 07 20    STZ $2007,x
# 03:B231 9E 08 20    STZ $2008,x
# 03:B234 C8          INY
# 03:B235 B9 8E 33    LDA $338E,y
# 03:B238 1D 04 20    ORA $2004,x
# 03:B23B 9D 04 20    STA $2004,x
# 03:B23E C8          INY
# 03:B23F B9 8E 33    LDA $338E,y
# 03:B242 1D 05 20    ORA $2005,x
# 03:B245 9D 05 20    STA $2005,x
# 03:B248 C8          INY
# 03:B249 B9 8E 33    LDA $338E,y
# 03:B24C 1D 06 20    ORA $2006,x
# 03:B24F 9D 06 20    STA $2006,x
# 03:B252 BD 03 20    LDA $2003,x
# 03:B255 29 C0       AND #$C0
# 03:B257 F0 20       BEQ $B279 (+32 B)
# 03:B259 BD 03 20    LDA $2003,x
# 03:B25C 29 FE       AND #$FE
# 03:B25E 9D 03 20    STA $2003,x
# 03:B261 BD 04 20    LDA $2004,x
# 03:B264 29 C0       AND #$C0
# 03:B266 9D 04 20    STA $2004,x
# 03:B269 BD 05 20    LDA $2005,x
# 03:B26C 29 A2       AND #$A2
# 03:B26E 9D 05 20    STA $2005,x
# 03:B271 BD 06 20    LDA $2006,x
# 03:B274 29 80       AND #$80
# 03:B276 9D 06 20    STA $2006,x
# 03:B279 E6 A9       INC $A9
# 03:B27B A5 A9       LDA $A9
# 03:B27D C9 0D       CMP #$0D
# 03:B27F F0 03       BEQ $B284 (+3 B)
# 03:B281 4C A4 B1    JMP $B1A4
# 03:B284 9C 82 38    STZ $3882
# 03:B287 60          RTS
# 03:B288 AD C4 34    LDA $34C4
# 03:B28B 10 49       BPL $B2D6 (+73 B)
# 03:B28D AD C5 34    LDA $34C5
# 03:B290 85 A9       STA $A9
# 03:B292 7B          TDC
# 03:B293 AA          TAX
# 03:B294 06 A9       ASL $A9
# 03:B296 90 38       BCC $B2D0 (+56 B)
# 03:B298 8A          TXA
# 03:B299 20 7C 84    JSR $847C
# 03:B29C A8          TAY
# 03:B29D 84 AB       STY $AB
# 03:B29F BD F7 35    LDA $35F7,x
# 03:B2A2 1A          INC a
# 03:B2A3 C9 04       CMP #$04
# 03:B2A5 D0 07       BNE $B2AE (+7 B)
# 03:B2A7 DA          PHX
# 03:B2A8 20 D7 B2    JSR $B2D7
# 03:B2AB FA          PLX
# 03:B2AC A9 03       LDA #$03
# 03:B2AE 9D F7 35    STA $35F7,x
# 03:B2B1 20 7F 84    JSR $847F
# 03:B2B4 18          CLC
# 03:B2B5 65 AB       ADC $AB
# 03:B2B7 A8          TAY
# 03:B2B8 A5 CD       LDA $CD
# 03:B2BA 99 78 2B    STA $2B78,y
# 03:B2BD AD FF 35    LDA $35FF
# 03:B2C0 99 79 2B    STA $2B79,y
# 03:B2C3 7B          TDC
# 03:B2C4 99 7A 2B    STA $2B7A,y
# 03:B2C7 AD 00 36    LDA $3600
# 03:B2CA 99 7B 2B    STA $2B7B,y
# 03:B2CD 9C 00 36    STZ $3600
# 03:B2D0 E8          INX
# 03:B2D1 E0 08 00    CPX #$0008
# 03:B2D4 D0 BE       BNE $B294 (-66 B)
# 03:B2D6 60          RTS
# 03:B2D7 DA          PHX
# 03:B2D8 7B          TDC
# 03:B2D9 A8          TAY
# 03:B2DA A6 AB       LDX $AB
# 03:B2DC BD 78 2B    LDA $2B78,x
# 03:B2DF 99 9C 28    STA $289C,y
# 03:B2E2 E8          INX
# 03:B2E3 C8          INY
# 03:B2E4 C0 20 00    CPY #$0020
# 03:B2E7 D0 F3       BNE $B2DC (-13 B)
# 03:B2E9 A0 04 00    LDY #$0004
# 03:B2EC A6 AB       LDX $AB
# 03:B2EE B9 9C 28    LDA $289C,y
# 03:B2F1 9D 78 2B    STA $2B78,x
# 03:B2F4 E8          INX
# 03:B2F5 C8          INY
# 03:B2F6 C0 24 00    CPY #$0024
# 03:B2F9 D0 F3       BNE $B2EE (-13 B)
# 03:B2FB FA          PLX
# 03:B2FC 60          RTS

# 03:B2FD A9 FF       LDA #$FF   Puts #$FF in $33C2-$34C1
# 03:B2FF A2 FF 00    LDX #$00FF
# 03:B302 9D C2 33    STA $33C2,x
# 03:B305 CA          DEX
# 03:B306 10 FA       BPL $B302 (-6 B)

# 03:B308 A2 11 00    LDX #$0011   Puts #$00 in $34C2-$34D3
# 03:B30B 9E C2 34    STZ $34C2,x
# 03:B30E CA          DEX
# 03:B30F 10 FA       BPL $B30B (-6 B)

# 03:B311 9C 28 35    STZ $3528   Puts #$00 in these three bytes
# 03:B314 9C 29 35    STZ $3529
# 03:B317 9C 2A 35    STZ $352A

# 03:B31A A2 33 00    LDX #$0033   Puts #$00 in $338E-$33C1
# 03:B31D 9E 8E 33    STZ $338E,x
# 03:B320 CA          DEX
# 03:B321 10 FA       BPL $B31D (-6 B)

# 03:B323 A9 FF       LDA #$FF   Puts #$FF in $34CA-$34D3
# 03:B325 A2 09 00    LDX #$0009
# 03:B328 9D CA 34    STA $34CA,x
# 03:B32B CA          DEX
# 03:B32C 10 FA       BPL $B328 (-6 B)

# 03:B32E AD 01 36    LDA $3601   If $3601 == #$FF, puts #$00 in $34D4-$3521
# 03:B331 C9 FF       CMP #$FF
# 03:B333 D0 09       BNE $B33E (+9 B)
# 03:B335 A2 4D 00    LDX #$004D
# 03:B338 9E D4 34    STZ $34D4,x
# 03:B33B CA          DEX
# 03:B33C 10 FA       BPL $B338 (-6 B)

# 03:B33E 60          RTS


# 03:B33F AD C2 34    LDA $34C2
# 03:B342 09 40       ORA #$40
# 03:B344 8D C2 34    STA $34C2
# 03:B347 60          RTS
# 03:B348 7B          TDC
# 03:B349 AA          TAX
# 03:B34A 86 A9       STX $A9
# 03:B34C BD C2 33    LDA $33C2,x
# 03:B34F C9 FF       CMP #$FF
# 03:B351 F0 18       BEQ $B36B (+24 B)
# 03:B353 C9 FC       CMP #$FC
# 03:B355 F0 03       BEQ $B35A (+3 B)
# 03:B357 E8          INX
# 03:B358 80 F2       BRA $B34C (-14 B)
# 03:B35A E6 A9       INC $A9
# 03:B35C A9 E1       LDA #$E1
# 03:B35E 8D C2 33    STA $33C2
# 03:B361 A9 FC       LDA #$FC
# 03:B363 8D C3 33    STA $33C3
# 03:B366 A9 FF       LDA #$FF
# 03:B368 8D C4 33    STA $33C4
# 03:B36B 60          RTS
# 03:B36C 89 C4       BIT #$C4
# 03:B36E A2 EB 36    LDX #$36EB
# 03:B371 CC 00 00    CPY $0000
# 03:B374 00 00       BRK #$00
# 03:B376 EC E9 57    CPX $57E9
# 03:B379 E9 1D       SBC #$1D
# 03:B37B EA          NOP
# 03:B37C 03 E9       ORA $E9,s
# 03:B37E 79 E8 DF    ADC $DFE8,y
# 03:B381 E2 55       SEP #$55
# 03:B383 E8          INX
# 03:B384 39 E8 F3    AND $F3E8,y
# 03:B387 E7 B7       SBC [$B7]
# 03:B389 E6 99       INC $99
# 03:B38B E6 41       INC $41
# 03:B38D E4 C6       CPX $C6
# 03:B38F EA          NOP
# 03:B390 E9 EA       SBC #$EA
# 03:B392 12 E6       ORA ($E6)
# 03:B394 56 E1       LSR $E1,x
# 03:B396 00 00       BRK #$00
# 03:B398 4F E3 CF E1 EOR $E1CFE3
# 03:B39C 00 00       BRK #$00
# 03:B39E FD E3 D4    SBC $D4E3,x
# 03:B3A1 E3 E1       SBC $E1,s
# 03:B3A3 E9 BE       SBC #$BE
# 03:B3A5 E8          INX
# 03:B3A6 56 E6       LSR $E6,x
# 03:B3A8 A5 E9       LDA $E9
# 03:B3AA 26 E8       ROL $E8
# 03:B3AC E6 E4       INC $E4
# 03:B3AE 3F B3 3B E4 AND $E43BB3,x
# 03:B3B2 00 00       BRK #$00
# 03:B3B4 B5 E1       LDA $E1,x
# 03:B3B6 20 F2 BB    JSR $BBF2
# 03:B3B9 20 12 BC    JSR $BC12
# 03:B3BC 38          SEC
# 03:B3BD A5 D2       LDA $D2
# 03:B3BF E9 05       SBC #$05
# 03:B3C1 8D 1C 36    STA $361C
# 03:B3C4 AD 77 38    LDA $3877
# 03:B3C7 F0 0D       BEQ $B3D6 (+13 B)
# 03:B3C9 A5 D2       LDA $D2
# 03:B3CB CD 01 36    CMP $3601
# 03:B3CE F0 03       BEQ $B3D3 (+3 B)
# 03:B3D0 4C 8C B6    JMP $B68C
# 03:B3D3 4C 17 B5    JMP $B517
# 03:B3D6 20 FD B2    JSR $B2FD
# 03:B3D9 A6 A6       LDX $A6
# 03:B3DB 9E 50 20    STZ $2050,x
# 03:B3DE 9E 52 20    STZ $2052,x
# 03:B3E1 9E 55 20    STZ $2055,x
# 03:B3E4 9E 56 20    STZ $2056,x
# 03:B3E7 9E 53 20    STZ $2053,x
# 03:B3EA 9E 54 20    STZ $2054,x
# 03:B3ED A9 E1       LDA #$E1
# 03:B3EF 9D 51 20    STA $2051,x
# 03:B3F2 BD 04 20    LDA $2004,x
# 03:B3F5 29 0C       AND #$0C
# 03:B3F7 D0 29       BNE $B422 (+41 B)
# 03:B3F9 BD 70 20    LDA $2070,x
# 03:B3FC 30 24       BMI $B422 (+36 B)
# 03:B3FE AD 83 35    LDA $3583
# 03:B401 DD 2F 20    CMP $202F,x
# 03:B404 90 1C       BCC $B422 (+28 B)
# 03:B406 20 93 85    JSR $8593
# 03:B409 A6 A6       LDX $A6
# 03:B40B DD 29 20    CMP $2029,x
# 03:B40E B0 12       BCS $B422 (+18 B)
# 03:B410 AD E5 38    LDA $38E5
# 03:B413 29 01       AND #$01
# 03:B415 D0 0B       BNE $B422 (+11 B)
# 03:B417 A9 E4       LDA #$E4
# 03:B419 9D 51 20    STA $2051,x
# 03:B41C 20 DE BB    JSR $BBDE
# 03:B41F 4C 8C B6    JMP $B68C
# 03:B422 A6 A6       LDX $A6
# 03:B424 BD 04 20    LDA $2004,x
# 03:B427 29 04       AND #$04
# 03:B429 F0 21       BEQ $B44C (+33 B)
# 03:B42B A9 05       LDA #$05
# 03:B42D 85 AB       STA $AB
# 03:B42F A6 A6       LDX $A6
# 03:B431 9E 51 20    STZ $2051,x
# 03:B434 20 31 BC    JSR $BC31
# 03:B437 BD 51 20    LDA $2051,x
# 03:B43A C9 E1       CMP #$E1
# 03:B43C F0 08       BEQ $B446 (+8 B)
# 03:B43E A9 C0       LDA #$C0
# 03:B440 9D 51 20    STA $2051,x
# 03:B443 8D 39 38    STA $3839
# 03:B446 20 DE BB    JSR $BBDE
# 03:B449 4C 8C B6    JMP $B68C
# 03:B44C 9C BE 35    STZ $35BE
# 03:B44F AD 1C 36    LDA $361C
# 03:B452 85 DF       STA $DF
# 03:B454 A9 14       LDA #$14
# 03:B456 85 E1       STA $E1
# 03:B458 20 E0 83    JSR $83E0
# 03:B45B A6 E3       LDX $E3
# 03:B45D 8E C3 35    STX $35C3
# 03:B460 8E C5 35    STX $35C5
# 03:B463 AD 1C 36    LDA $361C
# 03:B466 AA          TAX
# 03:B467 8E 3D 39    STX $393D
# 03:B46A A2 58 02    LDX #$0258
# 03:B46D 8E 3F 39    STX $393F
# 03:B470 20 B9 83    JSR $83B9
# 03:B473 AE 41 39    LDX $3941
# 03:B476 8E BF 35    STX $35BF
# 03:B479 8E C1 35    STX $35C1
# 03:B47C AD 1C 36    LDA $361C
# 03:B47F 85 DF       STA $DF
# 03:B481 A9 28       LDA #$28
# 03:B483 85 E1       STA $E1
# 03:B485 20 E0 83    JSR $83E0
# 03:B488 A6 E3       LDX $E3
# 03:B48A 8E C8 35    STX $35C8
# 03:B48D AD 1C 36    LDA $361C
# 03:B490 AA          TAX
# 03:B491 8E 3D 39    STX $393D
# 03:B494 A2 A0 00    LDX #$00A0
# 03:B497 8E 3F 39    STX $393F
# 03:B49A 20 B9 83    JSR $83B9
# 03:B49D AE 41 39    LDX $3941
# 03:B4A0 8E CC 35    STX $35CC
# 03:B4A3 AD BE 35    LDA $35BE
# 03:B4A6 20 7F 84    JSR $847F
# 03:B4A9 18          CLC
# 03:B4AA 6D C8 35    ADC $35C8
# 03:B4AD 8D CA 35    STA $35CA
# 03:B4B0 AD C9 35    LDA $35C9
# 03:B4B3 69 00       ADC #$00
# 03:B4B5 8D CB 35    STA $35CB
# 03:B4B8 AD BE 35    LDA $35BE
# 03:B4BB AA          TAX
# 03:B4BC C2 20       REP #$20
# 03:B4BE 8A          TXA
# 03:B4BF 20 7D 84    JSR $847D
# 03:B4C2 18          CLC
# 03:B4C3 6D CC 35    ADC $35CC
# 03:B4C6 8D CE 35    STA $35CE
# 03:B4C9 7B          TDC
# 03:B4CA E2 20       SEP #$20
# 03:B4CC AE C5 35    LDX $35C5
# 03:B4CF BD 7F 39    LDA $397F,x
# 03:B4D2 F0 43       BEQ $B517 (+67 B)
# 03:B4D4 9C C7 35    STZ $35C7
# 03:B4D7 AE CA 35    LDX $35CA
# 03:B4DA BD 1F 3A    LDA $3A1F,x
# 03:B4DD C9 FF       CMP #$FF
# 03:B4DF F0 36       BEQ $B517 (+54 B)
# 03:B4E1 7B          TDC
# 03:B4E2 A8          TAY
# 03:B4E3 AE CE 35    LDX $35CE
# 03:B4E6 BD 5F 3B    LDA $3B5F,x
# 03:B4E9 99 9C 28    STA $289C,y
# 03:B4EC E8          INX
# 03:B4ED C8          INY
# 03:B4EE C0 04 00    CPY #$0004
# 03:B4F1 D0 F3       BNE $B4E6 (-13 B)
# 03:B4F3 8E CE 35    STX $35CE
# 03:B4F6 20 CE BC    JSR $BCCE
# 03:B4F9 A5 DE       LDA $DE
# 03:B4FB F0 0F       BEQ $B50C (+15 B)
# 03:B4FD EE CA 35    INC $35CA
# 03:B500 EE C7 35    INC $35C7
# 03:B503 AD C7 35    LDA $35C7
# 03:B506 C9 04       CMP #$04
# 03:B508 D0 CD       BNE $B4D7 (-51 B)
# 03:B50A F0 0B       BEQ $B517 (+11 B)
# 03:B50C EE C5 35    INC $35C5
# 03:B50F EE C5 35    INC $35C5
# 03:B512 EE BE 35    INC $35BE
# 03:B515 80 8C       BRA $B4A3 (-116 B)
# 03:B517 AD 1C 36    LDA $361C
# 03:B51A AA          TAX
# 03:B51B AD BE 35    LDA $35BE
# 03:B51E DD 04 36    CMP $3604,x
# 03:B521 F0 09       BEQ $B52C (+9 B)
# 03:B523 9D 04 36    STA $3604,x
# 03:B526 20 4C B7    JSR $B74C
# 03:B529 9C 77 38    STZ $3877
# 03:B52C AD 77 38    LDA $3877
# 03:B52F D0 51       BNE $B582 (+81 B)
# 03:B531 7B          TDC
# 03:B532 AA          TAX
# 03:B533 8E 75 38    STX $3875
# 03:B536 AD 1C 36    LDA $361C
# 03:B539 0A          ASL a
# 03:B53A AA          TAX
# 03:B53B BD 0C 36    LDA $360C,x
# 03:B53E 8D C1 35    STA $35C1
# 03:B541 BD 0D 36    LDA $360D,x
# 03:B544 8D C2 35    STA $35C2
# 03:B547 AE C1 35    LDX $35C1
# 03:B54A 7B          TDC
# 03:B54B A8          TAY
# 03:B54C BD 5F 40    LDA $405F,x
# 03:B54F C9 FF       CMP #$FF
# 03:B551 D0 07       BNE $B55A (+7 B)
# 03:B553 A9 E1       LDA #$E1
# 03:B555 99 1D 36    STA $361D,y
# 03:B558 80 12       BRA $B56C (+18 B)
# 03:B55A BD 5F 40    LDA $405F,x
# 03:B55D E8          INX
# 03:B55E 99 1D 36    STA $361D,y
# 03:B561 C9 FE       CMP #$FE
# 03:B563 F0 0C       BEQ $B571 (+12 B)
# 03:B565 C9 FF       CMP #$FF
# 03:B567 F0 03       BEQ $B56C (+3 B)
# 03:B569 C8          INY
# 03:B56A 80 EE       BRA $B55A (-18 B)
# 03:B56C 20 4C B7    JSR $B74C
# 03:B56F 80 11       BRA $B582 (+17 B)
# 03:B571 86 A9       STX $A9
# 03:B573 AD 1C 36    LDA $361C
# 03:B576 0A          ASL a
# 03:B577 AA          TAX
# 03:B578 A5 A9       LDA $A9
# 03:B57A 9D 0C 36    STA $360C,x
# 03:B57D A5 AA       LDA $AA
# 03:B57F 9D 0D 36    STA $360D,x
# 03:B582 7B          TDC
# 03:B583 A8          TAY
# 03:B584 AE 75 38    LDX $3875
# 03:B587 BD 1D 36    LDA $361D,x
# 03:B58A C9 C0       CMP #$C0
# 03:B58C B0 13       BCS $B5A1 (+19 B)
# 03:B58E 99 3A 38    STA $383A,y
# 03:B591 DA          PHX
# 03:B592 A6 A6       LDX $A6
# 03:B594 9D 52 20    STA $2052,x
# 03:B597 FA          PLX
# 03:B598 A9 C2       LDA #$C2
# 03:B59A 99 39 38    STA $3839,y
# 03:B59D C8          INY
# 03:B59E E8          INX
# 03:B59F 80 26       BRA $B5C7 (+38 B)
# 03:B5A1 99 39 38    STA $3839,y
# 03:B5A4 E8          INX
# 03:B5A5 C9 FA       CMP #$FA
# 03:B5A7 B0 0E       BCS $B5B7 (+14 B)
# 03:B5A9 C9 E8       CMP #$E8
# 03:B5AB 90 1A       BCC $B5C7 (+26 B)
# 03:B5AD C8          INY
# 03:B5AE BD 1D 36    LDA $361D,x
# 03:B5B1 99 39 38    STA $3839,y
# 03:B5B4 E8          INX
# 03:B5B5 80 10       BRA $B5C7 (+16 B)
# 03:B5B7 C9 FB       CMP #$FB
# 03:B5B9 F0 12       BEQ $B5CD (+18 B)
# 03:B5BB C9 FC       CMP #$FC
# 03:B5BD F0 0E       BEQ $B5CD (+14 B)
# 03:B5BF C9 FE       CMP #$FE
# 03:B5C1 F0 07       BEQ $B5CA (+7 B)
# 03:B5C3 C9 FF       CMP #$FF
# 03:B5C5 F0 03       BEQ $B5CA (+3 B)
# 03:B5C7 C8          INY
# 03:B5C8 80 BD       BRA $B587 (-67 B)
# 03:B5CA 9C 77 38    STZ $3877
# 03:B5CD 8E 75 38    STX $3875
# 03:B5D0 7B          TDC
# 03:B5D1 AA          TAX
# 03:B5D2 86 CB       STX $CB
# 03:B5D4 A6 CB       LDX $CB
# 03:B5D6 BD 39 38    LDA $3839,x
# 03:B5D9 C9 FD       CMP #$FD
# 03:B5DB F0 10       BEQ $B5ED (+16 B)
# 03:B5DD C9 FC       CMP #$FC
# 03:B5DF F0 25       BEQ $B606 (+37 B)
# 03:B5E1 C9 FB       CMP #$FB
# 03:B5E3 F0 3A       BEQ $B61F (+58 B)
# 03:B5E5 C9 E8       CMP #$E8
# 03:B5E7 90 2A       BCC $B613 (+42 B)
# 03:B5E9 E8          INX
# 03:B5EA E8          INX
# 03:B5EB 80 E9       BRA $B5D6 (-23 B)
# 03:B5ED EE 77 38    INC $3877
# 03:B5F0 A5 D2       LDA $D2
# 03:B5F2 8D 01 36    STA $3601
# 03:B5F5 9C 02 36    STZ $3602
# 03:B5F8 A6 A6       LDX $A6
# 03:B5FA BD 60 20    LDA $2060,x
# 03:B5FD 8D 78 38    STA $3878
# 03:B600 7B          TDC
# 03:B601 9D 60 20    STA $2060,x
# 03:B604 80 19       BRA $B61F (+25 B)
# 03:B606 CE 77 38DEC $3877
# 03:B609 A6 A6       LDX $A6
# 03:B60B AD 78 38    LDA $3878
# 03:B60E 9D 60 20    STA $2060,x
# 03:B611 80 0C       BRA $B61F (+12 B)
# 03:B613 E8          INX
# 03:B614 BD 39 38    LDA $3839,x
# 03:B617 C9 FC       CMP #$FC
# 03:B619 F0 EB       BEQ $B606 (-21 B)
# 03:B61B C9 FF       CMP #$FF
# 03:B61D D0 F4       BNE $B613 (-12 B)
# 03:B61F 7B          TDC
# 03:B620 AA          TAX
# 03:B621 BD 39 38    LDA $3839,x
# 03:B624 C9 FA       CMP #$FA
# 03:B626 B0 05       BCS $B62D (+5 B)
# 03:B628 C9 E8       CMP #$E8
# 03:B62A 90 04       BCC $B630 (+4 B)
# 03:B62C E8          INX
# 03:B62D E8          INX
# 03:B62E 80 F1       BRA $B621 (-15 B)
# 03:B630 A6 A6       LDX $A6
# 03:B632 9D 51 20    STA $2051,x
# 03:B635 7B          TDC
# 03:B636 AA          TAX
# 03:B637 86 80       STX $80
# 03:B639 A6 80       LDX $80
# 03:B63B BD 39 38    LDA $3839,x
# 03:B63E C9 FF       CMP #$FF
# 03:B640 F0 3D       BEQ $B67F (+61 B)
# 03:B642 C9 F9       CMP #$F9
# 03:B644 F0 2A       BEQ $B670 (+42 B)
# 03:B646 C9 E8       CMP #$E8
# 03:B648 90 31       BCC $B67B (+49 B)
# 03:B64A C9 FB       CMP #$FB
# 03:B64C B0 2D       BCS $B67B (+45 B)
# 03:B64E C9 F0       CMP #$F0
# 03:B650 B0 0D       BCS $B65F (+13 B)
# 03:B652 48          PHA
# 03:B653 E8          INX
# 03:B654 BD 39 38    LDA $3839,x
# 03:B657 85 A9       STA $A9
# 03:B659 68          PLA
# 03:B65A 20 6F B7    JSR $B76F
# 03:B65D 80 1A       BRA $B679 (+26 B)
# 03:B65F C9 F4       CMP #$F4
# 03:B661 90 16       BCC $B679 (+22 B)
# 03:B663 48          PHA
# 03:B664 E8          INX
# 03:B665 BD 39 38    LDA $3839,x
# 03:B668 85 A9       STA $A9
# 03:B66A 68          PLA
# 03:B66B 20 77 B8    JSR $B877
# 03:B66E 80 09       BRA $B679 (+9 B)
# 03:B670 E8          INX
# 03:B671 BD 39 38    LDA $3839,x
# 03:B674 85 A9       STA $A9
# 03:B676 20 A1 B8    JSR $B8A1
# 03:B679 E6 80       INC $80
# 03:B67B E6 80       INC $80
# 03:B67D 80 BA       BRA $B639 (-70 B)
# 03:B67F 20 8F B6    JSR $B68F
# 03:B682 20 DE BB    JSR $BBDE
# 03:B685 AD 1C 36    LDA $361C
# 03:B688 AA          TAX
# 03:B689 9E 79 38    STZ $3879,x
# 03:B68C 4C 1E BC    JMP $BC1E
# 03:B68F AD 1C 36    LDA $361C
# 03:B692 AA          TAX
# 03:B693 BD 83 38    LDA $3883,x
# 03:B696 F0 0E       BEQ $B6A6 (+14 B)
# 03:B698 9E 83 38    STZ $3883,x
# 03:B69B A6 A6       LDX $A6
# 03:B69D 9D 54 20    STA $2054,x
# 03:B6A0 9E 53 20    STZ $2053,x
# 03:B6A3 4C 4B B7    JMP $B74B
# 03:B6A6 AD 1C 36    LDA $361C
# 03:B6A9 AA          TAX
# 03:B6AA BD 79 38    LDA $3879,x
# 03:B6AD F0 03       BEQ $B6B2 (+3 B)
# 03:B6AF 4C 4B B7    JMP $B74B
# 03:B6B2 A6 A6       LDX $A6
# 03:B6B4 BD 51 20    LDA $2051,x
# 03:B6B7 C9 C2       CMP #$C2
# 03:B6B9 F0 03       BEQ $B6BE (+3 B)
# 03:B6BB 4C 44 B7    JMP $B744
# 03:B6BE BD 52 20    LDA $2052,x
# 03:B6C1 C9 31       CMP #$31
# 03:B6C3 B0 1D       BCS $B6E2 (+29 B)
# 03:B6C5 AA          TAX
# 03:B6C6 86 E5       STX $E5
# 03:B6C8 A2 A0 97    LDX #$97A0
# 03:B6CB 86 80       STX $80
# 03:B6CD A9 0F       LDA #$0F
# 03:B6CF 85 82       STA $82
# 03:B6D1 A9 06       LDA #$06
# 03:B6D3 20 5E 84    JSR $845E
# 03:B6D6 AD 9C 28    LDA $289C
# 03:B6D9 10 03       BPL $B6DE (+3 B)
# 03:B6DB 4C 44 B7    JMP $B744
# 03:B6DE A9 08       LDA #$08
# 03:B6E0 80 64       BRA $B746 (+100 B)
# 03:B6E2 C9 5F       CMP #$5F
# 03:B6E4 B0 30       BCS $B716 (+48 B)
# 03:B6E6 38          SEC
# 03:B6E7 E9 30       SBC #$30
# 03:B6E9 A6 A6       LDX $A6
# 03:B6EB 9D 52 20    STA $2052,x
# 03:B6EE AA          TAX
# 03:B6EF 86 E5       STX $E5
# 03:B6F1 A2 A0 97    LDX #$97A0
# 03:B6F4 86 80       STX $80
# 03:B6F6 A9 0F       LDA #$0F
# 03:B6F8 85 82       STA $82
# 03:B6FA A9 06       LDA #$06
# 03:B6FC 20 5E 84    JSR $845E
# 03:B6FF A6 A6       LDX $A6
# 03:B701 AD 9C 28    LDA $289C
# 03:B704 30 07       BMI $B70D (+7 B)
# 03:B706 A9 FF       LDA #$FF
# 03:B708 9D 53 20    STA $2053,x
# 03:B70B 80 3E       BRA $B74B (+62 B)
# 03:B70D A9 F8       LDA #$F8
# 03:B70F A6 A6       LDX $A6
# 03:B711 9D 54 20    STA $2054,x
# 03:B714 80 35       BRA $B74B (+53 B)
# 03:B716 AA          TAX
# 03:B717 86 E5       STX $E5
# 03:B719 A2 A0 97    LDX #$97A0
# 03:B71C 86 80       STX $80
# 03:B71E A9 0F       LDA #$0F
# 03:B720 85 82       STA $82
# 03:B722 A9 06       LDA #$06
# 03:B724 20 5E 84    JSR $845E
# 03:B727 AD 9C 28    LDA $289C
# 03:B72A 29 E0       AND #$E0
# 03:B72C D0 0F       BNE $B73D (+15 B)
# 03:B72E AD 1C 36    LDA $361C
# 03:B731 AA          TAX
# 03:B732 7B          TDC
# 03:B733 20 5F 85    JSR $855F
# 03:B736 A6 A6       LDX $A6
# 03:B738 9D 53 20    STA $2053,x
# 03:B73B 80 0E       BRA $B74B (+14 B)
# 03:B73D 29 40       AND #$40
# 03:B73F D0 BE       BNE $B6FF (-66 B)
# 03:B741 4C D6 B6    JMP $B6D6
# 03:B744 A9 05       LDA #$05
# 03:B746 85 AB       STA $AB
# 03:B748 20 31 BC    JSR $BC31
# 03:B74B 60          RTS
# 03:B74C AD BE 35    LDA $35BE
# 03:B74F 85 DF       STA $DF
# 03:B751 A9 3C       LDA #$3C
# 03:B753 85 E1       STA $E1
# 03:B755 20 E0 83    JSR $83E0
# 03:B758 AD 1C 36    LDA $361C
# 03:B75B 0A          ASL a
# 03:B75C AA          TAX
# 03:B75D 18          CLC
# 03:B75E AD BF 35    LDA $35BF
# 03:B761 65 E3       ADC $E3
# 03:B763 9D 0C 36    STA $360C,x
# 03:B766 AD C0 35    LDA $35C0
# 03:B769 65 E4       ADC $E4
# 03:B76B 9D 0D 36    STA $360D,x
# 03:B76E 60          RTS
# 03:B76F A6 A6       LDX $A6
# 03:B771 C9 E8       CMP #$E8
# 03:B773 D0 08       BNE $B77D (+8 B)
# 03:B775 A5 A9       LDA $A9
# 03:B777 9D 40 20    STA $2040,x
# 03:B77A 4C FE B7    JMP $B7FE
# 03:B77D C9 E9       CMP #$E9
# 03:B77F D0 18       BNE $B799 (+24 B)
# 03:B781 A5 A9       LDA $A9
# 03:B783 20 89 94    JSR $9489
# 03:B786 AD 1C 29    LDA $291C
# 03:B789 9D 1B 20    STA $201B,x
# 03:B78C AD 1D 29    LDA $291D
# 03:B78F 9D 1C 20    STA $201C,x
# 03:B792 AD 1E 29    LDA $291E
# 03:B795 9D 1D 20    STA $201D,x
# 03:B798 60          RTS
# 03:B799 C9 EA       CMP #$EA
# 03:B79B D0 18       BNE $B7B5 (+24 B)
# 03:B79D A5 A9       LDA $A9
# 03:B79F 20 89 94    JSR $9489
# 03:B7A2 AD 1C 29    LDA $291C
# 03:B7A5 9D 28 20    STA $2028,x
# 03:B7A8 AD 1D 29    LDA $291D
# 03:B7AB 9D 29 20    STA $2029,x
# 03:B7AE AD 1E 29    LDA $291E
# 03:B7B1 9D 2A 20    STA $202A,x
# 03:B7B4 60          RTS
# 03:B7B5 C9 EB       CMP #$EB
# 03:B7B7 D0 18       BNE $B7D1 (+24 B)
# 03:B7B9 A5 A9       LDA $A9
# 03:B7BB 20 89 94    JSR $9489
# 03:B7BE AD 1C 29    LDA $291C
# 03:B7C1 9D 22 20    STA $2022,x
# 03:B7C4 AD 1D 29    LDA $291D
# 03:B7C7 9D 24 20    STA $2024,x
# 03:B7CA AD 1E 29    LDA $291E
# 03:B7CD 9D 24 20    STA $2024,x
# 03:B7D0 60          RTS
# 03:B7D1 C9 EC       CMP #$EC
# 03:B7D3 D0 03       BNE $B7D8 (+3 B)
# 03:B7D5 4C FF B7    JMP $B7FF
# 03:B7D8 C9 ED       CMP #$ED
# 03:B7DA D0 0E       BNE $B7EA (+14 B)
# 03:B7DC A5 A9       LDA $A9
# 03:B7DE 9D 25 20    STA $2025,x
# 03:B7E1 10 1B       BPL $B7FE (+27 B)
# 03:B7E3 9D 26 20    STA $2026,x
# 03:B7E6 9E 25 20    STZ $2025,x
# 03:B7E9 60          RTS
# 03:B7EA C9 EE       CMP #$EE
# 03:B7EC D0 06       BNE $B7F4 (+6 B)
# 03:B7EE A5 A9       LDA $A9
# 03:B7F0 9D 17 20    STA $2017,x
# 03:B7F3 60          RTS
# 03:B7F4 A5 A9       LDA $A9
# 03:B7F6 10 03       BPL $B7FB (+3 B)
# 03:B7F8 9D 21 20    STA $2021,x
# 03:B7FB 9D 20 20    STA $2020,x
# 03:B7FE 60          RTS
# 03:B7FF DA          PHX
# 03:B800 A5 A9       LDA $A9
# 03:B802 8D 3D 39    STA $393D
# 03:B805 9C 3E 39    STZ $393E
# 03:B808 BD 60 20    LDA $2060,x
# 03:B80B 8D 3F 39    STA $393F
# 03:B80E BD 61 20    LDA $2061,x
# 03:B811 8D 40 39    STA $3940
# 03:B814 20 B9 83    JSR $83B9
# 03:B817 AE 41 39    LDX $3941
# 03:B81A 8E 3D 39    STX $393D
# 03:B81D A2 64 00    LDX #$0064
# 03:B820 8E 3F 39    STX $393F
# 03:B823 20 B9 83    JSR $83B9
# 03:B826 AE 41 39    LDX $3941
# 03:B829 8E 45 39    STX $3945
# 03:B82C A2 E8 03    LDX #$03E8
# 03:B82F 8E 47 39    STX $3947
# 03:B832 20 07 84    JSR $8407
# 03:B835 AD 49 39    LDA $3949
# 03:B838 0D 4A 39    ORA $394A
# 03:B83B D0 03       BNE $B840 (+3 B)
# 03:B83D EE 49 39    INC $3949
# 03:B840 FA          PLX
# 03:B841 A5 A9       LDA $A9
# 03:B843 30 14       BMI $B859 (+20 B)
# 03:B845 18          CLC
# 03:B846 BD 60 20    LDA $2060,x
# 03:B849 6D 49 39    ADC $3949
# 03:B84C 9D 60 20    STA $2060,x
# 03:B84F BD 61 20    LDA $2061,x
# 03:B852 6D 4A 39    ADC $394A
# 03:B855 9D 61 20    STA $2061,x
# 03:B858 60          RTS
# 03:B859 38          SEC
# 03:B85A BD 60 20    LDA $2060,x
# 03:B85D ED 49 39    SBC $3949
# 03:B860 9D 60 20    STA $2060,x
# 03:B863 BD 61 20    LDA $2061,x
# 03:B866 ED 4A 39    SBC $394A
# 03:B869 9D 61 20    STA $2061,x
# 03:B86C B0 08       BCS $B876 (+8 B)
# 03:B86E A9 01       LDA #$01
# 03:B870 9D 60 20    STA $2060,x
# 03:B873 9E 61 20    STZ $2061,x
# 03:B876 60          RTS
# 03:B877 38          SEC
# 03:B878 E9 F4       SBC #$F4
# 03:B87A A8          TAY
# 03:B87B A5 A9       LDA $A9
# 03:B87D 48          PHA
# 03:B87E 29 3F       AND #$3F
# 03:B880 85 A9       STA $A9
# 03:B882 68          PLA
# 03:B883 29 C0       AND #$C0
# 03:B885 D0 08       BNE $B88F (+8 B)
# 03:B887 18          CLC
# 03:B888 A5 A9       LDA $A9
# 03:B88A 79 F3 35    ADC $35F3,y
# 03:B88D 80 0E       BRA $B89D (+14 B)
# 03:B88F 29 80       AND #$80
# 03:B891 D0 08       BNE $B89B (+8 B)
# 03:B893 38          SEC
# 03:B894 B9 F3 35    LDA $35F3,y
# 03:B897 E5 A9       SBC $A9
# 03:B899 80 02       BRA $B89D (+2 B)
# 03:B89B A5 A9       LDA $A9
# 03:B89D 99 F3 35    STA $35F3,y
# 03:B8A0 60          RTS
# 03:B8A1 86 88       STX $88
# 03:B8A3 AD 1C 36    LDA $361C
# 03:B8A6 AA          TAX
# 03:B8A7 FE 79 38    INC $3879,x
# 03:B8AA A6 A6       LDX $A6
# 03:B8AC 9E 53 20    STZ $2053,x
# 03:B8AF 9E 54 20    STZ $2054,x
# 03:B8B2 A5 A9       LDA $A9
# 03:B8B4 C9 16       CMP #$16
# 03:B8B6 90 18       BCC $B8D0 (+24 B)
# 03:B8B8 38          SEC
# 03:B8B9 E9 16       SBC #$16
# 03:B8BB 0A          ASL a
# 03:B8BC AA          TAX
# 03:B8BD BF B6 BB 03 LDA $03BBB6,x
# 03:B8C1 85 A9       STA $A9
# 03:B8C3 BF B7 BB 03 LDA $03BBB7,x
# 03:B8C7 85 AA       STA $AA
# 03:B8C9 A9 03       LDA #$03
# 03:B8CB 85 AB       STA $AB
# 03:B8CD DC A9 00    JML ($00A9)
# 03:B8D0 7B          TDC
# 03:B8D1 AA          TAX
# 03:B8D2 A8          TAY
# 03:B8D3 B9 40 35    LDA $3540,y
# 03:B8D6 D0 1C       BNE $B8F4 (+28 B)
# 03:B8D8 BD 00 20    LDA $2000,x
# 03:B8DB 29 1F       AND #$1F
# 03:B8DD C5 A9       CMP $A9
# 03:B8DF D0 13       BNE $B8F4 (+19 B)
# 03:B8E1 BD 03 20    LDA $2003,x
# 03:B8E4 29 C0       AND #$C0
# 03:B8E6 D0 15       BNE $B8FD (+21 B)
# 03:B8E8 BD 05 20    LDA $2005,x
# 03:B8EB 29 82       AND #$82
# 03:B8ED D0 0E       BNE $B8FD (+14 B)
# 03:B8EF BD 06 20    LDA $2006,x
# 03:B8F2 10 0C       BPL $B900 (+12 B)
# 03:B8F4 20 BC 85    JSR $85BC
# 03:B8F7 C8          INY
# 03:B8F8 C0 05 00    CPY #$0005
# 03:B8FB D0 D6       BNE $B8D3 (-42 B)
# 03:B8FD 4C 79 BB    JMP $BB79
# 03:B900 A6 A6       LDX $A6
# 03:B902 38          SEC
# 03:B903 7E 54 20    ROR $2054,x
# 03:B906 88          DEY
# 03:B907 10 FA       BPL $B903 (-6 B)
# 03:B909 60          RTS
# 03:B90A AD 1C 36    LDA $361C
# 03:B90D A8          TAY
# 03:B90E A6 A6       LDX $A6
# 03:B910 38          SEC
# 03:B911 7E 53 20    ROR $2053,x
# 03:B914 88          DEY
# 03:B915 10 FA       BPL $B911 (-6 B)
# 03:B917 60          RTS
# 03:B918 A6 A6       LDX $A6
# 03:B91A A9 FF       LDA #$FF
# 03:B91C 9D 53 20    STA $2053,x
# 03:B91F 60          RTS
# 03:B920 AD CD 29    LDA $29CD
# 03:B923 3A          DEC a
# 03:B924 D0 03       BNE $B929 (+3 B)
# 03:B926 4C 79 BB    JMP $BB79
# 03:B929 AD 1C 36    LDA $361C
# 03:B92C AA          TAX
# 03:B92D A9 FF       LDA #$FF
# 03:B92F 20 5A 85    JSR $855A
# 03:B932 A6 A6       LDX $A6
# 03:B934 9D 53 20    STA $2053,x
# 03:B937 60          RTS
# 03:B938 A9 00       LDA #$00
# 03:B93A 4C 3D B9    JMP $B93D
# 03:B93D 85 A9       STA $A9
# 03:B93F 7B          TDC
# 03:B940 AA          TAX
# 03:B941 86 AB       STX $AB
# 03:B943 BD B5 29    LDA $29B5,x
# 03:B946 C5 A9       CMP $A9
# 03:B948 D0 07       BNE $B951 (+7 B)
# 03:B94A A5 AB       LDA $AB
# 03:B94C 20 5F 85    JSR $855F
# 03:B94F 85 AB       STA $AB
# 03:B951 E8          INX
# 03:B952 E0 08 00    CPX #$0008
# 03:B955 D0 EC       BNE $B943 (-20 B)
# 03:B957 A6 A6       LDX $A6
# 03:B959 A5 AB       LDA $AB
# 03:B95B D0 03       BNE $B960 (+3 B)
# 03:B95D 4C 79 BB    JMP $BB79
# 03:B960 9D 53 20    STA $2053,x
# 03:B963 60          RTS
# 03:B964 A9 01       LDA #$01
# 03:B966 4C 3D B9    JMP $B93D
# 03:B969 A9 02       LDA #$02
# 03:B96B 4C 3D B9    JMP $B93D
# 03:B96E 20 7E B9    JSR $B97E
# 03:B971 A6 A6       LDX $A6
# 03:B973 A5 AB       LDA $AB
# 03:B975 D0 03       BNE $B97A (+3 B)
# 03:B977 4C 79 BB    JMP $BB79
# 03:B97A 9D 54 20    STA $2054,x
# 03:B97D 60          RTS
# 03:B97E 7B          TDC
# 03:B97F AA          TAX
# 03:B980 A8          TAY
# 03:B981 86 AB       STX $AB
# 03:B983 BD 40 35    LDA $3540,x
# 03:B986 D0 1F       BNE $B9A7 (+31 B)
# 03:B988 B9 03 20    LDA $2003,y
# 03:B98B 29 C0       AND #$C0
# 03:B98D D0 18       BNE $B9A7 (+24 B)
# 03:B98F B9 05 20    LDA $2005,y
# 03:B992 29 82       AND #$82
# 03:B994 D0 11       BNE $B9A7 (+17 B)
# 03:B996 B9 06 20    LDA $2006,y
# 03:B999 30 0C       BMI $B9A7 (+12 B)
# 03:B99B B9 01 20    LDA $2001,y
# 03:B99E 30 07       BMI $B9A7 (+7 B)
# 03:B9A0 A5 AB       LDA $AB
# 03:B9A2 20 5F 85    JSR $855F
# 03:B9A5 85 AB       STA $AB
# 03:B9A7 C2 20       REP #$20
# 03:B9A9 98          TYA
# 03:B9AA 18          CLC
# 03:B9AB 69 80 00    ADC #$0080
# 03:B9AE A8          TAY
# 03:B9AF 7B          TDC
# 03:B9B0 E2 20       SEP #$20
# 03:B9B2 E8          INX
# 03:B9B3 E0 05 00    CPX #$0005
# 03:B9B6 D0 CB       BNE $B983 (-53 B)
# 03:B9B8 60          RTS
# 03:B9B9 20 C9 B9    JSR $B9C9
# 03:B9BC A6 A6       LDX $A6
# 03:B9BE A5 AB       LDA $AB
# 03:B9C0 D0 03       BNE $B9C5 (+3 B)
# 03:B9C2 4C 79 BB    JMP $BB79
# 03:B9C5 9D 54 20    STA $2054,x
# 03:B9C8 60          RTS
# 03:B9C9 7B          TDC
# 03:B9CA AA          TAX
# 03:B9CB A8          TAY
# 03:B9CC 86 AB       STX $AB
# 03:B9CE BD 40 35    LDA $3540,x
# 03:B9D1 D0 1F       BNE $B9F2 (+31 B)
# 03:B9D3 B9 03 20    LDA $2003,y
# 03:B9D6 29 C0       AND #$C0
# 03:B9D8 D0 18       BNE $B9F2 (+24 B)
# 03:B9DA B9 05 20    LDA $2005,y
# 03:B9DD 29 82       AND #$82
# 03:B9DF D0 11       BNE $B9F2 (+17 B)
# 03:B9E1 B9 06 20    LDA $2006,y
# 03:B9E4 30 0C       BMI $B9F2 (+12 B)
# 03:B9E6 B9 01 20    LDA $2001,y
# 03:B9E9 10 07       BPL $B9F2 (+7 B)
# 03:B9EB A5 AB       LDA $AB
# 03:B9ED 20 5F 85    JSR $855F
# 03:B9F0 85 AB       STA $AB
# 03:B9F2 C2 20       REP #$20
# 03:B9F4 98          TYA
# 03:B9F5 18          CLC
# 03:B9F6 69 80 00    ADC #$0080
# 03:B9F9 A8          TAY
# 03:B9FA 7B          TDC
# 03:B9FB E2 20       SEP #$20
# 03:B9FD E8          INX
# 03:B9FE E0 05 00    CPX #$0005
# 03:BA01 D0 CB       BNE $B9CE (-53 B)
# 03:BA03 60          RTS
# 03:BA04 A9 20       LDA #$20
# 03:BA06 85 AD       STA $AD
# 03:BA08 64 AE       STZ $AE
# 03:BA0A 64 AF       STZ $AF
# 03:BA0C 64 B0       STZ $B0
# 03:BA0E 4C 11 BA    JMP $BA11
# 03:BA11 A0 80 02    LDY #$0280
# 03:BA14 7B          TDC
# 03:BA15 AA          TAX
# 03:BA16 86 AB       STX $AB
# 03:BA18 86 A9       STX $A9
# 03:BA1A 18          CLC
# 03:BA1B A5 A9       LDA $A9
# 03:BA1D 69 05       ADC #$05
# 03:BA1F AA          TAX
# 03:BA20 BD 40 35    LDA $3540,x
# 03:BA23 D0 22       BNE $BA47 (+34 B)
# 03:BA25 B9 03 20    LDA $2003,y
# 03:BA28 25 AF       AND $AF
# 03:BA2A D0 0E       BNE $BA3A (+14 B)
# 03:BA2C B9 04 20    LDA $2004,y
# 03:BA2F 25 AD       AND $AD
# 03:BA31 D0 07       BNE $BA3A (+7 B)
# 03:BA33 B9 06 20    LDA $2006,y
# 03:BA36 25 AE       AND $AE
# 03:BA38 F0 0D       BEQ $BA47 (+13 B)
# 03:BA3A A6 A9       LDX $A9
# 03:BA3C A5 AB       LDA $AB
# 03:BA3E 20 5F 85    JSR $855F
# 03:BA41 85 AB       STA $AB
# 03:BA43 A5 B0       LDA $B0
# 03:BA45 F0 13       BEQ $BA5A (+19 B)
# 03:BA47 C2 20       REP #$20
# 03:BA49 98          TYA
# 03:BA4A 18          CLC
# 03:BA4B 69 80 00    ADC #$0080
# 03:BA4E A8          TAY
# 03:BA4F 7B          TDC
# 03:BA50 E2 20       SEP #$20
# 03:BA52 E6 A9       INC $A9
# 03:BA54 A5 A9       LDA $A9
# 03:BA56 C9 08       CMP #$08
# 03:BA58 D0 C0       BNE $BA1A (-64 B)
# 03:BA5A A6 A6       LDX $A6
# 03:BA5C A5 AB       LDA $AB
# 03:BA5E D0 03       BNE $BA63 (+3 B)
# 03:BA60 4C 79 BB    JMP $BB79
# 03:BA63 9D 53 20    STA $2053,x
# 03:BA66 60          RTS
# 03:BA67 A9 10       LDA #$10
# 03:BA69 85 AD       STA $AD
# 03:BA6B 64 AE       STZ $AE
# 03:BA6D 64 AF       STZ $AF
# 03:BA6F 64 B0       STZ $B0
# 03:BA71 4C 11 BA    JMP $BA11
# 03:BA74 A9 08       LDA #$08
# 03:BA76 85 AD       STA $AD
# 03:BA78 64 AE       STZ $AE
# 03:BA7A 64 AF       STZ $AF
# 03:BA7C 64 B0       STZ $B0
# 03:BA7E 4C 11 BA    JMP $BA11
# 03:BA81 64 AD       STZ $AD
# 03:BA83 A9 01       LDA #$01
# 03:BA85 85 AE       STA $AE
# 03:BA87 64 AF       STZ $AF
# 03:BA89 64 B0       STZ $B0
# 03:BA8B 4C 11 BA    JMP $BA11
# 03:BA8E A9 0C       LDA #$0C
# 03:BA90 85 B0       STA $B0
# 03:BA92 A9 00       LDA #$00
# 03:BA94 85 AF       STA $AF
# 03:BA96 3A          DEC a
# 03:BA97 85 AD       STA $AD
# 03:BA99 4C 9C BA    JMP $BA9C
# 03:BA9C A5 AF       LDA $AF
# 03:BA9E AA          TAX
# 03:BA9F A5 B0       LDA $B0
# 03:BAA1 20 79 83    JSR $8379
# 03:BAA4 C5          ADCMP $AD
# 03:BAA6 F0 F4       BEQ $BA9C (-12 B)
# 03:BAA8 85 A9       STA $A9
# 03:BAAA AA          TAX
# 03:BAAB BD 40 35    LDA $3540,x
# 03:BAAE D0 EC       BNE $BA9C (-20 B)
# 03:BAB0 8A          TXA
# 03:BAB1 85 DF       STA $DF
# 03:BAB3 A9 80       LDA #$80
# 03:BAB5 85 E1       STA $E1
# 03:BAB7 20 E0 83    JSR $83E0
# 03:BABA A6 E3       LDX $E3
# 03:BABC BD 03 20    LDA $2003,x
# 03:BABF 29 C0       AND #$C0
# 03:BAC1 D0 D9       BNE $BA9C (-39 B)
# 03:BAC3 BD 05 20    LDA $2005,x
# 03:BAC6 29 82       AND #$82
# 03:BAC8 D0 D2       BNE $BA9C (-46 B)
# 03:BACA BD 06 20    LDA $2006,x
# 03:BACD 30 CD       BMI $BA9C (-51 B)
# 03:BACF A5 A9       LDA $A9
# 03:BAD1 C9 05       CMP #$05
# 03:BAD3 B0 0C       BCS $BAE1 (+12 B)
# 03:BAD5 AA          TAX
# 03:BAD6 7B          TDC
# 03:BAD7 20 5F 85    JSR $855F
# 03:BADA A6 A6       LDX $A6
# 03:BADC 9D 54 20    STA $2054,x
# 03:BADF 80 0D       BRA $BAEE (+13 B)
# 03:BAE1 38          SEC
# 03:BAE2 E9 05       SBC #$05
# 03:BAE4 AA          TAX
# 03:BAE5 7B          TDC
# 03:BAE6 20 5F 85    JSR $855F
# 03:BAE9 A6 A6       LDX $A6
# 03:BAEB 9D 53 20    STA $2053,x
# 03:BAEE 60          RTS
# 03:BAEF A9 00       LDA #$00
# 03:BAF1 85 AF       STA $AF
# 03:BAF3 A9 0C       LDA #$0C
# 03:BAF5 85 B0       STA $B0
# 03:BAF7 A5 D2       LDA $D2
# 03:BAF9 85 AD       STA $AD
# 03:BAFB 4C 9C BA    JMP $BA9C
# 03:BAFE A9 05       LDA #$05
# 03:BB00 85 AF       STA $AF
# 03:BB02 A9 0C       LDA #$0C
# 03:BB04 85 B0       STA $B0
# 03:BB06 A9 FF       LDA #$FF
# 03:BB08 85 AD       STA $AD
# 03:BB0A 4C 9C BA    JMP $BA9C
# 03:BB0D AD CD 29    LDA $29CD
# 03:BB10 3A          DEC a
# 03:BB11 D0 03       BNE $BB16 (+3 B)
# 03:BB13 4C 79 BB    JMP $BB79
# 03:BB16 A9 05       LDA #$05
# 03:BB18 85 AF       STA $AF
# 03:BB1A A9 0C       LDA #$0C
# 03:BB1C 85 B0       STA $B0
# 03:BB1E A5 D2       LDA $D2
# 03:BB20 85 AD       STA $AD
# 03:BB22 4C 9C BA    JMP $BA9C
# 03:BB25 20 7E B9    JSR $B97E
# 03:BB28 A5 AB       LDA $AB
# 03:BB2A D0 03       BNE $BB2F (+3 B)
# 03:BB2C 4C 79 BB    JMP $BB79
# 03:BB2F 20 82 85    JSR $8582
# 03:BB32 AA          TAX
# 03:BB33 A5 AB       LDA $AB
# 03:BB35 20 64 85    JSR $8564
# 03:BB38 F0 F5       BEQ $BB2F (-11 B)
# 03:BB3A 7B          TDC
# 03:BB3B 20 5F 85    JSR $855F
# 03:BB3E A6 A6       LDX $A6
# 03:BB40 9D 54 20    STA $2054,x
# 03:BB43 60          RTS
# 03:BB44 20 C9 B9    JSR $B9C9
# 03:BB47 A5 AB       LDA $AB
# 03:BB49 D0 03       BNE $BB4E (+3 B)
# 03:BB4B 4C 79 BB    JMP $BB79
# 03:BB4E 20 82 85    JSR $8582
# 03:BB51 AA          TAX
# 03:BB52 A5 AB       LDA $AB
# 03:BB54 20 64 85    JSR $8564
# 03:BB57 F0 F5       BEQ $BB4E (-11 B)
# 03:BB59 7B          TDC
# 03:BB5A 20 5F 85    JSR $855F
# 03:BB5D A6 A6       LDX $A6
# 03:BB5F 9D 54 20    STA $2054,x
# 03:BB62 60          RTS
# 03:BB63 A6 A6       LDX $A6
# 03:BB65 A9 F8       LDA #$F8
# 03:BB67 9D 54 20    STA $2054,x
# 03:BB6A 60          RTS
# 03:BB6B 64 AD       STZ $AD
# 03:BB6D 64 AE       STZ $AE
# 03:BB6F A9 80       LDA #$80
# 03:BB71 85 AF       STA $AF
# 03:BB73 85 B0       STA $B0
# 03:BB75 4C 11 BA    JMP $BA11
# 03:BB78 60          RTS
# 03:BB79 AD B3 38    LDA $38B3
# 03:BB7C F0 19       BEQ $BB97 (+25 B)
# 03:BB7E 20 31 C4    JSR $C431
# 03:BB81 A5 A9       LDA $A9
# 03:BB83 D0 29       BNE $BBAE (+41 B)
# 03:BB85 AE BB 38    LDX $38BB
# 03:BB88 A9 E1       LDA #$E1
# 03:BB8A 9D 59 36    STA $3659,x
# 03:BB8D 7B          TDC
# 03:BB8E 9D 5A 36    STA $365A,x
# 03:BB91 3A          DEC a
# 03:BB92 9D 5B 36    STA $365B,x
# 03:BB95 80 17       BRA $BBAE (+23 B)
# 03:BB97 A6 88       LDX $88
# 03:BB99 E8          INX
# 03:BB9A 20 5E C4    JSR $C45E
# 03:BB9D A5 A9       LDA $A9
# 03:BB9F D0 0D       BNE $BBAE (+13 B)
# 03:BBA1 A9 E1       LDA #$E1
# 03:BBA3 9D 39 38    STA $3839,x
# 03:BBA6 7B          TDC
# 03:BBA7 9D 3A 38    STA $383A,x
# 03:BBAA 3A          DEC a
# 03:BBAB 9D 3B 38    STA $383B,x
# 03:BBAE A9 E1       LDA #$E1
# 03:BBB0 A6 A6       LDX $A6
# 03:BBB2 9D 51 20    STA $2051,x
# 03:BBB5 60          RTS
# 03:BBB6 0A          ASL a
# 03:BBB7 B9 18 B9    LDA $B918,y
# 03:BBBA 20 B9 38    JSR $38B9
# 03:BBBD B9 64 B9    LDA $B964,y
# 03:BBC0 69 B9       ADC #$B9
# 03:BBC2 6E B9 B9    ROR $B9B9
# 03:BBC5 B9 04 BA    LDA $BA04,y
# 03:BBC8 67 BA       ADC [$BA]
# 03:BBCA 74 BA       STZ $BA,x
# 03:BBCC 81 BA       STA ($BA,x)
# 03:BBCE 8E BA EF    STX $EFBA
# 03:BBD1 BA          TSX
# 03:BBD2 FE BA 0D    INC $0DBA,x
# 03:BBD5 BB          TYX
# 03:BBD6 25 BB       AND $BB
# 03:BBD8 44 BB 63MVP $63BB
# 03:BBDB BB          TYX
# 03:BBDC 6B          RTL
# 03:BBDD BB          TYX
# 03:BBDE A9 02       LDA #$02
# 03:BBE0 85 D6       STA $D6
# 03:BBE2 A5 D2       LDA $D2
# 03:BBE4 20 2C 9E    JSR $9E2C
# 03:BBE7 A9 03       LDA #$03
# 03:BBE9 20 C8 85    JSR $85C8
# 03:BBEC A9 08       LDA #$08
# 03:BBEE 9D 06 2A    STA $2A06,x
# 03:BBF1 60          RTS
# 03:BBF2 38          SEC
# 03:BBF3 A5 D2       LDA $D2
# 03:BBF5 E9 05       SBC #$05
# 03:BBF7 85 DF       STA $DF
# 03:BBF9 A9 3C       LDA #$3C
# 03:BBFB 85 E1       STA $E1
# 03:BBFD 20 E0 83    JSR $83E0
# 03:BC00 A0 3C 00    LDY #$003C
# 03:BC03 A6 E3       LDX $E3
# 03:BC05 8E BB 38    STX $38BB
# 03:BC08 A9 FF       LDA #$FF
# 03:BC0A 9D 59 36    STA $3659,x
# 03:BC0D E8          INX
# 03:BC0E 88          DEY
# 03:BC0F D0 F9       BNE $BC0A (-7 B)
# 03:BC11 60          RTS
# 03:BC12 A2 3B 00    LDX #$003B
# 03:BC15 A9 FF       LDA #$FF
# 03:BC17 9D 39 38    STA $3839,x
# 03:BC1A CA          DEX
# 03:BC1B 10 FA       BPL $BC17 (-6 B)
# 03:BC1D 60          RTS
# 03:BC1E AE BB 38    LDX $38BB
# 03:BC21 7B          TDC
# 03:BC22 A8          TAY
# 03:BC23 B9 39 38    LDA $3839,y
# 03:BC26 9D 59 36    STA $3659,x
# 03:BC29 E8          INX
# 03:BC2A C8          INY
# 03:BC2B C0 3C 00    CPY #$003C
# 03:BC2E D0 F3       BNE $BC23 (-13 B)
# 03:BC30 60          RTS
# 03:BC31 A5 AB       LDA $AB
# 03:BC33 C9 08       CMP #$08
# 03:BC35 F0 4C       BEQ $BC83 (+76 B)
# 03:BC37 7B          TDC
# 03:BC38 AA          TAX
# 03:BC39 A8          TAY
# 03:BC3A 86 A9       STX $A9
# 03:BC3C B9 40 35    LDA $3540,y
# 03:BC3F D0 17       BNE $BC58 (+23 B)
# 03:BC41 BD 03 20    LDA $2003,x
# 03:BC44 29 C0       AND #$C0
# 03:BC46 D0 10       BNE $BC58 (+16 B)
# 03:BC48 BD 05 20    LDA $2005,x
# 03:BC4B 29 82       AND #$82
# 03:BC4D D0 09       BNE $BC58 (+9 B)
# 03:BC4F BD 06 20    LDA $2006,x
# 03:BC52 30 04       BMI $BC58 (+4 B)
# 03:BC54 E6 A9       INC $A9
# 03:BC56 80 09       BRA $BC61 (+9 B)
# 03:BC58 20 BC 85    JSR $85BC
# 03:BC5B C8          INY
# 03:BC5C C0 05 00    CPY #$0005
# 03:BC5F D0 DB       BNE $BC3C (-37 B)
# 03:BC61 A5 A9       LDA $A9
# 03:BC63 D0 1E       BNE $BC83 (+30 B)
# 03:BC65 7B          TDC
# 03:BC66 AA          TAX
# 03:BC67 20 5E C4    JSR $C45E
# 03:BC6A A5 A9       LDA $A9
# 03:BC6C D0 14       BNE $BC82 (+20 B)
# 03:BC6E A9 E1       LDA #$E1
# 03:BC70 8D 39 38    STA $3839
# 03:BC73 7B          TDC
# 03:BC74 8D 3A 38    STA $383A
# 03:BC77 3A          DEC a
# 03:BC78 8D 3B 38    STA $383B
# 03:BC7B A9 E1       LDA #$E1
# 03:BC7D A6 A6       LDX $A6
# 03:BC7F 9D 51 20    STA $2051,x
# 03:BC82 60          RTS
# 03:BC83 64 AD       STZ $AD
# 03:BC85 A5 AB       LDA $AB
# 03:BC87 C9 08       CMP #$08
# 03:BC89 D0 02       BNE $BC8D (+2 B)
# 03:BC8B E6 AD       INC $AD
# 03:BC8D A2 01 00    LDX #$0001
# 03:BC90 A5 AB       LDA $AB
# 03:BC92 20 79 83    JSR $8379
# 03:BC95 3A          DEC a
# 03:BC96 85 A9       STA $A9
# 03:BC98 A5 AD       LDA $AD
# 03:BC9A F0 07       BEQ $BCA3 (+7 B)
# 03:BC9C 18          CLC
# 03:BC9D A5 A9       LDA $A9
# 03:BC9F 69 05       ADC #$05
# 03:BCA1 80 02       BRA $BCA5 (+2 B)
# 03:BCA3 A5 A9       LDA $A9
# 03:BCA5 0A          ASL a
# 03:BCA6 A8          TAY
# 03:BCA7 B9 EB 29    LDA $29EB,y
# 03:BCAA F0 E1       BEQ $BC8D (-31 B)
# 03:BCAC A5 A9       LDA $A9
# 03:BCAE A8          TAY
# 03:BCAF C8          INY
# 03:BCB0 A6 A6       LDX $A6
# 03:BCB2 9E 54 20    STZ $2054,x
# 03:BCB5 9E 53 20    STZ $2053,x
# 03:BCB8 A5 AB       LDA $AB
# 03:BCBA C9 08       CMP #$08
# 03:BCBC F0 08       BEQ $BCC6 (+8 B)
# 03:BCBE 38          SEC
# 03:BCBF 7E 54 20    ROR $2054,x
# 03:BCC2 88          DEY
# 03:BCC3 D0 FA       BNE $BCBF (-6 B)
# 03:BCC5 60          RTS
# 03:BCC6 38          SEC
# 03:BCC7 7E 53 20    ROR $2053,x
# 03:BCCA 88          DEY
# 03:BCCB D0 FA       BNE $BCC7 (-6 B)
# 03:BCCD 60          RTS
# 03:BCCE 64 DE       STZ $DE
# 03:BCD0 AD 9C 28    LDA $289C
# 03:BCD3 0A          ASL a
# 03:BCD4 AA          TAX
# 03:BCD5 BF F4 C0 03 LDA $03C0F4,x
# 03:BCD9 85 80       STA $80
# 03:BCDB BF F5 C0 03 LDA $03C0F5,x
# 03:BCDF 85 81       STA $81
# 03:BCE1 A9 03       LDA #$03
# 03:BCE3 85 82       STA $82
# 03:BCE5 DC 80 00    JML ($0080)
# 03:BCE8 20 0F BF    JSR $BF0F
# 03:BCEB A5 DD       LDA $DD
# 03:BCED F0 4D       BEQ $BD3C (+77 B)
# 03:BCEF 18          CLC
# 03:BCF0 A9 03       LDA #$03
# 03:BCF2 6D 9E 28    ADC $289E
# 03:BCF5 85 80       STA $80
# 03:BCF7 A9 20       LDA #$20
# 03:BCF9 69 00       ADC #$00
# 03:BCFB 85 81       STA $81
# 03:BCFD 7B          TDC
# 03:BCFE AA          TAX
# 03:BCFF BD D0 35    LDA $35D0,x
# 03:BD02 85 A9       STA $A9
# 03:BD04 BD D1 35    LDA $35D1,x
# 03:BD07 85 AA       STA $AA
# 03:BD09 A4 A9       LDY $A9
# 03:BD0B C0 FF FF    CPY #$FFFF
# 03:BD0E F0 18       BEQ $BD28 (+24 B)
# 03:BD10 B2 80       LDA ($80)
# 03:BD12 2D 9F 28    AND $289F
# 03:BD15 D0 08       BNE $BD1F (+8 B)
# 03:BD17 AD 9D 28    LDA $289D
# 03:BD1A 10 0C       BPL $BD28 (+12 B)
# 03:BD1C 64 DE       STZ $DE
# 03:BD1E 60          RTS
# 03:BD1F A9 01       LDA #$01
# 03:BD21 85 DE       STA $DE
# 03:BD23 AD 9D 28    LDA $289D
# 03:BD26 10 14       BPL $BD3C (+20 B)
# 03:BD28 C2 20       REP #$20
# 03:BD2A 18          CLC
# 03:BD2B A5 80       LDA $80
# 03:BD2D 69 80 00    ADC #$0080
# 03:BD30 85 80       STA $80
# 03:BD32 7B          TDC
# 03:BD33 E2 20       SEP #$20
# 03:BD35 E8          INX
# 03:BD36 E8          INX
# 03:BD37 E0 1A 00    CPX #$001A
# 03:BD3A D0 C3       BNE $BCFF (-61 B)
# 03:BD3C 60          RTS
# 03:BD3D 20 0F BF    JSR $BF0F
# 03:BD40 A5 DD       LDA $DD
# 03:BD42 F0 64       BEQ $BDA8 (+100 B)
# 03:BD44 AD 9F 28    LDA $289F
# 03:BD47 0A          ASL a
# 03:BD48 AA          TAX
# 03:BD49 BF 00 E0 0E LDA $0EE000,x
# 03:BD4D 85 AD       STA $AD
# 03:BD4F BF 01 E0 0E LDA $0EE001,x
# 03:BD53 85 AE       STA $AE
# 03:BD55 7B          TDC
# 03:BD56 AA          TAX
# 03:BD57 86 A9       STX $A9
# 03:BD59 A5 A9       LDA $A9
# 03:BD5B 0A          ASL a
# 03:BD5C A8          TAY
# 03:BD5D B9 D0 35    LDA $35D0,y
# 03:BD60 85 AB       STA $AB
# 03:BD62 B9 D1 35    LDA $35D1,y
# 03:BD65 85 AC       STA $AC
# 03:BD67 A4 AB       LDY $AB
# 03:BD69 C0 FF FF    CPY #$FFFF
# 03:BD6C F0 2F       BEQ $BD9D (+47 B)
# 03:BD6E C2 20       REP #$20
# 03:BD70 A5 AD       LDA $AD
# 03:BD72 C9 FF FF    CMP #$FFFF
# 03:BD75 D0 0A       BNE $BD81 (+10 B)
# 03:BD77 BD 07 20    LDA $2007,x
# 03:BD7A DD 09 20    CMP $2009,x
# 03:BD7D D0 14       BNE $BD93 (+20 B)
# 03:BD7F F0 07       BEQ $BD88 (+7 B)
# 03:BD81 A5 AD       LDA $AD
# 03:BD83 DD 07 20    CMP $2007,x
# 03:BD86 B0 0B       BCS $BD93 (+11 B)
# 03:BD88 7B          TDC
# 03:BD89 E2 20       SEP #$20
# 03:BD8B AD 9D 28    LDA $289D
# 03:BD8E 10 0D       BPL $BD9D (+13 B)
# 03:BD90 64 DE       STZ $DE
# 03:BD92 60          RTS
# 03:BD93 7B          TDC
# 03:BD94 E2 20       SEP #$20
# 03:BD96 E6 DE       INC $DE
# 03:BD98 AD 9D 28    LDA $289D
# 03:BD9B 10 0B       BPL $BDA8 (+11 B)
# 03:BD9D 20 BC 85    JSR $85BC
# 03:BDA0 E6 A9       INC $A9
# 03:BDA2 A5 A9       LDA $A9
# 03:BDA4 C9 0D       CMP #$0D
# 03:BDA6 D0 B1       BNE $BD59 (-79 B)
# 03:BDA8 60          RTS
# 03:BDA9 AD 9E 28    LDA $289E
# 03:BDAC AA          TAX
# 03:BDAD BD F3 35    LDA $35F3,x
# 03:BDB0 CD 9F 28    CMP $289F
# 03:BDB3 D0 02       BNE $BDB7 (+2 B)
# 03:BDB5 E6 DE       INC $DE
# 03:BDB7 60          RTS
# 03:BDB8 20 0F BF    JSR $BF0F
# 03:BDBB AD 9E 28    LDA $289E
# 03:BDBE D0 06       BNE $BDC6 (+6 B)
# 03:BDC0 A5 DD       LDA $DD
# 03:BDC2 D0 29       BNE $BDED (+41 B)
# 03:BDC4 F0 29       BEQ $BDEF (+41 B)
# 03:BDC6 AD 9E 28    LDA $289E
# 03:BDC9 3A          DEC a
# 03:BDCA D0 06       BNE $BDD2 (+6 B)
# 03:BDCC A5 DD       LDA $DD
# 03:BDCE F0 1D       BEQ $BDED (+29 B)
# 03:BDD0 D0 1D       BNE $BDEF (+29 B)
# 03:BDD2 A5 DD       LDA $DD
# 03:BDD4 F0 19       BEQ $BDEF (+25 B)
# 03:BDD6 7B          TDC
# 03:BDD7 AA          TAX
# 03:BDD8 86 A9       STX $A9
# 03:BDDA BD EB 29    LDA $29EB,x
# 03:BDDD F0 02       BEQ $BDE1 (+2 B)
# 03:BDDF E6 A9       INC $A9
# 03:BDE1 E8          INX
# 03:BDE2 E8          INX
# 03:BDE3 E0 0A 00    CPX #$000A
# 03:BDE6 D0 F2       BNE $BDDA (-14 B)
# 03:BDE8 A5 A9       LDA $A9
# 03:BDEA 3A          DEC a
# 03:BDEB D0 02       BNE $BDEF (+2 B)
# 03:BDED E6 DE       INC $DE
# 03:BDEF 60          RTS
# 03:BDF0 7B          TDC
# 03:BDF1 AA          TAX
# 03:BDF2 86 A9       STX $A9
# 03:BDF4 AD 9F 28    LDA $289F
# 03:BDF7 DD AD 29    CMP $29AD,x
# 03:BDFA F0 0A       BEQ $BE06 (+10 B)
# 03:BDFC E8          INX
# 03:BDFD E0 03 00    CPX #$0003
# 03:BE00 D0 F2       BNE $BDF4 (-14 B)
# 03:BE02 E6 A9       INC $A9
# 03:BE04 80 0E       BRA $BE14 (+14 B)
# 03:BE06 BD CA 29    LDA $29CA,x
# 03:BE09 F0 F7       BEQ $BE02 (-9 B)
# 03:BE0B CD CD 29    CMP $29CD
# 03:BE0E D0 04       BNE $BE14 (+4 B)
# 03:BE10 E6 A9       INC $A9
# 03:BE12 E6 A9       INC $A9
# 03:BE14 AD 9E 28    LDA $289E
# 03:BE17 C5 A9       CMP $A9
# 03:BE19 D0 02       BNE $BE1D (+2 B)
# 03:BE1B E6 DE       INC $DE
# 03:BE1D 60          RTS
# 03:BE1E AD 9E 28    LDA $289E
# 03:BE21 CD 01 18    CMP $1801
# 03:BE24 D0 0A       BNE $BE30 (+10 B)
# 03:BE26 AD 9F 28    LDA $289F
# 03:BE29 CD 00 18    CMP $1800
# 03:BE2C D0 02       BNE $BE30 (+2 B)
# 03:BE2E E6 DE       INC $DE
# 03:BE30 60          RTS
# 03:BE31 38          SEC
# 03:BE32 A5 D2       LDA $D2
# 03:BE34 E9 05       SBC #$05
# 03:BE36 AA          TAX
# 03:BE37 BD B5 29    LDA $29B5,x
# 03:BE3A AA          TAX
# 03:BE3B BD CA 29    LDA $29CA,x
# 03:BE3E CD CD 29    CMP $29CD
# 03:BE41 D0 02       BNE $BE45 (+2 B)
# 03:BE43 E6 DE       INC $DE
# 03:BE45 60          RTS
# 03:BE46 38          SEC
# 03:BE47 A5 D2       LDA $D2
# 03:BE49 E9 05       SBC #$05
# 03:BE4B AA          TAX
# 03:BE4C 20 7C 84    JSR $847C
# 03:BE4F 85 A9       STA $A9
# 03:BE51 BD F7 35    LDA $35F7,x
# 03:BE54 C9 FF       CMP #$FF
# 03:BE56 F0 47       BEQ $BE9F (+71 B)
# 03:BE58 20 7F 84    JSR $847F
# 03:BE5B 18          CLC
# 03:BE5C 65 A9       ADC $A9
# 03:BE5E AA          TAX
# 03:BE5F 7B          TDC
# 03:BE60 A8          TAY
# 03:BE61 BD 78 2B    LDA $2B78,x
# 03:BE64 99 1C 29    STA $291C,y
# 03:BE67 E8          INX
# 03:BE68 C8          INY
# 03:BE69 C0 04 00    CPY #$0004
# 03:BE6C D0 F3       BNE $BE61 (-13 B)
# 03:BE6E 20 0F BF    JSR $BF0F
# 03:BE71 A5 DD       LDA $DD
# 03:BE73 F0 2A       BEQ $BE9F (+42 B)
# 03:BE75 AD 1C 29    LDA $291C
# 03:BE78 10 05       BPL $BE7F (+5 B)
# 03:BE7A 29 7F       AND #$7F
# 03:BE7C 18          CLC
# 03:BE7D 69 05       ADC #$05
# 03:BE7F 0A          ASL a
# 03:BE80 AA          TAX
# 03:BE81 BD D0 35    LDA $35D0,x
# 03:BE84 3D D1 35    AND $35D1,x
# 03:BE87 C9 FF       CMP #$FF
# 03:BE89 F0 14       BEQ $BE9F (+20 B)
# 03:BE8B AD 9E 28    LDA $289E
# 03:BE8E CD 1D 29    CMP $291D
# 03:BE91 D0 0C       BNE $BE9F (+12 B)
# 03:BE93 AD 9F 28    LDA $289F
# 03:BE96 F0 05       BEQ $BE9D (+5 B)
# 03:BE98 2D 1F 29    AND $291F
# 03:BE9B F0 02       BEQ $BE9F (+2 B)
# 03:BE9D E6 DE       INC $DE
# 03:BE9F 60          RTS
# 03:BEA0 38          SEC
# 03:BEA1 A5 D2       LDA $D2
# 03:BEA3 E9 05       SBC #$05
# 03:BEA5 20 7C 84    JSR $847C
# 03:BEA8 AA          TAX
# 03:BEA9 7B          TDC
# 03:BEAA A8          TAY
# 03:BEAB BD 78 2B    LDA $2B78,x
# 03:BEAE 99 1C 29    STA $291C,y
# 03:BEB1 E8          INX
# 03:BEB2 C8          INY
# 03:BEB3 C0 20 00    CPY #$0020
# 03:BEB6 D0 F3       BNE $BEAB (-13 B)
# 03:BEB8 7B          TDC
# 03:BEB9 AA          TAX
# 03:BEBA AD 9E 28    LDA $289E
# 03:BEBD DD 1D 29    CMP $291D,x
# 03:BEC0 F0 0A       BEQ $BECC (+10 B)
# 03:BEC2 E8          INX
# 03:BEC3 E8          INX
# 03:BEC4 E8          INX
# 03:BEC5 E8          INX
# 03:BEC6 E0 20 00    CPX #$0020
# 03:BEC9 D0 EF       BNE $BEBA (-17 B)
# 03:BECB 60          RTS
# 03:BECC 7B          TDC
# 03:BECD AD 9F 28    LDA $289F
# 03:BED0 F0 0F       BEQ $BEE1 (+15 B)
# 03:BED2 3D 1F 29    AND $291F,x
# 03:BED5 D0 0A       BNE $BEE1 (+10 B)
# 03:BED7 E8          INX
# 03:BED8 E8          INX
# 03:BED9 E8          INX
# 03:BEDA E8          INX
# 03:BEDB E0 20 00    CPX #$0020
# 03:BEDE D0 ED       BNE $BECD (-19 B)
# 03:BEE0 60          RTS
# 03:BEE1 E6 DE       INC $DE
# 03:BEE3 60          RTS
# 03:BEE4 AD D3 38    LDA $38D3
# 03:BEE7 F0 02       BEQ $BEEB (+2 B)
# 03:BEE9 E6 DE       INC $DE
# 03:BEEB 60          RTS
# 03:BEEC 38          SEC
# 03:BEED A5 D2       LDA $D2
# 03:BEEF E9 05       SBC #$05
# 03:BEF1 0A          ASL a
# 03:BEF2 AA          TAX
# 03:BEF3 BD D4 34    LDA $34D4,x
# 03:BEF6 1D D5 34    ORA $34D5,x
# 03:BEF9 F0 09       BEQ $BF04 (+9 B)
# 03:BEFB BD D5 34    LDA $34D5,x
# 03:BEFE 29 C0       AND #$C0
# 03:BF00 D0 02       BNE $BF04 (+2 B)
# 03:BF02 E6 DE       INC $DE
# 03:BF04 60          RTS
# 03:BF05 AD CD 29    LDA $29CD
# 03:BF08 C9 01       CMP #$01
# 03:BF0A D0 02       BNE $BF0E (+2 B)
# 03:BF0C E6 DE       INC $DE
# 03:BF0E 60          RTS
# 03:BF0F 64 DD       STZ $DD
# 03:BF11 A2 19 00    LDX #$0019
# 03:BF14 A9 FF       LDA #$FF
# 03:BF16 9D D0 35    STA $35D0,x
# 03:BF19 CA          DEX
# 03:BF1A 10 FA       BPL $BF16 (-6 B)
# 03:BF1C AD 9D 28    LDA $289D
# 03:BF1F 29 7F       AND #$7F
# 03:BF21 0A          ASL a
# 03:BF22 AA          TAX
# 03:BF23 BF 0C C1 03 LDA $03C10C,x
# 03:BF27 85 80       STA $80
# 03:BF29 BF 0D C1 03 LDA $03C10D,x
# 03:BF2D 85 81       STA $81
# 03:BF2F A9 03       LDA #$03
# 03:BF31 85 82       STA $82
# 03:BF33 DC 80 00    JML ($0080)
# 03:BF36 AD 9D 28    LDA $289D
# 03:BF39 85 AB       STA $AB
# 03:BF3B 7B          TDC
# 03:BF3C AA          TAX
# 03:BF3D 86 A9       STX $A9
# 03:BF3F A5 A9       LDA $A9
# 03:BF41 A8          TAY
# 03:BF42 B9 40 35    LDA $3540,y
# 03:BF45 D0 26       BNE $BF6D (+38 B)
# 03:BF47 BD 00 20    LDA $2000,x
# 03:BF4A 29 1F       AND #$1F
# 03:BF4C C5 AB       CMP $AB
# 03:BF4E D0 1D       BNE $BF6D (+29 B)
# 03:BF50 20 DA C0    JSR $C0DA
# 03:BF53 AD EA 35    LDA $35EA
# 03:BF56 D0 15       BNE $BF6D (+21 B)
# 03:BF58 8E AB 00    STX $00AB
# 03:BF5B A5 A9       LDA $A9
# 03:BF5D 0A          ASL a
# 03:BF5E AA          TAX
# 03:BF5F A5 AB       LDA $AB
# 03:BF61 9D D0 35    STA $35D0,x
# 03:BF64 A5 AC       LDA $AC
# 03:BF66 9D D1 35    STA $35D1,x
# 03:BF69 E6 DD       INC $DD
# 03:BF6B 80 0B       BRA $BF78 (+11 B)
# 03:BF6D 20 BC 85    JSR $85BC
# 03:BF70 E6 A9       INC $A9
# 03:BF72 A5 A9       LDA $A9
# 03:BF74 C9 05       CMP #$05
# 03:BF76 D0 C7       BNE $BF3F (-57 B)
# 03:BF78 60          RTS
# 03:BF79 38          SEC
# 03:BF7A A5 D2       LDA $D2
# 03:BF7C E9 05       SBC #$05
# 03:BF7E AA          TAX
# 03:BF7F BD B5 29    LDA $29B5,x
# 03:BF82 85 AB       STA $AB
# 03:BF84 A2 05 00    LDX #$0005
# 03:BF87 86 A9       STX $A9
# 03:BF89 7B          TDC
# 03:BF8A AA          TAX
# 03:BF8B 38          SEC
# 03:BF8C A5 A9       LDA $A9
# 03:BF8E E9 05       SBC #$05
# 03:BF90 A8          TAY
# 03:BF91 B9 B5 29    LDA $29B5,y
# 03:BF94 C5 AB       CMP $AB
# 03:BF96 D0 0E       BNE $BFA6 (+14 B)
# 03:BF98 A5 A9       LDA $A9
# 03:BF9A 0A          ASL a
# 03:BF9B A8          TAY
# 03:BF9C C2 20       REP #$20
# 03:BF9E 7B          TDC
# 03:BF9F 99 D0 35    STA $35D0,y
# 03:BFA2 E2 20       SEP #$20
# 03:BFA4 E6 DD       INC $DD
# 03:BFA6 20 BC 85    JSR $85BC
# 03:BFA9 E6 A9       INC $A9
# 03:BFAB A5 A9       LDA $A9
# 03:BFAD C9 0D       CMP #$0D
# 03:BFAF D0 DA       BNE $BF8B (-38 B)
# 03:BFB1 60          RTS
# 03:BFB2 A5 D2       LDA $D2
# 03:BFB4 0A          ASL a
# 03:BFB5 A8          TAY
# 03:BFB6 A5 A6       LDA $A6
# 03:BFB8 99 D0 35    STA $35D0,y
# 03:BFBB A5 A7       LDA $A7
# 03:BFBD 99 D1 35    STA $35D1,y
# 03:BFC0 E6 DD       INC $DD
# 03:BFC2 60          RTS
# 03:BFC3 64 A9       STZ $A9
# 03:BFC5 A9 05       LDA #$05
# 03:BFC7 85 AB       STA $AB
# 03:BFC9 7B          TDC
# 03:BFCA AA          TAX
# 03:BFCB A5 A9       LDA $A9
# 03:BFCD A8          TAY
# 03:BFCE B9 40 35    LDA $3540,y
# 03:BFD1 D0 17       BNE $BFEA (+23 B)
# 03:BFD3 20 DA C0    JSR $C0DA
# 03:BFD6 AD EA 35    LDA $35EA
# 03:BFD9 D0 0F       BNE $BFEA (+15 B)
# 03:BFDB A5 A9       LDA $A9
# 03:BFDD 0A          ASL a
# 03:BFDE A8          TAY
# 03:BFDF C2 20       REP #$20
# 03:BFE1 8A          TXA
# 03:BFE2 99 D0 35    STA $35D0,y
# 03:BFE5 7B          TDC
# 03:BFE6 E2 20       SEP #$20
# 03:BFE8 E6 DD       INC $DD
# 03:BFEA 20 BC 85    JSR $85BC
# 03:BFED E6 A9       INC $A9
# 03:BFEF A5 A9       LDA $A9
# 03:BFF1 C5 AB       CMP $AB
# 03:BFF3 D0 D6       BNE $BFCB (-42 B)
# 03:BFF5 60          RTS
# 03:BFF6 64 AB       STZ $AB
# 03:BFF8 4C FB BF    JMP $BFFB
# 03:BFFB 7B          TDC
# 03:BFFC A8          TAY
# 03:BFFD 84 A9       STY $A9
# 03:BFFF A2 80 02    LDX #$0280
