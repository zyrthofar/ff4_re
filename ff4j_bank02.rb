# Executes code in bank 0x02, based on `a`
# a    address
# 0x00 $ABDD
# 0x01 $ABD2
# 0x02 $8295
# 0x03 $8181
# 0x04 $8178
# 0x05 $BEAF
# 0x06 $8158
# 0x07 $8160
# 0x08 $8168
# 0x09 $8170
# 0x0A $A20C
# 0x0B $A39E
# 0x0C $A12C
# 0x0D $A1D7
# 0x0E $9EAD
# 0x0F $82B0
# 0x10 $E8F9
# 0x11 $BED7
# 0x12 $812D
# 0x13 $8069
# 0x14 $8064
# 0x15 $804B
def _8003(a)
	# 02:8003 4C 0A 80    JMP $800A
	# 02:8006 20 13 85    JSR $8513
	# 02:8009 6B          RTL
	# 02:800A 20 0E 80    JSR $800E
	_800e(a)

	# 02:800D 6B          RTL
	return
end

def _8006
	# 02:8006 20 13 85    JSR $8513
	_8513

	# 02:8009 6B          RTL
	return
end

def _800e(a)
	# 02:800E 0A          ASL a
	# 02:800F AA          TAX
	# 02:8010 BF 1F 80 02 LDA $02801F,x
	# 02:8014 85 02       STA $02
	# 02:8016 BF 20 80 02 LDA $028020,x
	# 02:801A 85 03       STA $03
	# 02:801C 6C 02 00    JMP ($0002)
	case a
		when 0x00 then _abdd
		when 0x01 then _abd2
		when 0x02 then _8295
		when 0x03 then _8181
		when 0x04 then _8178
		when 0x05 then _beaf
		when 0x06 then _8158
		when 0x07 then _8160
		when 0x08 then _8168
		when 0x09 then _8170
		when 0x0A then _a20c
		when 0x0B then _a39e
		when 0x0C then _a12c
		when 0x0D then _a1d7
		when 0x0E then _9ead
		when 0x0F then _82b0
		when 0x10 then _e8f9
		when 0x11 then _bed7
		when 0x12 then _812d
		when 0x13 then _8069
		when 0x14 then _8064
		when 0x15 then _804b
	end		

	# 02:8010                                               DD
	# 02:8020 AB D2 AB 95 82 81 81 78  81 AF BE 58 81 60 81 68
	# 02:8030 81 70 81 0C A2 9E A3 2C  A1 D7 A1 AD 9E B0 82 F9
	# 02:8040 E8 D7 BE 2D 81 69 80 64  80 4B 80
end

def _BEAF
	# 02:BEAF 20 C8 96    JSR $96C8
	# 02:BEB2 20 F0 BE    JSR $BEF0
	# 02:BEB5 AD 74 F4    LDA $F474
	# 02:BEB8 F0 07       BEQ (+7 B)
	# 02:BEBA 22 90 EF 01 JSL $01EF90
	# ...
end
